<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = [
        'club_id', 'user_id', 'message'
    ];

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
