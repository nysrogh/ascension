<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    protected $fillable = [
        'level', 'point', 'coin', 'badge', 'max_member', 'max_privilege', 'description', 'notice'
    ];

    public function members()
    {
        return $this->hasMany('App\User', 'club_id', 'id');
    }

    public function chats()
    {
        return $this->hasMany('App\Chat', 'club_id', 'id');
    }

    public function privileges()
    {
        return $this->belongsToMany('App\Privilege')->withPivot('expired_on');
    }

    public function applicants()
    {
        return $this->hasMany('App\ClubApplicant');
    }

    public function war()
    {
        return $this->hasOne('App\War');
    }
}
