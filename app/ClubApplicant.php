<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClubApplicant extends Model
{
    protected $fillable = [
        'club_id', 'user_id'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
