<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClubLog extends Model
{
    protected $fillable = [
        'club_id', 'message'
    ];
}
