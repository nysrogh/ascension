<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('down')->daily();

        //$schedule->exec('rm -r /var/www/ascension/storage/framework/cache/*')->daily()->evenInMaintenanceMode();
        $schedule->exec('rm /var/www/ascension/storage/framework/sessions/*')->daily()->evenInMaintenanceMode();
        //$schedule->exec('truncate -s 0 /var/www/ascension/storage/logs/laravel.log')->daily()->evenInMaintenanceMode();
        $schedule->call(function() {
            Cache::flush();
            DB::table('chats')->delete();
            DB::table('notifications')->delete();
            DB::table('arenas')->where('battles_left', '<', 20)->update(['battles_left' => 20]);
            DB::table('users')->where('club_id', '!=', 0)->where('mission_left', '<', 10)->update(['mission_left' => 10, 'report' => '']);
			DB::table('users')->where('paragon_limit', 0)->update(['paragon_limit' => 1]);
            DB::table('mails')->where('created_at', '<', Carbon::now()->subDays(3))->delete();
            DB::table('club_logs')->where('created_at', '<', Carbon::now()->subDays(3))->delete();
        })->daily()->evenInMaintenanceMode();

        $schedule->call(function() {
			DB::table('users')->where('legend_limit', '!=', 0)->decrement('legend_limit');
        })->twiceDaily()->evenInMaintenanceMode();

        $schedule->call(function() {
            $dungeons = DB::table('dungeons')->inRandomOrder()->get();

            DB::table('pokemon')->where('dungeon_id', '!=', 0)->update(['dungeon_id' => 0]);

            foreach ($dungeons as $dungeon) {
                DB::table('pokemon')->where('rarity', 1)
                    ->whereIn('primary_type', explode(',', $dungeon->types))
                    ->inRandomOrder()->limit(random_int(7,10))->update(['dungeon_id' => $dungeon->id]);

                DB::table('pokemon')->where('rarity', 2)
                    ->where('name', 'NOT LIKE', '%(Mega%')
                    ->whereIn('primary_type', explode(',', $dungeon->types))
                    ->inRandomOrder()->limit(random_int(4,7))->update(['dungeon_id' => $dungeon->id]);

                DB::table('pokemon')->where('rarity', 3)
                    ->where('name', 'NOT LIKE', '%(Mega%')
                    ->where('name', 'NOT LIKE', '%(Ash%')
                    ->whereIn('primary_type', explode(',', $dungeon->types))
                    ->inRandomOrder()->limit(random_int(3,7))->update(['dungeon_id' => $dungeon->id]);

                DB::table('pokemon')->where('alternate_form', 0)
                    ->where('rarity', 4)
                    ->whereNotIn('id', [793,794,795,796,797,798,799])
                    ->whereIn(random_int(1,2) == 1 ? 'primary_type' : 'secondary_type', explode(',', $dungeon->types))
                    ->inRandomOrder()->limit(random_int(1,2))->update(['dungeon_id' => $dungeon->id]);

                DB::table('pokemon')->where('alternate_form', 0)
                    ->where('rarity', 5)
                    ->whereNotIn('id', [790,791,792,800])
                    ->whereIn(random_int(1,2) == 1 ? 'primary_type' : 'secondary_type', explode(',', $dungeon->types))
                    ->inRandomOrder()->limit(random_int(1,2))->update(['dungeon_id' => $dungeon->id]);

                DB::table('dungeons')->where('id', $dungeon->id)->update(['distance' => random_int(40,70)]);
            }
        })->dailyAt('0:10')->evenInMaintenanceMode();

        $schedule->command('up')->dailyAt('0:16')->evenInMaintenanceMode();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
