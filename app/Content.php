<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = [
        'user_shop_id', 'item_id', 'user_pokemon_id', 'price', 'available', 'primary_value', 'upgrade'
    ];

    public function info()
    {
        return $this->hasOne('App\Item', 'id', 'item_id');
    }

    public function pokemon()
    {
        return $this->hasOne('App\UserPokemon', 'id', 'user_pokemon_id');
    }

    public function shop()
    {
        return $this->hasOne('App\UserShop', 'id', 'user_shop_id');
    }

    public function trades()
    {
        return $this->hasMany('App\Trade');
    }

    public function removeItem($item)
    {
        $userItem = new \App\UserItem();
        if ($item->item_id) {
            $userItem->addOrUpdateItem((object)['id' => $item->item_id], $item->available);
        } else {
            $item->pokemon->decrement('in_shop');

            foreach ($item->trades as $poke) {
                $poke->decrement('in_trade');
            }
        }

        $item->delete();

        return false;
    }
}
