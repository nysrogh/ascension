<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 8:55 AM
 */

namespace App\Contracts;

interface RepositoryInterface
{
    /**
     * Get all the resources.
     *
     * @return json array
     */
    public function all();

    /**
     * Create pagination for the resouces.
     *
     * @param  integer $length
     * @return json array
     */
    public function paginate($length);

    /**
     * Find the resource using the specified id or else fail.
     *
     * @param  int $id
     * @return json array
     */
    public function findOrFail($id);

    /**
     * Store the resource in storage.
     *
     * @param  json object $request
     * @return boolean
     */
    public function store($request);

    /**
     * Update the specified resource in storage.
     *
     * @param  json abject $request
     * @param  int $id
     * @return boolean
     */
    public function update($request, $id);

    /**
     * Delete the specified resource in storage.
     *
     * @param  int $id
     * @return boolean
     */
    public function destroy($id);

    /**
     * Search the specified data from the storage
     *
     * @param  string $key
     * @param
     * @return array
     */
    public function search($column, $key);

    /**
     * Flash success message
     *
     * @param String $message
     * @return mixed
     */
    public function flashError($message);

    /**
     * Flash failed message
     *
     * @param String $message
     * @return mixed
     */
    public function flashSuccess($message);
}
