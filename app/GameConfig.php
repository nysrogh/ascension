<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameConfig extends Model
{
    protected $fillable = [
        'arena_tournament_end'
    ];
}
