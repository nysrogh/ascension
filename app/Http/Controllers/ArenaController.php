<?php

namespace App\Http\Controllers;

use App\Contracts\ArenaInterface;
use App\Contracts\GameConfigInterface;
use App\Contracts\ItemInterface;
use App\Contracts\UserItemInterface;
use Illuminate\Http\Request;

class ArenaController extends Controller
{
    protected $arena;
    protected $config;
    protected $item;
    protected $userItem;

    public function __construct(ArenaInterface $arena, GameConfigInterface $config, ItemInterface $item, UserItemInterface $userItem)
    {
        $this->middleware('auth');

        $this->arena = $arena;
        $this->config = $config;
        $this->item = $item;
        $this->userItem = $userItem;
    }

    public function index()
    {
        if (session()->has('arena')) return redirect()->route('arena.battle');

        $data['rank'] = $this->arena->findRank();
        $data['trainers'] = $this->arena->withinRank();
        $data['tournament'] = $this->config->tournamentEnds();

        return view('locations.arena', $data);
    }

    public function rankings()
    {
        if (session()->has('arena')) return redirect()->route('arena.battle');

        $data['rankings'] = $this->arena->rankings();

        return view('locations.arena-rankings', $data);
    }

    public function join()
    {
        if (session()->has('arena')) return redirect()->route('arena.battle');

        if (count(explode(',', auth()->user()->pokedex)) < 50) {
            $this->arena->flashError("Sorry! You need to have at least 50 Pokedex records to join the tournament.");

            return back();
        }

        abort_if($this->arena->findRank(), 403);

        $this->arena->join();

        return back();
    }

    public function challenge($id)
    {
        abort_if(session()->has('battle'), 403);

        if (session()->has('arena')) return redirect()->route('arena.battle');
        $challenge = null;

        abort_if($id == auth()->user()->id || $this->arena->findRank()->battles_left == 0 || !$this->config->tournamentEnds(), 403);

        $challenge = $this->arena->challenger($id);

        $this->arena->initialize($challenge);
        $this->arena->findRank()->decrement('battles_left');

        //check mission
        if (auth()->user()->mission_id && auth()->user()->mission->modifier == 'arena') {
            auth()->user()->increment('mission_progress');
        }

        return redirect()->route('arena.battle');
    }

    public function battle()
    {
        abort_if(!session('arena') || session()->has('battle') || session()->has('paragon'), 403);

        //dd(session()->all());
        $data['enemy'] = session('arena.enemy');
        $data['pokemon'] = session('arena.pokemon');
        //$data['background'] = $this->arena->background();

        $this->arena->check();

        return view('battle.arena-battle', $data);
    }

    public function fight()
    {
        if (session('result')) return redirect()->route('arena');
        if ($this->arena->doubleClicked(request('prevent'))) return back();

        session()->forget('arena_log');

        abort_if(!$move = $this->arena->checkMove(request('id')), 404);

        $this->arena->fight($move);

        //dd(session()->all());
        return redirect()->route('arena');
    }

    public function end()
    {
        if (!session()->has('result')) {
            $this->arena->flashSuccess("You have forfeited from battle!");
        }

        session()->forget(['arena_log', 'arena', 'background', 'result']);

        return redirect()->route('arena');
    }

    public function shop()
    {
        $data['items'] = $this->item->shop('arena');

        return view('locations.arena-mart', $data);
    }

    public function shopBuy()
    {
        abort_if($this->item->doubleClicked(request('prevent')), 500);

        $item = $this->item->findOrFail(request('item'));

        abort_if(auth()->user()->medal < $item->price || !in_array(request('item'), [97,98,99,100,101,102,130,133,134,135,136,137,138,139,140,151,152,153,154,155,156]) || $item->price == 1, 403);

        $this->userItem->addOrUpdateItem($item, 1);
        auth()->user()->decrement('medal', $item->price);

        $this->item->flashSuccess("Item successfully bought: {$item->name}!");

        return back();
    }
}
