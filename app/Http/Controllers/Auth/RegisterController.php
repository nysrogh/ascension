<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\PokedexInterface;
use App\Contracts\PokemonInterface;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/welcome';

    protected $pokemon;

    /**
     * Added pokemon interface
     *
     * RegisterController constructor.
     * @param PokemonInterface $pokemon
     */
    public function __construct(PokemonInterface $pokemon)
    {
        $this->middleware('guest');

        $this->pokemon = $pokemon;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|min:2|max:15|alpha_num|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:2|confirmed',
            'gender' => 'required|in:1,2',
            'type' => 'required|in:normal,fire,fighting,water,flying,grass,poison,electric,ground,psychic,rock,ice,bug,dragon,ghost,dark,steel,fairy',
            'captcha' => 'required|numeric|min:3|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
//        $use = new User;
//        $ips = $use->where('ip_address', request()->ip())->count();
//
//        abort_if($ips >= 15, 403);
//        abort_if($use->allOnline()->count() >= 50, 405);



        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'gender' => $data['gender'],
            'password' => bcrypt($data['password']),
            'avatar' => $data['gender'] == 1 ? $data['gender'] .'-'. random_int(1,97) : $data['gender'] .'-'. random_int(1,62),
            'setting_bgcolor' => random_int(10000,99999)
        ]);

        $this->pokemon->starter($user->id, $data['type']);

        return $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $data['captcha'] = random_int(100,999);

        return view('auth.register', $data);
    }
}
