<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 10/14/2017
 * Time: 10:01 AM
 */
namespace App\Http\Controllers;

use App\Contracts\BattleInterface;
use App\Contracts\UserItemInterface;

class BattleController extends Controller
{
    public function __construct(BattleInterface $battle, UserItemInterface $userItem)
    {
        $this->middleware('auth');

        $this->battle = $battle;
        $this->userItem = $userItem;
    }

    public function index()
    {
        abort_if(!session('battle.enemy') || session()->has('paragon'), 403);

        $data['enemy'] = session('battle.enemy');
        $data['pokemon'] = session('battle.pokemon');

        return view('battle.index', $data);
    }

    public function fight()
    {
        if ($this->battle->doubleClicked(request('prevent')) || $this->battle->checker() == 'end') return redirect()->route('battle');

        session()->forget('battle.battle_log');

        abort_if(!$move = $this->battle->checkMove(request('id')), 404);

        $this->battle->fight($move);

        return redirect()->route('battle');
    }

    public function recall($id)
    {
        abort_if(!$this->battle->substitute(), 403);

        session()->forget('battle.battle_log');

        $this->battle->recall($id);

        return redirect()->route('battle');
    }

    public function end()
    {
        abort_if(!session()->has('battle.result'), 404);

        if (session('battle.result') == 'defeat') {
            session()->forget(['battle', 'dungeon', 'wild', 'tmp', 'victory_bonus']);

            return redirect()->route('home');
        }

        if (!is_numeric(session('dungeon.progress')) && session('battle.result') == 'victory') {
            $array['event'] = (object)['name' => "completed"];

            auth()->user()->dungeons_completed += 1;
            auth()->user()->save();
        } else {
            $array['event'] = (object)['name' => "nothing", 'message' => $this->battle->tips('dungeon')];
        }

        $this->battle->saveSession('dungeon', $array);

        session()->forget(['battle.enemy', 'battle.selected', 'battle.battle_log', 'battle.background', 'wild', 'battle.participants', 'battle.result', 'tmp', 'victory_bonus']);
        session()->save();

        return redirect()->route('dungeon');
    }

    public function capture()
    {
        abort_if($this->battle->doubleClicked(request('prevent')), 500);
        abort_if(session('battle.result'), 403);

        $pokeball = $this->userItem->findOrFail(request('ball'));

        $this->authorize('own', $pokeball);

        if (!session('battle.enemy') && $pokeball->info->category != 1 && $pokeball->info->section == 0) {
            return back();
        }

        session()->forget('battle.battle_log');

        $this->battle->capture($pokeball);

        $pokeball->quantity -= 1;

        if ($pokeball->quantity <= 0) {
            $pokeball->delete();
        } else {
            $pokeball->save();
        }

        return redirect()->route('battle');
    }
}