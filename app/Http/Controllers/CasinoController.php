<?php

namespace App\Http\Controllers;

use App\Contracts\CasinoInterface;

class CasinoController extends Controller
{
    protected $casino;

    public function __construct(CasinoInterface $casino)
    {
        $this->middleware('auth');

        $this->casino = $casino;
    }

    public function index()
    {
        return view('casino.index');
    }
}
