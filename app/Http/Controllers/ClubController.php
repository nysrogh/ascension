<?php

namespace App\Http\Controllers;

use App\Contracts\ChatInterface;
use App\Contracts\ClubApplicantInterface;
use App\Contracts\ClubInterface;
use App\Contracts\ClubLogInterface;
use App\Contracts\ItemInterface;
use App\Contracts\NotifInterface;
use App\Contracts\UserInterface;
use App\Contracts\UserItemInterface;
use App\Http\Requests\CreateClubCoinRequest;
use App\Http\Requests\CreateClubRequest;
use App\Http\Requests\EditClubRequest;
use Carbon\Carbon;

class ClubController extends Controller
{
    protected $club;
    protected $chat;
    protected $user;
    protected $applicant;
    protected $notif;
    protected $userItem;
    protected $log;
    protected $item;

    public function __construct(ClubInterface $club, ChatInterface $chat, UserInterface $user, ClubApplicantInterface $applicant, NotifInterface $notif, UserItemInterface $userItem, ClubLogInterface $log, ItemInterface $item)
    {
        $this->middleware('auth');

        $this->club = $club;
        $this->chat = $chat;
        $this->user = $user;
        $this->applicant = $applicant;
        $this->notif = $notif;
        $this->userItem = $userItem;
        $this->log = $log;
        $this->item = $item;
    }

    public function index()
    {
        abort_if(!auth()->user()->club_id, 403);

        $data['club'] = $this->club->home(auth()->user()->club_id);
        $data['nextLevel'] = $this->club->nextLevel($data['club']->level);
        $data['captcha'] = random_int(10,99);

        return view('clubs.index', $data);
    }

    public function create()
    {
        abort_if(auth()->user()->club_id, 403);

        $data['captcha'] = random_int(100,999);

        return view('clubs.create', $data);
    }

    public function manage()
    {
        abort_if(!auth()->user()->club_id || !in_array(auth()->user()->club_role, [2,3]), 403);

        $data['captcha'] = random_int(100,999);
        $data['club'] = $this->club->findOrFail(auth()->user()->club_id);

        return view('clubs.manage', $data);
    }

    public function clubs()
    {
        $data['clubs'] = $this->club->randomize();

        return view('locations.trainers-club', $data);
    }

    public function show($id)
    {
        $data['club'] = $this->club->show($id);
        $data['leader'] = $this->user->leader($id);

        return view('clubs.show', $data);
    }

    public function clubChat()
    {
        abort_if($this->club->doubleClicked(request('prevent')) || (session()->exists('chat') && session('chat') > Carbon::now()), 500);
        abort_if(auth()->user()->club_id != request('club_id'), 403);

        $data = $this->validate(request(), [
            'message' => 'required|min:2|max:150',
            'club_id' => 'required',
        ]);

        $data['user_id'] = auth()->user()->id;

        $this->chat->store($data);

        session(['chat' => Carbon::now()->addSeconds(5)]);

        return back();
    }

    public function apply()
    {
        abort_if($this->club->doubleClicked(request('prevent')), 500);
        abort_if(auth()->user()->club_id || !$this->club->findOrFail(request('id')), 403);

        if ($this->applicant->check(request('id'))) {
            $this->club->flashError('You have already applied to this club!');

            return back();
        }

        $this->applicant->apply(request('id'));
        $this->club->flashSuccess('You have successfully applied to a club!');

        return back();
    }

    public function members()
    {
        $data['club'] = $this->club->manageMembers(auth()->user()->club_id);
        $data['members'] = $this->user->clubMembers(auth()->user()->club_id);

        return view('clubs.members', $data);
    }

    public function applicants()
    {
        $data['applicants'] = $this->applicant->listing(auth()->user()->club_id);

        return view('clubs.applicants', $data);
    }

    public function changeRole()
    {
        abort_if($this->club->doubleClicked(request('prevent')), 500);
        abort_if(!in_array(auth()->user()->club_role, [2,3]) || !in_array(request('role'), [1,2,3,'kick']) || request('id') == auth()->user()->id, 403);

        if (in_array(request('role'), [2,3]) && auth()->user()->club_role == 2) {
            $this->club->flashError('You can only kick members!');

            return back();
        }

        $user = $this->user->findOrFail(request('id'));

        if ($user->club_role == 3 && auth()->user()->club_role == 2) {
            $this->club->flashError('You cannot demote/kick the club leader!');

            return back();
        }

        if (is_numeric(request('role'))) {
            if ($user->club_role < request('role')) {
                $this->notif->add($user->id, "You have been promoted in the club!");
                $this->log->add("". auth()->user()->username ." has promoted {$user->username}.");

                $this->club->flashSuccess("{$user->username} has been promoted!");
            } else {
                $this->notif->add($user->id, "You have been demoted in the club!");
                $this->log->add("". auth()->user()->username ." has demoted {$user->username}.");

                $this->club->flashSuccess("{$user->username} has been demoted!");
            }

            $user->club_role = request('role');
            $user->save();
        } else {
            $user->club_id = 0;
            $user->club_role = 0;
            $user->club_point = 0;
            $user->save();

            $this->notif->add($user->id, "You have been kicked out from the club!");
            $this->log->add("". auth()->user()->username ." has kicked out {$user->username}.");

            $this->club->flashSuccess("{$user->username} has been kicked!");
        }

        return back();
    }

    public function applicantsAction()
    {
        abort_if($this->club->doubleClicked(request('prevent')), 500);
        abort_if(!in_array(auth()->user()->club_role, [2,3]) || !in_array(request('appAction'), [1,2]), 403);

        if ($this->user->countMembers() >= auth()->user()->club->max_member) {
            $this->club->flashError('Club is full! You cannot accept new members.');

            return back();
        }

        if (request('appAction') == 1) {
            $user = $this->user->findOrFail(request('id'));

            if ($user->club_id) {
                $this->club->flashError('This user is already a member of another club!');

                $this->applicant->remove(request('id'));

                return back();
            }

            $user->club_id = auth()->user()->club_id;
            $user->club_role = 1;
            $user->save();

            $this->applicant->remove(request('id'));

            $this->notif->add(request('id'), "You are now a member of ". auth()->user()->club->name ." club!");

            $this->log->add("". auth()->user()->username ." has accepted {$user->username} into the club.");

            $this->club->flashSuccess('You have accepted a new member!');
        } else {
            $this->applicant->remove(request('id'));

            $this->notif->add(request('id'), auth()->user()->club->name ." club has rejected your application.");
            $this->club->flashSuccess('You have rejected a user!');
        }

        return redirect()->route('club.index');
    }

    public function store(CreateClubRequest $form)
    {
        abort_if(auth()->user()->club_id || auth()->user()->gold < 30000, 403);

        $form->persist();

        $this->club->flashSuccess('You have successfully created a club!');

        return redirect()->route('club.index');
    }

    public function edit(EditClubRequest $form)
    {
        abort_if(!auth()->user()->club_id || !in_array(auth()->user()->club_role, [2,3]), 403);

        $form->persist();

        $this->club->flashSuccess('You have successfully updated the club!');

        return redirect()->route('club.index');
    }

    public function leave()
    {
        abort_if(!auth()->user()->club_id, 403);

        $club = $this->club->findOrFail(auth()->user()->club_id);

        if (auth()->user()->club_role == 3 && $club->members()->count() > 1 && $this->user->countRoles(3) == 1) {
            $this->club->flashError("You can't leave a club without a leader!");

            return back();
        }

        if (auth()->user()->club_role == 3 && $club->members()->count() <= 1) {
            $club->delete();

            auth()->user()->club_id = 0;
            auth()->user()->club_role = 0;
            auth()->user()->club_point = 0;
            auth()->user()->save();

            $this->club->flashSuccess('Your club has been disbanded!');

        } else {
            $this->log->add("". auth()->user()->username ." has left the club.");

            auth()->user()->club_id = 0;
            auth()->user()->club_role = 0;
            auth()->user()->club_point = 0;
            auth()->user()->save();
        }

        return redirect()->route('location.trainers-club');
    }

    public function upgradeMembers()
    {
        abort_if($this->club->doubleClicked(request('prevent')), 500);
        abort_if(!in_array(auth()->user()->club_role, [2,3]), 403);

        if (auth()->user()->club->coin < auth()->user()->club->max_member * 20000) {
            $this->club->flashError("You don't have enough coins in your club's treasury!");

            return back();
        }

        if (auth()->user()->club->max_member >= 150) {
            $this->club->flashError("Your club reached the maximum members upgrade!");

            return back();
        }

        auth()->user()->club->coin -= auth()->user()->club->max_member * 20000;
        auth()->user()->club->max_member += 5;
        auth()->user()->club->save();

        $this->log->add("". auth()->user()->username ." has upgraded the club's Max Members.");

        $this->club->flashSuccess("You have successfully upgraded your club's max member!");

        return back();
    }

    public function upgradePrivilege()
    {
        abort_if($this->club->doubleClicked(request('prevent')), 500);
        abort_if(!in_array(auth()->user()->club_role, [2,3]), 403);

        if (auth()->user()->club->coin < auth()->user()->club->max_privilege * 130000) {
            $this->club->flashError("You don't have enough coins in your club's treasury!");

            return back();
        }

        if (auth()->user()->club->max_privilege >= auth()->user()->club->level) {
            $this->club->flashError("You already have max perks for your current club level!");

            return back();
        }

        auth()->user()->club->coin -= auth()->user()->club->max_privilege * 130000;
        auth()->user()->club->max_privilege += 1;
        auth()->user()->club->save();

        $this->log->add("". auth()->user()->username ." has upgraded the club's Max Perks.");
        $this->club->flashSuccess("You have successfully upgraded your club's max perks!");

        return back();
    }

    public function mart()
    {
        abort_if(!auth()->user()->club_id, 403);

        $data['items'] = $this->item->shop('club');

        return view('clubs.store', $data);
    }

    public function martBuy()
    {
        abort_if($this->club->doubleClicked(request('prevent')), 500);
        abort_if(!auth()->user()->club_id || !in_array(request('id'), [126,173,157,158,159,160,161,162,163,164,165,166,129,141,142,143,144,145,146,147]), 403);

        $item = $this->item->show(request('id'));

        if (auth()->user()->club_point < $item->price) {
            $this->club->flashError("You don't have enough club points!");

            return back();
        }

        auth()->user()->decrement('club_point', $item->price);

        $this->userItem->addOrUpdateItem((object)['id' => request('id')], 1);

        $this->club->flashSuccess("You have bought an item!");

        return back();
    }

    public function addFunds()
    {
        abort_if(!auth()->user()->club_id, 403);

        $data['captcha'] = random_int(100,999);

        return view('clubs.addFunds', $data);
    }

    public function storeFunds(CreateClubCoinRequest $form)
    {
        abort_if($this->club->doubleClicked(request('prevent')), 500);
        abort_if(!auth()->user()->club_id || auth()->user()->gold < request('amount') || request('amount') < 1, 403);

        $cp = $form->persist();

        $this->log->add("". auth()->user()->username ." added ". number_format(request('amount')) ." coins to club treasury.");

        $this->club->flashSuccess("You have successfully added ". request('amount') ." coins to the treasury and earned {$cp} cp!");

        return back();
    }

    public function rankings()
    {
        $data['rankings'] = $this->club->rankings();

        return view('locations.trainers-club-rankings', $data);
    }

    public function logs()
    {
        $data['logs'] = $this->log->retrieve();

        return view('clubs.logs', $data);
    }
}
