<?php

namespace App\Http\Controllers;

use App\Contracts\BattleInterface;
use App\Contracts\DungeonInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\RewardInterface;
use Carbon\Carbon;

class DungeonController extends Controller
{
    protected $dungeon;
    protected $pokemon;
    protected $battle;
    protected $reward;

    public function __construct(DungeonInterface $dungeon, PokemonInterface $pokemon, BattleInterface $battle, RewardInterface $reward)
    {
        $this->middleware('auth');
        $this->middleware('exploring', ['only' => ['index']]);
        $this->middleware('location', ['only' => ['map', 'enter']]);

        $this->dungeon = $dungeon;
        $this->pokemon = $pokemon;
        $this->battle = $battle;
        $this->reward = $reward;
    }

    public function index()
    {
        if (session('battle.enemy')) return redirect()->route('battle');

        if (session('dungeon.timer') < Carbon::now()->subHours(2) && is_numeric(session('dungeon.progress'))) {
            return $this->leave();
        }

        if (!session()->has('dungeon.event') && session()->has('dungeon.info')) {
            $array['event'] = (object)['name' => "nothing", 'message' => $this->dungeon->tips('dungeon')];
            $this->dungeon->saveSession('dungeon', $array);
        }

        session(['prevent' => random_int(1, 100)]);

        return view('dungeons.index');
    }

    public function battle()
    {
        abort_if(session('battle.enemy') || session('arena'), 403);

//        if (!is_numeric(session('dungeon.progress')) && session('dungeon.timer') > Carbon::now()) {
//            $this->battle->flashError("Whoops! It seems like you've reached the boss too fast. Please wait for a few more minutes.");
//
//            return back();
//        }

        $this->battle->initialize();

        return redirect()->route('battle');
    }

    public function explore()
    {
        abort_if($this->battle->doubleClicked(request('prevent')), 500);
        if (!is_numeric(session('dungeon.progress'))) return back();

        $array = [];

        session()->forget('dungeon.event');

        $array['progress'] = session('dungeon.progress') + 1;

        if ($array['progress'] >= session('dungeon.info')->distance) {
            $array['event'] = (object)['name' => "wild"];
            $array['progress'] = 'boss';

            $this->dungeon->saveSession('dungeon', $array);

            $this->pokemon->wildEncounter();

            session()->save();
        } else {
            if ($this->dungeon->events()) {
                $this->pokemon->wildEncounter();
            }

            $this->dungeon->saveSession('dungeon', $array);
        }

        return back();
    }

    public function map()
    {
        $data['maps'] = $this->dungeon->floorDungeons();

        return view('dungeons.map', $data);
    }

    public function show($id)
    {
        $data['dungeon'] = $this->dungeon->findOrFail($id);
        $data['pokemon'] = $this->pokemon->inDungeon($id);
        $data['pokedex'] = explode(',', auth()->user()->pokedex);

        return view('dungeons.view', $data);
    }

    public function store()
    {
        abort_if(!in_array(request('difficulty'), [20,55,85]), 403);

        session(['dungeon' => [
            'info' => $this->dungeon->findOrFail(request('dungeon_id')),
            'difficulty' => request('difficulty'),
            'timer' => Carbon::now()->addMinutes(3),
            'progress' => 0
        ]]);

        return redirect()->route('dungeon');
    }

    public function leave()
    {
        session()->forget('dungeon');
        session()->forget('battle');
        session()->forget('wild');
        session()->forget('tmp');
        session()->forget('victory_bonus');

        return redirect()->route('home');
    }

    public function chest()
    {
        abort_if($this->battle->doubleClicked(request('prevent')), 500);
        abort_if(session('dungeon.progress') == 'end' || session('dungeon.event')->name != 'completed' || !in_array(request('chest'), [1,2,3]), 403);

        $this->dungeon->flashSuccess("You have successfully claimed your reward!");

        $this->dungeon->chestEvent(request('chest'));

        //check mission
        if (auth()->user()->mission_id && auth()->user()->mission->modifier == 'boss') {
            auth()->user()->increment('mission_progress');
        }

        return back();
    }

    public function buy()
    {
        abort_if($this->battle->doubleClicked(request('prevent')), 500);
        abort_if(!session('dungeon.event')->item || auth()->user()->gold < session('dungeon.event')->price, 403);

        $this->dungeon->buyItem();

        session()->forget('dungeon.event');

        return back();
    }
}
