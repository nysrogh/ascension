<?php

namespace App\Http\Controllers;

use App\Contracts\ForumCommentInterface;
use App\Contracts\ForumInterface;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\CreateForumRequest;

class ForumController extends Controller
{
    protected $forum;
    protected $forumComment;

    public function __construct(ForumInterface $forum, ForumCommentInterface $forumComment)
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->middleware('activated', ['except' => ['index', 'show']]);

        $this->forum = $forum;
        $this->forumComment = $forumComment;
    }

    /**
     * Latest forum topics
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['forums'] = $this->forum->latestPosts();

        return view('forums.index', $data);
    }

    /**
     * Show forum topic
     *
     * @param $slug
     * @param string $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug, $order = 'asc')
    {
        $data['topic'] = $this->forum->findBySlug($slug);
        $data['comments'] = $this->forumComment->findByForum($data['topic']->id, $order);
        $data['captcha'] = random_int(100,999);

        return view('forums.show', $data);
    }

    /**
     * Create forum topic form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data['captcha'] = random_int(100,999);

        return view('forums.create', $data);
    }

    /**
     * Save forum topic
     *
     * @param CreateForumRequest $form
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateForumRequest $form)
    {
        $form->persist();

        $this->forum->flashSuccess('You have successfully created a topic!');

        return redirect()->route('forum');
    }

    public function comment(CreateCommentRequest $form)
    {
        $form->persist();

        $this->forum->flashSuccess('You have successfully posted a comment!');

        return back();
    }

    public function category($cat)
    {
        abort_if(!in_array($cat, [1,2,3,4]), 404);

        $data['forums'] = $this->forum->findByCategory($cat);
        $data['cat'] = $cat;

        return view('forums.category', $data);
    }

    public function delete($id)
    {
        abort_if(auth()->user()->id != 1, 403);

        $forum = $this->forum->findOrFail($id);

        $forum->delete();

        $this->forum->flashSuccess('You have successfully deleted a forum!');

        return redirect()->route('forum');
    }

    public function deleteComment($id)
    {
        abort_if(auth()->user()->status != 2, 403);

        $comment = $this->forumComment->findOrFail($id);
        $comment->delete();

        $this->forum->flashSuccess('You have successfully deleted a forum comment!');

        return back();
    }
}
