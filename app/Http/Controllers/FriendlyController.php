<?php

namespace App\Http\Controllers;

use App\Contracts\FriendlyInterface;
use App\Contracts\UserInterface;

class FriendlyController extends Controller
{
    protected $friendly;

    public function __construct(FriendlyInterface $friendly, UserInterface $user)
    {
        $this->middleware('auth');

        $this->friendly = $friendly;
        $this->user = $user;
    }

    public function index()
    {
        abort_if(!session('friendly') || session()->has('paragon') || session()->has('battle'), 403);

        $data['enemy'] = session('friendly.enemy');
        $data['pokemon'] = session('friendly.pokemon');

        return view('battle.friendly', $data);
    }

    public function challenge($id)
    {
        abort_if(session()->has('friendly') || session()->has('battle') || session()->has('arena') || session()->has('paragon') || $id == auth()->user()->id, 403);

        $this->friendly->initialize($this->user->findOrFail($id));

        return redirect()->route('friendly.index');
    }

    public function fight()
    {
        if (session('result')) return redirect()->route('friendly.index');
        if ($this->friendly->doubleClicked(request('prevent'))) return back();

        session()->forget('friendly_log');

        abort_if(!$move = $this->friendly->checkMove(request('id')), 404);

        $this->friendly->fight($move);

        //dd(session()->all());
        return redirect()->route('friendly.index');
    }

    public function end()
    {
        session()->forget(['friendly_log', 'friendly', 'background', 'result']);

        return redirect()->route('home');
    }
}
