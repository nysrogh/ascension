<?php

namespace App\Http\Controllers;

use App\Contracts\ForumInterface;
use Illuminate\Support\Facades\Cache;

class GameController extends Controller
{
    protected $forum;

    public function __construct(ForumInterface $forum)
    {
        $this->middleware('guest', ['except' => ['online', 'legal', 'guide']]);

        $this->forum = $forum;
    }

    /**
     * Index point of Ascension
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data['updates'] = $this->forum->latestUpdates();

        return view('index', $data);
    }

    public function online()
    {
        $data['results'] = null;

        return view('trainers.online', $data);
    }

    public function legal()
    {
        return view('legal');
    }
}
