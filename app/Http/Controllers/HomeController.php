<?php

namespace App\Http\Controllers;

use App\Contracts\ChatInterface;
use App\Contracts\ItemInterface;
use App\Contracts\MailInterface;
use App\Contracts\MoveInterface;
use App\Contracts\NotifInterface;
use App\Contracts\RewardInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use App\Http\Requests\CreateChatRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $userPokemon;
    protected $userItem;
    protected $move;
    protected $chat;
    protected $item;
    protected $mail;
    protected $reward;
    protected $notif;

    public function __construct(
        UserPokemonInterface $userPokemon,
        UserItemInterface $userItem,
        MoveInterface $move,
        ChatInterface $chat,
        ItemInterface $item,
        MailInterface $mail,
        RewardInterface $reward,
        NotifInterface $notif
    )
    {
        $this->middleware('auth');
        $this->middleware('activated', ['except' => ['welcome', 'store']]);
        $this->middleware('location', ['except' => ['pokeCenter', 'pokeCenterChat']]);

        $this->userPokemon = $userPokemon;
        $this->userItem = $userItem;
        $this->move = $move;
        $this->chat = $chat;
        $this->item = $item;
        $this->mail = $mail;
        $this->notif = $notif;
        $this->reward = $reward;
    }

    public function index()
    {
        $data['mail'] = $this->mail->unread();
        $data['notifs'] = $this->notif->unread();
        $data['trades'] = auth()->user()->trades->count();

        return view('home', $data);
    }

    /**
     * Display welcome page after registration
     *
     */
    public function welcome()
    {
        abort_if(auth()->user()->status != 0, 404);

        $data['starter'] = $this->userPokemon->starter();

        if (!auth()->user()->pokedex) {
            $this->mail->welcome();

            auth()->user()->pokedex = $data['starter']->info->id;
            auth()->user()->save();
        }

        return view('welcome', $data);
    }

    public function store()
    {
//        foreach ($this->item->all() as $i) {
//            $this->userItem->store(['user_id' => auth()->user()->id, 'item_id' => $i->id, 'quantity' => 20]);
//        }

        if (strtolower(request('code')) != auth()->user()->setting_bgcolor) {
            $this->mail->flashError('Activation code is incorrect!');

            return back();
        }

        abort_if($this->chat->doubleClicked(request('prevent')), 500);

        $this->userItem->store(['user_id' => auth()->user()->id, 'item_id' => 1, 'quantity' => 15]);
        $this->userItem->store(['user_id' => auth()->user()->id, 'item_id' => 2, 'quantity' => 10]);
        $this->userItem->store(['user_id' => auth()->user()->id, 'item_id' => 3, 'quantity' => 5]);

        auth()->user()->status = 1;
        auth()->user()->save();

        auth()->user()->rewards()->attach([1,2,3,4,24,42]);

        //delay reward claim
        $reward = $this->reward->find(1);
        $reward->user[0]->pivot->claim = Carbon::now()->addHours(3);
        $reward->user[0]->pivot->save();

        //halloween
//        $reward = $this->reward->find(53);
//        $reward->user[0]->pivot->claim = Carbon::now()->addHour();
//        $reward->user[0]->pivot->save();

        return redirect('/rewards');
    }

    public function pokeTech()
    {
        return view('locations.poke-tech');
    }

    public function pokeTechMoves($move)
    {
        $data['moves'] = $this->move->findAllByType($move);

        return view('locations.poke-tech-moves', $data);
    }

    public function pokeCenter()
    {
        abort_if(auth()->user()->status == 4, 301);

        $data['chats'] = $this->chat->retrieve();
        $data['captcha'] = random_int(10,99);

        return view('locations.poke-center', $data);
    }

    public function delete($id)
    {
        abort_if(auth()->user()->id != 1, 403);

        $chat = $this->chat->findOrFail($id);
        $chat->delete();

        $this->chat->flashSuccess('You have successfully deleted a chat!');

        return back();
    }

    public function pokeCenterChat(CreateChatRequest $form)
    {
        abort_if($this->chat->doubleClicked(request('prevent')), 500);

        $form->persist();

        return back();
    }

    public function pokeMart()
    {
        $data['items'] = $this->item->shop();

        return view('locations.poke-mart', $data);
    }

    public function pokeMartBuy()
    {
        abort_if($this->item->doubleClicked(request('prevent')), 500);

        $item = $this->item->findOrFail(request('id'));

        abort_if(auth()->user()->gold < $item->price * request('qty') || !in_array(request('id'), [1,2,3,4,92,93,94,95,96,103,104,105,106,107,108]), 403);

        $this->userItem->addOrUpdateItem($item, request('qty'));
        auth()->user()->decrement('gold', $item->price * request('qty'));

        $this->item->flashSuccess("Item successfully bought: {$item->name} x".request('qty')."!");

        return back();
    }

    public function buyCoins()
    {
        return view('buy');
    }

    public function tradeOffers()
    {
        return view('trainers.trade-offers');
    }
}
