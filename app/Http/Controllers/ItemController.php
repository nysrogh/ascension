<?php

namespace App\Http\Controllers;

use App\Contracts\BattleInterface;
use App\Contracts\ItemInterface;
use App\Contracts\ParagonInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;

class ItemController extends Controller
{
    protected $item;
    protected $userItem;
    protected $battle;
    protected $userPokemon;
    protected $pokemon;

    public function __construct(ItemInterface $item, UserItemInterface $userItem, BattleInterface $battle, UserPokemonInterface $userPokemon, PokemonInterface $pokemon, ParagonInterface $paragon)
    {
        $this->middleware('auth');

        $this->userItem = $userItem;
        $this->item = $item;
        $this->battle = $battle;
        $this->userPokemon = $userPokemon;
        $this->pokemon = $pokemon;
        $this->paragon = $paragon;
    }

    public function index($category = 1)
    {
        abort_if(session()->has('arena') || session()->has('friendly'), 403);

        $data['bag'] = $this->userItem->bag($category);

        return view('trainers.bag', $data);
    }

    public function show($id)
    {
        $data['item'] = $this->userItem->findOrFail($id);
        $data['ups'] = $this->userItem->upgradeChance($data['item']->upgrade);

        return view('items.show', $data);
    }

    public function useItem($id)
    {
        abort_if (session()->has('arena') || session()->has('friendly'), 403);

        $item = $this->userItem->findOrFail($id);

        abort_if($item->info->category == 1 && $item->info->section == 0 || $item->info->primary_attr == 'none', 403);

        $this->authorize('own', $item);

        if (session()->has('battle.battle_log')) {
            session()->forget('battle.battle_log');
        }

        if (session()->has('paragon.battle_log')) {
            session()->forget('paragon.battle_log');
        }

        session(['use' => $item]);

        if (session()->has('dungeon') && session('battle.enemy') && $item->info->category != 4) {
            $this->item->useItem(session('battle.pokemon.stats'), session('use'));

            $array["turns"] = session("battle.turns") + 1;
            $this->item->saveSession('battle', $array);

            $this->battle->useMove(session("battle.enemy"), $this->battle->smartMove(session('battle.pokemon.stats')), session("battle.pokemon"), 'enemy');
            $this->battle->checker();

            session('use')->decrement('quantity');
            if (session('use')->quantity <= 0) session('use')->delete();

            session()->forget('use');

            return redirect()->route('battle');
        } elseif (session()->has('paragon') && session('paragon.enemy') && $item->info->category != 4) {
            $this->item->useItem(session('paragon.pokemon.stats'), session('use'));

            $array["turns"] = session("paragon.turns") + 1;
            $this->item->saveSession('paragon', $array);

            $this->paragon->useMove(session("paragon.enemy"), $this->paragon->smartMove(session('paragon.pokemon.stats')), session("paragon.pokemon"), 'enemy');
            $this->paragon->checker();

            session('use')->decrement('quantity');
            if (session('use')->quantity <= 0) session('use')->delete();

            session()->forget('use');

            return redirect()->route('paragon.battle');
        } else {
            return redirect()->route('team');
        }
    }

    public function useOn()
    {
        abort_if(!session()->has('use') || session()->has('trade') || (session('use')->info->category == 4 && session('use')->info->section == 1), 404);
        abort_if($this->userPokemon->doubleClicked(request('prevent')), 500);

        $poke = $this->userPokemon->findOrFail(request('pokemon'));

        if (explode('-', session('use')->info->primary_attr)[0] == 'stats' && $poke->vitamins_used >= 40) {
            $this->item->flashError("This Pokemon has reached its limit on Vitamin intake.");

            session()->forget('use');

            return back();
        }

        if (session('use')->info->primary_attr == 'levelUp') {

            abort_if($poke->level >= 100, 403);

            $this->userPokemon->itemLevelUp($poke->id);

            session('use')->decrement('quantity');
            if (session('use')->quantity <= 0) session('use')->delete();

        } elseif (session('use')->info->primary_attr == 'restat') {
            $this->pokemon->changeForm($poke, $this->pokemon->findOrFail($poke->pokemon_id));

            session('use')->decrement('quantity');
            if (session('use')->quantity <= 0) session('use')->delete();

        } else {
            if (!$this->item->useItem($poke, session('use'))) {
                $this->item->flashError(session('use')->info->name ." has no effect!");
            } else {
                session('use')->decrement('quantity');
                if (session('use')->quantity <= 0) session('use')->delete();
            }
        }

        session()->forget('use');
        session()->save();

        return back();
    }

    public function equipOn()
    {
        abort_if(!session()->has('use') || session()->has('trade') || session('use')->is_held || in_array(session('use')->info->category, [1,2]) || (session('use')->info->category == 4 && session('use')->info->section != 1), 403);
        abort_if($this->userPokemon->doubleClicked(request('prevent')), 500);

        $poke = $this->userPokemon->findOrFail(request('pokemon'));

        if ($poke->item) {
            $this->item->flashError("You must remove the old item first!");

            session()->forget('use');

            return back();
        }

        $this->userItem->hold(session('use'), $poke->id);

        $this->item->flashSuccess("{$poke->info->name} has equipped ". session('use')->info->name ."!");

        session()->forget('use');

        return back();
    }

    public function unequip()
    {
        abort_if($this->userPokemon->doubleClicked(request('prevent')), 500);

        $item = $this->userItem->findOrFail(request('id'));

        abort_if($item->is_held != request('poke') || $item->user_id != auth()->user()->id, 403);

        $this->userItem->itemEffects($item, request('poke'), true);

        $item->is_held = 0;
        $item->save();

        $this->item->flashSuccess("Item has been removed from Pokemon.");

        return back();
    }

    public function enhance()
    {
        abort_if($this->userItem->doubleClicked(request('prevent')), 500);

        $item = $this->userItem->findOrFail(request('id'));

        abort_if($item->user_id != auth()->user()->id || $item->upgrade >= 10 || $item->info->category != 4 && $item->info->section != 1, 403);

        if ($item->is_held) {
            $this->userPokemon->flashError("You cannot enhance an item while a Pokemon is holding it!");

            return back();
        }

        $ups = $this->userItem->upgradeChance($item->upgrade);

        if ($item->upgrade < 7) {
            if (auth()->user()->gold < $ups['cost']) {
                $this->userPokemon->flashError("Not enough coins!");

                return back();
            }

            auth()->user()->gold -= $ups['cost'];
        } else {
            if (auth()->user()->crystal < $ups['cost']) {
                $this->userPokemon->flashError("Not enough crystal!");

                return back();
            }

            auth()->user()->crystal -= $ups['cost'];
        }

        auth()->user()->save();


        if (random_int(1,1000) > $ups['chance']) {
            $this->userPokemon->flashError("Enhancement failed!");

            return back();
        }

        if ($item->primary_value == 0) $item->primary_value = $item->info->primary_value;

        if ($item->info->primary_value >= 10) {
            if ($item->upgrade < 7) {
                $item->primary_value += 3;
            } else {
                $item->primary_value += 4;
            }
        } else {
            if ($item->upgrade < 7) {
                $item->primary_value += 1;
            } else {
                $item->primary_value += 2;
            }
        }

        $item->upgrade += 1;
        $item->save();

        $this->userPokemon->flashSuccess("Success! Item's effect has improved.");

        return back();
    }
}
