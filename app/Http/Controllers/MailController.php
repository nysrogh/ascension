<?php

namespace App\Http\Controllers;

use App\Contracts\MailInterface;
use App\Http\Requests\CreateMailRequest;

class MailController extends Controller
{
    protected $mail;

    public function __construct(MailInterface $mail)
    {
        $this->middleware('auth');
        $this->middleware('activated');

        $this->mail = $mail;
    }

    public function index()
    {
        $data['mails'] = $this->mail->inbox();
        $data['unread'] = $this->mail->unread();

        return view('mails.index', $data);
    }

    public function create($username = null)
    {
        $data['captcha'] = random_int(100,999);
        $data['username'] = $username;

        return view('mails.create', $data);
    }

    public function show($id)
    {
        $data['captcha'] = random_int(100,999);
        $data['mail'] = $this->mail->read($id);
        $data['reply'] = $this->mail->lastReply($data['mail']->sender_id);

        return view('mails.show', $data);
    }

    public function store(CreateMailRequest $form)
    {
        abort_if($this->mail->doubleClicked(request('prevent')), 500);

        $form->persist();

        $this->mail->flashSuccess('You have successfully sent a message!');

        return redirect()->route('mail.index');
    }
}
