<?php

namespace App\Http\Controllers;

use App\Contracts\ClubInterface;
use App\Contracts\ClubLogInterface;
use App\Contracts\MissionInterface;

class MissionController extends Controller
{
    protected $mission;
    protected $club;
    protected $log;

    public function __construct(MissionInterface $mission, ClubInterface $club, ClubLogInterface $log)
    {
        $this->middleware('auth');
        $this->middleware('location');

        $this->mission = $mission;
        $this->club = $club;
        $this->log = $log;
    }

    public function index()
    {
        abort_if(!auth()->user()->club_id, 403);

        $data['missions'] = $this->mission->available();

        return view('missions.index', $data);
    }

    public function store()
    {
        abort_if($this->mission->doubleClicked(request('prevent')), 500);
        abort_if(!auth()->user()->club_id || auth()->user()->mission_id, 403);

        if (auth()->user()->mission_left <= 0) {
            $this->mission->flashError("You've reached the mission limit for the day!");

            return back();
        }

        $mission = $this->mission->findOrFail(request('id'));

        auth()->user()->mission_id = $mission->id;
        auth()->user()->mission_left -= 1;
        auth()->user()->save();

        session(['mission' => $mission->id]);

        $this->mission->flashSuccess("You have accepted the mission!");

        return back();
    }

    public function abort()
    {
        abort_if(!auth()->user()->club_id || !auth()->user()->mission_id, 403);

        auth()->user()->mission_id = 0;
        auth()->user()->mission_progress = 0;
        auth()->user()->save();

        session()->forget('mission');

        $this->mission->flashSuccess("You have aborted the mission!");

        return back();
    }

    public function report()
    {
        abort_if($this->mission->doubleClicked(request('prevent')), 500);
        abort_if(!auth()->user()->club_id && auth()->user()->mission_id <= 0, 403);

        $mission = $this->mission->findOrFail(auth()->user()->mission_id);

        if (session()->has('mission') && session('mission') != $mission->id) {
            abort(403);
        }

        if ($mission->modifier == 'catch' && auth()->user()->mission_progress <= 0) {
            abort(403);
        }

        if (in_array($mission->modifier, ['coins', 'boss', 'arena']) && auth()->user()->mission_progress < $mission->value) {
            abort(403);
        }

        auth()->user()->club->coin += round($mission->coin * 0.3);
        auth()->user()->club->point += $mission->point;

        if (auth()->user()->club->point >= $this->club->nextLevel(auth()->user()->club->level)) {
            auth()->user()->club->level += 1;
        }

        auth()->user()->club->save();

        auth()->user()->club_point += $mission->point;
        auth()->user()->gold += $mission->coin;
        auth()->user()->mission_id = 0;
        auth()->user()->mission_progress = 0;
        auth()->user()->missions_completed += 1;
        auth()->user()->save();

        $this->log->add("". auth()->user()->username ." has completed a mission. ". $mission->point ." cp and ". number_format(round($mission->coin * 0.3)) ." coins has been added to the club.");

        $this->mission->flashSuccess("You have successfully completed a mission!");

        return back();
    }
}
