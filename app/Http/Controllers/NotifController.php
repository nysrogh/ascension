<?php

namespace App\Http\Controllers;

use App\Contracts\NotifInterface;
use Illuminate\Http\Request;

class NotifController extends Controller
{
    protected $notif;

    public function __construct(NotifInterface $notif)
    {
        $this->middleware('auth');

        $this->notif = $notif;
    }

    public function index()
    {
        $data['notifs'] = $this->notif->all();

        $this->notif->readAll();

        return view('trainers.notifs', $data);
    }
}
