<?php

namespace App\Http\Controllers;

use App\Contracts\ParagonInterface;
use App\Contracts\UserInterface;
use App\Contracts\UserItemInterface;

class ParagonController extends Controller
{
    public function __construct(ParagonInterface $paragon, UserItemInterface $userItem, UserInterface $user)
    {
        $this->middleware('auth');
        $this->paragon = $paragon;
        $this->userItem = $userItem;
        $this->user = $user;
    }

    public function entrance()
    {
        return view('locations.paragon-entrance');
    }

    public function index()
    {
        abort_if(!session()->has('paragon') || session()->has('battle'), 403);
        if (session('paragon.enemy')) return redirect()->route('paragon.battle');
        if (session()->has('battle')) session()->forget('battle');

        return view('paragon.index');
    }

    public function rankings()
    {
        $data['rankings'] = $this->user->paragonRankings();

        return view('paragon.rankings', $data);
    }

    public function enter()
    {
        abort_if($this->paragon->doubleClicked(request('prevent')), 500);
        abort_if(auth()->user()->paragon_limit <= 0, 403);

        if (count(explode(',',auth()->user()->pokedex)) < 50) {
            $this->paragon->flashError("You need at least 50 Pokedex records to enter Paragon Tower.");

            return back();
        }

        auth()->user()->paragon_limit = 0;
        auth()->user()->save();

        session(['paragon' => [
            'progress' => 1,
            'difficulty' => $this->paragon->difficulty(),
        ]]);

        return redirect()->route('paragon.index');
    }

    public function challenge()
    {
        abort_if($this->paragon->doubleClicked(request('prevent')), 500);
        abort_if(session()->has('paragon.enemy'), 403);

        $this->paragon->initialize(session('paragon.difficulty'));

        return redirect()->route('paragon.battle');
    }

    public function battle()
    {
        abort_if(!session('paragon.enemy'), 403);

        $data['enemy'] = session('paragon.enemy');
        $data['pokemon'] = session('paragon.pokemon');

        return view('paragon.battle', $data);
    }

    public function fight()
    {
        if ($this->paragon->doubleClicked(request('prevent')) || $this->paragon->checker() == 'end') return redirect()->route('paragon.battle');

        session()->forget('paragon.battle_log');

        abort_if(!$move = $this->paragon->checkMove(request('id')), 404);

        $this->paragon->fight($move);

        return redirect()->route('paragon.battle');
    }

    public function recall($id)
    {
        abort_if(!$this->paragon->substitute(), 403);

        session()->forget('paragon.battle_log');

        $this->paragon->recall($id);

        return redirect()->route('paragon.battle');
    }

    public function end()
    {
        abort_if(!session()->has('paragon.result'), 404);

        if (session('paragon.result') == 'defeat') {
            session()->forget(['paragon', 'tmp', 'victory_bonus']);

            return redirect()->route('home');
        }

        $array['progress'] = session('paragon.progress') + 1;
        $this->paragon->saveSession('paragon', $array);

        session()->forget(['paragon.enemy', 'paragon.battle_log', 'paragon.background', 'paragon.participants', 'paragon.result', 'tmp', 'victory_bonus']);
        session()->save();

        if (auth()->user()->paragon_level < session('paragon.progress')) {
            auth()->user()->increment('paragon_level', 1);
        }

        return redirect()->route('paragon.index');
    }

    public function capture()
    {
        abort_if($this->paragon->doubleClicked(request('prevent')), 500);
        abort_if(session('paragon.result') || !in_array(session('paragon.enemy.stats')->info->id, [793,794,795,796,797,798,799]), 403);

        $pokeball = $this->userItem->findOrFail(request('ball'));

        $this->authorize('own', $pokeball);

        if (!session('paragon.enemy') && $pokeball->info->category != 1 && $pokeball->info->section == 0) {
            return back();
        }

        session()->forget('paragon.battle_log');

        $this->paragon->capture($pokeball);

        $pokeball->quantity -= 1;

        if ($pokeball->quantity <= 0) {
            $pokeball->delete();
        } else {
            $pokeball->save();
        }

        return redirect()->route('paragon.battle');
    }

    public function leave()
    {
        session()->forget('paragon');
        session()->forget('battle');
        session()->forget('tmp');
        session()->forget('victory_bonus');

        return redirect()->route('home');
    }
}
