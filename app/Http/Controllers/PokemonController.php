<?php

namespace App\Http\Controllers;

use App\Contracts\MoveInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PokemonController extends Controller
{
    protected $pokemon;
    protected $userPokemon;
    protected $userItem;
    protected $move;

    public function __construct(PokemonInterface $pokemon, UserPokemonInterface $userPokemon, UserItemInterface $userItem, MoveInterface $move)
    {
        $this->middleware('auth');

        $this->pokemon = $pokemon;
        $this->userPokemon = $userPokemon;
        $this->userItem = $userItem;
        $this->move = $move;
    }

    public function index($id = null)
    {
        $data['box'] = $this->userPokemon->all($id);

        return view('pokemon.index', $data);
    }

    public function search()
    {
        $data['box'] = $this->userPokemon->search(request('user'), request('keyword'));
        $data['userBox'] = request('user');

        return view('pokemon.index', $data);
    }

    public function update()
    {
        abort_if(auth()->user()->id != 1, 403);
        $this->pokemon->updateDungeons();

        return back();
    }

    public function show($id)
    {
        $data['pokemon'] = $this->userPokemon->show($id);
        $data['nextLevel'] = $this->pokemon->nextLevel($data['pokemon']['level']);
        $data['evolution'] = $this->pokemon->evolution($data['pokemon']['info']['id']);
        $data['forms'] = $this->pokemon->forms($data['pokemon']['info']['id']);

//        if ($data['pokemon']['experience'] >= $this->pokemon->nextLevel($data['pokemon']['level'])) {
//            $this->pokemon->levelUp($data['pokemon']);
//        }

        return view('pokemon.show', $data);
    }

    public function team()
    {
        $data['team'] = $this->userPokemon->team();

        return view('trainers.team', $data);
    }

    public function move($id)
    {
        $pokemon = $this->userPokemon->findOrFail($id);

        abort_if($pokemon->in_shop, 403);
        abort_if($pokemon->in_trade, 403);

        $this->userPokemon->moveToTop($pokemon);

        $this->userPokemon->flashSuccess("{$pokemon->info->name} is now first to appear in battle.");

        return back();
    }

    public function remove($id)
    {
        if (session('dungeon') || session('arena')) {
            $this->userPokemon->flashError("You cannot add/remove a Pokemon while in dungeon!");

            return back();
        }

        $pokemon = $this->userPokemon->findOrFail($id);

        abort_if($pokemon->in_shop, 403);
        abort_if($pokemon->in_trade, 403);

        if ($this->userPokemon->team()->count() == 1) {
            $this->userPokemon->flashError("You must have atleast 1 pokemon in your team.");

            return back();
        }

        $this->authorize('own', $pokemon);
        $this->authorize('active', $pokemon);

        $pokemon->in_team = 0;
        $pokemon->save();

        $this->userPokemon->flashSuccess("You have removed {$pokemon->info->name} from your team.");

        return back();
    }

    public function add($id)
    {
        if (session()->has('dungeon') || session()->has('trade') || session()->has('arena') || session()->has('paragon') || session()->has('friendly')) {
            $this->userPokemon->flashError("You cannot add/remove a Pokemon while in dungeon!");

            return back();
        }

        $pokemon = $this->userPokemon->findOrFail($id);

        abort_if($pokemon->in_shop, 403);
        abort_if($pokemon->in_trade, 403);

        if ($this->userPokemon->team()->count() >= 6) {
            $this->userPokemon->flashError("You already have 6 pokemon in your team.");

            return back();
        }

        $this->authorize('own', $pokemon);
        $this->authorize('inactive', $pokemon);

        $pokemon->in_team = 1;
        $pokemon->save();

        $this->userPokemon->flashSuccess("You have added {$pokemon->info->name} to your team.");

        return back();
    }

    public function release($id)
    {
        abort_if(session('dungeon') || session()->has('trade') || session('arena'), 403);

        $pokemon = $this->userPokemon->findOrFail($id);

        abort_if($pokemon->in_shop, 403);
        abort_if($pokemon->in_trade, 403);

        if ($pokemon->item) {
            $this->userPokemon->flashError("You cannot release a Pokemon while it's holding an item!");

            return back();
        }

        $this->authorize('own', $pokemon);
        $this->authorize('inactive', $pokemon);

        //club privileges
        $clubCrys = 0;
        if (auth()->user()->club_id) {
            foreach (auth()->user()->club->privileges as $privilege) {
                if ($privilege->pivot->expired_on > Carbon::now() && $privilege->modifier == 'crystal') {
                    $clubCrys += $privilege->value;
                }
            }
        }

        $this->userPokemon->destroy($id);

        if ($pokemon->info->alternate_form && $pokemon->info->rarity >= 4) {
            $crystal = ($pokemon->info->alternate_cost * 0.30) + $clubCrys;
        } else {
            $crystal = ($pokemon->info->rarity * 10) + $clubCrys;
        }

        auth()->user()->crystal += $crystal;
        auth()->user()->save();

        $this->userPokemon->flashSuccess("{$pokemon->info->name} has been released back to the wild!<br>You earned +{$crystal} pokemon crystal.");

        return redirect()->route('pokemon.index');
    }

    public function evolve($id, $method)
    {
        abort_if(!is_numeric($method) && !in_array($method, ['sun-stone', 'fire-stone', 'water-stone', 'thunder-stone', 'moon-stone', 'leaf-stone', 'ice-stone', 'shiny-stone', 'dusk-stone', 'dawn-stone']), 404);

        $pokemon = $this->userPokemon->findOrFail($id);

        abort_if($pokemon->in_shop, 403);
        abort_if($pokemon->in_trade, 403);

        $this->authorize('own', $pokemon);

        $stone = null;

        if (is_numeric($method) && $pokemon->level < $method) {
            $this->userPokemon->flashError('Not enough pokemon level to evolve!');

            return back();
        } elseif (!is_numeric($method) && !$stone = $this->userItem->findStone($method)) {
            $this->userPokemon->flashError("You don't have the necessary stone for evolution.");

            return back();
        }

        $evolved = $this->pokemon->evolve($pokemon, $method);

        if ($stone) {
            $stone->decrement('quantity', 1);

            if ($stone->quantity <= 0) {
                $stone->delete();
            }
        }

        $this->userPokemon->flashSuccess("Your pokemon has evolved into {$evolved}!");

        return back();
    }

    public function form($id, $form)
    {
        $pokemon = $this->userPokemon->findOrFail($id);
        $change = $this->pokemon->findOrFail($form);

        abort_if($pokemon->in_shop, 403);
        abort_if($pokemon->in_trade, 403);
        abort_if($pokemon->info->id != $change->alternate_form, 403);

        $this->authorize('own', $pokemon);

        if (auth()->user()->crystal < $change->alternate_cost) {
            $this->userPokemon->flashError('Not enough crystals to change form!');

            return back();
        }

        $this->pokemon->changeForm($pokemon, $change);

        auth()->user()->crystal -= $change->alternate_cost;
        auth()->user()->save();

        $this->userPokemon->flashSuccess("Your pokemon has changed form into {$change->name}!");

        return back();
    }

    public function forget($id, $move)
    {
        $pokemon = $this->userPokemon->findOrFail($id);

        abort_if($pokemon->in_shop, 403);
        abort_if($pokemon->in_trade, 403);

        $this->authorize('own', $pokemon);

        if ($pokemon->moves->count() == 1) {
            $this->userPokemon->flashError("{$pokemon->info->name} cannot forget its remaining move!");

            return back();
        }

        $pokemon->moves()->detach($move);

        $this->userPokemon->flashSuccess("{$pokemon->info->name} has forgot a move!");

        return back();
    }

    public function learn($id)
    {
        $move = $this->move->findOrFail($id);

        if (auth()->user()->gold < $move->level * 695) {
            $this->userPokemon->flashError("You don't have enough coins to learn this move!");

            return back();
        }

        session(['learn' => $move]);

        return $this->team();
    }

    public function learnBy($id)
    {
        abort_if(!session()->has('learn'), 403);

        $pokemon = $this->userPokemon->findOrFail($id);

        $this->authorize('own', $pokemon);

        if ($this->move->learn($pokemon)) {
            $this->userPokemon->flashSuccess("{$pokemon->info->name} has learned ".session("learn")->name."!");
        }

        session()->forget('learn');

        return redirect()->route('team');
    }

    public function pokedex()
    {
        $data['pokedex'] = $this->pokemon->pokedex();
        $data['dex'] = explode(',', auth()->user()->pokedex);
        $data['result'] = null;

        return view('pokemon.pokedex', $data);
    }

    public function pokedexSearch()
    {
        abort_if($this->pokemon->doubleClicked(request('prevent')), 500);

        $data['result'] = $this->pokemon->pokedexSearch(request('keyword'));
        $data['dex'] = explode(',', auth()->user()->pokedex);

        return view('pokemon.pokedex', $data);
    }

    public function pokedexShow($id)
    {
        $data['pokemon'] = $this->pokemon->findOrFail($id);
        $data['typeEffects'] = $this->pokemon->typeEffects($data['pokemon']->primary_type, $data['pokemon']->secondary_type);
        $data['evolution'] = $this->pokemon->evolution($data['pokemon']->id);
        $data['forms'] = $this->pokemon->forms($data['pokemon']->id);
        $data['evolution2'] = null;

        foreach ($data['evolution'] as $evo) {
            $data['evolution2'] = $this->pokemon->evolution($evo->id);
        }

        return view('pokemon.pokedex-show', $data);
    }
}
