<?php

namespace App\Http\Controllers;

use App\Contracts\ClubInterface;
use App\Contracts\PrivilegeInterface;
use Carbon\Carbon;

class PrivilegeController extends Controller
{
    protected $privilege;
    protected $club;

    public function __construct(PrivilegeInterface $privilege, ClubInterface $club)
    {
        $this->middleware('auth');

        $this->privilege = $privilege;
        $this->club = $club;
    }

    public function index()
    {
        abort_if(!auth()->user()->club_id, 403);

        //delete expired
        foreach (auth()->user()->club->privileges as $privilege) {
            if ($privilege->pivot->expired_on < Carbon::now()) {
                auth()->user()->club->privileges()->detach($privilege->id);
            }
        }

        $data['perks'] = $this->privilege->available();

        return view('privileges.index', $data);
    }

    public function store()
    {
        abort_if($this->privilege->doubleClicked(request('prevent')), 500);
        abort_if(!auth()->user()->club_id || !in_array(auth()->user()->club_role, [2,3]), 403);

        $privilege = $this->privilege->findOrFail(request('id'));

        abort_if(auth()->user()->club->level < $privilege->level, 403);

        if (auth()->user()->club->coin < $privilege->coin_cost) {
            $this->club->flashError("You don't have enough coins in your club's treasury!");

            return back();
        }

        if (auth()->user()->club->privileges->count() >= auth()->user()->club->max_privilege) {
            $this->club->flashError("You already used up all your perk slot!");

            return back();
        }

        auth()->user()->club->privileges()->attach($privilege->id, ['expired_on' => Carbon::now()->addHours($privilege->expiration)]);
        auth()->user()->club->decrement('coin', $privilege->coin_cost);

        $this->privilege->flashSuccess("You have activated a new club perk!");

        return back();
    }
}
