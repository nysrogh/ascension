<?php

namespace App\Http\Controllers;

use App\Contracts\RewardInterface;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RewardController extends Controller
{
    protected $reward;

    public function __construct(RewardInterface $reward)
    {
        $this->middleware('auth');

        $this->reward = $reward;
    }

    public function index()
    {
        $data['rewards'] = $this->reward->all();

        foreach ($data['rewards'] as $reward) {
            $data['claimed'][$reward->id] = $this->reward->timeLeft($reward->user[0]->pivot->claim);
        }

        return view('trainers.rewards', $data);
    }

    public function claim()
    {
        abort_if($this->reward->doubleClicked(request('prevent')), 500);

        session()->forget('rewards');

        $reward = $this->reward->find(request('id'));

        $this->authorize('reward', $reward);

        abort_if($this->reward->timeLeft($reward->user[0]->pivot->claim) != 'claim', 403);

        $this->reward->claim($reward);

        return back();
    }
}
