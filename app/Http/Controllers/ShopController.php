<?php

namespace App\Http\Controllers;

use App\Contracts\NotifInterface;
use App\Contracts\TradeInterface;
use App\Contracts\TradeOfferInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use App\Contracts\ContentInterface;
use App\Contracts\UserShopInterface;
use App\Http\Requests\CreateShopRequest;
use App\Http\Requests\EditShopRequest;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    protected $userShop;
    protected $userShopContent;
    protected $userItem;
    protected $userPokemon;
    protected $notif;
    protected $trade;
    protected $tradeOffer;

    public function __construct(UserShopInterface $userShop, ContentInterface $userShopContent, UserItemInterface $userItem, UserPokemonInterface $userPokemon, NotifInterface $notif, TradeInterface $trade, TradeOfferInterface $tradeOffer)
    {
        $this->middleware('auth');
        $this->middleware('location', ['except' => ['tradeOffer', 'tradeCreate', 'tradeRemove', 'tradeCoinsCrystal', 'tradeCoinsCrystalStore', 'tradeStore']]);

        $this->userShop = $userShop;
        $this->userShopContent = $userShopContent;
        $this->userItem = $userItem;
        $this->userPokemon = $userPokemon;
        $this->notif = $notif;
        $this->trade = $trade;
        $this->tradeOffer = $tradeOffer;
    }

    public function index()
    {
        $data['shops'] = $this->userShop->randomShop();
        $data['results'] = null;

        return view('locations.grand-bazaar', $data);
    }

    public function show($id)
    {
        $data['shop'] = $this->userShop->findOrFail($id);
        $data['shopBg'] = $this->userShop->background($data['shop']->user->avatar, auth()->user()->avatar);
        $data['items'] = $this->userShopContent->items($id);
        $data['pokemon'] = $this->userShopContent->pokemon($id);
        $data['pokedex'] = explode(',', auth()->user()->pokedex);

        return view('locations.grand-bazaar-shop', $data);
    }

    public function search()
    {
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);

        $data['results'] = $this->userShopContent->searchStore(request('keyword'));

        return view('locations.grand-bazaar', $data);
    }

    public function create()
    {
        abort_if(auth()->user()->shop, 403);

        //$this->userShop->flashSuccess("You have successfully opened a store!");

        return view('locations.grand-bazaar-create');
    }

    public function edit()
    {
        abort_if(!auth()->user()->shop, 403);

        return view('locations.grand-bazaar-edit');
    }

    public function save(EditShopRequest $form)
    {
        $form->persist();

        $this->userShop->flashSuccess("You have successfully edited your store!");

        return redirect()->route('location.grand-bazaar.shop', auth()->user()->shop);
    }

    public function store(CreateShopRequest $form)
    {
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);

        if (count(explode(',', auth()->user()->pokedex)) < 15) {
            $this->userShop->flashError("Sorry! You need to have at least 15 Pokedex records to be able to setup a store.");

            return back();
        }

        $shop = $form->persist();

        $this->userShop->flashSuccess("You have successfully setup your store!");

        return redirect()->route('location.grand-bazaar.shop', $shop);
    }

    public function sellItem($id)
    {
        $data['item'] = $this->userItem->findOrFail($id);

        return view('locations.grand-bazaar-sell-item', $data);
    }

    public function sellPokemon($id)
    {
        if ($this->userShopContent->findPokemon($id)) {
            $this->userShop->flashError("Pokemon is already for sale! Remove it first.");

            return back();
        }

        $data['pokemon'] = $this->userPokemon->findOrFail($id);

        return view('locations.grand-bazaar-sell-pokemon', $data);
    }

    public function buy()
    {
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);

        $item = $this->userShopContent->findOrFail(request('item'));

        if (auth()->user()->gold < $item->price) {
            $this->userShop->flashError("Not enough coins to purchase the item!");

            return back();
        }

        if ($item->trades) {
            foreach ($item->trades as $trade) {
                foreach ($trade->offers as $offer) {
                    $this->tradeOffer->cancel($offer, $trade->user_id);
                }
                $this->notif->add($trade->user_id, auth()->user()->username ." has declined your trade offer.");
            }
        }

        $this->userShopContent->buy($item);

        $this->userShop->flashSuccess("You have purchased an item/pokemon for {$item->price} coins!");

        return back();
    }

    public function sellStore()
    {
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);
        abort_if(!in_array(request('category'), ['item', 'pokemon']) || request('price') > 99999999 || request('price') <=0, 403);

        if (request('category') == 'item') {
            $item = $this->userItem->findOrFail(request('id'));

            if ($this->userShopContent->findItem($item->info->id)) {
                $this->userShopContent->flashError("You already have this kind of item in store!");

                return back();
            }

            abort_if($item->user_id != auth()->user()->id || $item->quantity < request('quantity') || $item->is_held || request('quantity') <= 0, 403);

            $this->userShop->flashSuccess("You have successfully added an item to your store!");

        } else {
            $item = $this->userPokemon->findOrFail(request('id'));

//            if ($item->updated_at > Carbon::now()->subMinutes(30)) {
//                $this->userShop->flashError("You must not use this Pokemon for at least 30 minutes before you can sell it!");
//
//                return back();
//            }

            if ($item->item) {
                $this->userShop->flashError("You cannot sell a Pokemon while it's holding an item!");

                return back();
            }

            abort_if($item->user_id != auth()->user()->id || $item->in_team || $item->in_shop || $item->in_trade, 403);

            $this->userShop->flashSuccess("You have successfully added a Pokemon to your store!");
        }

        $this->userShopContent->addOrUpdate(request()->all(), $item);

        return redirect()->route('location.grand-bazaar.shop', auth()->user()->shop->id);
    }

    public function remove()
    {
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);

        $item = $this->userShopContent->findOrFail(request('item'));

        abort_if(auth()->user()->shop->id != $item->user_shop_id, 403);

        foreach ($item->trades as $trade) {
            foreach ($trade->offers as $offer) {
                $this->tradeOffer->cancel($offer, $trade->user_id);
            }
            $this->notif->add($trade->user_id, auth()->user()->username ." has declined your trade offer.");
        }

        $this->userShopContent->removeItem($item);

        $this->userShop->flashSuccess("Item has been removed from your shop!");

        return back();
    }

    public function tradeCreate()
    {
        abort_if(!session()->has('trade') || $this->trade->tradeExist(session('trade')), 403);

        return view('locations.grand-bazaar-shop-create-offer');
    }

    public function tradeOfferView($id)
    {
        $data['offers'] = $this->tradeOffer->getOffers($id);

        abort_if(!count($data['offers']), 404);

        return view('locations.grand-bazaar-shop-trade-view', $data);
    }

    public function tradeStore()
    {
        abort_if(!session()->has('trade'), 403);
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);

        $offer = $this->trade->saveOffer(session('offers'));

        if ($offer) {
            $this->userShop->flashSuccess("You have successfully created a trade offer!");
        }

        return redirect()->route('shop.trade', session('trade'));
    }

    public function tradeView($id)
    {
        $data['trade'] = $this->userShopContent->findOrFail($id);

        if (!session()->has('trade')) {
            session(['trade' => $id]);
        }

        return view('locations.grand-bazaar-shop-trade', $data);
    }

    public function tradeRemove($key)
    {
        abort_if(!session()->has('trade'), 403);

        session()->forget("offers.{$key}");

        return back();
    }

    public function tradeCoinsCrystal()
    {
        abort_if(!session()->has('trade'), 403);

        return view('locations.grand-bazaar-shop-trade-coins');
    }

    public function tradeCoinsCrystalStore()
    {
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);
        abort_if(!session()->has('trade') || !is_numeric(request('amount')), 403);

        if (request('type') == 'coins') {
            abort_if(request('amount') > auth()->user()->gold, 403);

            $offer = request('amount');
            $path = 'game/perks-coins';
        } else {
            abort_if(request('amount') > auth()->user()->crystal, 403);

            $offer = request('amount');
            $path = 'game/perks-crystal';
        }

        session()->push('offers', [
            'id' => 0,
            'img' => $path,
            'details' => $offer
        ]);

        return redirect()->route('shop.trade.create');
    }

    public function tradeOffer()
    {
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);
        abort_if(!session()->has('trade') || count(session('offers')) >= 10, 403);

        $offer = null;
        $path = null;

        if (request('type') == 'pokemon') {
            $offer = $this->userPokemon->findAvailable(request('offer'));

            abort_if($offer->in_shop || $offer->in_trade || $offer->in_team, 403);

            if ($offer->updated_at > Carbon::now()->subMinutes(30)) {
                $this->userShop->flashError("You must not use this Pokemon for at least 30 minutes before you can trade it!");

                return back();
            }

            if ($offer->item) {
                $this->userShop->flashError("You cannot trade a Pokemon while it's holding an item!");

                return back();
            }

            $path = 'pokemon/thumbnail';

        } elseif (request('type') == 'item') {
            $offer = $this->userItem->findOrFail(request('offer'));

            abort_if($offer->is_held, 403);

            $path = 'items';
        }

        session()->push('offers', [
            'id' => $offer->id,
            'img' => $path,
            'details' => $offer
        ]);

        return redirect()->route('shop.trade.create');
    }

    public function tradeCancel($id)
    {
        $trades = $this->trade->findTrade($id);

        foreach ($trades->offers as $offer) {
            $this->tradeOffer->cancel($offer, $trades->user_id);
        }

        $trades->delete();

        $this->userShop->flashSuccess("You have successfully removed your trade offer on this Pokemon!");

        return back();
    }

    public function tradeAccept()
    {
        abort_if($this->userShop->doubleClicked(request('prevent')), 500);

        $offers = $this->tradeOffer->getOffers(request('id'));

        abort_if(!count($offers) || $offers[0]->trade->content->shop->user_id != auth()->user()->id, 403);

        $this->notif->add($offers[0]->trade->user_id, auth()->user()->username ." has accepted your trade offer.");

        //receive offers
        foreach ($offers as $offer) {
            $this->tradeOffer->accept($offer);
        }

        //return declined offers
        $item = $this->userShopContent->findOrFail($offers[0]->trade->content->id);

        foreach ($item->trades as $trade) {
            if ($trade->id != request('id')) {
                foreach ($trade->offers as $offer) {
                    $this->tradeOffer->cancel($offer, $trade->user_id);
                }
                $this->notif->add($trade->user_id, auth()->user()->username ." has declined your trade offer.");
            }
        }

        //other user receive content
        $this->userShopContent->acceptTrade($offers[0]->trade->content, $offers[0]->trade->user_id);

        $this->userShop->flashSuccess("You have successfully completed a trade!");

        return redirect()->route('location.grand-bazaar.shop', request('shop'));
    }
}
