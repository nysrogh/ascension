<?php

namespace App\Http\Controllers;

use App\Contracts\ArenaInterface;
use App\Contracts\FriendlyInterface;
use App\Contracts\NotifInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\UserInterface;
use App\Contracts\UserPokemonInterface;
use App\Http\Requests\EditPasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class UserController extends Controller
{
    protected $user;
    protected $userPokemon;
    protected $pokemon;

    public function __construct(UserInterface $user, UserPokemonInterface $userPokemon, PokemonInterface $pokemon, ArenaInterface $arena, FriendlyInterface $friendly, NotifInterface $notif)
    {
        $this->middleware('auth');

        $this->user = $user;
        $this->pokemon = $pokemon;
        $this->userPokemon = $userPokemon;
        $this->arena = $arena;
        $this->friendly = $friendly;
        $this->notif = $notif;
    }

    public function show($username = null)
    {
        $data['trainer'] = $this->user->profile($username);
        $data['team'] = $this->userPokemon->team($data['trainer']['id']);
        $data['likes'] = count(explode(',', $data['trainer']->likes));
        $data['dislikes'] = count(explode(',', $data['trainer']->dislikes));

//        foreach ($this->user->old() as $users) {
//            $users->rewards()->attach(42);
//        }

        return view('trainers.profile', $data);
    }


    public function arenaReset()
    {
        abort_if(auth()->user()->id != 1, 403);

        foreach ($this->arena->all() as $play) {
            $this->user->arenaUpdate($play);
        }
    }

    public function clear()
    {
        abort_if(auth()->user()->id != 1, 403);

        foreach ($this->user->old() as $users) {
            $users->rewards()->detach(53);
        }

        return back();
    }

    public function getActivation($username)
    {
        abort_if(auth()->user()->id != 1, 403);

        $code = $this->user->profile($username)->setting_bgcolor;

        $this->user->flashSuccess("Activation code: ". $code);

        return back();
    }

    public function search()
    {
        abort_if($this->user->doubleClicked(request('prevent')), 500);

        $data['results'] = $this->user->searchUser(request('keyword'));

        return view('trainers.online', $data);
    }

    public function settings()
    {
        $data['captcha'] = random_int(100,999);

        return view('trainers.settings', $data);
    }

    public function avatars()
    {
        $avatar = [];

//        if (auth()->user()->gender == 1) {
//            for ($i = 1; $i < 151; $i++) {
//                if ($i >= 1 && $i <= 97) {
//                    $avatar[] = [$i, 30000];
//                } else {
//                    $avatar[] = [$i, 300000];
//                }
//            }
//        } else {
//            for ($i = 1; $i < 91; $i++) {
//                if ($i >= 1 && $i <= 61) {
//                    $avatar[] = [$i, 30000];
//                } else {
//                    $avatar[] = [$i, 300000];
//                }
//            }
//        }

        if (auth()->user()->gender == 1) {
            for ($i = 1; $i <= 97; $i++) {
                $avatar[] = [$i, 30000];
            }
        } else {
            for ($i = 1; $i <= 61; $i++) {
                $avatar[] = [$i, 30000];
            }
        }

        $ava = $avatar;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = new Collection($ava);
        $perPage = 15;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $ava2 = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        $data['avatars'] = $ava2->setPath(request()->url());

        return view('trainers.avatar', $data);
    }

    public function changePassword(EditPasswordRequest $form)
    {
        $form->persist();

        $this->user->flashSuccess("You have successfully changed your password!");

        return back();
    }

    public function avatarChange()
    {
        abort_if($this->user->doubleClicked(request('prevent')), 500);
        abort_if(!is_numeric(request('id')), 403);

        if (auth()->user()->gold < 30000) {
            $this->user->flashError("You don't have enough coins!");

            return back();
        }

        if (auth()->user()->gender == 1) {
            abort_if(request('id') > 97, 403);
        } else {
            abort_if(request('id') > 61, 403);
        }

        auth()->user()->avatar = auth()->user()->gender .'-'. request('id');
        auth()->user()->gold -= 30000;
        auth()->user()->save();

        $this->user->flashSuccess("You have successfully changed your avatar!");

        return back();
    }

    public function banUser($id)
    {
        abort_if($id == 1 || auth()->user()->status != 2, 403);

        $this->user->ban($id);

        $this->user->flashSuccess("You have successfully banned the user!");

        return back();
    }

    public function unbanUser($id)
    {
        abort_if($id == 1 || auth()->user()->status != 2, 403);

        $this->user->unban($id);

        $this->user->flashSuccess("You have successfully unbanned the user!");

        return back();
    }

    public function report()
    {
        abort_if($this->user->doubleClicked(request('prevent')), 500);
        abort_if(auth()->user()->id == request('id'), 403);

        $user = $this->user->findOrFail(request('id'));
        $list = explode(',', $user->report);

        if (in_array(auth()->user()->ip_address, $list) || in_array($user->status, [2,3,4]) || count(explode(',', auth()->user()->pokedex)) < 50) {
            $this->user->flashError("You have already reported this user.");

            return back();
        }

        $newList = implode(',', array_prepend($list, auth()->user()->ip_address));

        $user->report = $newList;
        $user->save();

        $count = count(explode(',', $user->report)) - 1;

        if ($count >= 15) {
            $this->notif->add($user->id, "Someone has reported you for inappropriate behaviour.");
            $this->notif->add(1, "{$user->id}: {$user->username}.");
        }

        $this->user->flashSuccess("You have reported this user.");

        return back();
    }

    public function like($id)
    {
        abort_if($id == auth()->user()->id, 403);

        $user = $this->user->findOrFail($id);
        $likes = explode(',', $user->likes);

        abort_if(in_array(auth()->user()->id, $likes), 403);

        $newLikes = implode(',',array_prepend($likes, auth()->user()->id));

        $user->likes = $newLikes;
        $user->save();

        $this->user->flashSuccess("You have successfully liked this user.");

        return back();
    }

//    public function dislike($id)
//    {
//        abort_if($id == auth()->user()->id, 403);
//
//        $user = $this->user->findOrFail($id);
//        $dislikes = explode(',', $user->dislikes);
//
//        abort_if(in_array(auth()->user()->id, $dislikes), 403);
//
//        $newDislikes = implode(',',array_prepend($dislikes, auth()->user()->id));
//
//        $user->dislikes = $newDislikes;
//        $user->save();
//
//        $this->user->flashSuccess("You have successfully disliked this user.");
//
//        return back();
//    }

    public function updateIntro()
    {
        abort_if($this->user->doubleClicked(request('prevent')), 500);

        $data = $this->validate(request(), [
            'message' => 'required|min:2|max:150',
        ]);

        auth()->user()->introduction = $data['message'];
        auth()->user()->save();

        $this->user->flashSuccess("You have successfully updated your intro message.");

        return back();
    }
}
