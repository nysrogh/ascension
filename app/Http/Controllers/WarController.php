<?php

namespace App\Http\Controllers;

use App\Contracts\WarInterface;
use Illuminate\Http\Request;

class WarController extends Controller
{
    public function __construct(WarInterface $war)
    {
        $this->middleware('auth');

        $this->war = $war;
    }

    public function index()
    {

    }

    public function start()
    {
        return view('wars.create');
    }

    public function store()
    {

    }

    public function results($club = null)
    {

    }
}
