<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class Activated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        abort_if(Auth::user()->status == 4, 301);

        if (Auth::user() && Auth::user()->status < 1) {
            return redirect()->route('home.welcome');
        }

        return $next($request);
    }
}
