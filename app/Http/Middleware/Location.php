<?php

namespace App\Http\Middleware;

use Closure;

class Location
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        abort_if(auth()->user()->status == 4, 301);

        if (session('use')) session()->forget('use');
        if (session('learn')) session()->forget('learn');
        if (session('trade')) {
            session()->forget('trade');
            session()->forget('offers');
        }
        if (session('rewards')) session()->forget('rewards');
        if (session('paragon')) return redirect()->route('paragon.index');
        if (session('friendly')) return redirect()->route('friendly.index');
        if (session('paragon.enemy')) return redirect()->route('paragon.battle');
        if (session('battle.enemy')) return redirect()->route('battle');
        if (session('arena')) return redirect()->route('arena.battle');
        if (session('dungeon')) return redirect()->route('dungeon');

        return $next($request);
    }
}
