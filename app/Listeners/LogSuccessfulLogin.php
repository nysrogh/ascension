<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;

class LogSuccessfulLogin
{
    public function handle(Login $event)
    {
        $event->user->ip_address = request()->ip();
        $event->user->save();

        $event->user->touch();
    }
}