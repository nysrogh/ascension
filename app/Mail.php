<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $fillable = [
        'user_id', 'sender_id', 'message'
    ];

    public function sender()
    {
        return $this->hasOne('App\User', 'id', 'sender_id');
    }

    public function recipient()
    {
        return $this->hasOne('App\User');
    }
}
