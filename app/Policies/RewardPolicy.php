<?php

namespace App\Policies;

use App\User;
use App\Reward;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class RewardPolicy
{
    use HandlesAuthorization;

    public function reward(User $user, Reward $reward)
    {
        $started = new Carbon($reward->user[0]->pivot->claim);
        if ($started > Carbon::now()) return false;

        if ($reward->pokedex && count(explode(',', $user->pokedex)) < $reward->pokedex) return false;

        if ($reward->dungeon && $user->dungeons_completed < $reward->dungeon) return false;

        if ($reward->mission && $user->missions_completed < $reward->mission) return false;

        return true;
    }
}
