<?php

namespace App\Policies;

use App\User;
use App\UserItem;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserItemPolicy
{
    use HandlesAuthorization;

    public function own(User $user, UserItem $userItem)
    {
        return $user->id == $userItem->user_id;
    }

}
