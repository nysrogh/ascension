<?php

namespace App\Policies;

use App\User;
use App\UserPokemon;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPokemonPolicy
{
    use HandlesAuthorization;

    public function own(User $user, UserPokemon $pokemon)
    {
        return $user->id == $pokemon->user_id;
    }

    public function active(User $user, UserPokemon $pokemon)
    {
        return $pokemon->in_team;
    }

    public function inactive(User $user, UserPokemon $pokemon)
    {
        return !$pokemon->in_team;
    }
}
