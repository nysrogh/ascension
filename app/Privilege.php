<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    public function club()
    {
        return $this->belongsToMany('App\Club')->withPivot('expired_on');
    }
}
