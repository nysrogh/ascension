<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('user', auth()->user() ?: new \App\User);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\ArenaInterface',
            'App\Repositories\ArenaRepository'
        );

        $this->app->bind(
            'App\Contracts\ArenaStatusEffectInterface',
            'App\Repositories\ArenaStatusEffectRepository'
        );

        $this->app->bind(
            'App\Contracts\BattleInterface',
            'App\Repositories\BattleRepository'
        );

        $this->app->bind(
            'App\Contracts\ChatInterface',
            'App\Repositories\ChatRepository'
        );

        $this->app->bind(
            'App\Contracts\ContentInterface',
            'App\Repositories\ContentRepository'
        );

        $this->app->bind(
            'App\Contracts\ClubInterface',
            'App\Repositories\ClubRepository'
        );

        $this->app->bind(
            'App\Contracts\ClubApplicantInterface',
            'App\Repositories\ClubApplicantRepository'
        );

        $this->app->bind(
            'App\Contracts\ClubLogInterface',
            'App\Repositories\ClubLogRepository'
        );

        $this->app->bind(
            'App\Contracts\DungeonInterface',
            'App\Repositories\DungeonRepository'
        );

        $this->app->bind(
            'App\Contracts\ForumInterface',
            'App\Repositories\ForumRepository'
        );

        $this->app->bind(
            'App\Contracts\ForumCommentInterface',
            'App\Repositories\ForumCommentRepository'
        );

        $this->app->bind(
            'App\Contracts\FriendlyInterface',
            'App\Repositories\FriendlyRepository'
        );

        $this->app->bind(
            'App\Contracts\FriendlyStatusEffectInterface',
            'App\Repositories\FriendlyStatusEffectRepository'
        );

        $this->app->bind(
            'App\Contracts\GameConfigInterface',
            'App\Repositories\GameConfigRepository'
        );

        $this->app->bind(
            'App\Contracts\ItemInterface',
            'App\Repositories\ItemRepository'
        );

        $this->app->bind(
            'App\Contracts\MailInterface',
            'App\Repositories\MailRepository'
        );

        $this->app->bind(
            'App\Contracts\MissionInterface',
            'App\Repositories\MissionRepository'
        );

        $this->app->bind(
            'App\Contracts\MoveInterface',
            'App\Repositories\MoveRepository'
        );

        $this->app->bind(
            'App\Contracts\NotifInterface',
            'App\Repositories\NotifRepository'
        );

        $this->app->bind(
            'App\Contracts\ParagonInterface',
            'App\Repositories\ParagonRepository'
        );

        $this->app->bind(
            'App\Contracts\ParagonStatusEffectInterface',
            'App\Repositories\ParagonStatusEffectRepository'
        );

        $this->app->bind(
            'App\Contracts\PokemonInterface',
            'App\Repositories\PokemonRepository'
        );

        $this->app->bind(
            'App\Contracts\PrivilegeInterface',
            'App\Repositories\PrivilegeRepository'
        );

        $this->app->bind(
            'App\Contracts\RewardInterface',
            'App\Repositories\RewardRepository'
        );

        $this->app->bind(
            'App\Contracts\TradeInterface',
            'App\Repositories\TradeRepository'
        );

        $this->app->bind(
            'App\Contracts\TradeOfferInterface',
            'App\Repositories\TradeOfferRepository'
        );

        $this->app->bind(
            'App\Contracts\StatusEffectInterface',
            'App\Repositories\StatusEffectRepository'
        );

        $this->app->bind(
            'App\Contracts\UserInterface',
            'App\Repositories\UserRepository'
        );

        $this->app->bind(
            'App\Contracts\UserItemInterface',
            'App\Repositories\UserItemRepository'
        );

        $this->app->bind(
            'App\Contracts\UserPokemonInterface',
            'App\Repositories\UserPokemonRepository'
        );

        $this->app->bind(
            'App\Contracts\UserShopInterface',
            'App\Repositories\UserShopRepository'
        );

        $this->app->bind(
            'App\Contracts\WarInterface',
            'App\Repositories\WarRepository'
        );
    }
}
