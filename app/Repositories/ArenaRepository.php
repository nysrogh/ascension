<?php
namespace App\Repositories;

use App\Contracts\ArenaInterface;
use App\Arena;
use App\Contracts\BattleInterface;
use App\Contracts\ArenaStatusEffectInterface;
use App\Contracts\NotifInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;

class ArenaRepository extends Repository implements ArenaInterface
{
    protected $arena;
    protected $userPokemon;
    protected $battle;
    protected $statusEffect;
    protected $pokemon;
    protected $notif;

    public function __construct(
        Arena $arena,
        UserPokemonInterface $userPokemon,
        BattleInterface $battle,
        ArenaStatusEffectInterface $statusEffect,
        PokemonInterface $pokemon,
        NotifInterface $notif,
        UserItemInterface $userItem
    )
    {
        parent::__construct($arena);

        $this->arena = $arena;
        $this->pokemon = $pokemon;
        $this->userPokemon = $userPokemon;
        $this->battle = $battle;
        $this->statusEffect = $statusEffect;
        $this->notif = $notif;
        $this->userItem = $userItem;
    }

    public function findRank($user = null)
    {
        return $this->arena->where('user_id', $user ?? auth()->user()->id)->first();
    }

    public function rankings()
    {
        return $this->arena->orderBy('rank')->simplePaginate(15);
    }

    public function challenger($user)
    {
        $player = $this->findRank(auth()->user()->id);

        return $this->arena->where('user_id', $user)
            ->whereBetween('rank', [$player->rank - 3, $player->rank])
            ->firstOrFail();
    }

    public function withinRank()
    {
        $joined = $this->findRank();

        if (!$joined || in_array($joined->rank, [1,2,3])) {
            return $this->arena->with('user')->orderBy('rank')->limit(3)->get();
        }

        return $this->arena->with('user')
            ->where('id', '!=', $joined->id)
            ->whereBetween('rank', [$joined->rank - 3, $joined->rank])
            ->orderBy('rank')->limit(3)->get();
    }

    public function join()
    {
        $last = $this->arena->orderBy('rank', 'desc')->first(['rank']);

        return $this->arena->create([
            'user_id' => auth()->user()->id,
            'rank' => ($last) ? $last->rank + 1 : 1
        ]);
    }

    public function background($result = false)
    {
        $background = Image::make(public_path("images/battle/arena-bg.png"));

        if (!$result || !is_array($result)) {
            if ($result == 'defeat' || $result == 'victory') {
                $background->insert(public_path("images/trainers/" . session('arena.opponent')->user->avatar . ".png"), '', 110, -5);
            } else {
                $background->insert(public_path("images/pokemon/front/" . str_slug(session('arena.enemy.stats')->info->name) . ".png"), '',
                    session('arena.enemy.stats')->info->front_x, session('arena.enemy.stats')->info->front_y);
            }
        }

        if (!$result || is_array($result)) {
            $background->insert(public_path("images/pokemon/back/" . str_slug(session('arena.pokemon.stats.info')->name) . ".png"), '',
                session('arena.pokemon.stats.info')->back_x, session('arena.pokemon.stats.info')->back_y);
        }

        return 'data:image/png;base64,' . base64_encode($background->encode('jpg', 90));
    }

    public function initialize($challenge)
    {
        $array = [];

        $array['enemy_team'] = $this->userPokemon->arenaTeam($challenge->user_id);
        $array['enemy'] = [
            'stats' => (object)$array['enemy_team'][0],
            "enemy_current_hp" => $array['enemy_team'][0]->hp,
            "not_effective" => [],
            "super_effective" => []
        ];

        $array['pokemon_team'] = $this->userPokemon->arenaTeam();
        $array["pokemon"] = [
            'stats' => $array['pokemon_team'][0],
            "pokemon_current_hp" => $array['pokemon_team'][0]->hp
        ];

        $array["turns"] = 1;
        $array['opponent'] = $challenge;

        $this->saveSession('arena', $array);

        session(['background' => $this->background()]);

        return false;
    }

    public function switchPokemon($pokemon, $type)
    {
        $oldName = session("arena.{$type}.stats")->info->name;
        $who = $type == 'pokemon' ? auth()->user()->username : session("arena.opponent")->user->username;

        session()->push('arena_log', "<strong>{$oldName} fainted!</strong>");
        session()->push('arena_log', "<strong>{$who} sent out {$pokemon->info->name}!</strong>");

        $array["{$type}"] = [
            'stats' => $pokemon,
            "{$type}_current_hp" => $pokemon->hp,
            'tmp' => [],
            'not_effective' => [],
            'super_effective' => []
        ];

        $this->saveSession('arena', $array);

        session(['background' => $this->background()]);

        session()->save();
    }

    public function check()
    {
        if (session("arena.pokemon.pokemon_current_hp") <= 0 && !session()->has('result')) {
            if ($new = $this->checkTeam('pokemon')) {
                $this->switchPokemon($new, 'pokemon');
            } else {
                $this->defeat();
            }

            return true;
        }

        if (session("arena.enemy.enemy_current_hp") <= 0 && !session()->has('result')) {
            if ($new = $this->checkTeam('enemy')) {
                $this->switchPokemon($new, 'enemy');
            } else {
                $this->victory();
            }

            return true;
        }

        return false;
    }

    public function defeat()
    {
        session()->forget('arena_log');

        $enemy = session("arena.opponent")->user;
        $this->findRank()->increment('lose');

        $club = 0;

        //club privileges
        if (auth()->user()->club_id) {
            foreach (auth()->user()->club->privileges as $privilege) {
                if ($privilege->pivot->expired_on > Carbon::now() && $privilege->modifier == 'medal') {
                    $club += $privilege->value;
                }
            }
        }

        auth()->user()->increment('medal', 1 + $club);

        session()->push('arena_log', "<strong>You lost!</strong>");
        session()->push('arena_log', "&#8220;". strip_tags(stripslashes($enemy->arena_message)) ."&#8221;");
        session()->push('arena_log', "<span class='text-success'>You gained +3 medals!</span>");

        $this->notif->add(session("arena.opponent")->user_id, "Someone has challenged you in arena but lost.");

        session("arena.opponent")->increment('win');

        session(['result' => 'defeat']);
        session(['background' => $this->background('defeat')]);

        session()->save();
    }

    public function victory()
    {
        $exp = 0; $gold = 0;
        session()->forget('arena_log');
        $rank = $this->findRank();
        $opRank = $this->findRank(session("arena.opponent")->user_id);

        session(['oldRank' => $rank->rank]);

        $club = 0;

        //club privileges
        if (auth()->user()->club_id) {
            foreach (auth()->user()->club->privileges as $privilege) {
                if ($privilege->pivot->expired_on > Carbon::now() && $privilege->modifier == 'medal') {
                    $club += $privilege->value;
                }
            }
        }

        $club += 7;
        auth()->user()->increment('medal', $club);

        session()->push('arena_log', "<strong>You won!</strong>");
        session()->push('arena_log', "<span class='text-success'>You are now <strong>rank ". $opRank->rank ."</strong>!</span>");
        session()->push('arena_log', "<span class='text-success'>You gained +{$club} medals!</span>");

        $rank->rank = $opRank->rank;
        $rank->win += 1;
        $opRank->rank = session('oldRank');
        $opRank->lose += 1;

        $rank->save();
        $opRank->save();

//        session("arena.opponent")->increment('lose');
//        session("arena.opponent")->update([
//            'rank' => $rank->rank
//        ]);

        foreach (session("arena.enemy_team") as $pokemon) {
            $value = $pokemon['level'] * $pokemon['info']->rarity;

            $exp += random_int(($value * 1.5) * .75, $value * 1.5);
            $gold += random_int(($value * 2.5) * .75, $value * 2.5);
        }

        if (session()->has('arena.enemy.add_exp')) {
            $exp += $exp * session('arena.enemy.add_exp');
        }

        if (session()->has('arena.enemy.add_coins')) {
            $gold += round($gold * session('arena.enemy.add_coins'));
        }

        $exp = round(($exp * 2) / count(session("arena.pokemon_team")));

        foreach (session("arena.pokemon_team") as $pokemon) {
            if ($pokemon->level < 100) {
                $pokemon->increment('experience', $exp);

                session()->push('arena_log', "<span class='text-success'>{$pokemon['info']->name} gained +{$exp} experience!</span>");
            }

            while($this->userPokemon->levelUp($pokemon->id)) {
                session()->push('arena_log', "<span class='text-success'>{$pokemon['info']->name} has leveled up!</span>");
            }
        }

        auth()->user()->increment('gold', $gold * 3);
        session()->push('arena_log', "<span class='text-success'>You received +{$gold} coins for winning!</span>");

        $this->notif->add($opRank->user_id, "Someone has defeated you in arena. You are now rank {$opRank->rank}.");

        session()->forget('oldRank');
        session(['result' => 'victory']);
        session(['background' => $this->background('victory')]);

        session()->save();
    }

    public function checkTeam($team)
    {
        session()->push("arena.{$team}_dead", session("arena.{$team}.stats")->id);

        foreach (session("arena.{$team}_team") as $pokemon) {
            if (!in_array($pokemon->id, session("arena.{$team}_dead"))) {
                return $pokemon;
            }
        }

        return false;
    }

    public function checkMove($id)
    {
        foreach (session('arena.pokemon.stats')->moves as $list)
        {
            if ($list->id == $id) return $list;
        }

        return false;
    }

    public function fight($move)
    {
        $pokemon = session('arena.pokemon.stats');
        $enemy = session('arena.enemy.stats');

        //held item [turn] - pokemon
        $quickp = 0;
        if (session('arena.pokemon.stats')->item) {
            if ($pokemon->item->info->secondary_attr == 'turn') {
                switch ($pokemon->item->info->primary_attr) {
                    case 'heal': $this->userItem->itemEffectTurnHeal($pokemon, 'pokemon', 'arena'); break;
                    case 'selected': $this->userItem->itemEffectTurnSelected($pokemon, $move, 'pokemon', 'arena'); break;
                    case 'first': $quickp = $this->userItem->itemEffectTurnFirst($pokemon, 'pokemon', 'arena'); break;
                    case 'exp': $this->userItem->itemEffectTurnAddBonus($pokemon, 'exp', 'enemy', 'arena'); break;
                    case 'coins': $this->userItem->itemEffectTurnAddBonus($pokemon, 'coins', 'enemy', 'arena'); break;
                }
            }
        }

        //held item [turn] - enemy
        $quicke = 0;
        $sm = $this->smartMove();
        if (session('arena.enemy.stats')->item) {
            if ($enemy->item->info->secondary_attr == 'turn') {
                switch ($enemy->item->info->primary_attr) {
                    case 'heal': $this->userItem->itemEffectTurnHeal($enemy, 'enemy', 'arena'); break;
                    case 'selected': $this->userItem->itemEffectTurnSelected($enemy, $sm, 'enemy', 'arena'); break;
                    case 'first': $quicke = $this->userItem->itemEffectTurnFirst($enemy, 'enemy', 'arena'); break;
                    case 'exp': $this->userItem->itemEffectTurnAddBonus($enemy, 'exp', 'pokemon', 'arena'); break;
                    case 'coins': $this->userItem->itemEffectTurnAddBonus($enemy, 'coins', 'pokemon', 'arena'); break;
                }
            }
        }

        $battle = [
            "pokemon" => [
                'speed' => $pokemon->speed + session("arena.pokemon.tmp.speed") + $quickp,
                'move' => session("arena.selected.{$pokemon->id}") ?? $move
            ],
            "enemy" => [
                'speed' => $enemy->speed + session("arena.enemy.tmp.speed") + $quicke,
                'move' => session("arena.selected.{$enemy->id}") ?? $sm
            ]
        ];

        $sorted = array_reverse(array_sort($battle, function($value) {
            return $value['speed'];
        }));

        foreach ($sorted as $key => $value) {
            $defKey = ($key == 'pokemon') ? 'enemy' : 'pokemon';
            $this->useMove(session("arena.{$key}"), session("arena.{$defKey}"), $value['move'], $key);

            if ($this->check()) break;
        }

        $array["turns"] = session("arena.turns") + 1;
        $this->saveSession('arena', $array);
    }

    public function critical($speed)
    {
        if (random_int(1,10000) <= $speed) {
            session()->push('arena_log', "<span class='text-danger'>A critical hit!</span>");

            return 2;
        }

        return 1;
    }

    public function useMove($attacker, $defender, $move, $key)
    {
        $array = [];
        $defKey = ($key == 'pokemon') ? 'enemy' : 'pokemon';

        if (!$this->statusEffect->check($key, $attacker['stats']->id)) return false;

        session()->push('arena_log', $attacker['stats']->info->name ." used ". $move->name ."!");

        //damaging move
        if (in_array($move->primary_attr, ['attack', 'special_attack'])) {

            //held items [accuracy]
            $accuracy = 0;
            if ($attacker['stats']->item) {
                if ($attacker['stats']->item->info->secondary_attr == 'accuracy') {
                    switch ($attacker['stats']->item->info->primary_attr) {
                        case 'critical': $accuracy = $this->userItem->itemEffectMoveAccuracy($attacker['stats']); break;
                        case 'evasion': $accuracy = $this->userItem->itemEffectMoveAccuracy($attacker['stats']); break;
                    }
                }
            }

            //evasion else attack
            if (random_int(1,10000) <= $defender['stats']->speed + session("arena.enemy.tmp.speed") + $accuracy) {
                session()->push('arena_log', "<span class='text-danger'>". $defender['stats']->info->name ." avoided the attack!</span>");
            } else {
                $attack = $attacker['stats']->{$move->primary_attr} + session("arena.{$key}.tmp.{$move->primary_attr}");

                $defense = $move->primary_attr == 'attack'
                    ? $defender['stats']->defense + session("arena.{$defKey}.tmp.defense")
                    : $defender['stats']->special_defense + session("arena.{$defKey}.tmp.special_defense");

                $isEnemy = $attacker['stats']->id != session('arena.pokemon.stats')->id ? true : false;

                $type = $this->typeModifiers($isEnemy, $move,
                    array_filter([$defender['stats']->info->primary_type, $defender['stats']->info->secondary_type], function ($var) {
                        return !is_null($var);
                    }));

                //damage calculation formula
                $damage = (((((($attacker['stats']->level * 2) / 5) + 2) * $move->primary_value
                                * $attack/$defense) / 50) + 2)
                    //modifiers = targets * weather * badge * critical * random * stab * type * burn * other
                    * (1 * 1 * 1 * $this->critical($attacker['stats']->speed + session("arena.pokemon.tmp.speed") + $accuracy) * (random_int(85,100)/100) * $this->battle->stab($move->type, array_filter([$attacker['stats']->info->primary_type, $attacker['stats']->info->secondary_type], function ($var) {
                            return !is_null($var);
                        })) * $type * 1 * 1);

                //held items [move]
                $add = 0; $protect = false;
                if ($attacker['stats']->item) {
                    if ($attacker['stats']->item->info->secondary_attr == 'move') {
                        switch ($attacker['stats']->item->info->primary_attr) {
                            case 'super': $damage = $this->userItem->itemEffectMoveSuper($attacker['stats'], $type, $damage); break;
                            case 'heal': $this->userItem->itemEffectMoveHeal($attacker['stats'], $damage, 'arena', $key); break;
                            case 'add-heal': $add = $this->userItem->itemEffectMoveAddHeal($attacker['stats'], $move, 'arena', $key); break;
                            case 'recoil': $damage = $this->userItem->itemEffectMoveRecoil($attacker['stats'], $damage, 'arena', $key); break;
                            case 'attack': $damage = $this->userItem->itemEffectMoveAttack('attack', $attacker['stats'], $damage, $move); break;
                            case 'special_attack': $damage = $this->userItem->itemEffectMoveAttack('special_attack', $attacker['stats'], $damage, $move); break;
                            case 'flinch': $this->userItem->itemEffectMoveFlinch($attacker['stats'], $defender['stats']->id, $defKey, 'arena'); break;
                            case 'protect-recoil': $protect = $this->userItem->itemEffectMoveProtectRecoil($attacker['stats'], $move, $key, 'arena'); break;
                            default:
                                if (in_array($attacker['stats']->item->info->primary_attr, ['poison', 'fighting', 'ice', 'dragon', 'ghost', 'flying', 'grass', 'normal', 'ground', 'fire', 'fairy', 'electric', 'steel', 'dark', 'rock', 'psychic', 'water', 'bug'])) {
                                    $damage = $this->userItem->itemEffectMoveType($attacker['stats'], $move, $damage);
                                }
                        }
                    }
                }

                if ($move->secondary_attr && !$protect) {
                    $obj = (object) [
                        'attacker' => $attacker['stats'],
                        'defender' => $defender['stats'],
                        'attribute' => $move->secondary_attr,
                        'value' => $move->secondary_value + $add,
                        'key' => $key,
                        'defKey' => $defKey,
                        'chance' => 10
                    ];

                    $this->statusEffect->secondaryEffects($obj);
                }

                session()->push('arena_log', $defender['stats']->info->name ." took ". round(max($damage, 0)) ." damage!");

                $array[$defKey]["{$defKey}_current_hp"] = max(session("arena.{$defKey}.{$defKey}_current_hp") - round(max($damage, 0)), 0);

                //held items [defender]
                if ($defender['stats']->item) {
                    if ($defender['stats']->item->info->secondary_attr == 'counter') {
                        switch ($defender['stats']->item->info->primary_attr) {
                            case 'return': $this->userItem->itemEffectMoveReturn($defender['stats'], $attacker['stats']->id, $damage, $key, 'arena'); break;
                        }
                    }
                }
            }

        } else {
            $chance = (!in_array($move->primary_attr, ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) ? 60 : 20;
            $obj = (object) [
                'attacker' => $attacker['stats'],
                'defender' => $defender['stats'],
                'attribute' => $move->primary_attr,
                'value' => $move->primary_value,
                'key' => $key,
                'defKey' => $defKey,
                'chance' => $move->primary_value + $chance
            ];

            $this->statusEffect->secondaryEffects($obj);

            if ($move->secondary_attr) {
                $chance = (!in_array($move->secondary_attr, ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) ? 70 : 20;

                $obj = (object) [
                    'attacker' => $attacker['stats'],
                    'defender' => $defender['stats'],
                    'attribute' => $move->secondary_attr,
                    'value' => $move->secondary_value,
                    'key' => $key,
                    'defKey' => $defKey,
                    'chance' => $move->secondary_value + $chance
                ];

                $this->statusEffect->secondaryEffects($obj);
            }
        }

        $this->saveSession('arena', $array);

        //held items [defender]
        if ($defender['stats']->item) {
            if ($defender['stats']->item->info->secondary_attr == 'counter') {
                switch ($defender['stats']->item->info->primary_attr) {
                    case 'ko': $this->userItem->itemEffectCounterKo($defender['stats'], $defKey, 'arena'); break;
                }
            }
        }

        return false;
    }

    public function typeModifiers($isEnemy, $move, $defender)
    {
        $modifier = 1;

        foreach ($defender as $type) {
            if ($this->battle->checkStrength($move->type, $type)) {
                if ($isEnemy && !in_array($move->name, session("arena.enemy.super_effective"))) {
                    session()->push("arena.enemy.super_effective", $move->name);
                }

                $modifier *= 2;
            } elseif ($this->battle->checkWeakness($move->type, $type)) {
                if ($isEnemy && !in_array($move->name, session("arena.enemy.not_effective"))) {
                    session()->push("arena.enemy.not_effective", $move->name);
                }

                $modifier /= 2;
            } elseif ($this->battle->checkNull($move->type, $type)) {
                if ($isEnemy && !in_array($move->name, session("arena.enemy.not_effective"))) {
                    session()->push("arena.enemy.not_effective", $move->name);
                }

                $modifier = 0;
            }
        }

        if ($modifier >= 2) {
            session()->push('arena_log', "It's super effective!");
        } elseif ($modifier < 1 && $modifier != 0) {
            session()->push('arena_log', "It's not very effective!");
        } elseif ($modifier == 0) {
            session()->push('arena_log', "It doesn't affect the opponent!");
        }

        return $modifier;
    }

    public function smartMove()
    {
        $mvs = [];

        //convert object moves to array;
        foreach (session('arena.enemy.stats')->moves as $m) {

            if (random_int(1,3) <= 2 && in_array($m->name, session("arena.enemy.super_effective"))) {
                return $m;
            }

            $mvs[] = $m;
        }

        shuffle($mvs);

        $move = array_first($mvs);

        //$move = $this->_randomMove($mvs);
        $ailments = session("arena.pokemon.tmp") ?? [];

//        //check for super-effective move
//        if (count(session("arena.enemy.super_effective"))
//            && in_array($move->name, session("arena.enemy.super_effective"))) {
//
//            return $move;
//        }

        //check already inflicted status
        if (in_array($move->primary_attr, $ailments)) {
            shuffle($mvs);

            return array_first($mvs);
        }

        //check for not very effective move
        if (count(session("arena.enemy.not_effective"))
            && in_array($move->name, session("arena.enemy.not_effective"))) {

            shuffle($mvs);

            return array_first($mvs);
        }

        return $move;
    }
}