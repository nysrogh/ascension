<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 10/14/2017
 * Time: 9:58 AM
 */
namespace App\Repositories;

namespace App\Repositories;

use App\Contracts\BattleInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use App\Traits\BattleConditionTrait;
use App\Traits\BattleTrait;

class BattlerRepository extends Repository implements BattleInterface
{
    use BattleTrait, BattleConditionTrait;

    public function __construct(
        UserPokemonInterface $userPokemon,
        UserItemInterface $userItem,
        PokemonInterface $pokemon
    )
    {
        $this->userPokemon = $userPokemon;
        $this->userItem = $userItem;
        $this->pokemon = $pokemon;
    }

    public function initialize()
    {
        $enemyTeam = [];
        $pokemonTeam = [];
        $type = null;

        if (session()->has('wild')) {
            //data
            $enemyTeam = (array)session('wild');
            $pokemonTeam = $this->userPokemon->team();

            //boss HP modifier
            $enemyTeam[0]['hp'] *= !is_numeric(session('dungeon.progress')) ? 5 : 1;

            //battle type
            $type = 'wild';
        } elseif (session()->has('arena')) {
            $enemyTeam = $this->userPokemon->arenaTeam(session()->has('arena'));
            $pokemonTeam = $this->userPokemon->arenaTeam();

            $type = 'arena';
        }

        $this->setup($pokemonTeam, $enemyTeam, $type);

        //forget wild data
        session()->forget('wild');

        return false;
    }

    public function checker()
    {
        $pokemon = session('battle.pokemon');
        $enemy = session('battle.enemy');
        $array = [];

        if (session()->has('battle.result')) return true;

        if ($this->checkTimeout()) return true;

        //check enemy dead
        if ($enemy["current_hp_{$enemy['stats']->id}"] <= 0) {

            //check for possible substitution
            if ($new = $this->checkTeam('enemy')) {
                $this->switchPokemon($new, 'enemy');

                return false;

            //victory
            } else {
                $this->victory();

                return true;
            }
        }

        //check pokemon if dead
        if ($pokemon["current_hp_{$pokemon['stats']->id}"] <= 0) {

            if ($this->substitute()) return false;

            $this->defeat();

            return true;
        }

        return false;
    }

    public function fight($move)
    {
        $pokemon = session('arena.pokemon.stats');
        $enemy = session('arena.enemy.stats');

        //held item [turn] - pokemon
        $quickp = 0;
        if (session('arena.pokemon.stats')->item) {
            if ($pokemon->item->info->secondary_attr == 'turn') {
                switch ($pokemon->item->info->primary_attr) {
                    case 'heal': $this->userItem->itemEffectTurnHeal($pokemon, 'pokemon', 'arena'); break;
                    case 'selected': $this->userItem->itemEffectTurnSelected($pokemon, $move, 'pokemon', 'arena'); break;
                    case 'first': $quickp = $this->userItem->itemEffectTurnFirst($pokemon, 'pokemon', 'arena'); break;
                    case 'exp': $this->userItem->itemEffectTurnAddBonus($pokemon, 'exp', 'enemy', 'arena'); break;
                    case 'coins': $this->userItem->itemEffectTurnAddBonus($pokemon, 'coins', 'enemy', 'arena'); break;
                }
            }
        }

        //held item [turn] - enemy
        $quicke = 0;
        $sm = $this->smartMove();
        if (session('arena.enemy.stats')->item) {
            if ($enemy->item->info->secondary_attr == 'turn') {
                switch ($enemy->item->info->primary_attr) {
                    case 'heal': $this->userItem->itemEffectTurnHeal($enemy, 'enemy', 'arena'); break;
                    case 'selected': $this->userItem->itemEffectTurnSelected($enemy, $sm, 'enemy', 'arena'); break;
                    case 'first': $quicke = $this->userItem->itemEffectTurnFirst($enemy, 'enemy', 'arena'); break;
                    case 'exp': $this->userItem->itemEffectTurnAddBonus($enemy, 'exp', 'pokemon', 'arena'); break;
                    case 'coins': $this->userItem->itemEffectTurnAddBonus($enemy, 'coins', 'pokemon', 'arena'); break;
                }
            }
        }

        $battle = [
            "pokemon" => [
                'speed' => $pokemon->speed + session("arena.pokemon.tmp.speed") + $quickp,
                'move' => session("arena.selected.{$pokemon->id}") ?? $move
            ],
            "enemy" => [
                'speed' => $enemy->speed + session("arena.enemy.tmp.speed") + $quicke,
                'move' => session("arena.selected.{$enemy->id}") ?? $sm
            ]
        ];

        $sorted = array_reverse(array_sort($battle, function($value) {
            return $value['speed'];
        }));

        foreach ($sorted as $key => $value) {
            $defKey = ($key == 'pokemon') ? 'enemy' : 'pokemon';
            $this->useMove(session("arena.{$key}"), session("arena.{$defKey}"), $value['move'], $key);

            if ($this->check()) break;
        }

        $array["turns"] = session("arena.turns") + 1;
        $this->saveSession('arena', $array);
    }
}

