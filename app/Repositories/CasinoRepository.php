<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 2:53 PM
 */

namespace App\Repositories;

use App\Contracts\CasinoInterface;
use App\User;

class CasinoRepository extends Repository implements CasinoInterface
{
    protected $mail;

    public function __construct(User $mail)
    {
        parent::__construct($mail);
        $this->mail = $mail;
    }
}