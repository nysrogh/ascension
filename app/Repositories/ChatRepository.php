<?php
namespace App\Repositories;

use App\Contracts\ChatInterface;
use App\Chat;
use Carbon\Carbon;

class ChatRepository extends Repository implements ChatInterface
{
    protected $chat;

    public function __construct(Chat $chat)
    {
        parent::__construct($chat);

        $this->chat = $chat;
    }

    public function retrieve($club = 0)
    {
        return $this->chat->with(['users' => function ($q) {
            $q->select(['id', 'username', 'avatar', 'status', 'setting_bgcolor']);
        }])->where('club_id', $club)->select(['id', 'user_id', 'message', 'created_at'])->latest()->simplePaginate(15);
    }
}