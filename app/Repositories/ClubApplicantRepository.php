<?php
namespace App\Repositories;

use App\Contracts\ClubApplicantInterface;
use App\ClubApplicant;

class ClubApplicantRepository extends Repository implements ClubApplicantInterface
{
    protected $applicant;

    public function __construct(ClubApplicant $applicant)
    {
        parent::__construct($applicant);

        $this->applicant = $applicant;
    }

    public function listing($id)
    {
        return $this->applicant->with(['user' => function ($q) {
            $q->select(['id', 'username', 'avatar', 'gender']);
        }])->where('club_id', $id)->latest()->simplePaginate(10);
    }

    public function check($club)
    {
        return $this->applicant->where('club_id', $club)->where('user_id', auth()->user()->id)->count();
    }

    public function apply($club)
    {
        return $this->applicant->create([
            'club_id' => $club,
            'user_id' => auth()->user()->id
        ]);
    }

    public function remove($user)
    {
        return $this->applicant->where('user_id', $user)->where('club_id', auth()->user()->club_id)->delete();
    }
}