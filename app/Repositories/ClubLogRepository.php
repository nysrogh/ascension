<?php
namespace App\Repositories;

use App\Contracts\ClubLogInterface;
use App\ClubLog;

class ClubLogRepository extends Repository implements ClubLogInterface
{
    protected $log;

    public function __construct(ClubLog $log)
    {
        parent::__construct($log);

        $this->log = $log;
    }

    public function retrieve()
    {
        return $this->log->where('club_id', auth()->user()->club_id)->latest()->paginate(20);
    }

    public function add($message)
    {
        return $this->log->create([
            'club_id' => auth()->user()->club_id,
            'message' => $message
        ]);
    }
}