<?php
namespace App\Repositories;

use App\Contracts\ClubInterface;
use App\Club;

class ClubRepository extends Repository implements ClubInterface
{
    protected $club;

    public function __construct(Club $club)
    {
        parent::__construct($club);

        $this->club = $club;
    }

    public function home($id)
    {
        return $this->club->with(['chats' => function ($q) {
            $q->with(['users' => function ($q) {
                $q->select(['id', 'username', 'avatar']);
            }])->select(['id', 'user_id', 'message', 'club_id', 'created_at'])->limit(15)->latest();
        }, 'applicants' => function ($q) {
            $q->select(['id']);
        }])->findOrFail($id);
    }

    public function show($id)
    {
        return $this->club->with(['privileges' => function ($q) {
            $q->select(['title', 'modifier']);
        }])->findOrFail($id);
    }

    public function randomize()
    {
        return $this->club->inRandomOrder()->limit(10)->get();
    }

    public function nextLevel($level)
    {
        return ceil(pow($level, 2.95) * 250.132);
    }

    public function rankings()
    {
        return $this->club->orderByDesc('point')->simplePaginate(15);
    }

    public function manageMembers($id)
    {
        return $this->club->select(['max_member'])->findOrFail($id);
    }
}