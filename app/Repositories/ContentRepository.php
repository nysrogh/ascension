<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\NotifInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\ContentInterface;
use App\Contracts\UserShopInterface;
use App\Content;

class ContentRepository extends Repository implements ContentInterface
{
    protected $userShopContent;
    protected $userShop;
    protected $userItem;
    protected $poke;
    protected $notif;

    public function __construct(Content $userShopContent, UserShopInterface $userShop, UserItemInterface $userItem, PokemonInterface $poke, NotifInterface $notif)
    {
        parent::__construct($userShopContent);

        $this->userShopContent = $userShopContent;
        $this->userShop = $userShop;
        $this->userItem = $userItem;
        $this->poke = $poke;
        $this->notif = $notif;
    }

    public function items($shop)
    {
        return $this->userShopContent->with(['info' => function ($q) {
            $q->select(['id', 'name', 'description', 'primary_value']);
        }])->where('user_shop_id', $shop)->where('item_id', '!=', 0)
            ->select(['id', 'item_id', 'price', 'primary_value', 'upgrade', 'available'])->orderBy('price', 'desc')->simplePaginate(5);
    }

    public function pokemon($shop)
    {
        return $this->userShopContent->with(['pokemon' => function ($q) {
            $q->with(['info' => function ($q) {
                $q->select(['id', 'name', 'primary_type', 'secondary_type']);
            }])->select(['id', 'pokemon_id', 'level', 'ratings']);
        }, 'trades'])->where('user_shop_id', $shop)->where('user_pokemon_id', '!=', 0)
            ->select(['id', 'user_pokemon_id', 'price', 'available'])->orderBy('price', 'desc')->simplePaginate(5);
    }

    public function searchStore($keyword)
    {
        return $this->userShopContent->whereHas('info', function($q) use ($keyword) {
            $q->where('name', 'LIKE', "%{$keyword}%");
        })->orWhereHas('pokemon.info', function($q) use ($keyword) {
            $q->where('name', 'LIKE', "%{$keyword}%");
        })->limit(15)->inRandomOrder()->get();
    }

    public function findItem($id)
    {
        return $this->userShopContent->where('user_shop_id', auth()->user()->shop->id)->where('item_id', $id)->first();
    }

    public function findPokemon($id)
    {
        return $this->userShopContent->where('user_shop_id', auth()->user()->shop->id)->where('user_pokemon_id', $id)->first();
    }

    public function acceptTrade($content, $user)
    {
        if ($content->pokemon) {
            $content->pokemon->in_trade = 0;
            $content->pokemon->in_shop = 0;
            $content->pokemon->user_id = $user;
            $content->pokemon->save();

            $this->poke->checkPokedex($content->pokemon->info->id, $user);
        } else {
            $this->userItem->addOrUpdateItem((object)['id' => $content->info->id], 1, [$content->primary_value, $content->upgrade], $user);
        }

        $content->delete();

//        $pokemon = $this->userShopContent->with(['trades' => function ($q) use ($offer) {
//            $q->where('id', $offer);
//        }])->where('id', $trade)->whereHas('trades', function ($q) use ($offer) {
//            $q->where('id', $offer);
//        })->firstOrFail();

//        $this->notif->add($userId, auth()->user()->username ." has accepted your trade offer on ". $pokemon->pokemon->info->name .".");
//
//        //update trade to offer
//        $pokemon->pokemon->update(['user_id' => $userId]);
//        $pokemon->pokemon->decrement('in_shop');
//
//        //update offer to trade
//        $pokemon->trades[0]->update(['user_id' => auth()->user()->id]);
//
//        //Remove trades
//        foreach ($this->userShopContent->find($trade)->trades as $poke) {
//            $poke->decrement('in_trade');
//        }
//
//        $this->poke->checkPokedex($pokemon->pokemon->info->id, $userId);
//        $this->poke->checkPokedex($pokemon->trades[0]->info->id);
//
//        $pokemon->delete();

        return true;
    }

    public function buy($item)
    {
        if ($item->item_id) {
            $this->userItem->addOrUpdateItem((object)['id' => $item->item_id], 1, [$item->primary_value, $item->upgrade]);

            //decrement sale quantity
            $item->decrement('available');

            if ($item->available <= 0) {
                $item->delete();
            }

            $this->notif->add($item->shop->user_id, "Someone has bought your {$item->info->name}.");
        } else {
            $item->pokemon->user_id = auth()->user()->id;
            $item->pokemon->in_shop = 0;
            $item->pokemon->in_trade = 0;
            $item->pokemon->save();

            $this->notif->add($item->shop->user_id, "Someone has bought your {$item->pokemon->info->name}.");

            $this->poke->checkPokedex($item->pokemon->info->id);

            $item->delete();
        }

        //update user coins
        auth()->user()->decrement('gold', $item->price);
        $item->shop->user->increment('gold', $item->price);

        return false;
    }

    public function addOrUpdate($data, $pokemon)
    {
        if ($data['category'] == 'item') {
            $content = $this->findItem($data['id']);

            if (!$content) {
                $this->userShopContent->create([
                    'user_shop_id' => $this->userShop->findShopByUser(auth()->user()->id)->id,
                    'item_id' => $pokemon->info->id,
                    'price' => round($data['price']),
                    'available' => round($data['quantity']),
                    'primary_value' => $pokemon->primary_value,
                    'upgrade' => $pokemon->upgrade
                ]);

            } else {
                $content->update([
                    'price' => round($data['price']),
                    'available' => round($data['quantity'])
                ]);
            }

            $pokemon->quantity -= $data['quantity'];

            if ($pokemon->quantity <= 0) {
                $pokemon->delete();
            } else {
                $pokemon->save();
            }

        } else {
            $this->userShopContent->create([
                'user_shop_id' => $this->userShop->findShopByUser(auth()->user()->id)->id,
                'user_pokemon_id' => $data['id'],
                'price' => round($data['price']),
                'available' => 1
            ]);

            $pokemon->increment('in_shop');
        }
    }

    public function removeItem($item)
    {
        if ($item->item_id) {
            $this->userItem->addOrUpdateItem((object)['id' => $item->item_id], $item->available, [$item->primary_value, $item->upgrade]);
        } else {
            $item->pokemon->decrement('in_shop');
        }

        $item->delete();

        return false;
    }

    public function hasTrade($trade)
    {
        return $this->userShopContent->where('id', $trade)->whereHas('trades', function ($q) {
            $q->where('user_id', auth()->user()->id);
        })->count();
    }
}
