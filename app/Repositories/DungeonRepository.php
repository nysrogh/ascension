<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\DungeonInterface;
use App\Contracts\ItemInterface;
use App\Contracts\UserItemInterface;
use App\Dungeon;
use Carbon\Carbon;

class DungeonRepository extends Repository implements DungeonInterface
{
    protected $dungeon;
    protected $item;
    protected $userItem;

    public function __construct(Dungeon $dungeon, ItemInterface $item, UserItemInterface $userItem)
    {
        parent::__construct($dungeon);

        $this->dungeon = $dungeon;
        $this->item = $item;
        $this->userItem = $userItem;
    }

    public function floorDungeons()
    {
        return $this->dungeon->select(['id', 'background', 'name', 'types', 'level', 'description'])->get();
    }

    public function randomAll()
    {
        return $this->dungeon->inRandomOrder()->get();
    }

    public function updateDistance($id, $distance)
    {
        return $this->dungeon->where('id', $id)->update(['distance' => $distance]);
    }

    public function buyItem()
    {
        $this->userItem->addOrUpdateItem(session('dungeon.event')->item, 1);

        auth()->user()->decrement('gold', session('dungeon.event')->price);

        return false;
    }

    public function events()
    {
        $array = [];
        $wild = false;

        if (random_int(1,100) <= 70) {
            $array['event'] = (object)['name' => 'wild'];

            $wild = true;
        } elseif (random_int(1,100) <= 10 && session()->has('battle')) {
            switch (random_int(1,2)) {
                case 1: $array['event'] = (object)[
                            'name' => 'npc',
                            'npc' => 'Nurse Joy',
                            'message' => "Hello there trainer! It seems that some of your Pokemon is hurt. Let me heal them."
                        ];
                        session()->forget('battle');
                break;
                case 2:
                    $item = $this->item->randomChestItem(4, 0);
                    $price = random_int(5000,35000);

                    $array['event'] = (object)[
                            'name' => 'npc',
                            'npc' => 'Ruin Maniac Fred',
                            'item' => $item,
                            'price' => $price,
                            'message' => "Howdy! I've just found a very rare {$item->name} from a nearby ruins. I can sell it to you for a very cheap price! What you say?"
                        ];
                break;
            }
        } else {
            $array['event'] = (object)['name' => 'nothing', 'message' => $this->tips('dungeon')];
        }

        $this->saveSession('dungeon', $array);

        return $wild;
    }

    public function chestEvent($id)
    {
        $array = [];

        foreach (range(1,3) as $chest) {
            //random pokeball
            if (random_int(1,1000) <= 300) {
                $item = $this->item->randomChestItem(1, 0, ['name', ['Master Ball']]);

                $qty = random_int(3, 5);

                $result = $item->name . " x" . $qty;

                if ($chest == $id) {
                    $this->userItem->addOrUpdateItem($item, $qty);
                }

            //random item
            } elseif (random_int(1,1000) <= 50) {
                $item = $this->item->randomChestItem(2, 0, ['name', ['Restat Candy']]);
                $qty = random_int(1,2);

                $result = $item->name . " x" . $qty;

                if ($chest == $id) {
                    $this->userItem->addOrUpdateItem($item, $qty);
                }

            //random berry
            } elseif (random_int(1,1000) <= 300) {
                $item = $this->item->randomChestItem(3, 0);
                $qty = 5;

                $result = $item->name . " x" . $qty;

                if ($chest == $id) {
                    $this->userItem->addOrUpdateItem($item, $qty);
                }

            //random special item
            } elseif (random_int(1,1000) <= 100) {
                $item = $this->item->randomChestItem(4, 0);
                $qty = 1;

                $result = $item->name . " x" . $qty;

                if ($chest == $id) {
                    $this->userItem->addOrUpdateItem($item, $qty);
                }

            //random crystal
            } elseif (random_int(1,1000) <= 250) {
                $clubCrystal = 0;

                //club privileges
                if (auth()->user()->club_id) {
                    foreach (auth()->user()->club->privileges as $privilege) {
                        if ($privilege->pivot->expired_on > Carbon::now() && $privilege->modifier == 'crystal') {
                            $clubCrystal += $privilege->value;
                        }
                    }
                }

                if (session('dungeon.difficulty') == 55) {
                    $item = random_int(150, 200);
                } elseif (session('dungeon.difficulty') == 85) {
                    $item = random_int(250, 300);
                } else {
                    $item = random_int(50, 100);
                }

                $item += ($clubCrystal * 3);
                $result = $item ." crystals";

                if ($chest == $id) {
                    auth()->user()->increment('crystal', $item);
                }

            //random held item
            } elseif (random_int(1,1000) <= 20) {
                $item = $this->item->randomChestItem(4, 1);
                $qty = 1;

                $result = $item->name . " x" . $qty;

                if ($chest == $id) {
                    $this->userItem->addOrUpdateItem($item, $qty);
                }

            //random coins
            } else {
                $clubCoin = 0;

                //club privileges
                if (auth()->user()->club_id) {
                    foreach (auth()->user()->club->privileges as $privilege) {
                        if ($privilege->pivot->expired_on > Carbon::now() && $privilege->modifier == 'chest') {
                            $clubCoin += $privilege->value;
                        }
                    }
                }

                $clubCoin = ($clubCoin) ? ($clubCoin / 100) + 1 : 1;

                $item = round(random_int((session('dungeon.info')->distance * session('dungeon.difficulty')) * .85, (session('dungeon.info')->distance * session('dungeon.difficulty'))) * 2.3);

                $item = round($item * $clubCoin);
                $result = $item ." coins";

                if ($chest == $id) {
                    auth()->user()->increment('gold', $item);
                }
            }

            $array["chest_{$chest}"] = $result;
        }

        $array["selected"] = $id;
        $array["progress"] = "end";

        $this->saveSession('dungeon', $array);

        return false;
    }
}