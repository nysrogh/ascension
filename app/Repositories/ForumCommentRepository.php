<?php

namespace App\Repositories;

use App\Contracts\ForumCommentInterface;
use App\ForumComment;

class ForumCommentRepository extends Repository implements ForumCommentInterface
{
    protected $comment;

    public function __construct(ForumComment $comment)
    {
        parent::__construct($comment);
        $this->comment = $comment;
    }

    public function findByForum($id, $order = 'asc')
    {
        return $this->comment->with(['user' => function ($q) {
            $q->select(['id', 'avatar', 'username', 'status', 'setting_bgcolor', 'created_at']);
        }])->where('forum_id', $id)->select(['id', 'user_id', 'body', 'created_at'])->orderBy('created_at', $order)->simplePaginate(15);
    }
}