<?php

namespace App\Repositories;

use App\Contracts\ForumInterface;
use App\Forum;

class ForumRepository extends Repository implements ForumInterface
{
    protected $forum;

    public function __construct(Forum $forum)
    {
        parent::__construct($forum);
        $this->forum = $forum;
    }

    public function latestPosts()
    {
        return $this->forum->with(['user' => function ($q) {
            $q->select(['id', 'avatar', 'username', 'status', 'setting_bgcolor']);
        }, 'comments'])->latest('updated_at')->select(['id', 'category', 'user_id', 'title', 'body', 'slug', 'updated_at'])->limit(15)->get();
    }

    public function latestUpdates()
    {
        return $this->forum->where('category', 4)->where('user_id', 1)->latest()->limit(5)->select(['slug', 'title', 'created_at'])->get();
    }

    public function findBySlug($slug)
    {
        return $this->forum->with(['user' => function ($q) {
            $q->select(['id', 'avatar', 'username', 'status', 'setting_bgcolor']);
        }])->where('slug', $slug)->firstOrFail(['id', 'user_id', 'slug', 'title', 'body', 'created_at']);
    }

    public function findByCategory($category)
    {
        return $this->forum->with(['user' => function ($q) {
            $q->select(['id', 'avatar', 'username', 'status', 'setting_bgcolor']);
        }, 'comments'])->where('category', $category)->latest('updated_at')
            ->select(['id', 'user_id', 'title', 'body', 'slug', 'updated_at'])->simplePaginate(10);
    }
}