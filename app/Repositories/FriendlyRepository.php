<?php
namespace App\Repositories;

use App\Contracts\FriendlyInterface;
use App\Contracts\BattleInterface;
use App\Contracts\FriendlyStatusEffectInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use Intervention\Image\ImageManagerStatic as Image;
use Carbon\Carbon;

class FriendlyRepository extends Repository implements FriendlyInterface
{
    protected $userPokemon;
    protected $battle;
    protected $statusEffect;
    protected $pokemon;

    public function __construct(
        UserPokemonInterface $userPokemon,
        BattleInterface $battle,
        FriendlyStatusEffectInterface $statusEffect,
        PokemonInterface $pokemon,
        UserItemInterface $userItem
    )
    {
        $this->pokemon = $pokemon;
        $this->userPokemon = $userPokemon;
        $this->battle = $battle;
        $this->statusEffect = $statusEffect;
        $this->userItem = $userItem;
    }

    public function background($result = false)
    {
        //set background based on current time
        $now = Carbon::now();
        $from = Carbon::parse('2017-01-01 06:00:00');
        $to = Carbon::parse('2017-01-01 18:00:00');

        if ($now->hour > $from->hour && $now->hour < $to->hour) {
            $background = Image::make(public_path("images/battle/friendly-bg.png"));
        } else {
            $background = Image::make(public_path("images/battle/friendly-bgn.png"));
        }

        if (!$result || !is_array($result)) {
            if ($result == 'defeat' || $result == 'victory') {
                $background->insert(public_path("images/trainers/" . session('friendly.opponent')->avatar . ".png"), '', 110, -5);
            } else {
                $background->insert(public_path("images/pokemon/front/" . str_slug(session('friendly.enemy.stats')->info->name) . ".png"), '',
                    session('friendly.enemy.stats')->info->front_x, session('friendly.enemy.stats')->info->front_y);
            }
        }

        if (!$result || is_array($result)) {
            $background->insert(public_path("images/pokemon/back/" . str_slug(session('friendly.pokemon.stats.info')->name) . ".png"), '',
                session('friendly.pokemon.stats.info')->back_x, session('friendly.pokemon.stats.info')->back_y);
        }

        return 'data:image/png;base64,' . base64_encode($background->encode('jpg', 90));
    }

    public function initialize($user)
    {
        $array = [];

        $array['enemy_team'] = $this->userPokemon->arenaTeam($user->id);
        $array['enemy'] = [
            'stats' => (object)$array['enemy_team'][0],
            "enemy_current_hp" => $array['enemy_team'][0]->hp,
            "not_effective" => [],
            "super_effective" => []
        ];

        $array['pokemon_team'] = $this->userPokemon->arenaTeam();
        $array["pokemon"] = [
            'stats' => $array['pokemon_team'][0],
            "pokemon_current_hp" => $array['pokemon_team'][0]->hp
        ];

        $array["turns"] = 1;
        $array['opponent'] = $user;

        $this->saveSession('friendly', $array);

        session(['background' => $this->background()]);

        return false;
    }

    public function switchPokemon($pokemon, $type)
    {
        $oldName = session("friendly.{$type}.stats")->info->name;
        $who = $type == 'pokemon' ? auth()->user()->username : session("friendly.opponent")->username;

        session()->push('friendly_log', "<strong>{$oldName} fainted!</strong>");
        session()->push('friendly_log', "<strong>{$who} sent out {$pokemon->info->name}!</strong>");

        $array["{$type}"] = [
            'stats' => $pokemon,
            "{$type}_current_hp" => $pokemon->hp,
            'tmp' => [],
            'not_effective' => [],
            'super_effective' => []
        ];

        $this->saveSession('friendly', $array);

        session(['background' => $this->background()]);

        session()->save();
    }

    public function check()
    {
        if (session("friendly.pokemon.pokemon_current_hp") <= 0 && !session()->has('result')) {
            if ($new = $this->checkTeam('pokemon')) {
                $this->switchPokemon($new, 'pokemon');
            } else {
                $this->defeat();
            }

            return true;
        }

        if (session("friendly.enemy.enemy_current_hp") <= 0 && !session()->has('result')) {
            if ($new = $this->checkTeam('enemy')) {
                $this->switchPokemon($new, 'enemy');
            } else {
                $this->victory();
            }

            return true;
        }

        return false;
    }

    public function defeat()
    {
        session()->forget('friendly_log');

        session()->push('friendly_log', "<strong>You lost!</strong>");
        session()->push('friendly_log', "&#8220;". strip_tags(stripslashes(session("friendly.opponent")->friendly_message)) ."&#8221;");

        session(['result' => 'defeat']);
        session(['background' => $this->background('defeat')]);

        session()->save();
    }

    public function victory()
    {
        session()->forget('friendly_log');

        session()->push('friendly_log', "<strong>You won!</strong>");

        session(['result' => 'victory']);
        session(['background' => $this->background('victory')]);

        session()->save();
    }

    public function checkTeam($team)
    {
        session()->push("friendly.{$team}_dead", session("friendly.{$team}.stats")->id);

        foreach (session("friendly.{$team}_team") as $pokemon) {
            if (!in_array($pokemon->id, session("friendly.{$team}_dead"))) {
                return $pokemon;
            }
        }

        return false;
    }

    public function checkMove($id)
    {
        foreach (session('friendly.pokemon.stats')->moves as $list)
        {
            if ($list->id == $id) return $list;
        }

        return false;
    }

    public function fight($move)
    {
        $pokemon = session('friendly.pokemon.stats');
        $enemy = session('friendly.enemy.stats');

        //held item [turn] - pokemon
        $quickp = 0;
        if (session('friendly.pokemon.stats')->item) {
            if ($pokemon->item->info->secondary_attr == 'turn') {
                switch ($pokemon->item->info->primary_attr) {
                    case 'heal': $this->userItem->itemEffectTurnHeal($pokemon, 'pokemon', 'friendly'); break;
                    case 'selected': $this->userItem->itemEffectTurnSelected($pokemon, $move, 'pokemon', 'friendly'); break;
                    case 'first': $quickp = $this->userItem->itemEffectTurnFirst($pokemon, 'pokemon', 'friendly'); break;
                    case 'exp': $this->userItem->itemEffectTurnAddBonus($pokemon, 'exp', 'enemy', 'friendly'); break;
                    case 'coins': $this->userItem->itemEffectTurnAddBonus($pokemon, 'coins', 'enemy', 'friendly'); break;
                }
            }
        }

        //held item [turn] - enemy
        $quicke = 0;
        $sm = $this->smartMove();
        if (session('friendly.enemy.stats')->item) {
            if ($enemy->item->info->secondary_attr == 'turn') {
                switch ($enemy->item->info->primary_attr) {
                    case 'heal': $this->userItem->itemEffectTurnHeal($enemy, 'enemy', 'friendly'); break;
                    case 'selected': $this->userItem->itemEffectTurnSelected($enemy, $sm, 'enemy', 'friendly'); break;
                    case 'first': $quicke = $this->userItem->itemEffectTurnFirst($enemy, 'enemy', 'friendly'); break;
                    case 'exp': $this->userItem->itemEffectTurnAddBonus($enemy, 'exp', 'pokemon', 'friendly'); break;
                    case 'coins': $this->userItem->itemEffectTurnAddBonus($enemy, 'coins', 'pokemon', 'friendly'); break;
                }
            }
        }

        $battle = [
            "pokemon" => [
                'speed' => $pokemon->speed + session("friendly.pokemon.tmp.speed") + $quickp,
                'move' => session("friendly.selected.{$pokemon->id}") ?? $move
            ],
            "enemy" => [
                'speed' => $enemy->speed + session("friendly.enemy.tmp.speed") + $quicke,
                'move' => session("friendly.selected.{$enemy->id}") ?? $sm
            ]
        ];

        $sorted = array_reverse(array_sort($battle, function($value) {
            return $value['speed'];
        }));

        foreach ($sorted as $key => $value) {
            $defKey = ($key == 'pokemon') ? 'enemy' : 'pokemon';
            $this->useMove(session("friendly.{$key}"), session("friendly.{$defKey}"), $value['move'], $key);

            if ($this->check()) break;
        }

        $array["turns"] = session("friendly.turns") + 1;
        $this->saveSession('friendly', $array);
    }

    public function critical($speed)
    {
        if (random_int(1,10000) <= $speed) {
            session()->push('friendly_log', "<span class='text-danger'>A critical hit!</span>");

            return 2;
        }

        return 1;
    }

    public function useMove($attacker, $defender, $move, $key)
    {
        $array = [];
        $defKey = ($key == 'pokemon') ? 'enemy' : 'pokemon';

        if (!$this->statusEffect->check($key, $attacker['stats']->id)) return false;

        session()->push('friendly_log', $attacker['stats']->info->name ." used ". $move->name ."!");

        //damaging move
        if (in_array($move->primary_attr, ['attack', 'special_attack'])) {

            //held items [accuracy]
            $accuracy = 0;
            if ($attacker['stats']->item) {
                if ($attacker['stats']->item->info->secondary_attr == 'accuracy') {
                    switch ($attacker['stats']->item->info->primary_attr) {
                        case 'critical': $accuracy = $this->userItem->itemEffectMoveAccuracy($attacker['stats']); break;
                        case 'evasion': $accuracy = $this->userItem->itemEffectMoveAccuracy($attacker['stats']); break;
                    }
                }
            }

            //evasion else attack
            if (random_int(1,10000) <= $defender['stats']->speed + session("friendly.enemy.tmp.speed") + $accuracy) {
                session()->push('friendly_log', "<span class='text-danger'>". $defender['stats']->info->name ." avoided the attack!</span>");
            } else {
                $attack = $attacker['stats']->{$move->primary_attr} + session("friendly.{$key}.tmp.{$move->primary_attr}");

                $defense = $move->primary_attr == 'attack'
                    ? $defender['stats']->defense + session("friendly.{$defKey}.tmp.defense")
                    : $defender['stats']->special_defense + session("friendly.{$defKey}.tmp.special_defense");

                $isEnemy = $attacker['stats']->id != session('friendly.pokemon.stats')->id ? true : false;

                $type = $this->typeModifiers($isEnemy, $move,
                    array_filter([$defender['stats']->info->primary_type, $defender['stats']->info->secondary_type], function ($var) {
                        return !is_null($var);
                    }));

                //damage calculation formula
                $damage = (((((($attacker['stats']->level * 2) / 5) + 2) * $move->primary_value
                                * $attack/$defense) / 50) + 2)
                    //modifiers = targets * weather * badge * critical * random * stab * type * burn * other
                    * (1 * 1 * 1 * $this->critical($attacker['stats']->speed + session("friendly.pokemon.tmp.speed") + $accuracy) * (random_int(85,100)/100) * $this->battle->stab($move->type, array_filter([$attacker['stats']->info->primary_type, $attacker['stats']->info->secondary_type], function ($var) {
                            return !is_null($var);
                        })) * $type * 1 * 1);

                //held items [move]
                $add = 0; $protect = false;
                if ($attacker['stats']->item) {
                    if ($attacker['stats']->item->info->secondary_attr == 'move') {
                        switch ($attacker['stats']->item->info->primary_attr) {
                            case 'super': $damage = $this->userItem->itemEffectMoveSuper($attacker['stats'], $type, $damage); break;
                            case 'heal': $this->userItem->itemEffectMoveHeal($attacker['stats'], $damage, 'friendly', $key); break;
                            case 'add-heal': $add = $this->userItem->itemEffectMoveAddHeal($attacker['stats'], $move, 'friendly', $key); break;
                            case 'recoil': $damage = $this->userItem->itemEffectMoveRecoil($attacker['stats'], $damage, 'friendly', $key); break;
                            case 'attack': $damage = $this->userItem->itemEffectMoveAttack('attack', $attacker['stats'], $damage, $move); break;
                            case 'special_attack': $damage = $this->userItem->itemEffectMoveAttack('special_attack', $attacker['stats'], $damage, $move); break;
                            case 'flinch': $this->userItem->itemEffectMoveFlinch($attacker['stats'], $defender['stats']->id, $defKey, 'friendly'); break;
                            case 'protect-recoil': $protect = $this->userItem->itemEffectMoveProtectRecoil($attacker['stats'], $move, $key, 'arena'); break;
                            default:
                                if (in_array($attacker['stats']->item->info->primary_attr, ['poison', 'fighting', 'ice', 'dragon', 'ghost', 'flying', 'grass', 'normal', 'ground', 'fire', 'fairy', 'electric', 'steel', 'dark', 'rock', 'psychic', 'water', 'bug'])) {
                                    $damage = $this->userItem->itemEffectMoveType($attacker['stats'], $move, $damage);
                                }
                        }
                    }
                }

                if ($move->secondary_attr && !$protect) {
                    $obj = (object) [
                        'attacker' => $attacker['stats'],
                        'defender' => $defender['stats'],
                        'attribute' => $move->secondary_attr,
                        'value' => $move->secondary_value + $add,
                        'key' => $key,
                        'defKey' => $defKey,
                        'chance' => 10
                    ];

                    $this->statusEffect->secondaryEffects($obj);
                }

                session()->push('friendly_log', $defender['stats']->info->name ." took ". round(max($damage, 0)) ." damage!");

                $array[$defKey]["{$defKey}_current_hp"] = max(session("friendly.{$defKey}.{$defKey}_current_hp") - round(max($damage, 0)), 0);

                //held items [defender]
                if ($defender['stats']->item) {
                    if ($defender['stats']->item->info->secondary_attr == 'counter') {
                        switch ($defender['stats']->item->info->primary_attr) {
                            case 'return': $this->userItem->itemEffectMoveReturn($defender['stats'], $attacker['stats']->id, $damage, $key, 'friendly'); break;
                        }
                    }
                }
            }

        } else {
            $chance = (!in_array($move->primary_attr, ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) ? 60 : 20;
            $obj = (object) [
                'attacker' => $attacker['stats'],
                'defender' => $defender['stats'],
                'attribute' => $move->primary_attr,
                'value' => $move->primary_value,
                'key' => $key,
                'defKey' => $defKey,
                'chance' => $move->primary_value + $chance
            ];

            $this->statusEffect->secondaryEffects($obj);

            if ($move->secondary_attr) {
                $chance = (!in_array($move->secondary_attr, ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) ? 70 : 20;

                $obj = (object) [
                    'attacker' => $attacker['stats'],
                    'defender' => $defender['stats'],
                    'attribute' => $move->secondary_attr,
                    'value' => $move->secondary_value,
                    'key' => $key,
                    'defKey' => $defKey,
                    'chance' => $move->secondary_value + $chance
                ];

                $this->statusEffect->secondaryEffects($obj);
            }
        }

        $this->saveSession('friendly', $array);

        //held items [defender]
        if ($defender['stats']->item) {
            if ($defender['stats']->item->info->secondary_attr == 'counter') {
                switch ($defender['stats']->item->info->primary_attr) {
                    case 'ko': $this->userItem->itemEffectCounterKo($defender['stats'], $defKey, 'friendly'); break;
                }
            }
        }

        return false;
    }

    public function typeModifiers($isEnemy, $move, $defender)
    {
        $modifier = 1;

        foreach ($defender as $type) {
            if ($this->battle->checkStrength($move->type, $type)) {
                if ($isEnemy && !in_array($move->name, session("friendly.enemy.super_effective"))) {
                    session()->push("friendly.enemy.super_effective", $move->name);
                }

                $modifier *= 2;
            } elseif ($this->battle->checkWeakness($move->type, $type)) {
                if ($isEnemy && !in_array($move->name, session("friendly.enemy.not_effective"))) {
                    session()->push("friendly.enemy.not_effective", $move->name);
                }

                $modifier /= 2;
            } elseif ($this->battle->checkNull($move->type, $type)) {
                if ($isEnemy && !in_array($move->name, session("friendly.enemy.not_effective"))) {
                    session()->push("friendly.enemy.not_effective", $move->name);
                }

                $modifier = 0;
            }
        }

        if ($modifier >= 2) {
            session()->push('friendly_log', "It's super effective!");
        } elseif ($modifier < 1 && $modifier != 0) {
            session()->push('friendly_log', "It's not very effective!");
        } elseif ($modifier == 0) {
            session()->push('friendly_log', "It doesn't affect the opponent!");
        }

        return $modifier;
    }

    public function smartMove()
    {
        $mvs = [];

        //convert object moves to array;
        foreach (session('friendly.enemy.stats')->moves as $m) {
            $mvs[] = $m;
        }

        shuffle($mvs);

        $move = array_first($mvs);

        //$move = $this->_randomMove($mvs);
        $ailments = session("friendly.pokemon.tmp") ?? [];

        //check for super-effective move
        if (count(session("friendly.enemy.super_effective"))
            && in_array($move->name, session("friendly.enemy.super_effective"))) {
            return $move;
        }

        //check already inflicted status
        if (in_array($move->primary_attr, $ailments)) {
            shuffle($mvs);

            return array_first($mvs);
        }

        //check for not very effective move
        if (count(session("friendly.enemy.not_effective"))
            && in_array($move->name, session("friendly.enemy.not_effective"))) {
            shuffle($mvs);

            return array_first($mvs);
        }

        return $move;
    }
}