<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\FriendlyStatusEffectInterface;
use App\UserPokemon;
use Carbon\Carbon;

class FriendlyStatusEffectRepository extends Repository implements FriendlyStatusEffectInterface
{
    public function __construct(UserPokemon $userPokemon)
    {
        parent::__construct($userPokemon);

        $this->userPokemon = $userPokemon;
    }

    public function check($key)
    {
        if (!session()->has("friendly.{$key}.tmp")) return true;

        foreach (session("friendly.{$key}.tmp") as $status => $duration) {
            if (in_array($status, ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) {
                if (!$this->{$status}($key, $duration)) return false;
            }
        }

        return true;
    }

    public function burn($target, $duration)
    {
        //check duration
        if ($duration <= 0) {
            session()->forget("friendly.{$target}.tmp.burn");

            return true;
        }

        //calculate damage
        $damage = round(session("friendly.{$target}.stats")->hp * .05);

        //deduct hp
        $array[$target]["{$target}_current_hp"] = max(session("friendly.{$target}.{$target}_current_hp") - $damage, 0);

        //deduct duration
        $array["{$target}.tmp"]['burn'] = session("friendly.{$target}.tmp.burn") - 1;

        //push battle log
        session()->push('friendly_log', session("friendly.{$target}.stats")->info->name ." took ". $damage ." damage from burn!");

        //save session
        $this->saveSession('friendly', $array);

        return true;
    }

    public function poison($target)
    {
        $damage = round(session("friendly.{$target}.stats")->hp * .02);

        $array[$target]["{$target}_current_hp"] = max(session("friendly.{$target}.{$target}_current_hp") - $damage, 0);

        session()->push('friendly_log', session("friendly.{$target}.stats")->info->name ." took ". $damage ." damage from poison!");

        $this->saveSession('friendly', $array);

        return true;
    }

    /**
     * Inflict confuse
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function confuse($target, $duration)
    {
        //check duration
        if ($duration <= 0) {
            session()->forget("friendly.{$target}.tmp.confuse");
            session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." has recovered from confusion!");

            return true;
        }

        if (random_int(1, 100) > 40) {
            $array["{$target}.tmp"]['confuse'] = session("friendly.{$target}.tmp.confusion") - 1;
            $this->saveSession('friendly', $array);

            return true;
        }

        $damage = round(session("friendly.{$target}.stats")->attack * .10);

        $array[$target]["{$target}_current_hp"] = max(session("friendly.{$target}.{$target}_current_hp") - $damage, 0);

        $array["{$target}.tmp"]['confuse'] = session("friendly.{$target}.tmp.confusion") - 1;

        session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." hurt itself in confusion and took ". $damage ." damage!");

        $this->saveSession('friendly', $array);

        return false;
    }

    /**
     * Inflict freeze
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function freeze($target, $duration)
    {
        if ($duration <= 0 || session("friendly.{$target}.tmp.burn")) {
            session()->forget("friendly.{$target}.tmp.freeze");

            session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." has thawed!");

            return true;
        }

        session()->push('friendly_log', "". session("friendly.{$target}.stats")->info->name ." is frozen solid!");

        $array["{$target}.tmp"]['freeze'] = session("friendly.{$target}.tmp.freeze") - 1;

        $this->saveSession('friendly', $array);

        return false;
    }

    /**
     * Inflict sleep
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function sleep($target, $duration)
    {
        if ($duration <= 0) {
            session()->forget("friendly.{$target}.tmp.sleep");
            session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." woke up!");

            return true;
        }

        session()->push('friendly_log', "". session("friendly.{$target}.stats")->info->name ." is fast asleep!");

        $array["{$target}.tmp"]['sleep'] = session("friendly.{$target}.tmp.sleep") - 1;

        $this->saveSession('friendly', $array);

        return false;
    }

    /**
     * Inflict paralyze
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function paralyze($target, $duration)
    {
        if ($duration <= 0) {
            $array["{$target}.tmp"]['speed'] = round(session("friendly.{$target}.tmp.speed") + (session("friendly.{$target}.stats")->speed / 2));

            session()->forget("friendly.{$target}.tmp.paralyze");
            session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." has recovered from paralysis!");

            return true;
        }

        if (random_int(1,100) > 25) {
            $array["{$target}.tmp"]['paralyze'] = session("friendly.{$target}.tmp.paralyze") - 1;
            $this->saveSession('friendly', $array);

            return true;
        }

        session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." is paralyzed. It can't move!");

        $array["{$target}.tmp"]['paralyze'] = session("friendly.{$target}.tmp.paralyze") - 1;
        $this->saveSession('friendly', $array);

        return false;
    }

    /**
     * Inflict infatuate
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function infatuate($target, $duration)
    {
        if ($duration <= 0) {
            session()->forget("friendly.{$target}.tmp.infatuate");
            session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." has recovered from infatuation!");

            return true;
        }

        if (random_int(1, 100) > 50) {
            $array["{$target}.tmp"]['infatuate'] = session("friendly.{$target}.tmp.infatuate") - 1;
            $this->saveSession('friendly', $array);

            return true;
        }

        session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." is in love with the target!");

        $array["{$target}.tmp"]['infatuate'] = session("friendly.{$target}.tmp.infatuate") - 1;
        $this->saveSession('friendly', $array);

        return false;
    }

    /**
     * Inflict flinch
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function flinch($target, $duration)
    {
        if ($duration <= 0) {
            session()->forget("friendly.{$target}.tmp.flinch");

            return true;
        }

        session()->push('friendly_log', "".session("friendly.{$target}.stats")->info->name ." is stunned and couldn't move!");

        $array["{$target}.tmp"]['flinch'] = session("friendly.{$target}.tmp.flinch") - 1;
        $this->saveSession('friendly', $array);

        return false;
    }

    /**
     * Initialize burn status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictBurn($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        if ($this->immunity($arr[1], 'fire', 'Burn')) return false;

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => random_int(3,5)
        ];

        session()->push('friendly_log', "<span style='color:red'>The target is burned!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize poison status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictPoison($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        if ($this->immunity($arr[1], 'poison', 'Poison')) return false;
        if ($this->immunity($arr[1], 'steel', 'Poison')) return false;

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => 99
        ];

        session()->push('friendly_log', "<span style='color:purple'>The target is poisoned!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize confuse status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictConfuse($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => random_int(3,5)
        ];

        session()->push('friendly_log', "<span style='color:darkorange'>The target is confused!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize freeze status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictFreeze($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        if ($this->immunity($arr[1], 'ice', 'Freeze')) return false;

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => random_int(3,5)
        ];

        session()->push('friendly_log', "<span style='color:#0275d8'>The target is frozen solid!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize paralyze status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictParalyze($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        $pokemon = session("friendly.".preg_replace("/_[0-9]+/", "", $arr[1]).".stats");

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => random_int(3,5),
            'speed' => round(session("friendly.{$arr[1]}.tmp.speed") - ($pokemon->speed / 2)),
        ];

        session()->push('friendly_log', "<span style='color:orange'>The target is paralyzed!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize sleep status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictSleep($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => random_int(1,5)
        ];

        session()->push('friendly_log', "<span style='color:#110069'>The target has fell asleep!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize infatuate status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictInfatuate($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        if (session("friendly.enemy.stats")->gender == session("friendly.pokemon.stats")->gender) {
            session()->push('friendly_log', "Infatuate status doesn't affect same gender target!");

            return false;
        }

        $arr = explode('.', $target);

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => random_int(2,5)
        ];

        session()->push('friendly_log', "<span style='color:#ff00aa'>The target is in love!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize flinch status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictFlinch($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => 1
        ];

        session()->push('friendly_log', "<span style='color:#75453a'>The target flinched!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize buff effects
     *
     * @param $target
     * @param $value
     * @param $chance
     * @return bool
     */
    public function buffEffect($target, $value, $chance)
    {
        $array = [];
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);
        $pokemon = session("friendly.".preg_replace("/_[0-9]+/", "", $arr[1]).".stats");

        foreach (explode(',', $arr[3]) as $attr) {
            if (session($target) && session($target) >= $pokemon->{$attr}) return false;

            $array["{$arr[1]}.tmp"][$attr] = round(session($target) + (($pokemon->{$attr} * $value) / 100));

            session()->push('friendly_log', "<span class='text-info'>{$pokemon->info->name}'s ". ucwords(preg_replace('/_/', ' ', $attr)) ." rose!</span>");
        }

        $this->saveSession('friendly', $array);

        return true;
    }

    public function debuffEffect($target, $value, $chance)
    {
        $array = [];
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);
        $pokemon = session("friendly.".preg_replace("/_[0-9]+/", "", $arr[1]).".stats");

        foreach (explode(',', $arr[3]) as $attr) {
            if (session($target) && session($target) >= (0 - $pokemon->{$attr})) return false;

            $array["{$arr[1]}.tmp"][$attr] = round(session($target) - (($pokemon->{$attr} * $value) / 100));

            session()->push('friendly_log', "<span class='text-info'>{$pokemon->info->name}'s ". ucwords(preg_replace('/_/', ' ', $attr)) ." is lowered!</span>");
        }

        $this->saveSession('friendly', $array);

        return true;
    }

    public function recoil($target, $value)
    {
        $arr = explode('.', $target);

        $pokemon = session("friendly.".preg_replace("/_[0-9]+/", "", $arr[1]).".stats");

        if ($arr[3] == 'hp') {

            $damage = $pokemon->hp * $value / 100;

            session()->push('friendly_log', $pokemon->info->name ." received ". round(max($damage, 0)) ." damage from recoil!");

            $array[$arr[1]]["{$arr[1]}_current_hp"] = max(session("friendly.{$arr[1]}.{$arr[1]}_current_hp") - round(max($damage, 0)), 0);

            $this->saveSession('friendly', $array);

            return true;
        }

        //if (session($target) && session($target) >= (0 - $pokemon->{$arr[2]})) return false;

        $array["{$arr[1]}.tmp"] = [
            $arr[3] => round(session($target) - (($pokemon->{$arr[3]} * $value) / 100))
        ];

        session()->push('friendly_log', "<span class='text-info'>{$pokemon->info->name}'s ". ucwords(preg_replace('/_/', ' ', $arr[3])) ." is lowered from recoil!</span>");

        $this->saveSession('friendly', $array);

        return true;
    }

    /**
     * Check for status immunity
     *
     * @param $target
     * @param $type
     * @param $effect
     * @return bool
     */
    public function immunity($target, $type, $effect)
    {
        $pokemon = session("friendly.".preg_replace("/_[0-9]+/", "", $target.".stats"));

        if ($pokemon->info->primary_type == $type || $pokemon->info->secondary_type == $type) {
            session()->push('friendly_log', "<span>{$effect} status doesn't affect the target!</span>");

            return true;
        }

        return false;
    }

    public function heal($key, $value, $modifier)
    {
        $hp = session("friendly.{$key}.{$key}_current_hp");

        if ($modifier == 'daylight') {
            $now = Carbon::now();
            $from = Carbon::parse('2017-01-01 06:00:00');
            $to = Carbon::parse('2017-01-01 18:00:00');

            if ($now->hour < $from->hour && $now->hour > $to->hour) return false;

            $array[$key]["{$key}_current_hp"] = max($hp + ((session("friendly.{$key}.stats")->hp * random_int($value * .8, $value)) / 100), 1);

            if ($array[$key]["{$key}_current_hp"] > session("friendly.{$key}.stats")->hp) {
                $array[$key]["{$key}_current_hp"] = session("friendly.{$key}.stats")->hp;
            }

            session()->push('friendly_log', "<span class='text-success'>".session("friendly.{$key}.stats")->info->name . " has absorbed the sunlight and recovered its HP!</span>");

        } elseif ($modifier == 'fixed') {
            $array[$key]["{$key}_current_hp"] = $hp + $value;

            if ($array[$key]["{$key}_current_hp"] > session("friendly.{$key}.stats")->hp) {
                $array[$key]["{$key}_current_hp"] = session("friendly.{$key}.stats")->hp;
            }

            session()->push('friendly_log', "<span class='text-success'>".session("friendly.{$key}.stats")->info->name ." has recovered its HP!</span>");
        } else {
            $array[$key]["{$key}_current_hp"] = round(max($hp + ((session("friendly.{$key}.stats")->hp * random_int($value * .8, $value)) / 100), 1));

            if ($array[$key]["{$key}_current_hp"] > session("friendly.{$key}.stats")->hp) {
                $array[$key]["{$key}_current_hp"] = session("friendly.{$key}.stats")->hp;
            }

            session()->push('friendly_log', "<span class='text-success'>".session("friendly.{$key}.stats")->info->name ." has recovered its HP!</span>");
        }

        $this->saveSession('friendly', $array);

        return true;
    }

    public function secondaryEffects($obj)
    {
        $attr = explode('-', $obj->attribute);

        if (in_array($attr[0], ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) {
            $method = "inflict".ucfirst($attr[0]);

            $result = $this->{$method}("friendly.{$obj->defKey}.tmp.{$obj->attribute}", $obj->value);

            if (!$result) session()->push('friendly_log', "But inflicting {$attr[0]} status failed!");
        }

        if (in_array($attr[0], ['increase', 'decrease', 'recoil', 'recover', 'heal'])) {
            foreach (explode(',', $attr[1]) as $stat) {
                switch ($attr[0]) {
                    case 'increase':
                        $result = $this->buffEffect("friendly.{$obj->key}.tmp.{$stat}", $obj->value, $obj->chance);

                        if (!$result) session()->push('friendly_log', "But ".ucwords(preg_replace('/_/', ' ', $stat))." increase failed!");
                        break;

                    case 'decrease':
                        $result = $this->debuffEffect("friendly.{$obj->defKey}.tmp.{$stat}", $obj->value, $obj->chance);

                        if (!$result) session()->push('friendly_log', "But ".ucwords(preg_replace('/_/', ' ', $stat))." decrease failed!");
                        break;

                    case 'recoil':
                        if (in_array($attr[1], ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) {
                            $method = "inflict".ucfirst($attr[1]);
                            $result = $this->{$method}("friendly.{$obj->key}.tmp.{$attr[1]}", $obj->chance);

                            if (!$result) session()->push('friendly_log', "Status condition {$attr[0]} failed!");
                        } else {
                            $result = $this->recoil("friendly.{$obj->key}.tmp.{$stat}", $obj->value);

                            if (!$result) session()->push('friendly_log', ucwords(preg_replace('/_/', ' ', $stat))." reduction from recoil failed!");
                        }
                        break;

                    case 'recover':
                        $statuses = session("friendly.{$obj->key}.tmp") ?? [];
                        $toRecover = explode(',',$attr[1]) ?? [];
                        $result = false;

                        foreach (array_keys($statuses) as $status) {
                            if (in_array($status, $toRecover)) {
                                session()->forget("friendly.{$obj->key}.tmp.{$status}");

                                $result = $status;
                            }
                        }

                        if ($result) session()->push('friendly_log', "{$obj->attacker->info->name} has recovered from {$result}!");
                        if (!$result) session()->push('friendly_log', "Status condition recovery failed!");
                        break;

                    case 'heal':
                        $result = $this->heal($obj->key, $obj->value, $stat);

                        if (!$result) session()->push('friendly_log', "But it failed!");
                        break;
                }
            }
        }
    }
}