<?php
namespace App\Repositories;

use App\Contracts\GameConfigInterface;
use App\GameConfig;
use Carbon\Carbon;

class GameConfigRepository extends Repository implements GameConfigInterface
{
    protected $config;

    public function __construct(GameConfig $config)
    {
        parent::__construct($config);
        $this->config = $config;
    }

    public function tournamentEnds()
    {
        $started = new Carbon($this->config->first()->arena_tournament_end);

        return Carbon::now() < $started ? $started->diffForHumans() : false;
    }
}