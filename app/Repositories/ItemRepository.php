<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\ItemInterface;
use App\Contracts\ParagonStatusEffectInterface;
use App\Contracts\StatusEffectInterface;
use App\Contracts\UserItemInterface;
use App\Item;

class ItemRepository extends Repository implements ItemInterface
{
    protected $item;
    protected $userItem;
    protected $statusEffect;
    protected $pokemon;

    public function __construct(Item $item, UserItemInterface $userItem, StatusEffectInterface $statusEffect, ParagonStatusEffectInterface $paragonStatusEffect)
    {
        parent::__construct($item);

        $this->item = $item;
        $this->userItem = $userItem;
        $this->statusEffect = $statusEffect;
        $this->paragonStatusEffect = $paragonStatusEffect;
    }

    /**
     * Random dungeon chest item
     *
     * @param $category
     * @param $section
     * @param array $except
     * @return mixed
     */
    public function randomChestItem($category, $section, $except = ['name', ['test']])
    {
        return $this->item->where('category', $category)
            ->where('section', $section)
            ->where($except[0], '!=', $except[1])
            ->inRandomOrder()->first(['id', 'name']);
    }

    public function shop($type = null)
    {
        switch ($type) {
            case 'arena': return $this->item->whereIn('id', [97,98,99,100,101,102,130,133,134,135,136,137,138,139,140,151,152,153,154,155,156])->get(); break;
            case 'club': return $this->item->whereIn('id', [126,173,157,158,159,160,161,162,163,164,165,166,129,141,142,143,144,145,146,147])->get(); break;
            default: return $this->item->whereIn('id', [1,2,3,4,92,93,94,95,96,103,104,105,106,107,108])->get();
        }
    }

    public function useItem($pokemon, $item)
    {
        $attr = explode('-', $item->info->primary_attr);
        $attr2 = explode('-', $item->info->secondary_attr);

        //item use in battle
        if (session()->has('battle.enemy') || session()->has('paragon.enemy')) {
            if (session()->has('paragon.enemy')) {
                session()->push('paragon.battle_log', "{$pokemon->info->name} used {$item->info->name}.");
            } else {
                session()->push('battle.battle_log', "{$pokemon->info->name} used {$item->info->name}.");
            }

            $obj = (object)[
                'attacker' => $pokemon,
                'defender' => $pokemon,
                'attribute' => $item->info->primary_attr,
                'value' => $item->info->primary_value,
                'key' => 'pokemon',
                'chance' => 100
            ];

            if (session()->has('paragon.enemy')) {
                $this->paragonStatusEffect->secondaryEffects($obj);
            } else {
                $this->statusEffect->secondaryEffects($obj);
            }

            if ($item->info->secondary_attr) {
                $obj = (object)[
                    'attacker' => $pokemon,
                    'defender' => $pokemon,
                    'attribute' => $item->info->secondary_attr,
                    'value' => $item->info->secondary_value,
                    'key' => 'pokemon',
                    'chance' => $item->info->secondary_value
                ];

                if (session()->has('paragon.enemy')) {
                    $this->paragonStatusEffect->secondaryEffects($obj);
                } else {
                    $this->statusEffect->secondaryEffects($obj);
                }
            }

            return true;

        //item use outside battle (or in dungeon)
        } elseif (session()->has('battle.pokemon') || session()->has('paragon.pokemon') && ($attr[0] == 'heal' || $attr2[0] == 'heal')) {
            if ($attr[0] == 'heal') {
                $obj = (object)[
                    'attacker' => $pokemon,
                    'defender' => $pokemon,
                    'attribute' => $item->info->primary_attr,
                    'value' => $item->info->primary_value,
                    'key' => 'pokemon',
                    'chance' => 100
                ];
            } else {
                $obj = (object)[
                    'attacker' => $pokemon,
                    'defender' => $pokemon,
                    'attribute' => $item->info->secondary_attr,
                    'value' => $item->info->secondary_value,
                    'key' => 'pokemon',
                    'chance' => $item->info->secondary_value
                ];
            }

            if (session()->has('paragon.pokemon')) {
                $this->paragonStatusEffect->secondaryEffects($obj);
            } else {
                $this->statusEffect->secondaryEffects($obj);
            }

            return true;
        } elseif (in_array($attr[0], ['stats', 'experience'])) {
            switch ($attr[0]) {
                case 'stats': $this->stats($attr[1], $item->info->primary_value, $pokemon); break;
                case 'experience': $this->experience($item->info->primary_value, $pokemon); break;
            }

            return true;
        }

        return false;
    }

    public function stats($attribute, $value, $pokemon)
    {
        $pokemon->increment('vitamins_used');

        return $pokemon->increment($attribute, $value);
    }

    public function experience($value, $pokemon)
    {
        return $pokemon->increment('experience', $value);
    }

    public function show($id)
    {
        return $this->item->select(['id', 'price'])->findOrFail($id);
    }
}