<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 2:53 PM
 */

namespace App\Repositories;

use App\Contracts\MailInterface;
use App\Mail;

class MailRepository extends Repository implements MailInterface
{
    protected $mail;

    public function __construct(Mail $mail)
    {
        parent::__construct($mail);
        $this->mail = $mail;
    }

    public function inbox()
    {
        return $this->mail->with('sender')->where('user_id', auth()->user()->id)->latest()->paginate(10);
//        return $this->mail->select('sender_id', 'id', 'message')->where('user_id', auth()->user()->id)->distinct()->latest()->paginate(10);
    }

    public function unread()
    {
        return $this->mail->where('user_id', auth()->user()->id)->where('is_read', 0)->count();
    }

    public function read($id)
    {
        $mail = $this->mail->with('sender')->where('user_id', auth()->user()->id)->findOrFail($id);

        !$mail->is_read ? $mail->increment('is_read') : false;

        return $mail;
    }

    public function lastReply($sender)
    {
        return $this->mail->where('user_id', $sender)->where('sender_id', auth()->user()->id)->latest()->first();
    }

    public function welcome()
    {
        $this->mail->create([
            'user_id' => auth()->user()->id,
            'sender_id' => 1,
            'message' => "Welcome to Pokemon Ascension! I hope you'll enjoy playing this simple poke RPG. If you have any questions/suggestions, I'd recommend you to interact with your fellow trainers in Pokemon Center or post it on our forum. Have a good day!"
        ]);

        return false;
    }
}