<?php
namespace App\Repositories;

use App\Contracts\MissionInterface;
use App\Mission;

class MissionRepository extends Repository implements MissionInterface
{
    protected $mission;

    public function __construct(Mission $mission)
    {
        parent::__construct($mission);

        $this->mission = $mission;
    }

    public function available()
    {
        return $this->mission->inRandomOrder()->limit(5)->get();
    }
}