<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\MoveInterface;
use App\Move;

class MoveRepository extends Repository implements MoveInterface
{
    protected $move;

    public function __construct(Move $move)
    {
        parent::__construct($move);

        $this->move = $move;
    }

    public function generate($types, $level)
    {
        $array = [];
        $count = sqrt($level) <= 4 ? round(sqrt($level)) : 4;

        for ($i = 1; $i <= $count; $i++) {
            $array[] = $this->move->whereIn('type', $types)
                ->where('id', '!=', 86)
                ->where('level', '<=', $level)
                ->where('is_utility', 0)
                ->inRandomOrder()->first();

            if ($i < 4 && random_int(1,100) <= 40) {
                $array[] = $this->move->whereIn('type', $this->categories($types))
                    ->where('level', '<=', $level)
                    ->where('is_utility', 0)
                    ->inRandomOrder()->first();

                $i++;
            }

            if ($i < 4 && random_int(1,100) <= 30) {
                $array[] = $this->move->where('level', '<=', $level)->where('is_utility', 2)->inRandomOrder()->first();

                $i++;
            }

        }

        return array_map("unserialize", array_unique(array_map("serialize", $array)));
    }

    public function findAllByType($type)
    {
        return $this->move->where('type', $type)->where('is_utility', '!=', 2)->orderBy('level')->paginate(10);
    }

    public function learn($pokemon)
    {
        if ($pokemon->level < session("learn")->level) {
            $this->flashError("{$pokemon->info->name} has not enough level to learn this move!");

            return false;
        }

//        if (!in_array(session("learn")->type, [$pokemon->info->primary_type, $pokemon->info->secondary_type])) {
//            $this->flashError("{$pokemon->info->name} cannot learn this move!");
//
//            return false;
//        }

        if (!in_array($pokemon->info->name, ['Mew', 'Ditto'])) {
            if (!$this->checkTypeCategory($pokemon->info->primary_type) && !$this->checkTypeCategory($pokemon->info->secondary_type)) {
                $this->flashError("{$pokemon->info->name} cannot learn this move!");

                return false;
            }
        }

        if ($pokemon->moves->count() >= 4) {
            $this->flashError("{$pokemon->info->name} has already 4 moves! Forget a move and try again.");

            return false;
        }

        foreach ($pokemon->moves as $move) {
            if ($move->id == session("learn")->id) {
                $this->flashError("{$pokemon->info->name} has already learned this move!");

                return false;
            }
        }

        $pokemon->moves()->attach(session("learn")->id);

        auth()->user()->gold -= session("learn")->level * 695;

        if (auth()->user()->gold < 0) {
            auth()->user()->gold = 0;
        }

        auth()->user()->save();

        return true;
    }

    public function checkTypeCategory($type)
    {
        //type of pokemon and learn move
        $types = [$type, session('learn')->type];

        $cat1 = ['ice', 'water', 'ground', 'rock', 'grass', 'bug'];
        $cat2 = ['fighting', 'dragon', 'normal', 'fairy', 'flying', 'steel'];
        $cat3 = ['poison', 'psychic', 'electric', 'ghost', 'dark', 'fire'];

        if (count(array_diff($types, $cat1)) == 0) {
            return true;
        } elseif (count(array_diff($types, $cat2)) == 0) {
            return true;
        } elseif (count(array_diff($types, $cat3)) == 0) {
            return true;
        }

        return false;
    }

    /**
     * Categories can learn
     *
     * @param $types
     * @return array
     */
    public function categories($types)
    {
        $array = [];

        $cat1 = ['ice', 'water', 'ground', 'rock', 'grass', 'bug'];
        $cat2 = ['fighting', 'dragon', 'normal', 'fairy', 'flying', 'steel'];
        $cat3 = ['poison', 'psychic', 'electric', 'ghost', 'dark', 'fire'];

        if (count(array_intersect($types, $cat1))) {
            $array[] = $cat1;
        }

        if (count(array_intersect($types, $cat2))) {
            $array[] = $cat2;
        }

        if (count(array_intersect($types, $cat3))) {
            $array[] = $cat3;
        }

        return array_collapse($array);
    }
}