<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 2:53 PM
 */

namespace App\Repositories;

use App\Contracts\NotifInterface;
use App\Notification;

class NotifRepository extends Repository implements NotifInterface
{
    protected $notif;

    public function __construct(Notification $notif)
    {
        parent::__construct($notif);
        $this->notif = $notif;
    }

    public function all()
    {
        return $this->notif->where('user_id', auth()->user()->id)->latest()->paginate(15);
    }

    public function unread()
    {
        return $this->notif->where('user_id', auth()->user()->id)->where('is_read', 0)->count();
    }

    public function readAll()
    {
        return $this->notif->where('user_id', auth()->user()->id)->where('is_read', 0)->update(['is_read' => 1]);
    }

    public function add($user, $message)
    {
        return $this->notif->create([
            'user_id' => $user,
            'message' => $message
        ]);
    }
}