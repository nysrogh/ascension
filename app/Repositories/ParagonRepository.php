<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 2:53 PM
 */

namespace App\Repositories;

use App\Contracts\ParagonInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\ParagonStatusEffectInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use Intervention\Image\ImageManagerStatic as Image;

class ParagonRepository extends Repository implements ParagonInterface
{
    public function __construct(
        UserPokemonInterface $userPokemon,
        ParagonStatusEffectInterface $statusEffect,
        UserItemInterface $userItem,
        PokemonInterface $pokemon
    )
    {
        $this->userPokemon = $userPokemon;
        $this->statusEffect = $statusEffect;
        $this->userItem = $userItem;
        $this->pokemon = $pokemon;
    }

    public function difficulty()
    {
        foreach ($this->userPokemon->team() as $poke) {
            if ($poke->level > 50) {
                return 100;
            }
        }

        return 50;
    }

    public function initialize($diff)
    {
        $array = [];

        //data
        $enemy = $this->pokemon->paragonEncounter($diff);
        $pokemon = $this->userPokemon->arenaTeam()[0];

        //boss HP modifier
        $boss = (session('paragon.progress')%50 == 0) ? 1.5 : 1;
        $enemy['hp'] *= $boss + (session('paragon.progress') / 8.7);
        $enemy['hp'] = round($enemy['hp']);
        $enemy['attack'] *= $boss + (session('paragon.progress') / 15);
        $enemy['special_attack'] *= $boss + (session('paragon.progress') / 15);
        $enemy['defense'] *= $boss + (session('paragon.progress') / 20);
        $enemy['special_defense'] *= $boss + (session('paragon.progress') / 20);

        //enemy info
        $array['enemy'] = [
            "stats" => (object)$enemy,
            "current_hp_{$enemy['id']}" => $enemy['hp'],
            "not_effective_{$enemy['id']}" => [],
            "super_effective_{$enemy['id']}" => []
        ];

        //pokemon info
        $array["pokemon"] = [
            "stats" => $pokemon,
            "current_hp_{$pokemon->id}" => session("paragon.pokemon.current_hp_{$pokemon->id}") ??  $pokemon->hp
        ];

        //initialize turn
        $array["turns"] = 1;

        //initialize participant
        $array["participants"][] = $pokemon->id;

        //initialize background
        $array["background"] = $this->background((object)$enemy['info'], $pokemon->info);
        
        //save session data
        $this->saveSession('paragon', $array);

        return false;
    }

    public function background($enemy, $pokemon, $result = false)
    {
        $background = Image::make(public_path("images/battle/paragon.png"));

        //enemy front background
        if (!$result || $result == 'defeat') {
            $background->insert(public_path("images/pokemon/front/" . str_slug($enemy->name) . ".png"), '',
                $enemy->front_x, $enemy->front_y);
        }

        //caught pokemon background
        if (is_array($result)) {
            $background->insert(public_path("images/items/" . str_slug($result['ball']) . ".png"), '', 132, 48);
        }

        //pokemon back background
        if (!$result || $result == 'victory') {
            $background->insert(public_path("images/pokemon/back/" . str_slug($pokemon->name) . ".png"), '',
                $pokemon->back_x, $pokemon->back_y);
        }

        return 'data:image/png;base64,' . base64_encode($background->encode('jpg', 90));
    }

    public function checker()
    {
        $pokemon = session('paragon.pokemon');
        $enemy = session('paragon.enemy');
        $array = [];

        if (session()->has('paragon.result')) return 'end';

        //check enemy hp
        if ($enemy['current_hp_1'] <= 0) {
            $array['result'] = 'victory';
            $array['background'] = $this->background($enemy['stats']->info, $pokemon['stats']->info, 'victory');
            $array['battle_log'] = $this->victory($enemy['stats'], $this->participants());

            $this->saveSession('paragon', $array);

            return 'win';
        }

        //check pokemon hp
        if ($pokemon["current_hp_{$pokemon['stats']->id}"] <= 0) {
            if ($this->substitute()) return true;

            $array['result'] = 'defeat';
            $array['background'] = $this->background($enemy['stats']->info, $pokemon['stats']->info, 'defeat');
            $array['battle_log'] = $this->defeat();

            $this->saveSession('paragon', $array);

            return 'lose';
        }

        return false;
    }

    public function participants()
    {
        $used = [];

        foreach (array_unique(session('paragon.participants')) as $id) {
            if (session("paragon.pokemon.current_hp_{$id}") > 0) {
                $used[] = $id;
            }
        }

        return $used;
    }

    public function checkMove($move)
    {
        foreach (session('paragon.pokemon.stats')->moves as $list)
        {
            if ($list->id == $move) return $list;
        }

        return false;
    }

    public function substitute()
    {
        foreach ($this->userPokemon->team() as $pokemon)
        {
            if (!session()->has("paragon.pokemon.current_hp_{$pokemon->id}") || session("paragon.pokemon.current_hp_{$pokemon->id}") > 0) {
                return true;
            }
        }

        return false;
    }

    public function victory($enemy, $used)
    {
        session()->forget('paragon.battle_log');

        $value = ($enemy->level / 3.5) * ($enemy->ratings / 70) * (1.2 + ($enemy->info->rarity / 2.5)) * (session('victory_bonus') ?? 1);

        $exp = round((random_int($value * .90, $value) / count($used)) * 7);
        $gold = round(random_int($value * .30, $value * .70));

        if (session()->has('paragon.enemy.add_exp')) {
            $exp += $exp * session('paragon.enemy.add_exp');
        }

        $logs[] = "<strong>Enemy {$enemy->info->name} fainted!</strong>";
        foreach ($this->userPokemon->battleGain($exp, $used) as $log) {
            $logs[] = $log;
        }
        $logs[] = "<span class='text-success'>You found +{$gold} crystal!</span>";

        auth()->user()->increment('crystal', $gold);

        return $logs;
    }

    public function defeat()
    {
        if ($this->substitute()) return false;

        session()->forget('paragon.battle_log');

        $array['battle_log'][] = "<strong>You lost! All your pokemon has fainted!</strong>";
        $array['result'] = 'defeat';
        $array['background'] = $this->background(session('paragon.enemy.stats')->info, session('paragon.pokemon.stats')->info, 'defeat');
        $this->saveSession('paragon', $array);

        return true;
    }

    public function recall($id)
    {
        $array = [];

        session()->forget('paragon.battle_log');

        $old = session("paragon.pokemon.stats");

        if (session("paragon.pokemon.current_hp_{$old->id}") <= 0) {
            $array['battle_log'][] = "<strong>". session("paragon.pokemon.stats.info")->name ." fainted!</strong>";
        }

        $array['battle_log'][] = "{$old->info->name}, switch out! Come back!";

        $pokemon = $this->userPokemon->findAvailable($id);
        $array["pokemon"] = [
            'stats' => $pokemon,
            "current_hp_{$pokemon->id}" => session("paragon.pokemon.current_hp_{$pokemon->id}") ??  $pokemon->hp
        ];

        $array['battle_log'][] = "<strong>Go, {$array["pokemon"]["stats"]->info->name}!</strong>";
        $array['participants'] = $array["pokemon"]["stats"]->id;

        $array["turns"] = session("paragon.turns") + 1;
        $array["background"] = $this->background(session('paragon.enemy.stats')->info, $pokemon->info);

        $this->saveSession('paragon', $array);

        if (session('paragon.pokemon.stats')->speed < session('paragon.enemy.stats')->speed) {
            $this->useMove(session("paragon.enemy"), $this->smartMove(session('paragon.pokemon.stats')), session("paragon.pokemon"), 'enemy');
            $this->checker();
        }

        return false;
    }

    public function fight($move)
    {
        $pokemon = session('paragon.pokemon.stats');
        $enemy = session('paragon.enemy.stats');

        //held item [turn]
        $quick = 0;
        if (session('paragon.pokemon.stats')->item) {
            if ($pokemon->item->info->secondary_attr == 'turn') {
                switch ($pokemon->item->info->primary_attr) {
                    case 'heal': $this->userItem->itemEffectTurnHeal($pokemon, 'pokemon', 'paragon'); break;
                    case 'selected': $this->userItem->itemEffectTurnSelected($pokemon, $move, 'pokemon', 'paragon'); break;
                    case 'first': $quick = $this->userItem->itemEffectTurnFirst($pokemon, 'pokemon', 'paragon'); break;
                    case 'exp': $this->userItem->itemEffectTurnAddBonus($pokemon, 'exp', 'enemy', 'paragon'); break;
                    case 'coins': $this->userItem->itemEffectTurnAddBonus($pokemon, 'coins', 'enemy', 'paragon'); break;
                }
            }
        }

        $battle = [
            "pokemon" => [
                'speed' => $pokemon->speed + session("tmp.pokemon_{$pokemon->id}.speed") + $quick,
                'move' => session("paragon.selected.{$pokemon->id}") ?? $move
            ],
            "enemy" => [
                'speed' => $pokemon->speed + session("tmp.enemy_{$enemy->id}.speed"),
                'move' => $this->smartMove(session('paragon.pokemon.stats'))
            ]
        ];

        $sorted = array_reverse(array_sort($battle, function($value) {
            return $value['speed'];
        }));

        foreach ($sorted as $key => $value) {
            $defKey = ($key == 'pokemon') ? 'enemy' : 'pokemon';
            $this->useMove(session("paragon.{$key}"), $value['move'], session("paragon.{$defKey}"), $key);

            if (in_array($this->checker(), ['win', 'lose'])) break;
        }

        $array["turns"] = session("paragon.turns") + 1;

        $this->saveSession('paragon', $array);
    }

    public function useMove($attacker, $move, $defender, $key)
    {
        $array = [];
        $defKey = ($key == 'pokemon') ? 'enemy' : 'pokemon';

        if (!$this->statusEffect->check($key, $attacker['stats']->id)) return false;

        session()->push('paragon.battle_log', $attacker['stats']->info->name ." used ". $move->name ."!");

        //damaging move
        if (in_array($move->primary_attr, ['attack', 'special_attack'])) {

            //held items [accuracy]
            $accuracy = 0;
            if (in_array($attacker['stats']->id, session('paragon.participants')) && $attacker['stats']->item) {
                if ($attacker['stats']->item->info->secondary_attr == 'accuracy') {
                    switch ($attacker['stats']->item->info->primary_attr) {
                        case 'critical': $accuracy = $this->userItem->itemEffectMoveAccuracy($attacker['stats']); break;
                        case 'evasion': $accuracy = $this->userItem->itemEffectMoveAccuracy($attacker['stats']); break;
                    }
                }
            }

            //evasion else attack
            if (random_int(1,10000) <= $defender['stats']->speed + session("tmp.{$defKey}_". $defender['stats']->id .".speed") + $accuracy) {
                session()->push('paragon.battle_log', "<span class='text-danger'>". $defender['stats']->info->name ." avoided the attack!</span>");
            } else {
                $attack = $attacker['stats']->{$move->primary_attr} + session("tmp.{$key}_{$attacker['stats']->id}.{$move->primary_attr}");
                $defense = $move->primary_attr == 'attack'
                    ? $defender['stats']->defense + session("tmp.{$defKey}_{$defender['stats']->id}.defense")
                    : $defender['stats']->special_defense + session("tmp.{$defKey}_{$defender['stats']->id}.special_defense");

                $smartID = $attacker['stats']->id != session('paragon.pokemon.stats')->id ?? null;

                $type = $this->typeModifiers($smartID, $move,
                    array_filter([$defender['stats']->info->primary_type, $defender['stats']->info->secondary_type], function ($var) {
                        return !is_null($var);
                    }));

                //damage calculation formula
                $damage = (((((($attacker['stats']->level * 2) / 5) + 2) * $move->primary_value
                                * $attack/$defense) / 50) + 2)
                    //modifiers = targets * weather * badge * critical * random * stab * type * burn * other
                    * (1 * 1 * 1 * $this->critical($attacker['stats']->speed + session("tmp.{$key}_". $attacker['stats']->id .".speed") + $accuracy) * (random_int(85,100)/100) * $this->stab($move->type, array_filter([$attacker['stats']->info->primary_type, $attacker['stats']->info->secondary_type], function ($var) {
                            return !is_null($var);
                        })) * $type * 1 * 1);

                //held items [move]
                $add = 0; $protect = false;
                if (in_array($attacker['stats']->id, session('paragon.participants')) && $attacker['stats']->item) {
                    if ($attacker['stats']->item->info->secondary_attr == 'move') {
                        switch ($attacker['stats']->item->info->primary_attr) {
                            case 'super': $damage = $this->userItem->itemEffectMoveSuper($attacker['stats'], $type, $damage); break;
                            case 'heal': $this->userItem->itemEffectMoveHeal($attacker['stats'], $damage, 'paragon', $key); break;
                            case 'add-heal': $add = $this->userItem->itemEffectMoveAddHeal($attacker['stats'], $move, 'paragon', $key); break;
                            case 'recoil': $damage = $this->userItem->itemEffectMoveRecoil($attacker['stats'], $damage, 'paragon', $key); break;
                            case 'attack': $damage = $this->userItem->itemEffectMoveAttack('attack', $attacker['stats'], $damage, $move); break;
                            case 'special_attack': $damage = $this->userItem->itemEffectMoveAttack('special_attack', $attacker['stats'], $damage, $move); break;
                            case 'flinch': $this->userItem->itemEffectMoveFlinch($attacker['stats'], $defender['stats']->id, $key, 'paragon'); break;
                            case 'protect-recoil': $protect = $this->userItem->itemEffectMoveProtectRecoil($attacker['stats'], $move); break;
                            default:
                                if (in_array($attacker['stats']->item->info->primary_attr, ['poison', 'fighting', 'ice', 'dragon', 'ghost', 'flying', 'grass', 'normal', 'ground', 'fire', 'fairy', 'electric', 'steel', 'dark', 'rock', 'psychic', 'water', 'bug'])) {
                                    $damage = $this->userItem->itemEffectMoveType($attacker['stats'], $move, $damage);
                                }
                        }
                    }
                }

                if ($move->secondary_attr && !$protect) {
                    $obj = (object) [
                        'attacker' => $attacker['stats'],
                        'defender' => $defender['stats'],
                        'attribute' => $move->secondary_attr,
                        'value' => $move->secondary_value + $add,
                        'key' => $key,
                        'defKey' => $defKey,
                        'chance' => 10
                    ];

                    $this->statusEffect->secondaryEffects($obj);
                }

                session()->push('paragon.battle_log', $defender['stats']->info->name ." took ". round(max($damage, 0)) ." damage!");

                $array[$defKey]["current_hp_{$defender['stats']->id}"] = max(session("paragon.{$defKey}.current_hp_{$defender['stats']->id}") - round(max($damage, 0)), 0);

                //held items [defender]
                if (in_array($defender['stats']->id, session('paragon.participants')) && $defender['stats']->item) {
                    if ($defender['stats']->item->info->secondary_attr == 'counter') {
                        switch ($defender['stats']->item->info->primary_attr) {
                            case 'return': $this->userItem->itemEffectMoveReturn($defender['stats'], $attacker['stats']->id, $damage, $key, 'paragon'); break;
                        }
                    }
                }
            }

        } else {
            $chance = (!in_array($move->primary_attr, ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) ? 60 : 20;
            $obj = (object) [
                'attacker' => $attacker['stats'],
                'defender' => $defender['stats'],
                'attribute' => $move->primary_attr,
                'value' => $move->primary_value,
                'key' => $key,
                'defKey' => $defKey,
                'chance' => $move->primary_value + $chance
            ];

            $this->statusEffect->secondaryEffects($obj);

            if ($move->secondary_attr) {
                $chance = (!in_array($move->secondary_attr, ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) ? 70 : 20;

                $obj = (object) [
                    'attacker' => $attacker['stats'],
                    'defender' => $defender['stats'],
                    'attribute' => $move->secondary_attr,
                    'value' => $move->secondary_value,
                    'key' => $key,
                    'defKey' => $defKey,
                    'chance' => $move->secondary_value + $chance
                ];

                $this->statusEffect->secondaryEffects($obj);
            }
        }

        $this->saveSession('paragon', $array);

        //held items [defender]
        if (in_array($defender['stats']->id, session('paragon.participants')) && $defender['stats']->item) {
            if ($defender['stats']->item->info->secondary_attr == 'counter') {
                switch ($defender['stats']->item->info->primary_attr) {
                    case 'ko': $this->userItem->itemEffectCounterKo($defender['stats'], $defKey, 'paragon'); break;
                }
            }
        }

        return false;
    }

    public function capture($pokeball)
    {
        $array['battle_log'][] = auth()->user()->username." used {$pokeball->info->name}!";

        $enemy = session('paragon.enemy.stats');
        $ballModifier = $this->userItem->ballModifier($pokeball);

        if (session('paragon.turns') == 1 && $pokeball->info->name == 'Quick Ball') {
            $chance = max(($enemy->info->catch_rate * $ballModifier) / 3, 1);
        } else {
            $chance = max(floor(floor((3 * $enemy->info->hp - 2 * session("paragon.enemy.current_hp_{$enemy->id}")) * floor($enemy->info->catch_rate * $ballModifier) / (3 * $enemy->info->hp)) * $this->catchStatusCondition($enemy->id)), 1);
        }

        if (random_int(1, 255) <= $chance || $pokeball->info->name == 'Master Ball') {
            $this->pokemon->rewardPokemon(1, $enemy->info->id);

            session()->forget('paragon.battle_log');

            //legend limit
            auth()->user()->increment('legend_limit', 1);

            $array['battle_log'][] = "<strong>Gotcha! ". $enemy->info->name ." was caught!</strong>";

            $array['result'] = 'victory';
            $array['background'] = $this->background($enemy, session('paragon.pokemon.stats')->info, ['ball' => $pokeball->info->name]);

            session()->save();

            $array["turns"] = session("paragon.turns") + 1;
            $this->saveSession('paragon', $array);

            return false;
        }

        $array['battle_log'][] = "Oh no! The Pokemon broke free!";

        $array["turns"] = session("paragon.turns") + 1;
        $this->saveSession('paragon', $array);

        $this->useMove(session("paragon.enemy"), $this->smartMove(session('paragon.pokemon.stats')), session("paragon.pokemon"), 'enemy');
        $this->checker();

        return false;
    }

    public function catchStatusCondition($id)
    {
        if (session("tmp.enemy.{$id}")) {
            foreach (session("tmp.enemy_{$id}") as $status) {
                if (in_array($status, ['freeze', 'burn', 'sleep', 'paralyze', 'poison'])) {
                    return 2;
                }
            }
        }

        return 1;
    }

    public function stab($move, $poke)
    {
        return (in_array($move, $poke)) ? 1.5 : 1;
    }

    public function critical($speed)
    {
        if (random_int(1,10000) <= $speed) {
            session()->push('paragon.battle_log', "<span class='text-danger'>A critical hit!</span>");

            return 2;
        }

        return 1;
    }

    public function typeModifiers($enemy, $move, $defender)
    {
        $modifier = 1;
        $effects = session("paragon.enemy.not_effective_{$enemy}") ?? [];

        foreach ($defender as $type) {
            if ($this->checkStrength($move->type, $type)) {
                if ($enemy && !in_array($move->name, $effects)) {
                    session()->push("paragon.enemy.super_effective_{$enemy}", $move->name);
                }

                $modifier *= 2;
            } elseif ($this->checkWeakness($move->type, $type)) {
                if ($enemy && !in_array($move->name, $effects)) {
                    session()->push("paragon.enemy.not_effective_{$enemy}", $move->name);
                }

                $modifier /= 2;
            } elseif ($this->checkNull($move->type, $type)) {
                if ($enemy && !in_array($move->name, $effects)) {
                    session()->push("paragon.enemy.not_effective_{$enemy}", $move->name);
                }

                $modifier = 0;
            }
        }

        if ($modifier >= 2) {
            session()->push('paragon.battle_log', "It's super effective!");
        } elseif ($modifier < 1 && $modifier != 0) {
            session()->push('paragon.battle_log', "It's not very effective!");
        } elseif ($modifier == 0) {
            session()->push('paragon.battle_log', "It doesn't affect the opponent!");
        }

        return $modifier;
    }

    /**
     * Check weakness moves
     *
     * @param $offense
     * @param $defense
     * @return bool
     */
    public function checkWeakness($offense, $defense)
    {
        switch ($offense) {
            case 'fire': if (in_array($defense, ['dragon', 'fire', 'rock', 'water'])) return true; break;
            case 'normal': if (in_array($defense, ['rock', 'steel'])) return true; break;
            case 'fighting': if (in_array($defense, ['bug', 'fairy', 'flying', 'poison', 'psychic'])) return true; break;
            case 'water': if (in_array($defense, ['dragon', 'grass', 'water'])) return true; break;
            case 'flying': if (in_array($defense, ['electric', 'rock', 'steel'])) return true; break;
            case 'grass': if (in_array($defense, ['bug', 'dragon', 'fire', 'flying', 'grass', 'poison', 'steel'])) return true; break;
            case 'poison': if (in_array($defense, ['poison', 'ground', 'rock', 'ghost'])) return true; break;
            case 'steel': if (in_array($defense, ['electric', 'fire', 'steel', 'water'])) return true; break;
            case 'electric': if (in_array($defense, ['dragon', 'electric', 'grass'])) return true; break;
            case 'psychic': if (in_array($defense, ['psychic', 'steel'])) return true; break;
            case 'rock': if (in_array($defense, ['fighting', 'ground', 'steel'])) return true; break;
            case 'fairy': if (in_array($defense, ['fire', 'poison', 'steel'])) return true; break;
            case 'dark': if (in_array($defense, ['dark', 'fairy', 'fighting'])) return true; break;
            case 'ice': if (in_array($defense, ['fire', 'ice', 'steel', 'water'])) return true; break;
            case 'ground': if (in_array($defense, ['bug', 'grass'])) return true; break;
            case 'bug': if (in_array($defense, ['fairy', 'fighting', 'fire', 'flying', 'ghost', 'poison', 'steel'])) return true; break;
            case 'dragon': if ($defense == 'steel') return true; break;
            case 'ghost': if ($defense == 'dark') return true; break;
        }

        return false;
    }

    /**
     * Check strength moves
     *
     * @param $offense
     * @param $defense
     * @return bool
     */
    public function checkStrength($offense, $defense)
    {
        switch ($offense) {
            case 'fire': if (in_array($defense, ['bug', 'grass', 'ice', 'steel'])) return true; break;
            case 'fighting': if (in_array($defense, ['dark', 'ice', 'normal', 'rock', 'steel'])) return true; break;
            case 'water': if (in_array($defense, ['fire', 'ground', 'rock'])) return true; break;
            case 'flying': if (in_array($defense, ['bug', 'fighting', 'grass'])) return true; break;
            case 'grass': if (in_array($defense, ['ground', 'rock', 'water'])) return true; break;
            case 'fairy': if (in_array($defense, ['dark', 'dragon', 'fighting'])) return true; break;
            case 'steel': if (in_array($defense, ['fairy', 'ice', 'rock'])) return true; break;
            case 'poison': if (in_array($defense, ['fairy', 'grass'])) return true; break;
            case 'electric': if (in_array($defense, ['flying', 'water'])) return true; break;
            case 'psychic': if (in_array($defense, ['fighting', 'poison'])) return true; break;
            case 'ghost': if (in_array($defense, ['ghost', 'psychic'])) return true; break;
            case 'dark': if (in_array($defense, ['ghost', 'psychic'])) return true; break;
            case 'rock': if (in_array($defense, ['bug', 'fire', 'flying', 'ice'])) return true; break;
            case 'ice': if (in_array($defense, ['dragon', 'flying', 'grass', 'ground'])) return true; break;
            case 'bug': if (in_array($defense, ['dark', 'grass', 'psychic'])) return true; break;
            case 'ground': if (in_array($defense, ['electric', 'fire', 'poison', 'rock', 'steel'])) return true; break;
            case 'dragon': if ($defense == 'dragon') return true; break;
        }

        return false;
    }

    /**
     * Check null moves
     *
     * @param $offense
     * @param $defense
     * @return bool
     */
    public function checkNull($offense, $defense)
    {
        switch($offense) {
            case 'normal': if ($defense == 'ghost') return true; break;
            case 'ghost': if ($defense == 'normal') return true; break;
            case 'fighting': if ($defense == 'ghost') return true; break;
            case 'poison': if ($defense == 'steel') return true; break;
            case 'electric': if ($defense == 'ground') return true; break;
            case 'psychic': if ($defense == 'dark') return true; break;
            case 'dragon': if ($defense == 'fairy') return true; break;
            case 'ground': if ($defense == 'flying') return true; break;
        }

        return false;
    }

    public function smartMove($target)
    {
        $enemy = session('paragon.enemy.stats')->id;
        $moves = session('paragon.enemy.stats')->moves;
        $move = $this->_randomMove($moves);
        $ailments = session("tmp.pokemon_{$target->id}") ?? [];

        //check already inflicted status
        if (in_array($move[0]->primary_attr, $ailments)) {
            $moves = array_except(session('paragon.enemy.stats')->moves, key($move));

            return count($moves) > 1 ? $this->_randomMove($moves)[0] : $move[0];
        }

        //check for super-effective move
        if (count(session("paragon.enemy.super_effective_{$enemy}")) && in_array($move[0]->name, session("paragon.enemy.super_effective_{$enemy}"))) {

            return $move[0];
        }

        //check for not very effective move
        if (!empty(session("paragon.enemy.not_effective_{$enemy}")) && in_array($move[0]->name, session("paragon.enemy.not_effective_{$enemy}"))) {
            $moves = array_except(session('paragon.enemy.stats')->moves, key($move));

            return count($moves) > 1 ? $this->_randomMove($moves)[0] : $move[0];
        }

        return $move[0];
    }

    private function _randomMove($arr, $num = 1) {
        $keys = array_keys($arr);
        shuffle($keys);

        $r = array();
        for ($i = 0; $i < $num; $i++) {
            $r[$keys[$i]] = $arr[$keys[$i]];
        }

        return array_flatten($r);
    }
}