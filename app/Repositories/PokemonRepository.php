<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\DungeonInterface;
use App\Contracts\MoveInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\UserPokemonInterface;
use App\Pokemon;
use Carbon\Carbon;

class PokemonRepository extends Repository implements PokemonInterface
{
    protected $pokemon;
    protected $userPokemon;
    protected $move;
    protected $dungeon;

    public function __construct(
        Pokemon $pokemon,
        DungeonInterface $dungeon,
        UserPokemonInterface $userPokemon,
        MoveInterface $move)
    {
        parent::__construct($pokemon);

        $this->pokemon = $pokemon;
        $this->userPokemon = $userPokemon;
        $this->move = $move;
        $this->dungeon = $dungeon;
    }

    /**
     * Randomize pokemon acquired rarity
     *
     * @return int|null
     */
    public function randomizer()
    {
        $club = 0;

        //club privileges
        if (auth()->check() && auth()->user()->club_id) {
            foreach (auth()->user()->club->privileges as $privilege) {
                if ($privilege->pivot->expired_on > Carbon::now() && $privilege->modifier == 'find') {
                    $club += $privilege->value;
                }
            }
        }

        //changed - 120, 20, 10
        if (random_int(1, 10000) <= 8000) {
            $rarity = 1;
        } else if (random_int(1, 10000) <= 1500 + $club) {
            $rarity = 2;
        } else if (random_int(1, 10000) <= 120 + $club) {
            $rarity = 3;
        } else if (random_int(1, 10000) <= 20 + $club && auth()->user()->legend_limit < 2) {
            $rarity = 4;
        } else if (random_int(1, 10000) <= 10 + $club && auth()->user()->legend_limit < 1) {
            $rarity = 5;
        } else {
            $rarity = 1;
        }

        return $rarity;
    }

    /**
     * Randomize BOSS pokemon acquired rarity
     *
     * @return int|null
     */
    public function eliteRandomizer()
    {
        //changed - 7900, 800, 30
        if (random_int(1, 10000) <= 7900) {
            $rarity = 2;
        } else if (random_int(1, 10000) <= 800) {
            $rarity = 3;
        } else if (random_int(1, 10000) <= 30 && auth()->user()->legend_limit < 2) {
            $rarity = 4;
        } else {
            $rarity = 2;
        }

        return $rarity;
    }

    /**
     * Generate pokemon based on given object
     *
     * @param $object
     * @return bool
     */
    public function generate($object)
    {
        switch ($object->action) {
            case 'starter':
                return $this->pokemon->where('rarity', $object->rarity)
                    ->where('primary_type', $object->type)
                    ->where('alternate_form', 0)->whereNotIn('id', [793,794,795,796,797,798,799,800])
                    ->inRandomOrder()
                    ->first();
                break;

            case 'evolution':
                return $this->pokemon->where('evolution_from', $object->id)->where('evolution_type', $object->method)
                    ->first();
                break;

            case 'wild':
                $wild = $this->pokemon->where('dungeon_id', $object->dungeon)->where('rarity', $object->rarity)
                    ->inRandomOrder()->first();

                if (!$wild) {
                    $wild = $this->pokemon->where('dungeon_id', $object->dungeon)->where('rarity', $object->rarity - 1)
                        ->inRandomOrder()->first();
                }

                return $wild;

                break;
            case 'reward':
                if (is_numeric($object->id)) {
                    return $this->pokemon->where('id', $object->id)->first();
                }

                return $this->pokemon->where('rarity', $object->rarity)->where('alternate_form', 0)->whereNotIn('id', [793,794,795,796,797,798,799,800])->inRandomOrder()->first();

                break;
            case 'paragon':
                if ($object->ubeast) {
                    return $this->pokemon->whereIn('id', [793,794,795,796,797,798,799])->inRandomOrder()->first();
                }

                return $this->pokemon->whereNotIn('id', [793,794,795,796,797,798,799,800])->inRandomOrder()->first();

                break;
        }

        return false;
    }

    /**
     * Generate pokemon gender
     *
     * @param $ratio
     * @return int
     */
    public function gender($ratio)
    {
        if ($ratio != 999) {
            return (random_int(1, 100) <= $ratio) ? 1 : 2;
        }

        return 0;
    }

    public function inDungeon($id)
    {
        return $this->pokemon->where('dungeon_id', $id)->orderBy('rarity')->get();
    }

    /**
     * Calculate initial stats
     *
     * @param $pokemon
     * @param $level
     * @return object
     */
    public function initialStats($pokemon, $level)
    {
        $array = [];
        $ratings = 0;
        $perfect = 0;
        $stats = ['hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed'];

        foreach ($stats as $attr) {
            $value = random_int($pokemon->{$attr} * .80, $pokemon->{$attr});
            ($attr == 'hp') ? $array[$attr] = round($value * 1.35) : $array[$attr] = $value;
            $ratings += $value;
            $perfect += $pokemon->{$attr};
        }

        $array['pokemon_id'] = $pokemon->id;
        $array['gender'] = $this->gender($pokemon->gender_ratio);
        $array['ratings'] = round(($ratings / $perfect) * 100);
        $array['level'] = $level;

        return (object)$array;
    }

    /**
     * Increment stats based on level
     *
     * @param $pokemon
     * @return mixed
     */
    public function incrementStats($pokemon)
    {
        $increase = (3 + (($pokemon->ratings - 80) / 2)) / 100;
        $hp = 0;
        $attack = 0;
        $defense = 0;
        $special_attack = 0;
        $special_defense = 0;
        $speed = 0;

        for ($i = 1; $i < $pokemon->level; $i++) {
            $hp += $pokemon->hp * $increase;
            $attack += $pokemon->attack * $increase;
            $defense += $pokemon->defense * $increase;
            $special_attack += $pokemon->special_attack * $increase;
            $special_defense += $pokemon->special_defense * $increase;
            $speed += $pokemon->speed * $increase;
        }

        $pokemon->hp += round($hp);
        $pokemon->attack += round($attack);
        $pokemon->defense += round($defense);
        $pokemon->special_attack += round($special_attack);
        $pokemon->special_defense += round($special_defense);
        $pokemon->speed += round($speed);

        return $pokemon;
    }

    public function starter($user, $type)
    {
        $object = (object)[
            'action' => 'starter',
            'rarity' => $this->randomizer() ?: 1,
            'type' => $type
        ];

        $data = $this->incrementStats($this->initialStats($this->generate($object), 15));
        $data->user_id = $user;
        $data->in_team = 1;

        $data = (array)$data;

        $starter = $this->userPokemon->store($data);

        $moves = $this->move->generate(
            array_filter([$starter->info->primary_type, $starter->info->secondary_type], function ($var) {
                return !is_null($var);
            }), 5);

        foreach ($moves as $move) {
            $starter->moves()->attach($move);
        }
    }

    public function rewardPokemon($rarity, $id = 'catch')
    {
        $object = (object)[
            'action' => 'reward',
            'rarity' => ($rarity != 0) ? $rarity : $this->randomizer(),
            'id' => $id
        ];

        $data = $this->incrementStats($this->initialStats($this->generate($object), 5));
        $data->user_id = auth()->user()->id;

        $data = (array)$data;

        $starter = $this->userPokemon->store($data);

        $moves = $this->move->generate(
            array_filter([$starter->info->primary_type, $starter->info->secondary_type], function ($var) {
                return !is_null($var);
            }), 5);

        foreach ($moves as $move) {
            $starter->moves()->attach($move);
        }

        $this->checkPokedex($starter->info->id);

        return $starter->info->name;
    }

    public function save($pokemon, $user)
    {
        $pokemon = ($pokemon['id']) ? array_except($pokemon, 'id') : $pokemon;

        $pokemon['user_id'] = $user;
        if ($this->userPokemon->team()->count() < 6) $pokemon['in_team'] = 1;

        $saved = $this->userPokemon->store($pokemon);

        foreach ($pokemon['moves'] as $move) {
            $saved->moves()->attach($move);
        }

        $this->checkPokedex($pokemon['info']->id);
    }

    public function checkPokedex($id, $user = null)
    {
        $userDex = ($user) ? auth()->user()->findOrFail($user) : auth()->user();
        $pokedex = explode(',', $userDex->pokedex);

        if (!in_array($id, $pokedex) && $id <= 802) {
            $newDex = implode(',',array_prepend($pokedex, $id));

            $userDex->pokedex = $newDex;
            $userDex->save();
        }

        return false;
    }

    public function paragonEncounter($difficulty)
    {
        $object = (object)[
            'action' => 'paragon',
            'ubeast' => (session('paragon.progress')%50 == 0) ? true : false
        ];

        $wild = $this->generate($object);

        $data = (array)$this->incrementStats($this->initialStats($wild, $difficulty));
        $data['id'] = 1;
        $data['info'] = $wild;

        $data['moves'] = $this->move->generate(
            array_filter([$wild->primary_type, $wild->secondary_type], function ($var) {
                return !is_null($var);
            }), $data['level']);

        return $data;
    }

    public function wildEncounter($type = null)
    {
        $object = (object)[
            'action' => 'wild',
            'dungeon' => session('dungeon.info')->id,
            'rarity' => (!is_numeric(session('dungeon.progress'))) ? $this->eliteRandomizer() : $this->randomizer()
        ];

        if (!is_numeric(session('dungeon.progress'))) {
            $level = random_int(session('dungeon.difficulty') - 10, session('dungeon.difficulty'));
        } else {
            $level = random_int(session('dungeon.difficulty') - 17, session('dungeon.difficulty'));
        }

        if ($level > 100) $level = 100;

        $wild = $this->generate($object);

        if (!$wild) return back();

        $data = (array)$this->incrementStats($this->initialStats($wild, $level));
        $data['id'] = 1;
        $data['info'] = $wild;

        $data['moves'] = $this->move->generate(
            array_filter([$wild->primary_type, $wild->secondary_type], function ($var) {
                return !is_null($var);
            }), $data['level']);

        session(['wild' => $data]);

        return true;
    }

    public function evolve($pokemon, $method)
    {
        $object = (object)[
            'action' => 'evolution',
            'id' => $pokemon->pokemon_id,
            'method' => $method
        ];

        $data = (array)$this->incrementStats($this->initialStats($this->generate($object), $pokemon->level));
        $data['gender'] = $pokemon->gender;
        $data['vitamins_used'] = 0;

        $pokemon->update($data);

        $this->checkPokedex($pokemon->pokemon_id);

        return $pokemon->info->name;
    }

    public function changeForm($pokemon, $form)
    {
        $data = (array)$this->incrementStats($this->initialStats($form, $pokemon->level));
        $data['gender'] = $pokemon->gender;
        $data['vitamins_used'] = 0;

        $pokemon->update($data);

        return true;
    }

    public function evolution($id)
    {
        return $this->pokemon->where('evolution_from', $id)->get();
    }

    public function forms($id)
    {
        return $this->pokemon->where('alternate_form', $id)->get();
    }

    public function updateDungeons()
    {
        $dungeons = $this->dungeon->randomAll();

        $this->pokemon->where('dungeon_id', '!=', 0)->update(['dungeon_id' => 0]);

        foreach ($dungeons as $dungeon) {
            $this->pokemon->where('rarity', 1)
                ->whereIn('primary_type', explode(',', $dungeon->types))
                ->inRandomOrder()->limit(random_int(7,10))->update(['dungeon_id' => $dungeon->id]);

            $this->pokemon->where('rarity', 2)
                ->where('name', 'NOT LIKE', '%(Mega%')
                ->whereIn('primary_type', explode(',', $dungeon->types))
                ->inRandomOrder()->limit(random_int(4,7))->update(['dungeon_id' => $dungeon->id]);

            $this->pokemon->where('rarity', 3)
                ->where('name', 'NOT LIKE', '%(Mega%')
                ->where('name', 'NOT LIKE', '%(Ash%')
                ->whereIn('primary_type', explode(',', $dungeon->types))
                ->inRandomOrder()->limit(random_int(3,7))->update(['dungeon_id' => $dungeon->id]);

            $this->pokemon->where('alternate_form', 0)
                ->where('rarity', 4)
                ->whereNotIn('id', [793,794,795,796,797,798,799])
                ->whereIn(random_int(1,2) == 1 ? 'primary_type' : 'secondary_type', explode(',', $dungeon->types))
                ->inRandomOrder()->limit(random_int(1,2))->update(['dungeon_id' => $dungeon->id]);

            $this->pokemon->where('alternate_form', 0)
                ->where('rarity', 5)
                ->whereNotIn('id', [790,791,792,800])
                ->whereIn(random_int(1,2) == 1 ? 'primary_type' : 'secondary_type', explode(',', $dungeon->types))
                ->inRandomOrder()->limit(random_int(1,2))->update(['dungeon_id' => $dungeon->id]);

            $this->dungeon->updateDistance($dungeon->id, random_int(40,70));
        }
    }

    public function show($id)
    {
        return $this->pokemon->select(['hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed'])->findOrFail($id);
    }

    public function pokedex()
    {
        return $this->pokemon->where('alternate_form', 0)->select(['id', 'name', 'primary_type', 'secondary_type'])->simplePaginate(10);
    }

    public function pokedexSearch($keyword)
    {
        return $this->pokemon->where('name', 'LIKE', "%{$keyword}%")->select(['id', 'name', 'primary_type', 'secondary_type'])->limit(15)->get();
    }

    public function typeEffects($type, $type2 = null)
    {
        $weak = [];
        $checks = ['normal', 'fire', 'fighting', 'water', 'flying', 'grass', 'poison', 'electric', 'ground', 'psychic', 'rock', 'ice', 'bug', 'dragon', 'ghost', 'dark', 'steel', 'fairy'];


        foreach ($checks as $list) {
            $weak[$list] = $this->typeModifiers($list, array_filter([$type, $type2], function ($var) {
                return !is_null($var);
            }));
        }

        return $weak;
    }

    public function typeModifiers($list, $defender)
    {
        $modifier = 1;

        foreach ($defender as $type) {
            if ($this->checkStrength($list, $type)) {

                $modifier *= 2;
            } elseif ($this->checkWeakness($list, $type)) {

                $modifier /= 2;
            } elseif ($this->checkNull($list, $type)) {

                $modifier = 0;
            }
        }

        return $modifier;
    }

    /**
     * Check weakness moves
     *
     * @param $offense
     * @param $defense
     * @return bool
     */
    public function checkWeakness($offense, $defense)
    {
        switch ($offense) {
            case 'fire': if (in_array($defense, ['dragon', 'fire', 'rock', 'water'])) return true; break;
            case 'normal': if (in_array($defense, ['rock', 'steel'])) return true; break;
            case 'fighting': if (in_array($defense, ['bug', 'fairy', 'flying', 'poison', 'psychic'])) return true; break;
            case 'water': if (in_array($defense, ['dragon', 'grass', 'water'])) return true; break;
            case 'flying': if (in_array($defense, ['electric', 'rock', 'steel'])) return true; break;
            case 'grass': if (in_array($defense, ['bug', 'dragon', 'fire', 'flying', 'grass', 'poison', 'steel'])) return true; break;
            case 'poison': if (in_array($defense, ['poison', 'ground', 'rock', 'ghost'])) return true; break;
            case 'steel': if (in_array($defense, ['electric', 'fire', 'steel', 'water'])) return true; break;
            case 'electric': if (in_array($defense, ['dragon', 'electric', 'grass'])) return true; break;
            case 'psychic': if (in_array($defense, ['psychic', 'steel'])) return true; break;
            case 'rock': if (in_array($defense, ['fighting', 'ground', 'steel'])) return true; break;
            case 'fairy': if (in_array($defense, ['fire', 'poison', 'steel'])) return true; break;
            case 'dark': if (in_array($defense, ['dark', 'fairy', 'fighting'])) return true; break;
            case 'ice': if (in_array($defense, ['fire', 'ice', 'steel', 'water'])) return true; break;
            case 'ground': if (in_array($defense, ['bug', 'grass'])) return true; break;
            case 'bug': if (in_array($defense, ['fairy', 'fighting', 'fire', 'flying', 'ghost', 'poison', 'steel'])) return true; break;
            case 'dragon': if ($defense == 'steel') return true; break;
            case 'ghost': if ($defense == 'dark') return true; break;
        }

        return false;
    }

    /**
     * Check strength moves
     *
     * @param $offense
     * @param $defense
     * @return bool
     */
    public function checkStrength($offense, $defense)
    {
        switch ($offense) {
            case 'fire': if (in_array($defense, ['bug', 'grass', 'ice', 'steel'])) return true; break;
            case 'fighting': if (in_array($defense, ['dark', 'ice', 'normal', 'rock', 'steel'])) return true; break;
            case 'water': if (in_array($defense, ['fire', 'ground', 'rock'])) return true; break;
            case 'flying': if (in_array($defense, ['bug', 'fighting', 'grass'])) return true; break;
            case 'grass': if (in_array($defense, ['ground', 'rock', 'water'])) return true; break;
            case 'fairy': if (in_array($defense, ['dark', 'dragon', 'fighting'])) return true; break;
            case 'steel': if (in_array($defense, ['fairy', 'ice', 'rock'])) return true; break;
            case 'poison': if (in_array($defense, ['fairy', 'grass'])) return true; break;
            case 'electric': if (in_array($defense, ['flying', 'water'])) return true; break;
            case 'psychic': if (in_array($defense, ['fighting', 'poison'])) return true; break;
            case 'ghost': if (in_array($defense, ['ghost', 'psychic'])) return true; break;
            case 'dark': if (in_array($defense, ['ghost', 'psychic'])) return true; break;
            case 'rock': if (in_array($defense, ['bug', 'fire', 'flying', 'ice'])) return true; break;
            case 'ice': if (in_array($defense, ['dragon', 'flying', 'grass', 'ground'])) return true; break;
            case 'bug': if (in_array($defense, ['dark', 'grass', 'psychic'])) return true; break;
            case 'ground': if (in_array($defense, ['electric', 'fire', 'poison', 'rock', 'steel'])) return true; break;
            case 'dragon': if ($defense == 'dragon') return true; break;
        }

        return false;
    }

    /**
     * Check null moves
     *
     * @param $offense
     * @param $defense
     * @return bool
     */
    public function checkNull($offense, $defense)
    {
        switch($offense) {
            case 'normal': if ($defense == 'ghost') return true; break;
            case 'ghost': if ($defense == 'normal') return true; break;
            case 'fighting': if ($defense == 'ghost') return true; break;
            case 'poison': if ($defense == 'steel') return true; break;
            case 'electric': if ($defense == 'ground') return true; break;
            case 'psychic': if ($defense == 'dark') return true; break;
            case 'dragon': if ($defense == 'fairy') return true; break;
            case 'ground': if ($defense == 'flying') return true; break;
        }

        return false;
    }
}
