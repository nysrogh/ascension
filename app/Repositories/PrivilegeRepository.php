<?php
namespace App\Repositories;

use App\Contracts\PrivilegeInterface;
use App\Privilege;

class PrivilegeRepository extends Repository implements PrivilegeInterface
{
    protected $privilege;

    public function __construct(Privilege $privilege)
    {
        parent::__construct($privilege);

        $this->privilege = $privilege;
    }

    public function available()
    {
        return $this->privilege->with(['club' => function ($q) {
                $q->where('club_id', auth()->user()->club_id);
            }])
            ->where('level', '<=', auth()->user()->club->level)
            ->paginate(10);
    }

}