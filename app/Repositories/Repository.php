<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 8:53 AM
 */

namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use Carbon\Carbon;

abstract class Repository implements RepositoryInterface
{
    /**
     * Main repository model
     *
     * @var $model
     */
    protected $model;

    /**
     * Create a new repository instance.
     *
     * @param object $model Main repository model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Get all resources in the storage.
     *
     * @return json array
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Create pagination for the resources.
     *
     * @param  integer $length
     * @return json array
     */
    public function paginate($length = 15)
    {
        return $this->model->paginate($length);
    }

    /**
     * Find the resource using the specified id or else fail.
     *
     * @param  int $id
     * @return json object
     */
    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * Store the data in storage.
     *
     * @param  array $request The data that is posted.
     * @return boolean
     */
    public function store($data)
    {
        return $this->model->create($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  json object $request
     * @param  int $id
     * @return boolean
     */
    public function update($request, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->fill($request->all());

        return $model->save();
    }

    /**
     * Remove the specified resource from the storage.
     *
     * @param  int $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * Search the specified data from the storage.
     *
     * @param  string $column
     * @param  string $key
     * @return boolean
     */
    public function search($column, $key)
    {
        return $this->model->where($column, 'LIKE', $key . '%')->limit(15)->get();
    }

    public function flashError($message)
    {
        session()->flash('error', $message);
    }

    public function flashSuccess($message)
    {
        session()->flash('success', $message);
    }

    public function saveSession($code, $array)
    {
        foreach ($array as $key => $value)
        {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    session()->put("{$code}.{$key}.{$k}", $v);
                }
            } else {
                if (in_array($key, ['battle_log', 'participants'])) {
                    session()->push("{$code}.{$key}", $value);
                } else {
                    session()->put("{$code}.{$key}", $value);
                }
            }
        }

        session()->save();

        return true;
    }

    public function doubleClicked($prevent)
    {
        if (session('prevent') == $prevent) {
            return true;
        }

        session(['prevent' => $prevent]);

        return false;
    }

    public function tips($category)
    {
        $message = json_decode(file_get_contents(storage_path()."/json/{$category}.json"), true);

        return $message[random_int(0, count($message) - 1)]['message'];

    }

    /**
     * Generate next level exp
     *
     * @param $level
     * @return float
     */
    public function nextLevel($level)
    {
        return ($level < 100) ? ceil(pow($level, 1.60) * 14.975) : ceil(pow($level, 2.22) * 40);
    }
}
