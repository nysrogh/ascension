<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\ItemInterface;
use App\Contracts\MoveInterface;
use App\Contracts\PokemonInterface;
use App\Contracts\RewardInterface;
use App\Contracts\UserItemInterface;
use App\Reward;
use Carbon\Carbon;

class RewardRepository extends Repository implements RewardInterface
{
    protected $reward;
    protected $item;
    protected $userItem;
    protected $pokemon;
    protected $move;

    public function __construct(
        Reward $reward,
        ItemInterface $item,
        UserItemInterface $userItem,
        PokemonInterface $pokemon
    )
    {
        parent::__construct($reward);

        $this->reward = $reward;
        $this->item = $item;
        $this->userItem = $userItem;
        $this->pokemon = $pokemon;
    }

    /**
     * Retrieve all rewards for user
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all()
    {
        return $this->reward->with(['modifiers.item' => function ($q) {
            $q->select(['id']);
        }, 'user' => function ($q) {
            $q->where('id', auth()->user()->id)->select('id');
        }])->whereHas('user', function ($q) {
            $q->where('id', auth()->user()->id)->select('id');
        })->select(['id', 'title', 'description', 'pokedex', 'dungeon', 'mission', 'hours'])->get();
    }

    public function find($id)
    {
        return $this->reward->with(['modifiers.item' => function ($q) {
            $q->select(['id']);
        }, 'user' => function ($q) {
            $q->where('id', auth()->user()->id)->select('id');
        }])->whereHas('user', function ($q) {
            $q->where('id', auth()->user()->id)->select('id');
        })->select(['id', 'dungeon', 'pokedex', 'mission', 'next_reward', 'hours'])->findOrFail($id);

    }

    /**
     * Time left for a reward to be able to claim again
     *
     * @param $started
     * @return string
     */
    public function timeLeft($started)
    {
        $started = new Carbon($started);

        return Carbon::now() < $started ? $started->diffForHumans() : 'claim';
    }

    public function claim($reward)
    {
        foreach ($reward->modifiers as $modifier) {
            if ($modifier->attribute == 'item') {
                if ($modifier->item_id) {
                    $item = $this->userItem->addOrUpdateItem((object)['id' => $modifier->item_id], $modifier->value);
                } else {
                    $item = $this->userItem->addOrUpdateItem($this->item->randomChestItem(random_int(1,3), 0, ['name', ['Master Ball']]), $modifier->value);
                }

                session()->push('rewards', "Received item: {$item} x{$modifier->value}<br>");
            } elseif ($modifier->attribute == 'pokemon') {
                $name = $this->pokemon->rewardPokemon($modifier->rarity);
                session()->push('rewards', "Received pokemon: {$name}<br>");
            } elseif ($modifier->attribute == 'treat') {

                if (random_int(1,1000) <= 120) {
                    $id = 126;
                } elseif (random_int(1,1000) <= 30) {
                    $id = 173;
                } else {
                    $id = random_int(109,110);
                }

                $item = $this->userItem->addOrUpdateItem((object)['id' => $id], 1);

                session()->push('rewards', "Received candy treat: {$item} x1<br>");
            } else {
                auth()->user()->{$modifier->attribute} += $modifier->value;
                session()->push('rewards', "Received coins: +{$modifier->value}<br>");
            }
        }

        if ($reward->next_reward) {
            auth()->user()->rewards()->detach($reward->id);
            auth()->user()->rewards()->attach($reward->next_reward);
        }

        if ($reward->hours) {
            $reward->user[0]->pivot->claim = Carbon::now()->addHours($reward->hours);
            $reward->user[0]->pivot->save();
        }

        auth()->user()->save();
    }
}
