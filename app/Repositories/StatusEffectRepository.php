<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\StatusEffectInterface;
use App\UserPokemon;
use Carbon\Carbon;

class StatusEffectRepository extends Repository implements StatusEffectInterface
{
    public function __construct(UserPokemon $userPokemon)
    {
        parent::__construct($userPokemon);

        $this->userPokemon = $userPokemon;
    }

    /**
     * Check existing statuses
     *
     * @param $key
     * @param $id
     * @return bool
     */
    public function check($key, $id)
    {
        if (!session("tmp.{$key}_{$id}")) return true;

        foreach (session("tmp.{$key}_{$id}") as $status => $duration) {
            if (in_array($status, ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) {
                if (!$this->{$status}($key, $id, $duration)) return false;
            }
        }

        return true;
    }

    /**
     * Inflict burn damage
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function burn($target, $id, $duration)
    {
        //check duration
        if ($duration <= 0) {
            session()->forget("tmp.{$target}_{$id}.burn");

            return true;
        }

        //calculate damage
        $damage = round(session("battle.{$target}.stats")->hp * .05);

        //deduct hp
        $array[$target]["current_hp_{$id}"] = max(session("battle.{$target}.current_hp_{$id}") - $damage, 0);
        //if ($array[$target]["{$target}_current_hp_{$id}"] < 1) $array[$target]["{$target}_current_hp_{$id}"] = 1;

        //deduct duration
        $array2["{$target}_{$id}"]['burn'] = session("tmp.{$target}_{$id}.burn") - 1;

        //push battle log
        session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." took ". $damage ." damage from burn!");

        //save session
        $this->saveSession('battle', $array);
        $this->saveSession('tmp', $array2);

        return true;
    }

    /**
     * Inflict poison damage
     *
     * @param $target
     * @param $id
     * @return bool
     */
    public function poison($target, $id)
    {
        $damage = round(session("battle.{$target}.stats")->hp * .02);

        $array[$target]["current_hp_{$id}"] = max(session("battle.{$target}.current_hp_{$id}") - $damage, 0);
        //if ($array[$target]["{$target}_current_hp_{$id}"] < 1) $array[$target]["{$target}_current_hp_{$id}"] = 1;

        session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." took ". $damage ." damage from poison!");

        $this->saveSession('battle', $array);

        return true;
    }

    /**
     * Inflict confuse
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function confuse($target, $id, $duration)
    {
        //check duration
        if ($duration <= 0) {
            session()->forget("tmp.{$target}_{$id}.confuse");
            session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." has recovered from confusion!");

            return true;
        }

        if (random_int(1, 100) > 40) {
            $array2["{$target}_{$id}"]['confuse'] = session("tmp.{$target}_{$id}.confusion") - 1;
            $this->saveSession('tmp', $array2);

            return true;
        }

        $damage = round(session("battle.{$target}.stats")->attack * .10);

        $array[$target]["current_hp_{$id}"] = max(session("battle.{$target}.current_hp_{$id}") - $damage, 0);
        if ($array[$target]["current_hp_{$id}"] < 1) $array[$target]["current_hp_{$id}"] = 1;

        $array2["{$target}_{$id}"]['confuse'] = session("tmp.{$target}_{$id}.confusion") - 1;

        session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." hurt itself in confusion and took ". $damage ." damage!");

        $this->saveSession('battle', $array);
        $this->saveSession('tmp', $array2);

        return false;
    }

    /**
     * Inflict freeze
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function freeze($target, $id, $duration)
    {
        if ($duration <= 0 || session("tmp.{$target}_{$id}.burn")) {
            session()->forget("tmp.{$target}_{$id}.freeze");

            session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." has thawed!");

            return true;
        }

        session()->push('battle.battle_log', "". session("battle.{$target}.stats")->info->name ." is frozen solid!");

        $array["{$target}_{$id}"]['freeze'] = session("tmp.{$target}_{$id}.freeze") - 1;

        $this->saveSession('tmp', $array);

        return false;
    }

    /**
     * Inflict sleep
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function sleep($target, $id, $duration)
    {
        if ($duration <= 0) {
            session()->forget("tmp.{$target}_{$id}.sleep");
            session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." woke up!");

            return true;
        }

        session()->push('battle.battle_log', "". session("battle.{$target}.stats")->info->name ." is fast asleep!");

        $array["{$target}_{$id}"]['sleep'] = session("tmp.{$target}_{$id}.sleep") - 1;

        $this->saveSession('tmp', $array);

        return false;
    }

    /**
     * Inflict paralyze
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function paralyze($target, $id, $duration)
    {
        if ($duration <= 0) {
            $array["{$target}_{$id}"]['speed'] = round(session("tmp.{$target}_{$id}.speed") + (session("battle.{$target}.stats")->speed / 2));
            $this->saveSession('tmp', $array);

            session()->forget("tmp.{$target}_{$id}.paralyze");
            session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." has recovered from paralysis!");

            return true;
        }

        if (random_int(1,100) > 30) {
            $array["{$target}_{$id}"]['paralyze'] = session("tmp.{$target}_{$id}.paralyze") - 1;
            $this->saveSession('tmp', $array);

            return true;
        }

        session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." is paralyzed. It can't move!");

        $array["{$target}_{$id}"]['paralyze'] = session("tmp.{$target}_{$id}.paralyze") - 1;
        $this->saveSession('tmp', $array);

        return false;
    }

    /**
     * Inflict infatuate
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function infatuate($target, $id, $duration)
    {
        if ($duration <= 0) {
            session()->forget("tmp.{$target}_{$id}.infatuate");
            session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." has recovered from infatuation!");

            return true;
        }

        if (random_int(1, 100) > 50) {
            $array["{$target}_{$id}"]['infatuate'] = session("tmp.{$target}_{$id}.infatuate") - 1;
            $this->saveSession('tmp', $array);

            return true;
        }

        session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." is in love with the target!");

        $array["{$target}_{$id}"]['infatuate'] = session("tmp.{$target}_{$id}.infatuate") - 1;
        $this->saveSession('tmp', $array);

        return false;
    }

    /**
     * Inflict flinch
     *
     * @param $target
     * @param $id
     * @param $duration
     * @return bool
     */
    public function flinch($target, $id, $duration)
    {
        if ($duration <= 0) {
            session()->forget("tmp.{$target}_{$id}.flinch");

            return true;
        }

        session()->push('battle.battle_log', "".session("battle.{$target}.stats")->info->name ." is stunned and couldn't move!");

        $array["{$target}_{$id}"]['flinch'] = session("tmp.{$target}_{$id}.flinch") - 1;

        $this->saveSession('tmp', $array);

        return false;
    }

    /**
     * Initialize burn status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictBurn($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        if ($this->immunity($arr[1], 'fire', 'Burn')) return false;

        $array[$arr[1]] = [
            $arr[2] => random_int(3,5)
        ];

        session()->push('battle.battle_log', "<span style='color:red'>The target is burned!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize poison status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictPoison($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        if ($this->immunity($arr[1], 'poison', 'Poison')) return false;
        if ($this->immunity($arr[1], 'steel', 'Poison')) return false;

        $array[$arr[1]] = [
            $arr[2] => 99
        ];

        session()->push('battle.battle_log', "<span style='color:purple'>The target is poisoned!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize confuse status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictConfuse($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        $array[$arr[1]] = [
            $arr[2] => random_int(3,5)
        ];

        session()->push('battle.battle_log', "<span style='color:darkorange'>The target is confused!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize freeze status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictFreeze($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        if ($this->immunity($arr[1], 'ice', 'Freeze')) return false;

        $array[$arr[1]] = [
            $arr[2] => random_int(3,5)
        ];

        session()->push('battle.battle_log', "<span style='color:#0275d8'>The target is frozen solid!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize paralyze status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictParalyze($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        $array[$arr[1]] = [
            $arr[2] => random_int(3,5)
        ];

        $pokemon = session("battle.".preg_replace("/_[0-9]+/", "", $arr[1]).".stats");

        $array2[$arr[1]]['speed'] = round(session("tmp.{$arr[1]}.speed") - ($pokemon->speed / 2));

        session()->push('battle.battle_log', "<span style='color:orange'>The target is paralyzed!</span>");

        $this->saveSession($arr[0], $array);
        $this->saveSession('tmp', $array2);

        return true;
    }

    /**
     * Initialize sleep status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictSleep($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        $array[$arr[1]] = [
            $arr[2] => random_int(1,5)
        ];

        session()->push('battle.battle_log', "<span style='color:#110069'>The target has fell asleep!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize infatuate status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictInfatuate($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        if (session("battle.enemy.stats")->gender == session("battle.pokemon.stats")->gender) {
            session()->push('battle.battle_log', "Infatuate status doesn't affect same gender target!");

            return false;
        }

        $arr = explode('.', $target);

        $array[$arr[1]] = [
            $arr[2] => random_int(2,5)
        ];

        session()->push('battle.battle_log', "<span style='color:#ff00aa'>The target is in love!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize flinch status
     *
     * @param $target
     * @param $chance
     * @return bool
     */
    public function inflictFlinch($target, $chance)
    {
        if (session($target)) return false;
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);

        $array[$arr[1]] = [
            $arr[2] => 1
        ];

        session()->push('battle.battle_log', "<span style='color:#75453a'>The target flinched!</span>");

        $this->saveSession($arr[0], $array);

        return true;
    }

    /**
     * Initialize buff effects
     *
     * @param $target
     * @param $value
     * @param $chance
     * @return bool
     */
    public function buffEffect($target, $value, $chance)
    {
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);
        $pokemon = session("battle.".preg_replace("/_[0-9]+/", "", $arr[1]).".stats");

        foreach (explode(',', $arr[2]) as $attr) {
            if (session($target) && session($target) >= $pokemon->{$attr}) return false;

            $array[$arr[1]][$attr] = round(session($target) + (($pokemon->{$attr} * $value) / 100));

            session()->push('battle.battle_log', "<span class='text-info'>{$pokemon->info->name}'s ". ucwords(preg_replace('/_/', ' ', $attr)) ." rose!</span>");
        }

        $this->saveSession('tmp', $array);

        return true;
    }

    public function debuffEffect($target, $value, $chance)
    {
        if (random_int(1, 100) > $chance) return false;

        $arr = explode('.', $target);
        $pokemon = session("battle.".preg_replace("/_[0-9]+/", "", $arr[1]).".stats");

        foreach (explode(',', $arr[2]) as $attr) {
            if (session($target) && session($target) >= (0 - $pokemon->{$attr})) return false;

            $array[$arr[1]][$attr] = round(session($target) - (($pokemon->{$attr} * $value) / 100));

            session()->push('battle.battle_log', "<span class='text-info'>{$pokemon->info->name}'s ". ucwords(preg_replace('/_/', ' ', $attr)) ." is lowered!</span>");
        }

        $this->saveSession('tmp', $array);

        return true;
    }

    public function recoil($target, $value)
    {
        $arr = explode('.', $target);

        $pregTarget = preg_replace("/_[0-9]+/", "", $arr[1]);
        $pokemon = session("battle.{$pregTarget}.stats");

        if ($arr[2] == 'hp') {

            $damage = ($pokemon->hp * $value) / 100;

            session()->push('battle.battle_log', $pokemon->info->name ." received ". round(max($damage, 0)) ." damage from recoil!");

            $array[$pregTarget]["current_hp_{$pokemon->id}"] = max(session("battle.{$pregTarget}.current_hp_{$pokemon->id}") - round(max($damage, 0)), 0);

            $this->saveSession('battle', $array);

            return true;
        }

        //if (session($target) && session($target) >= (0 - $pokemon->{$arr[2]})) return false;

        $array[$arr[1]][$arr[2]] = round(session($target) - (($pokemon->{$arr[2]} * $value) / 100));

        session()->push('battle.battle_log', "<span class='text-info'>{$pokemon->info->name}'s ". ucwords(preg_replace('/_/', ' ', $arr[2])) ." is lowered from recoil!</span>");

        $this->saveSession('tmp', $array);

        return true;
    }

    /**
     * Check for status immunity
     *
     * @param $target
     * @param $type
     * @param $effect
     * @return bool
     */
    public function immunity($target, $type, $effect)
    {
        $pokemon = session("battle.".preg_replace("/_[0-9]+/", "", $target.".stats"));

        if ($pokemon->info->primary_type == $type || $pokemon->info->secondary_type == $type) {
            session()->push('battle.battle_log', "<span>{$effect} status doesn't affect the target!</span>");

            return true;
        }

        return false;
    }

    public function heal($key, $id, $value, $modifier)
    {
        $hp = session("battle.{$key}.current_hp_{$id}");

        if ($hp <= 0) {
            session()->push('battle.battle_log', "<span class='text-info'>Has no effect on Pokemon!</span>");

            return true;
        }

        if ($modifier == 'daylight') {
            $now = Carbon::now();
            $from = Carbon::parse('2017-01-01 06:00:00');
            $to = Carbon::parse('2017-01-01 18:00:00');

            if ($now->hour < $from->hour && $now->hour > $to->hour) return false;

            $array[$key]["current_hp_{$id}"] = round($hp + (session("battle.{$key}.stats")->hp * ($value / 100)));

            if ($array[$key]["current_hp_{$id}"] > session("battle.{$key}.stats")->hp) {
                $array[$key]["current_hp_{$id}"] = session("battle.{$key}.stats")->hp;
            }

            session()->push('battle.battle_log', "<span class='text-success'>".session("battle.{$key}.stats")->info->name . " has absorbed the sunlight and recovered its HP!</span>");

        } elseif ($modifier == 'fixed') {
            $array[$key]["current_hp_{$id}"] = $hp + $value;

            if ($array[$key]["current_hp_{$id}"] > session("battle.{$key}.stats")->hp) {
                $array[$key]["current_hp_{$id}"] = session("battle.{$key}.stats")->hp;
            }

            session()->push('battle.battle_log', "<span class='text-success'>".session("battle.{$key}.stats")->info->name ." has recovered its HP!</span>");
        } else {
            $array[$key]["current_hp_{$id}"] = round($hp + (session("battle.{$key}.stats")->hp * ($value / 100)));

            if ($array[$key]["current_hp_{$id}"] > session("battle.{$key}.stats")->hp) {
                $array[$key]["current_hp_{$id}"] = session("battle.{$key}.stats")->hp;
            }

            session()->push('battle.battle_log', "<span class='text-success'>".session("battle.{$key}.stats")->info->name ." has recovered its HP!</span>");
        }

        $this->saveSession('battle', $array);

        return true;
    }

    public function secondaryEffects($obj)
    {
        $attr = explode('-', $obj->attribute);

        if (in_array($attr[0], ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) {
            $method = "inflict".ucfirst($attr[0]);

            $result = $this->{$method}("tmp.{$obj->defKey}_{$obj->defender->id}.{$obj->attribute}", $obj->value);

            if (!$result) session()->push('battle.battle_log', "But inflicting {$attr[0]} status failed!");
        }

        if (in_array($attr[0], ['increase', 'decrease', 'recoil', 'recover', 'heal'])) {
            foreach (explode(',', $attr[1]) as $stat) {
                switch ($attr[0]) {
                    case 'increase':
                        $result = $this->buffEffect("tmp.{$obj->key}_{$obj->attacker->id}.{$stat}", $obj->value, $obj->chance);

                        if (!$result) session()->push('battle.battle_log', "But ".ucwords(preg_replace('/_/', ' ', $stat))." increase failed!");
                    break;

                    case 'decrease':
                        $result = $this->debuffEffect("tmp.{$obj->defKey}_{$obj->defender->id}.{$stat}", $obj->value, $obj->chance);

                        if (!$result) session()->push('battle.battle_log', "But ".ucwords(preg_replace('/_/', ' ', $stat))." decrease failed!");
                    break;

                    case 'recoil':
                        if (in_array($attr[1], ['burn', 'poison', 'confuse', 'paralyze', 'freeze', 'sleep', 'infatuate', 'flinch'])) {
                            $method = "inflict".ucfirst($attr[1]);
                            $result = $this->{$method}("tmp.{$obj->key}_{$obj->attacker->id}.{$attr[1]}", $obj->chance);

                            if (!$result) session()->push('battle.battle_log', "Status condition {$attr[0]} failed!");
                        } else {
                            $result = $this->recoil("tmp.{$obj->key}_{$obj->attacker->id}.{$stat}", $obj->value);

                            if (!$result) session()->push('battle.battle_log', ucwords(preg_replace('/_/', ' ', $stat))." reduction from recoil failed!");
                        }
                    break;

                    case 'recover':
                        $statuses = session("tmp.{$obj->key}_{$obj->attacker->id}") ?? [];
                        $toRecover = explode(',',$attr[1]) ?? [];
                        $result = false;

                        foreach (array_keys($statuses) as $status) {
                            if (in_array($status, $toRecover)) {
                                session()->forget("tmp.{$obj->key}_{$obj->attacker->id}.{$status}");

                                $result = $status;
                            }
                        }

                        if ($result) session()->push('battle.battle_log', "{$obj->attacker->info->name} has recovered from {$result}!");
                        if (!$result) session()->push('battle.battle_log', "Status condition recovery failed!");
                    break;

                    case 'heal':
                        $result = $this->heal($obj->key, $obj->attacker->id, $obj->value, $stat);

                        if (!$result) session()->push('battle.battle_log', "But it failed!");
                    break;
                }
            }
        }
    }
}