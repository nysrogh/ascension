<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\PokemonInterface;
use App\Contracts\TradeOfferInterface;
use App\Contracts\UserInterface;
use App\Contracts\UserItemInterface;
use App\TradeOffer;

class TradeOfferRepository extends Repository implements TradeOfferInterface
{
    protected $tradeOffer;
    protected $userItem;
    protected $user;
    protected $poke;

    public function __construct(TradeOffer $tradeOffer, UserItemInterface $userItem, UserInterface $user, PokemonInterface $poke)
    {
        parent::__construct($tradeOffer);
        $this->tradeOffer = $tradeOffer;
        $this->userItem = $userItem;
        $this->user = $user;
        $this->poke = $poke;
    }

    public function getOffers($id)
    {
        return $this->tradeOffer->where('trade_id', $id)->get();
    }

    public function cancel($offer, $id = null)
    {
        if ($offer->pokemon) {
            $offer->pokemon->decrement('in_trade');
        }

        if ($offer->item) {
            $this->userItem->addOrUpdateItem((object)['id' => $offer->item_id], 1, [$offer->primary_value, $offer->upgrade], $id);
        }

        if ($offer->gold) {
            $user = $this->user->findOrFail($id);

            $user->gold += $offer->gold;
            $user->save();
        }

        if ($offer->crystal) {
            $user = $this->user->findOrFail($id);

            $user->crystal += $offer->crystal;
            $user->save();
        }

        return false;
    }

    public function accept($offer)
    {
        if ($offer->pokemon) {
            $offer->pokemon->in_trade = 0;
            $offer->pokemon->in_shop = 0;
            $offer->pokemon->user_id = auth()->user()->id;
            $offer->pokemon->save();
        }

        if ($offer->item) {
            $this->userItem->addOrUpdateItem((object)['id' => $offer->item_id], 1, [$offer->primary_value, $offer->upgrade]);
        }

        if ($offer->gold) {
            auth()->user()->gold += $offer->gold;
            auth()->user()->save();
        }

        if ($offer->crystal) {
            auth()->user()->crystal += $offer->crystal;
            auth()->user()->save();
        }

        return false;
    }
}
