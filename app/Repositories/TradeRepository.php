<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\NotifInterface;
use App\Contracts\TradeInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use App\Trade;

class TradeRepository extends Repository implements TradeInterface
{
    protected $trade;
    protected $userPokemon;
    protected $userItem;
    protected $notif;

    public function __construct(Trade $trade, UserPokemonInterface $userPokemon, UserItemInterface $userItem, NotifInterface $notif)
    {
        parent::__construct($trade);
        $this->trade = $trade;
        $this->userPokemon = $userPokemon;
        $this->userItem = $userItem;
        $this->notif = $notif;
    }

    public function tradeExist($id)
    {
        return $this->trade->where('user_id', auth()->user()->id)->where('content_id', $id)->exists();
    }

    public function allTrades()
    {
        return $this->trade->where('user_id', auth()->user()->id)->latest()->get();
    }

    public function findTrade($id)
    {
        return $this->trade->where('content_id', $id)->where('user_id', auth()->user()->id)->firstOrFail();
    }

    public function saveOffer($offers)
    {
        $trade = $this->trade->create([
            'content_id' => session('trade'),
            'user_id' => auth()->user()->id
        ]);

        if ($trade->content->available > 1 || $trade->content->shop->user_id == auth()->user()->id) {
            $this->flashError("You cannot offer on items with more than 1 availability.");

            $trade->delete();

            return false;
        }

        foreach ($offers as $offer) {
            switch ($offer['img']) {
                case 'pokemon/thumbnail': $this->offerPokemon($offer['id'], $trade); break;
                case 'items': $this->offerItem($offer['id'], $trade); break;
                case 'game/perks-coins': $this->offerCoins($offer['details'], $trade); break;
                case 'game/perks-crystal': $this->offerCrystal($offer['details'], $trade); break;
            }
        }

        auth()->user()->gold -= 1000 * count(session('offers'));

        if (auth()->user()->gold <= 0) {
            auth()->user()->gold = 0;
        }

        auth()->user()->save();

        $this->notif->add($trade->content->shop->user_id, "Someone has made an offer on your store.");

        return true;
    }

    public function offerPokemon($id, $trade)
    {
        $pokemon = $this->userPokemon->find($id);

        if (!$pokemon || $pokemon->in_trade) return false;

        $trade->offers()->create([
            'user_pokemon_id' => $pokemon->id,
        ]);

        $pokemon->increment('in_trade');

        return false;
    }

    public function offerItem($id, $trade)
    {
        $item = $this->userItem->find($id);

        if (!$item) return false;

        if ($item->info->category == 4 && $item->info->section == 1) {
            $trade->offers()->create([
                'item_id' => $item->item_id,
                'primary_value' => $item->primary_value,
                'upgrade' => $item->upgrade
            ]);

            $item->delete();

            return false;

        }

        $trade->offers()->create([
            'item_id' => $item->item_id,
        ]);

        $item->quantity -= 1;

        if ($item->quantity <= 0) {
            $item->delete();
        } else {
            $item->save();
        }

        return false;
    }

    public function offerCoins($item, $trade)
    {
        if (auth()->user()->gold < $item) return false;

        $trade->offers()->create([
            'gold' => $item
        ]);

        auth()->user()->gold -= $item;

        if (auth()->user()->gold <= 0) {
            auth()->user()->gold = 0;
        }

        auth()->user()->save();

        return false;
    }

    public function offerCrystal($item, $trade)
    {
        if (auth()->user()->crystal < $item) return false;

        $trade->offers()->create([
            'crystal' => $item
        ]);

        auth()->user()->crystal -= $item;

        if (auth()->user()->crystal <= 0) {
            auth()->user()->crystal = 0;
        }

        auth()->user()->save();

        return false;
    }
}
