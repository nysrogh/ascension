<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\PokemonInterface;
use App\Contracts\UserItemInterface;
use App\Contracts\UserPokemonInterface;
use App\UserItem;
use Carbon\Carbon;

class UserItemRepository extends Repository implements UserItemInterface
{
    protected $userItem;

    public function __construct(UserItem $userItem, UserPokemonInterface $userPokemon)
    {
        parent::__construct($userItem);

        $this->userItem = $userItem;
        $this->userPokemon = $userPokemon;
    }

    /**
     * User bag sort by category
     *
     * @param $category
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function bag($category)
    {
        return $this->userItem->with('info')
            ->where('user_id', auth()->user()->id)
            ->where('is_held', 0)
            ->whereHas('info', function($query) use ($category) {
                $query->where('category', $category);
            })->latest()->paginate(10);
    }

    public function findOrigItem($id)
    {
        return $this->userItem->where('user_id', auth()->user()->id)->where('item_id', $id)->firstOrFail();
    }

    /**
     * Find evolution stone by name
     *
     * @param $name
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function findStone($name)
    {
        return $this->userItem->where('user_id', auth()->user()->id)
            ->whereHas('info', function($query) use ($name) {
                $query->where('name', ucwords(preg_replace('/-/', ' ', $name)));
            })->first();
    }

    /**
     * Primary ball modifier
     *
     * @param $pokeball
     * @return int
     */
    public function ballModifier($pokeball)
    {
        if ($pokeball->info->primary_attr == 'catch') return $pokeball->info->primary_value;

        return $this->options(explode(',', $pokeball->info->primary_attr));
    }

    public function addOrUpdateItem($item, $qty, $ups = null, $user = null)
    {
        if (!$ups) {
            $ups = [0,0];
        }

        $exists = $this->userItem->where('item_id', $item->id)->where('user_id', $user ?? auth()->user()->id)->first();

        if (!$exists) {
            $exists = $this->userItem->create([
                'user_id' => $user ?? auth()->user()->id,
                'item_id' => $item->id,
                'quantity' => $qty,
                'primary_value' => $ups[0],
                'upgrade' => $ups[1],
            ]);
            $ups[0];
        } else {
            if ($exists->info->category == 4 && $exists->info->section == 1) {
                $exists = $this->userItem->create([
                    'user_id' => $user ?? auth()->user()->id,
                    'item_id' => $item->id,
                    'quantity' => 1,
                    'primary_value' => $ups[0],
                    'upgrade' => $ups[1],
                ]);
            } else {
                $exists->increment('quantity', $qty);
            }
        }

        return $exists->info->name;
    }

    /**
     * @param $hold
     * @param $pokemon
     * @return bool
     */
    public function hold($hold, $pokemon)
    {
        $item = $this->userItem->where('id', $hold->id)->where('quantity', '>', 1)->first();

        $this->itemEffects($hold, $pokemon);

        if ($item) {
            $this->userItem->create([
                'user_id' => auth()->user()->id,
                'item_id' => $hold->item_id,
                'quantity' => 1,
                'is_held' => $pokemon
            ]);

            $item->decrement('quantity');
        } else {
            $this->userItem->where('id', $hold->id)->update(['is_held' => $pokemon]);
        }

        return true;
    }

    public function itemEffects($item, $poke, $rev = false)
    {
        //check effect
        if (in_array($item->info->secondary_attr, ['recoil', 'type', 'low-star'])) {
            switch ($item->info->secondary_attr) {
                case 'recoil': $this->itemEffectRecoil($poke, $item->info->primary_attr, $item->primary_value > 0 ? $item->primary_value : $item->info->primary_value, $rev);
                    break;
                case 'type': $this->itemEffectType($poke, $item->info->primary_attr, $item->primary_value > 0 ? $item->primary_value : $item->info->primary_value, $rev);
                    break;
                case 'low-star': $this->itemEffectLowStar($poke, $item->primary_value > 0 ? $item->primary_value : $item->info->primary_value, $rev);
                    break;
            }
        }

        return false;
    }

    //held item - [leftover]
    public function itemEffectTurnHeal($pokemon, $key, $area)
    {
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;
        $heal = round($pokemon->hp * ($val / 100));

        //limit to 120
        if ($heal > 120) {
            $heal = 120;
        }

        if (in_array($area, ['arena', 'friendly'])) {

            $array[$key]["{$key}_current_hp"] = session("{$area}.{$key}.{$key}_current_hp") + $heal;

            if ($array[$key]["{$key}_current_hp"] > session("{$area}.{$key}.stats")->hp) {
                $array[$key]["{$key}_current_hp"] = session("{$area}.{$key}.stats")->hp;
            }

            session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." has recovered its HP using Leftovers!</span>");

        } else {
            $array[$key]["current_hp_{$pokemon->id}"] = session("{$area}.{$key}.current_hp_{$pokemon->id}") + $heal;

            if ($array[$key]["current_hp_{$pokemon->id}"] > session("{$area}.{$key}.stats")->hp) {
                $array[$key]["current_hp_{$pokemon->id}"] = session("{$area}.{$key}.stats")->hp;
            }

            session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." has recovered its HP using Leftovers!</span>");
        }

        $this->saveSession($area, $array);

        return false;
    }

    //held item - [choice band,specs,scarf]
    public function itemEffectTurnSelected($pokemon, $move, $key, $area)
    {
        if (session()->has("{$area}.selected.{$pokemon->id}")) return false;
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        switch ($pokemon->item->info->name) {
            case 'Choice Band': $attr = 'attack'; break;
            case 'Choice Scarf': $attr = 'speed'; break;
            case 'Choice Specs': $attr = 'special_attack'; break;
        }

        $array["selected"][$pokemon->id] = $move;
        $array2["{$key}_{$pokemon->id}"][$attr] = round(session("tmp.{$key}_{$pokemon->id}.{$attr}") + ($pokemon->{$attr} * ($val / 100)));

        if (in_array($area, ['arena', 'friendly'])) {
            session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." has locked on using {$move->name}!</span>");

            session()->push("{$area}_log", "<span class='text-info'>{$pokemon->info->name}'s ". ucwords(preg_replace('/_/', ' ', $attr)) ." sharply rose!</span>");
        } else {
            session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." has locked on using {$move->name}!</span>");

            session()->push("{$area}.{$area}_log", "<span class='text-info'>{$pokemon->info->name}'s ". ucwords(preg_replace('/_/', ' ', $attr)) ." sharply rose!</span>");
        }


        $this->saveSession($area, $array);
        $this->saveSession("tmp", $array2);

        return false;
    }

    //held item - [expert belt]
    public function itemEffectMoveSuper($pokemon, $type, $damage)
    {
        if ($type >= 2) {
            $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

            $damage = $damage + ($damage * ($val / 100));
        }

        return $damage;
    }

    //held item - [shell bell]
    public function itemEffectMoveHeal($pokemon, $damage, $area = 'battle', $key = 'pokemon')
    {
        if ($damage > 0) {
            $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

            $heal = round($pokemon->hp * ($val / 100));

            if (in_array($area, ['arena', 'friendly'])) {

                $array[$key]["{$key}_current_hp"] = session("{$area}.{$key}.{$key}_current_hp") + $heal;

                if ($array[$key]["{$key}_current_hp"] > session("{$area}.{$key}.stats")->hp) {
                    $array[$key]["{$key}_current_hp"] = session("{$area}.{$key}.stats")->hp;
                }

                session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." has recovered its HP using Shell Bell!</span>");

            } else {

                $array[$key]["current_hp_{$pokemon->id}"] = session("{$area}.{$key}.current_hp_{$pokemon->id}") + $heal;

                if ($array[$key]["current_hp_{$pokemon->id}"] > session("{$area}.{$key}.stats")->hp) {
                    $array[$key]["current_hp_{$pokemon->id}"] = session("{$area}.{$key}.stats")->hp;
                }

                session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." has recovered its HP using Shell Bell!</span>");

            }


            $this->saveSession($area, $array);
        }

        return false;
    }

    //held item - [big root]
    public function itemEffectMoveAddHeal($pokemon, $move, $area = 'battle', $key = 'pokemon')
    {
        if ($move->secondary_attr == 'heal-default') {

            if (in_array($area, ['arena', 'friendly'])) {
                session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." received extra HP heal from using Big Root!</span>");
            } else {
                session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." received extra HP heal from using Big Root!</span>");
            }


            return $pokemon->primary_value;
        }

        return 0;
    }

    //held item - [life orb]
    public function itemEffectMoveRecoil($pokemon, $damage, $area = 'battle', $key = 'pokemon')
    {
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        $damage += round($damage * ($val / 100));
        $recoil = round($pokemon->hp * (($val - 25) / 100));

        if (in_array($area, ['arena', 'friendly'])) {
            $array[$key]["{$key}_current_hp"] = session("{$area}.{$key}.{$key}_current_hp") - $recoil;

            if ($array[$key]["{$key}_current_hp"] < 0) {
                $array[$key]["{$key}_current_hp"] = 0;
            }

            session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." suffered HP recoil from Life Orb!</span>");
        } else {
            $array[$key]["current_hp_{$pokemon->id}"] = session("{$area}.{$key}.current_hp_{$pokemon->id}") - $recoil;

            if ($array[$key]["current_hp_{$pokemon->id}"] < 0) {
                $array[$key]["current_hp_{$pokemon->id}"] = 0;
            }

            session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." suffered HP recoil from Life Orb!</span>");
        }


        $this->saveSession($area, $array);

        return $damage;
    }

    //held item - [muscle band,wise glasses]
    public function itemEffectMoveAttack($type, $pokemon, $damage, $move)
    {
        if ($move->primary_attr == $type) {
            $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

            $damage += round($damage * ($val / 100));
        }

        return $damage;
    }

    //held item - [types based]
    public function itemEffectMoveType($pokemon, $move, $damage)
    {
        if ($pokemon->item->primary_attr == $move->type) {
            $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

            $damage += round($damage * ($val / 100));

            return $damage;
        }

        return $damage;
    }

    //held item - [scope lens,bright powder]
    public function itemEffectMoveAccuracy($pokemon)
    {
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        return $val * 100;
    }

    //held item - [quick claw]
    public function itemEffectTurnFirst($pokemon, $key, $area)
    {
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        if (random_int(1,100) <= $val) {
            if (in_array($area, ['arena', 'friendly'])) {
                session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." activated Quick Claw's effect!</span>");
            } else {
                session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." activated Quick Claw's effect!</span>");
            }

            return 10000;
        }

        return 0;
    }

    //held item - [focus band]
    public function itemEffectCounterKo($pokemon, $key = 'pokemon', $area = 'battle')
    {
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        if (in_array($area, ['arena', 'friendly']) && random_int(1,100) <= $val && session("{$area}.{$key}.{$key}_current_hp") <= 0) {
            session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." endured the damage!</span>");

            $array[$key]["{$key}_current_hp"] = 1;

            $this->saveSession($area, $array);
        } elseif (!in_array($area, ['arena', 'friendly']) && random_int(1,100) <= $val && session("{$area}.{$key}.current_hp_{$pokemon->id}") <= 0) {
            session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." endured the damage!</span>");

            $array[$key]["current_hp_{$pokemon->id}"] = 1;

            $this->saveSession($area, $array);
        }

        return false;
    }

    //held item - [king's rock]
    public function itemEffectMoveFlinch($pokemon, $enemy, $key = 'pokemon', $area = 'battle')
    {
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        if (random_int(1,100) <= $val) {
            if (in_array($area, ['arena', 'friendly'])) {
                $array2[$key]['tmp']['flinch'] = 1;

                session()->push("{$area}_log", "<span style='color:#75453a'>The target flinched!</span>");
                $this->saveSession($area, $array2);
            } else {
                $array2["enemy_{$enemy}"]['flinch'] = 1;

                session()->push("{$area}.{$area}_log", "<span style='color:#75453a'>The target flinched!</span>");
                $this->saveSession("tmp", $array2);
            }
        }

        return false;
    }

    //held item - [protected pads]
    public function itemEffectMoveProtectRecoil($pokemon, $move, $key = 'pokemon', $area = 'battle')
    {
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        if (random_int(1,100) <= $val && $move->secondary_attr == 'recoil-hp') {
            if (in_array($area, ['arena', 'friendly'])) {
                session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." was protected from recoil!</span>");
            } else {
                session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." was protected from recoil!</span>");
            }

            return true;
        }

        return false;
    }

    //held item - [lucky egg, amulet coin]
    public function itemEffectTurnAddBonus($pokemon, $type, $key, $area)
    {
        if (session()->has("{$area}.{$key}.add_{$type}")) return false;

        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        $array[$key]["add_{$type}"] = ($val / 100);

        $this->saveSession($area, $array);

        return false;
    }

    //held item - [rocky helmet]
    public function itemEffectMoveReturn($pokemon, $enemy, $damage, $key = 'enemy', $area = 'battle')
    {
        $val = $pokemon->item->primary_value == 0 ? $pokemon->item->info->primary_value : $pokemon->item->primary_value;

        if (in_array($area, ['arena', 'friendly'])) {
            $array[$key]["{$key}_current_hp"] = session("{$area}.{$key}.{$key}_current_hp") - max(round($damage * ($val / 100)),1);

            session()->push("{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." is also hurt from recoil!</span>");
        } else {
            $array[$key]["current_hp_{$enemy}"] = session("{$area}.{$key}.current_hp_{$enemy}") - max(round($damage * ($val / 100)),1);

            session()->push("{$area}.{$area}_log", "<span class='text-info'>".session("{$area}.{$key}.stats")->info->name ." is also hurt from recoil!</span>");
        }

        $this->saveSession($area, $array);

        return false;
    }

    public function itemEffectRecoil($poke, $attr, $value, $reverse = false)
    {
        if ($reverse) {
            $this->userPokemon->clearStats($poke);

            return false;
        }

        $this->userPokemon->increaseStats($poke, $attr, $value);

        //recoil
        $this->userPokemon->decreaseStats($poke, $attr == 'speed' ? 'hp' : 'speed', $value);

        return false;
    }

    public function itemEffectType($poke, $attr, $value, $reverse = false)
    {
        $attri = explode('-', $attr);

        abort_if(!$this->userPokemon->checkInType($poke, $attri[0]), 403);

        if ($reverse) {
            $this->userPokemon->clearStats($poke);

            return false;
        }

        $this->userPokemon->increaseStats($poke, $attri[1], $value);

        return false;
    }

    public function itemEffectLowStar($poke, $value, $reverse = false)
    {
        abort_if($this->userPokemon->show($poke)->info->rarity > 3 && $reverse == false, 403);

        if ($reverse) {
            $this->userPokemon->clearStats($poke);

            return false;
        }

        $this->userPokemon->increaseStats($poke, 'defense', $value);
        $this->userPokemon->increaseStats($poke, 'special_defense', $value);

        return false;
    }


    /**
     * Pokeball modifiers
     *
     * @param $modifier
     * @return int
     */
    public function options($modifier)
    {
        if (session()->has('paragon')) {
            $target = "paragon";
        } else {
            $target = "battle";
        }
        
        switch ($modifier[0]) {
            case 'level':
                if (session("{$target}.pokemon.stats")->level * 1.75 > session("{$target}.enemy.stats")->level) {
                    return 7;
                } elseif (session("{$target}.pokemon.stats")->level * 1.5 > session("{$target}.enemy.stats")->level) {
                    return 5;
                } elseif (session("{$target}.pokemon.stats")->level > session("{$target}.enemy.stats")->level) {
                    return 3;
                }
            break;

            case 'evolution':
                if (session("{$target}.enemy.stats")->info->evolution_type == $modifier[1]) {
                    return 5;
                }
            break;

            case 'gender':
                if (session("{$target}.enemy.stats")->gender != 0) {
                    if ($modifier[1] == 'opposite') {
                        if (session("{$target}.enemy.stats")->gender != session("{$target}.pokemon.stats")->gender) {
                            return 5;
                        }
                    } else {
                        if (session("{$target}.enemy.stats")->gender == session("{$target}.pokemon.stats")->gender) {
                            return 5;
                        }
                    }
                }
            break;

            case 'speed':
                if ($modifier[1] == 'higher') {
                    if (session("{$target}.pokemon.stats")->speed * 1.75 < session("{$target}.enemy.stats")->speed) {
                        return 7;
                    } elseif (session("{$target}.pokemon.stats")->speed * 1.5 < session("{$target}.enemy.stats")->speed) {
                        return 5;
                    } elseif (session("{$target}.pokemon.stats")->speed < session("{$target}.enemy.stats")->speed) {
                        return 3;
                    }
                } else {
                    if (session("{$target}.pokemon.stats")->speed * 1.75 > session("{$target}.enemy.stats")->speed) {
                        return 7;
                    } elseif (session("{$target}.pokemon.stats")->speed * 1.5 > session("{$target}.enemy.stats")->speed) {
                        return 5;
                    } elseif (session("{$target}.pokemon.stats")->speed > session("{$target}.enemy.stats")->speed) {
                        return 3;
                    }
                }
            break;

            case 'repeat':
                if (in_array(session("{$target}.enemy.stats")->info->id, explode(',', auth()->user()->pokedex))) {
                    return 4;
                }
            break;

            case 'weak':
                if (session("{$target}.enemy.stats")->level <= 36 && !in_array(session("{$target}.enemy.stats")->info->rarity, [4,5])) {
					return (8 - 0.2 * (session("{$target}.enemy.stats")->level - 1));
				}
            break;

            case 'type':
                $types = explode('-', $modifier[1]);
                if (in_array(session("{$target}.enemy.stats")->info->primary_type, $types)
                    || in_array(session("{$target}.enemy.stats")->info->secondary_type, $types)) {
                    return 4;
                }
            break;

            case 'first':
                if (!session('battle_log')) {
                    return 6;
                }
            break;

            case 'time':
                $now = Carbon::now();
                if ($modifier[1] == 'night') {
                    $from = Carbon::parse('2017-01-01 18:01:00');
                    $to = Carbon::parse('2017-01-01 05:00:00');

                    if ($now->hour > $from->hour && $now->hour < $to->hour) {
                        return 5;
                    }
                } else {
                    $from = Carbon::parse('2017-01-01 06:00:00');
                    $to = Carbon::parse('2017-01-01 18:00:00');

                    if ($now->hour > $from->hour && $now->hour < $to->hour) {
                        return 5;
                    }
                }
            break;

            case 'heal':
                $id = session("{$target}.pokemon.stats")->id;

                $array["pokemon"]["current_hp_{$id}"] = session("{$target}.pokemon.stats")->hp;

                $this->saveSession($target, $array);

                return 3;
            break;

            case 'gain':
                session(['victory_bonus' => 2]);

                return 3;
            break;

            case 'timer':
                $rate = 1 + (session('paragon.turns') * 496/4096);

                return ($rate > 5) ? 5 : $rate;
            break;
        }

        return 1;
    }

    public function upgradeChance($ups)
    {
        if ($ups < 7) {
            $chance = 700 - ($ups * (73.52 - ($ups * 1.49)));
            $cost = round(($ups + 1.3) * 50000);

            return [
                'chance' => $chance,
                'cost' => $cost
            ];
        }

        $chance = 700 - ($ups * (72.11 - ($ups * 1.17)));
        $cost = round(($ups + 1.4) * 350);

        return [
            'chance' => $chance,
            'cost' => $cost
        ];
    }
}