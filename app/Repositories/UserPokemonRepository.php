<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\UserPokemonInterface;
use App\UserPokemon;

class UserPokemonRepository extends Repository implements UserPokemonInterface
{
    protected $userPokemon;

    public function __construct(UserPokemon $userPokemon)
    {
        parent::__construct($userPokemon);

        $this->userPokemon = $userPokemon;
    }

    /**
     * Generate starter pokemon
     *
     * @return mixed
     */
    public function starter()
    {
        return $this->userPokemon->where('user_id', auth()->user()->id)->first();
    }

    public function checkInType($poke, $type)
    {
        $info = $this->userPokemon->findOrFail($poke);

        if ($info->info->primary_type == $type || $info->info->secondary_type == $type) {
            return true;
        }

        return false;
    }

    public function increaseStats($pokemon, $attr, $value)
    {
        $poke = $this->userPokemon->findOrFail($pokemon);

        $poke->update([
            'tmp_'.$attr => round($poke->{$attr} * ($value / 100)),
            $attr => round($poke->{$attr} * ($value / 100 + 1))
        ]);

        return false;
    }

    public function decreaseStats($pokemon, $attr, $value)
    {
        $poke = $this->userPokemon->findOrFail($pokemon);

        $poke->update([
            'tmp_'.$attr => 0 - round($poke->{$attr} * ($value / 100)),
            $attr => $poke->{$attr} - round($poke->{$attr} * ($value / 100))
        ]);

//        switch ($attr) {
//            case 'hp': $poke->tmp_hp -= round($poke->hp * ($value / 100)); break;
//            case 'attack': $poke->tmp_attack -= round($poke->attack * ($value / 100)); break;
//            case 'defense': $poke->tmp_defense -= round($poke->defense * ($value / 100)); break;
//            case 'special_attack': $poke->tmp_special_attack -= round($poke->special_attack * ($value / 100)); break;
//            case 'special_defense': $poke->tmp_special_defense -= round($poke->special_defense * ($value / 100)); break;
//            case 'speed': $poke->tmp_speed -= round($poke->speed * ($value / 100)); break;
//        }
//
//        return $poke->save();
    }

    public function clearStats($pokemon)
    {
        $poke = $this->userPokemon->findOrFail($pokemon);

        $poke->hp -= $poke->tmp_hp;
        $poke->attack -=  $poke->tmp_attack;
        $poke->defense -= $poke->tmp_defense;
        $poke->special_attack -= $poke->tmp_special_attack;
        $poke->special_defense -= $poke->tmp_special_defense;
        $poke->speed -= $poke->tmp_speed;

        $poke->tmp_hp = 0;
        $poke->tmp_attack = 0;
        $poke->tmp_defense = 0;
        $poke->tmp_special_attack = 0;
        $poke->tmp_special_defense = 0;
        $poke->tmp_speed = 0;

        return $poke->save();
    }

    public function battleGain($exp, $used)
    {
        $logs = [];

        foreach ($this->team() as $pokemon) {
            if (in_array($pokemon->id, $used) && $pokemon->level < 100) {
                $pokemon->increment('experience', $exp);

                $logs[] = "<span class='text-success'>{$pokemon->info->name} gained +{$exp} experience!</span>";
            }

            while($this->levelUp($pokemon->id)) {
                $logs[] = "<span class='text-success'>{$pokemon->info->name} has leveled up!</span>";
            }
        }

        return $logs;
    }

    public function levelUp($id)
    {
        $poke = $this->userPokemon->findOrFail($id);

        if ($poke->experience < $this->nextLevel($poke->level)) return false;

        $increase = (3 + (($poke->ratings - 80) / 2)) / 100;

        $stats = ['hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed'];

        foreach ($stats as $attr) {
            $poke->{$attr} += round($poke->info->{$attr} * $increase);
        }

        $poke->experience -= $this->nextLevel($poke->level);
        if ($poke->experience <= 0) $poke->experience = 0;

        $poke->level += 1;

        $poke->save();

        return true;
    }

    public function itemLevelUp($id)
    {
        $poke = $this->userPokemon->findOrFail($id);

        $increase = (3 + (($poke->ratings - 80) / 2)) / 100;

        $stats = ['hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed'];

        foreach ($stats as $attr) {
            $poke->{$attr} += round($poke->info->{$attr} * $increase);
        }

        $poke->level += 1;

        $poke->save();

        return true;
    }

    public function findAvailable($pokemon, $user = null)
    {
        return $this->userPokemon->where('user_id', $user ?? auth()->user()->id)->findOrFail($pokemon);
    }

    /**
     * Get default user team
     *
     * @param null $user
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function team($user = null)
    {
        return $this->userPokemon->with('info')
            ->where('user_id', $user ?: auth()->user()->id)
            ->where('in_team', '!=', 0)
            ->latest()
            ->get();
    }

    /**
     * View pokemon details
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->userPokemon->with(['info' => function ($q) {
            $q->select(['id', 'name', 'rarity', 'primary_type', 'secondary_type', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']);
        }, 'moves' => function ($q) {
            $q->select(['id', 'name', 'description', 'type', 'primary_attr']);
        }, 'item' => function ($q) {
            $q->with(['info' => function ($q) {
                $q->select(['id', 'name', 'description', 'primary_value']);
            }])->select(['id', 'item_id', 'is_held', 'primary_value', 'upgrade']);
        }])->findOrFail($id);
    }

    /**
     * Arena team disables switching and randomize pokemon order
     *
     * @param null $user
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function arenaTeam($user = null)
    {
        return $this->userPokemon->with('info')
            ->where('user_id', $user ?: auth()->user()->id)
            ->where('in_team', '!=', 0)
            ->inRandomOrder()
            ->get();
    }

    /**
     * Retrieve pokemon box
     *
     * @param null $user
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all($user = null)
    {
        return $this->userPokemon->with(['info' => function ($q) {
            $q->select(['id', 'name', 'primary_type', 'secondary_type']);
        }, 'user' => function ($q) {
            $q->select(['id', 'username']);
        }])
            ->where('user_id', $user ?: auth()->user()->id)
            ->where('in_team', 0)
            ->where('in_shop', 0)
            ->where('in_trade', 0)
            ->orderBy('updated_at', 'desc')->select(['id', 'user_id', 'gender', 'pokemon_id', 'level', 'ratings'])->paginate(10);
    }

    public function ifOwnedBy($poke, $user)
    {
        return $this->userPokemon->where('id', $poke)->where('user_id', $user)->count();
    }

    public function search($user, $keyword)
    {
        return $this->userPokemon->with(['info' => function ($q) {
            $q->select(['id', 'name', 'primary_type', 'secondary_type']);
        }, 'user' => function ($q) {
            $q->select(['id', 'username']);
        }])
            ->where('user_id', $user)
            ->where('in_team', 0)
            ->where('in_shop', 0)
            ->where('in_trade', 0)
            ->whereHas('info', function ($q) use ($keyword) {
                $q->where('name', 'like', "%{$keyword}%");
            })
            ->orderBy('updated_at', 'desc')->select(['id', 'user_id', 'gender', 'pokemon_id', 'level', 'ratings'])->paginate(10);
    }

    public function trades()
    {
        return $this->userPokemon->with(['info' => function ($q) {
            $q->select(['id', 'name', 'primary_type', 'secondary_type']);
        }])->where('user_id', auth()->user()->id)->where('in_trade', 1)->latest('updated_at')
            ->select(['id', 'pokemon_id', 'gender', 'level'])->get();
    }

    public function moveToTop($pokemon)
    {
        $pokemon->created_at = $this->userPokemon->freshTimestamp();
        $pokemon->save();

        return false;
    }
}