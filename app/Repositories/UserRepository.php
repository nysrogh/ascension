<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\UserInterface;
use App\User;
use Carbon\Carbon;

class UserRepository extends Repository implements UserInterface
{
    protected $user;

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->user = $user;
    }

    public function profile($username = null)
    {
        return $this->user->with(['arena' => function ($q) {
            $q->select(['user_id', 'rank']);
        }, 'club' => function ($q) {
            $q->select(['id', 'badge', 'name']);
        }, 'shop' => function ($q) {
            $q->select(['id', 'user_id']);
        }])->where('username', $username ?? auth()->user()->username)->select(['id', 'status', 'username', 'email', 'setting_bgcolor', 'avatar', 'gender', 'pokedex', 'dungeons_completed', 'missions_completed', 'battle_win', 'battle_lose', 'club_id', 'club_role', 'created_at', 'likes', 'dislikes', 'introduction'])->firstOrFail();
    }

    public function paragonRankings()
    {
        return $this->user->select(['id', 'gender', 'username', 'paragon_level', 'avatar'])->orderByDesc('paragon_level')->limit(30)->get();
    }

    public function searchUser($keyword)
    {
        return $this->user->where('username', 'LIKE', "%{$keyword}%")->select(['username', 'status', 'updated_at'])->limit(15)->get();
    }

    public function arenaUpdate($play)
    {
        $this->user->where('id', $play->user_id)->increment('battle_win', $play->win);
        $this->user->where('id', $play->user_id)->increment('battle_lose', $play->lose);
    }

    public function countMembers()
    {
        return $this->user->where('club_id', auth()->user()->club_id)->count();
    }

    public function countRoles($role)
    {
        return $this->user->where('club_id', auth()->user()->club_id)->where('club_role', $role)->get()->count();
    }

    public function leader($club)
    {
        return $this->user->where('club_id', $club)->where('club_role', 3)->select(['username'])->get();
    }

    public function clubMembers($club)
    {
        return $this->user->where('club_id', $club)->orderBy('club_point', 'desc')->select(['id', 'username', 'club_role', 'club_point', 'avatar', 'gender'])->paginate(15);
    }

    public function ban($id)
    {
        return $this->user->where('id', $id)->update(['status' => 4]);
    }

    public function unban($id)
    {
        return $this->user->where('id', $id)->update(['status' => 1]);
    }

    public function old()
    {
        return $this->user->where('updated_at', '>=', Carbon::now()->subWeek())->where('status', '!=', 0)->get();
    }

    public function getId($user)
    {
        return $this->user->where('id', $user)->firstOrFail(['id']);
    }
}