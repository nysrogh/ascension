<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\UserShopInterface;
use Intervention\Image\ImageManagerStatic as Image;
use App\UserShop;

class UserShopRepository extends Repository implements UserShopInterface
{
    protected $userShop;

    public function __construct(UserShop $userShop)
    {
        parent::__construct($userShop);
        $this->userShop = $userShop;
    }

    public function background($owner, $buyer)
    {
        $background = Image::make(public_path("images/locations/shop.png"));

        //owner
        $background->insert(public_path("images/trainers/thumbnail/{$owner}.png"), '', 25, 33);

        //buyer
        $background->insert(public_path("images/trainers/thumbnail/{$buyer}.png"), '', 80, 60);

        return 'data:image/png;base64,' . base64_encode($background->encode('jpg', 70));
    }

    public function findShopByUser($user)
    {
        return $this->userShop->where('user_id', $user)->first();
    }

    public function randomShop()
    {
        return $this->userShop->with(['contents' => function ($q) {
            $q->select(['available']);
        }, 'user' => function ($q) {
            $q->select(['id', 'avatar']);
        }])
            ->whereHas('contents', function ($q) {
                $q->where('available', '>=', 1);
            })
            ->where('user_id', '!=', auth()->user()->id)->select(['id', 'user_id', 'name', 'description'])
            ->inRandomOrder()->limit(10)->get();
    }
}
