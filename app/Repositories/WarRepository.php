<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 4/7/2017
 * Time: 11:57 AM
 */

namespace App\Repositories;

use App\Contracts\WarInterface;
use App\War;

class WarRepository extends Repository implements WarInterface
{
    protected $war;

    public function __construct(War $war)
    {
        parent::__construct($war);
        $this->war = $war;
    }
}