<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Chat;
use Carbon\Carbon;

class CreateChatRequest extends FormRequest
{
    private $forbiddenKeywords = [
        'fuck', 'motherfucker', 'bitch', 'shit', 'pota', 'puta', 'gago'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|min:2|max:150',
            'captcha' => 'required|confirmed',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->noForbiddenKeywords()) {
                $validator->errors()->add('message', 'Your message must not include explicit words!');
            }

            if ($this->heldDownSpam()) {
                $validator->errors()->add('message', 'Possible spam content is not allowed!');
            }

            if ($this->antiSpamTimer()) {
                $validator->errors()->add('message', 'Sending chat has a 10-second cooldown!');
            }
        });
    }

    public function noForbiddenKeywords()
    {
        foreach ($this->forbiddenKeywords as $forbidden) {
            if (stripos($this->message, $forbidden) !== false) {
                return true;
            }
        }

        return false;
    }

    public function heldDownSpam()
    {
        //ex. dddddddddddddddddddd
        if (preg_match("/(.)\\1{5,}/u", $this->message)) {
            return true;
        }

        return false;
    }

    public function antiSpamTimer()
    {
        if (session()->exists('chat') && session('chat') > Carbon::now()) {
            return true;
        }

        return false;
    }

    public function persist()
    {
        session(['chat' => Carbon::now()->addSeconds(10)]);

        abort_if($this->club_id && auth()->user()->club_id != $this->club_id, 403);

        Chat::create([
            'club_id' => $this->club_id ?? 0,
            'user_id' => auth()->user()->id,
            'message' => $this->message
        ]);
    }
}
