<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class CreateClubCoinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|min:1',
            'captcha' => 'required|confirmed',
        ];
    }

    public function persist()
    {
        $cp = floor($this->amount / 7000);

        auth()->user()->club->coin += $this->amount;
        auth()->user()->club->point += $cp;
        auth()->user()->club->save();

        auth()->user()->gold -= $this->amount;
        auth()->user()->club_point += $cp;
        auth()->user()->save();

        return $cp;
    }
}
