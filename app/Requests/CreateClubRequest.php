<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Club;

class CreateClubRequest extends FormRequest
{
    private $forbiddenKeywords = [
        'fuck', 'motherfucker', 'bitch', 'shit', 'pota', 'puta', 'gago', 'tangina'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:20|regex:/^[\pL\pN]/',
            'badge' => 'required|in:1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50',
            'captcha' => 'required|confirmed',
            'description' => 'required|min:2|max:80',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->noForbiddenKeywords()) {
                $validator->errors()->add('name', 'Your message must not include explicit words!');
            }

            if ($this->heldDownSpam()) {
                $validator->errors()->add('name', 'Possible spam content is not allowed!');
            }
        });
    }

    public function noForbiddenKeywords()
    {
        foreach ($this->forbiddenKeywords as $forbidden) {
            if (stripos($this->name, $forbidden) !== false) {
                return true;
            }
        }

        return false;
    }

    public function heldDownSpam()
    {
        //ex. dddddddddddddddddddd
        if (preg_match("/(.)\\1{5,}/u", $this->name)) {
            return true;
        }

        return false;
    }

    public function persist()
    {
        $id = Club::insertGetId([
            'name' => $this->name,
            'badge' => $this->badge,
            'description' => $this->description
        ]);

        auth()->user()->club_id = $id;
        auth()->user()->club_role = 3;
        auth()->user()->gold -= 30000;
        auth()->user()->save();

        return true;
    }
}
