<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\ForumComment;
use App\Forum;

class CreateCommentRequest extends FormRequest
{
    private $forbiddenKeywords = [
        'fuck', 'motherfucker', 'bitch', 'shit', 'pota', 'puta', 'gago'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => 'required|min:2|max:300',
            'captcha' => 'required|confirmed',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->noForbiddenKeywords()) {
                $validator->errors()->add('body', 'Your message must not include explicit words!');
            }

            if ($this->heldDownSpam()) {
                $validator->errors()->add('body', 'Possible spam content is not allowed!');
            }
        });
    }

    public function noForbiddenKeywords()
    {
        foreach ($this->forbiddenKeywords as $forbidden) {
            if (stripos($this->body, $forbidden) !== false) {
                return true;
            }
        }

        return false;
    }

    public function heldDownSpam()
    {
        //ex. dddddddddddddddddddd
        if (preg_match("/(.)\\1{5,}/u", $this->message)) {
            return true;
        }

        return false;
    }

    public function persist()
    {
        Forum::find($this->forum_id)->touch();

        ForumComment::create([
            'forum_id' => $this->forum_id,
            'user_id' => auth()->user()->id,
            'body' => $this->body
        ]);
    }
}
