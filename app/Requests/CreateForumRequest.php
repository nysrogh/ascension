<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Forum;

class CreateForumRequest extends FormRequest
{
    private $forbiddenKeywords = [
        'fuck', 'motherfucker', 'bitch', 'shit', 'pota', 'puta', 'gago'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2|max:50|regex:/^[\pL\pN]/',
            'category' => 'required|in:1,2,3,4',
            'captcha' => 'required|confirmed',
            'body' => 'required|min:10|max:1000',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->noForbiddenKeywords()) {
                $validator->errors()->add('title', 'Your content must not include explicit words!');
            }

            if ($this->heldDownSpam()) {
                $validator->errors()->add('title', 'Possible spam content is not allowed!');
            }
        });
    }

    public function noForbiddenKeywords()
    {
        foreach ($this->forbiddenKeywords as $forbidden) {
            if (stripos($this->title, $forbidden) !== false || stripos($this->body, $forbidden) !== false) {
                return true;
            }
        }

        return false;
    }

    public function heldDownSpam()
    {
        //ex. dddddddddddddddddddd
        if (preg_match("/(.)\\1{5,}/u", $this->title) || preg_match("/(.)\\1{5,}/u", $this->body)) {
            return true;
        }

        return false;
    }

    public function persist()
    {
        abort_if($this->category == 4 && auth()->user()->id != 1, 403);

        return Forum::create([
            'user_id' => auth()->user()->id,
            'category' => $this->category,
            'slug' => str_slug($this->title) ."-". random_int(100,999),
            'title' => title_case($this->title),
            'body' => $this->body
        ]);
    }
}
