<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Mail;
use App\User;

class CreateMailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|min:2|max:500',
            'user_id' => 'required|min:1|alpha_num',
            'captcha' => 'required|confirmed',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->invalidUser()) {
                $validator->errors()->add('user_id', 'UserID or Username does not exist.');
            }

            if ($this->cantSendToSelf()) {
                $validator->errors()->add('user_id', 'Sending of message failed.');
            }

            if ($this->heldDownSpam()) {
                $validator->errors()->add('message', 'Possible spam content is not allowed.');
            }
        });
    }

    public function invalidUser()
    {
        return (!User::where('id', $this->user_id)->orWhere('username', $this->user_id)->count()) ? true : false;
    }

    public function cantSendToSelf()
    {
        return (auth()->user()->id == $this->user_id || auth()->user()->username == $this->user_id) ? true : false;
    }

    public function heldDownSpam()
    {
        //ex. dddddddddddddddddddd
        return preg_match("/(.)\\1{3,}/u", $this->body) ?: false;
    }

    public function persist()
    {
        if (!is_numeric($this->user_id)) {
            $this->user_id = User::where('username', $this->user_id)->first()->id;
        }

        Mail::create([
            'user_id' => $this->user_id,
            'sender_id' => auth()->user()->id,
            'message' => $this->message,
        ]);
    }
}
