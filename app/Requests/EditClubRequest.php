<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Club;

class EditClubRequest extends FormRequest
{
    private $forbiddenKeywords = [
        'fuck', 'motherfucker', 'bitch', 'shit', 'pota', 'puta', 'gago'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'notice' => 'max:150',
            'description' => 'required|min:2|max:150',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->noForbiddenKeywords()) {
                $validator->errors()->add('description', 'Your message must not include explicit words!');
            }

            if ($this->heldDownSpam()) {
                $validator->errors()->add('description', 'Possible spam content is not allowed!');
            }
        });
    }

    public function noForbiddenKeywords()
    {
        foreach ($this->forbiddenKeywords as $forbidden) {
            if (stripos($this->description, $forbidden) !== false) {
                return true;
            }
        }

        return false;
    }

    public function heldDownSpam()
    {
        //ex. dddddddddddddddddddd
        if (preg_match("/(.)\\1{5,}/u", $this->description)) {
            return true;
        }

        return false;
    }

    public function persist()
    {

        Club::where('id', auth()->user()->club_id)->update([
            'description' => $this->description,
            'notice' => $this->notice,
        ]);

        return true;
    }
}
