<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:2|confirmed',
            'captcha' => 'required|numeric|min:3|confirmed'
        ];
    }

    public function persist()
    {
        auth()->user()->update([
            'password' => bcrypt($this->password)
        ]);

        return true;
    }
}
