<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\UserShop;

class EditShopRequest extends FormRequest
{
    private $forbiddenKeywords = [
        'fuck', 'motherfucker', 'bitch', 'shit', 'pota', 'puta', 'gago'
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:25',
            'description' => 'required|min:2|max:80',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->noForbiddenKeywords()) {
                $validator->errors()->add('name', 'Your message must not include explicit words!');
            }

            if ($this->heldDownSpam()) {
                $validator->errors()->add('name', 'Possible spam content is not allowed!');
            }
        });
    }

    public function noForbiddenKeywords()
    {
        foreach ($this->forbiddenKeywords as $forbidden) {
            if (stripos($this->name, $forbidden) !== false) {
                return true;
            }
        }

        return false;
    }

    public function heldDownSpam()
    {
        //ex. dddddddddddddddddddd
        if (preg_match("/(.)\\1{5,}/u", $this->name)) {
            return true;
        }

        return false;
    }

    public function persist()
    {

        UserShop::where('user_id', auth()->user()->id)->update([
            'name' => $this->name,
            'description' => $this->description
        ]);

        return true;
    }
}
