<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    public function modifiers()
    {
        return $this->hasMany('App\RewardModifier');
    }

    public function user()
    {
        return $this->belongsToMany('App\User')->withPivot('claim');
    }
}
