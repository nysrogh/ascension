<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    protected $fillable = [
        'content_id', 'user_id'
    ];

    public function offers()
    {
        return $this->hasMany('App\TradeOffer');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function content()
    {
        return $this->belongsTo('App\Content');
    }
}
