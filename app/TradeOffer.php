<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradeOffer extends Model
{
    protected $fillable = [
        'trade_id', 'user_pokemon_id', 'item_id', 'gold', 'crystal', 'primary_value', 'upgrade'
    ];

    public function pokemon()
    {
        return $this->hasOne('App\UserPokemon', 'id', 'user_pokemon_id');
    }

    public function item()
    {
        return $this->hasOne('App\Item', 'id', 'item_id');
    }

    public function trade()
    {
        return $this->belongsTo('App\Trade');
    }
}
