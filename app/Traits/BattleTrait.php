<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 11/2/2017
 * Time: 8:06 AM
 */

namespace App\Traits;

use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

trait BattleTrait
{

    /**
     * Setup battle session
     *
     * @param $pokemon
     * @param $enemy
     * @param $type
     * @return bool
     */
    public function setup($pokemon, $enemy, $type)
    {
        //enemy info
        $array['enemy'] = [
            "stats" => (object)$enemy[0],
            "current_hp_{$enemy[0]['id']}" => $enemy[0]['hp'],
            "not_effective_{$enemy[0]['id']}" => [],
            "super_effective_{$enemy[0]['id']}" => []
        ];

        //pokemon info
        $array["pokemon"] = [
            "stats" => $pokemon[0],
            "current_hp_{$pokemon[0]->id}" => session("battle.pokemon.current_hp_{$pokemon[0]->id}") ??  $pokemon[0]->hp
        ];

        //initialize turn
        $array["turns"] = 1;

        //initialize participant
        $array["participants"][] = $pokemon[0]->id;

        //initialize opponent (arena/friendly)
        $array['opponent'] = session('arena');

        //initialize background
        $array["background"] = $this->background($type, (object)$enemy['info'], $pokemon->info);

        //battle timer
        $array["timer"] = Carbon::now()->addHour();

        //save session data
        $this->saveSession('battle', $array);

        return false;
    }

    /**
     * Setup battle background
     *
     * @param $type
     * @param $enemy
     * @param $pokemon
     * @param bool $result
     * @return string
     */
    public function background($type, $enemy, $pokemon, $result = false)
    {
        if ($type == 'wild') {
            //set background based on current time
            $now = Carbon::now();
            $from = Carbon::parse('2017-01-01 06:00:00');
            $to = Carbon::parse('2017-01-01 18:00:00');

            if ($now->hour > $from->hour && $now->hour < $to->hour) {
                $background = Image::make(public_path("images/battle/" . session('dungeon.info')->background . ".png"));
            } else {
                $background = Image::make(public_path("images/battle/" . session('dungeon.info')->background . "n.png"));
            }
        }

        //enemy front background
        if (!$result || $result == 'defeat') {
            $background->insert(public_path("images/pokemon/front/" . str_slug($enemy->name) . ".png"), '',
                $enemy->front_x, $enemy->front_y);
        }

        //caught pokemon background
        if (is_array($result)) {
            $background->insert(public_path("images/items/" . str_slug($result['ball']) . ".png"), '', 132, 48);
        }

        //pokemon back background
        if (!$result || $result == 'victory') {
            $background->insert(public_path("images/pokemon/back/" . str_slug($pokemon->name) . ".png"), '',
                $pokemon->back_x, $pokemon->back_y);
        }

        return 'data:image/png;base64,' . base64_encode($background->encode('jpg', 90));
    }

    /**
     * Check if battle has timed out
     *
     * @return bool
     */
    public function checkTimeout()
    {
        if (session('battle.timer') < Carbon::now()->subHour()) {
            $array['result'] = 'victory';
            $array['logs'] = "<strong>Your battle has timed out!</strong>";

            $this->saveSession('battle', $array);

            return true;
        }

        return false;
    }

    /**
     * Check if team member alive and exists
     *
     * @param $team
     * @return bool
     */
    public function checkTeam($team)
    {
        session()->push("arena.{$team}_dead", session("arena.{$team}.stats")->id);

        foreach (session("arena.{$team}_team") as $pokemon) {
            if (!in_array($pokemon->id, session("arena.{$team}_dead"))) {
                return $pokemon;
            }
        }

        return false;
    }

    public function evasion($defender, $defKey, $accuracy)
    {
        if (random_int(1,10000) <= $defender['stats']->speed + session("tmp.{$defKey}_". $defender['stats']->id .".speed") + $accuracy) {
            return true;
        }

        return false;
    }
}