<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/28/2017
 * Time: 8:04 AM
 */

namespace App\Traits;


trait ChatTrait
{
    public function retrieve($club = 0)
    {
        return $this->with(['users' => function ($q) {
            $q->select(['id', 'username', 'avatar', 'status']);
        }])->where('club_id', $club)->select(['user_id', 'message', 'created_at'])->latest()->simplePaginate(15);
    }
}