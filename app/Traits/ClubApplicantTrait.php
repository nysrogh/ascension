<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 10/6/2017
 * Time: 7:25 PM
 */

namespace App\Traits;


trait ClubApplicantTrait
{
    /**
     * Applicants listing
     *
     * @param $id
     * @return mixed
     */
    public function listing($id)
    {
        return $this->with(['user' => function ($q) {
            $q->select(['id', 'username', 'avatar', 'gender']);
        }])->where('club_id', $id)->latest()->simplePaginate(10);
    }

    /**
     * Remove applicant
     *
     * @param $id
     * @return mixed
     */
    public function remove($id)
    {
        return $this->where('club_id', auth()->user()->club_id)->where('user_id', $id)->delete();
    }
}