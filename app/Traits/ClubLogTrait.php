<?php
/**
 * Created by PhpStorm.
 * User: Taguiwalo
 * Date: 10/6/2017
 * Time: 10:08 PM
 */

namespace App\Traits;


trait ClubLogTrait
{
    /**
     * Get club logs
     *
     * @return mixed
     */
    public function retrieve()
    {
        return $this->where('club_id', auth()->user()->club_id)->latest()->simplePaginate(20);
    }
}