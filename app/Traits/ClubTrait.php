<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 10/3/2017
 * Time: 12:32 PM
 */

namespace App\Traits;


trait ClubTrait
{
    /**
     * Club's next level cp
     *
     * @param $level
     * @return float
     */
    public function nextLevel($level)
    {
        return ceil(pow($level, 2.95) * 250.132);
    }

    /**
     * Random club for users to join
     *
     * @return mixed
     */
    public function random()
    {
        return $this->inRandomOrder()->limit(10)->select(['id', 'badge', 'name', 'description'])->get();
    }

    /**
     * Club home data
     *
     * @param $id
     * @return mixed
     */
    public function home($id)
    {
        return $this->with(['chats' => function ($q) {
            $q->with(['users' => function ($q) {
                $q->select(['id', 'username', 'avatar']);
            }])->select(['id', 'user_id', 'message', 'club_id', 'created_at'])->limit(15)->latest();
        }, 'applicants' => function ($q) {
            $q->select(['id']);
        }])->findOrFail($id);
    }

    /**
     * Retrieve description and notice
     *
     * @param $id
     * @return mixed
     */
    public function manage($id)
    {
        return $this->select(['id', 'description', 'notice'])->findOrFail($id);
    }

    /**
     * Show club info
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->with(['privileges' => function ($q) {
            $q->select(['title', 'modifier']);
        }])->findOrFail($id);
    }

    /**
     * Check Application
     *
     * @param $id
     * @return mixed
     */
    public function checkApplication($id)
    {
        return $this->with(['applicants'])->whereHas('applicants', function ($q) {
            $q->where('user_id', auth()->user()->id);
        })->where('id', $id)->exists();
    }

    /**
     * Manage Members
     *
     * @param $id
     * @return mixed
     */
    public function manageMembers($id)
    {
        return $this->select(['max_member'])->findOrFail($id);
    }

    /**
     * Club rankings
     *
     * @return mixed
     */
    public function rankings()
    {
        return $this->select(['id', 'name', 'description', 'badge', 'point'])->orderByDesc('point')->simplePaginate(15);
    }
}