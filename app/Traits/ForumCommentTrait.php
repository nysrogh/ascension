<?php
/**
 * Created by PhpStorm.
 * User: Taguiwalo
 * Date: 9/24/2017
 * Time: 11:24 PM
 */

namespace App\Traits;


trait ForumCommentTrait
{
    /**
     * Find forum comments
     *
     * @param $id
     * @return mixed
     */
    public function findByForum($id)
    {
        return $this->with(['user' => function ($q) {
            $q->select(['id', 'avatar', 'username', 'created_at']);
        }])->where('forum_id', $id)->select(['id', 'user_id', 'body', 'created_at'])->simplePaginate(15);
    }
}