<?php
/**
 * Created by PhpStorm.
 * User: Taguiwalo
 * Date: 9/24/2017
 * Time: 10:19 PM
 */

namespace App\Traits;


trait ForumTrait
{
    /**
     * Latest forum posts
     *
     * @return mixed
     */
    public function latestPosts()
    {
        return $this->with(['user' => function ($q) {
            $q->select(['id', 'avatar', 'username']);
        }, 'comments'])->latest('updated_at')->select(['id', 'user_id', 'title', 'body', 'slug', 'updated_at'])->limit(15)->get();
    }

    /**
     * Latest game updates
     *
     * @return mixed
     */
    public function latestUpdates()
    {
        return $this->where('category', 4)->where('user_id', 1)->latest()->limit(5)->select(['slug', 'title', 'created_at'])->get();
    }

    public function latestByCategory($category)
    {
        return $this->with(['user' => function ($q) {
            $q->select(['id', 'avatar', 'username']);
        }, 'comments'])->where('category', $category)->latest('updated_at')
            ->select(['id', 'user_id', 'title', 'body', 'slug', 'updated_at'])->simplePaginate(10);
    }

    /**
     * Find forum by slug
     *
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        return $this->with(['user' => function ($q) {
            $q->select(['id', 'avatar', 'username', 'status']);
        }])->where('slug', $slug)->firstOrFail(['id', 'user_id', 'title', 'body', 'created_at']);
    }
}