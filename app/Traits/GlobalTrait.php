<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/25/2017
 * Time: 7:39 AM
 */

namespace App\Traits;


trait GlobalTrait
{
    /**
     * Double click prevention
     *
     * @param $prevent
     * @return bool
     */
    public function doubleClick($prevent)
    {
        if (session('prevent') == $prevent) {
            return true;
        }

        session(['prevent' => $prevent]);

        return false;
    }

    /**
     * Privilege value
     *
     * @param $modifier
     * @return int
     */
    public function privilege($modifier)
    {
        $value = 0;
        if (auth()->user()->club_id) {
            foreach (auth()->user()->club->privileges as $privilege) {
                if ($privilege->pivot->expired_on > $this->freshTimestamp() && $privilege->modifier == $modifier) {
                    $value += $privilege->value;
                }
            }
        }

        return $value;
    }
}