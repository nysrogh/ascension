<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/25/2017
 * Time: 9:24 PM
 */

namespace App\Traits;


trait ItemTrait
{
    /**
     * Random dungeon chest item
     *
     * @param $category
     * @param $section
     * @param array $except
     * @return mixed
     */
    public function randomChestItem($category, $section, $except = ['name', ['test']])
    {
        return $this->where('category', $category)
            ->where('section', $section)
            ->where($except[0], '!=', $except[1])
            ->inRandomOrder()->first(['id', 'name']);
    }

    /**
     * Pokemart/Arena shop
     *
     * @param bool $arena
     * @return mixed
     */
    public function shop($arena = false)
    {
        if ($arena) {
            return $this->whereIn('id', [97,98,99,100,101,102])->select(['id', 'name', 'description', 'price'])->get();
        }

        return $this->whereNotIn('id', [97,98,99,100,101,102,126])->where('price', '>', 1)
                ->select(['id', 'name', 'description', 'price'])->get();
    }

    /**
     * Find item by id
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->select(['id', 'name', 'price'])->findOrFail($id);
    }
}