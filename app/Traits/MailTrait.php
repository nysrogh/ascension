<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/26/2017
 * Time: 8:12 AM
 */

namespace App\Traits;


trait MailTrait
{
    /**
     * Welcome message
     *
     * @return bool
     */
    public function welcome()
    {
        $this->create([
            'user_id' => auth()->user()->id,
            'sender_id' => 1,
            'message' => "Welcome to Pokemon Ascension! I hope you'll enjoy playing this simple poke RPG. If you have any questions/suggestions, I'd recommend you to interact with your fellow trainers in Pokemon Center or post it on our forum. Have a good day!"
        ]);

        return false;
    }

    /**
     * Count unread mails
     *
     * @return mixed
     */
    public function unread()
    {
        return $this->where('user_id', auth()->user()->id)->where('is_read', 0)->count();
    }

    /**
     * View inbox
     *
     * @return mixed
     */
    public function inbox()
    {
        return $this->with(['sender' => function ($q) {
            $q->select(['id', 'username']);
        }])->where('user_id', auth()->user()->id)->latest()->simplePaginate(10);
    }

    /**
     * Read mail
     *
     * @param $id
     * @return mixed
     */
    public function read($id)
    {
        $mail = $this->with(['sender' => function ($q) {
            $q->select(['id', 'avatar', 'username']);
        }])->where('user_id', auth()->user()->id)->findOrFail($id);

        !$mail->is_read ? $mail->increment('is_read') : false;

        return $mail;
    }

    /**
     * Last user reply
     *
     * @param $sender
     * @return mixed
     */
    public function lastReply($sender)
    {
        return $this->where('user_id', $sender)->where('sender_id', auth()->user()->id)->latest()->first();
    }
}