<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/26/2017
 * Time: 8:44 AM
 */

namespace App\Traits;


trait NotifTrait
{
    /**
     * Count unread notifs
     *
     * @return mixed
     */
    public function unread()
    {
        return $this->where('user_id', auth()->user()->id)->where('is_read', 0)->count();
    }
}