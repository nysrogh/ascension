<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/25/2017
 * Time: 8:17 AM
 */

namespace App\Traits;

use Carbon\Carbon;

trait PokemonEncounterTrait
{
    /**
     * Starter pokemon
     *
     * @param $user
     * @param $type
     * @return array
     */
    public function starter($user, $type)
    {
        $object = (object)[
            'action' => 'starter',
            'rarity' => $this->rarity() ?: 1,
            'type' => $type
        ];

        $data = $this->incrementStats($this->initialStats($this->generatePokemon($object), 5));
        $data->user_id = $user;
        $data->in_team = 1;

        return (array)$data;
    }

    /**
     * Reward pokemon
     *
     * @param $rarity
     * @param string $id
     * @return array
     */
    public function rewardPokemon($rarity, $id = 'catch')
    {
        $object = (object)[
            'action' => 'reward',
            'rarity' => ($rarity != 0) ? $rarity : $this->rarity(),
            'id' => $id
        ];

        $data = $this->incrementStats($this->initialStats($this->generatePokemon($object), 5));
        $data->user_id = auth()->user()->id;

        return (array)$data;
    }

    /**
     * Evolve pokemon
     *
     * @param $pokemon
     * @param $method
     * @return mixed
     */
    public function evolve($pokemon, $method)
    {
        $object = (object)[
            'action' => 'evolution',
            'id' => $pokemon->pokemon_id,
            'method' => $method
        ];

        $data = (array)$this->incrementStats($this->initialStats($this->generatePokemon($object), $pokemon->level));
        $data['gender'] = $pokemon->gender;
        $data['vitamins_used'] = 0;

        $pokemon->update($data);

        return $data['pokemon_id'];
    }

    /**
     * Randomize encounter rarity
     *
     * @return int
     */
    public function rarity($boss = false)
    {
        $club = 0;

        //club privileges
        if (auth()->check() && auth()->user()->club_id) {
            foreach (auth()->user()->club->privileges as $privilege) {
                if ($privilege->pivot->expired_on > Carbon::now() && $privilege->modifier == 'find') {
                    $club += $privilege->value;
                }
            }
        }

        if ($boss) {
            if (random_int(1, 10000) <= 7900) {
                $rarity = 2;
            } else if (random_int(1, 10000) <= 800 + $club) {
                $rarity = 3;
            } else if (random_int(1, 10000) <= 30 + $club) {
                $rarity = 4;
            } else {
                $rarity = 2;
            }
        } else {
            if (random_int(1, 10000) <= 8000) {
                $rarity = 1;
            } else if (random_int(1, 10000) <= 1500 + $club) {
                $rarity = 2;
            } else if (random_int(1, 10000) <= 70 + $club) {
                $rarity = 3;
            } else if (random_int(1, 10000) <= 30 + $club) {
                $rarity = 4;
            } else if (random_int(1, 10000) <= 10 + $club) {
                $rarity = 5;
            } else {
                $rarity = 1;
            }
        }

        return $rarity;
    }

    /**
     * Get pokemon query by rarity
     *
     * @param $object
     * @return bool
     */
    public function generatePokemon($object)
    {
        switch ($object->action) {
            case 'starter':
                return $this->where('rarity', $object->rarity)
                    ->where('primary_type', $object->type)
                    ->where('alternate_form', 0)
                    ->whereNotIn('id', [793,794,795,796,797,798,799,800])
                    ->inRandomOrder()
                    ->first(['id', 'primary_type', 'secondary_type', 'gender_ratio', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']);
                break;

            case 'evolution':
                return $this->where('evolution_from', $object->id)->where('evolution_type', $object->method)
                    ->first(['id', 'primary_type', 'secondary_type', 'gender_ratio', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']);
                break;

            case 'wild':
                $wild = $this->where('dungeon_id', $object->dungeon)->where('rarity', $object->rarity)
                    ->inRandomOrder()->first(['id', 'primary_type', 'secondary_type', 'gender_ratio', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']);

                if (!$wild) {
                    $wild = $this->where('dungeon_id', $object->dungeon)->where('rarity', $object->rarity - 1)
                        ->inRandomOrder()->first(['id', 'primary_type', 'secondary_type', 'gender_ratio', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']);
                }

                return $wild;

                break;
            case 'reward':
                if (is_numeric($object->id)) {
                    return $this->where('id', $object->id)
                        ->first(['id', 'primary_type', 'secondary_type', 'gender_ratio', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']);
                }

                return $this->where('rarity', $object->rarity)->where('alternate_form', 0)
                    ->whereNotIn('id', [793,794,795,796,797,798,799,800])->inRandomOrder()
                    ->first(['id', 'primary_type', 'secondary_type', 'gender_ratio', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']);

                break;
        }

        return false;
    }

    /**
     * Pokedex index
     *
     * @return mixed
     */
    public function pokedex()
    {
        return $this->where('alternate_form', 0)->select(['id', 'name', 'primary_type', 'secondary_type'])->simplePaginate(10);
    }

    /**
     * Pokedex search
     *
     * @param $keyword
     * @return mixed
     */
    public function pokedexSearch($keyword)
    {
        return $this->where('name', 'LIKE', "%{$keyword}%")->select(['id', 'name', 'primary_type', 'secondary_type'])->limit(15)->get();
    }
}