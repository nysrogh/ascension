<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/25/2017
 * Time: 2:51 PM
 */

namespace App\Traits;


trait PokemonMoveTrait
{
    /**
     * Assign generated moves to pokemon
     *
     * @param $pokemon
     * @return bool
     */
    public function assignMoves($pokemon)
    {
        $moves = $this->generate(
            array_filter([$pokemon->info->primary_type, $pokemon->info->secondary_type], function ($var) {
                return !is_null($var);
            }), 5);

        foreach ($moves as $move) {
            $pokemon->moves()->attach($move);
        }

        return false;
    }

    /**
     * Generate moves for pokemon
     *
     * @param $types
     * @param $level
     * @return array
     */
    public function generate($types, $level)
    {
        $array = [];
        $count = sqrt($level) <= 4 ? round(sqrt($level)) : 4;

        for ($i = 1; $i <= $count; $i++) {
            $array[] = $this->whereIn('type', $this->categories($types))
                ->where('id', '!=', 86)
                ->where('level', '<=', $level)
                ->where('is_utility', 0)
                ->inRandomOrder()->first();

            if ($i < 4 && random_int(1,100) <= 30) {
                $array[] = $this->where('level', '<=', $level)->where('is_utility', 2)->inRandomOrder()->first();

                $i++;
            }
        }

        return array_map("unserialize", array_unique(array_map("serialize", $array)));
    }

    /**
     * Get type categories
     *
     * @param $types
     * @return bool
     */
    public function categories($types)
    {
        $array = [];

        $cat1 = ['ice', 'water', 'ground', 'rock', 'grass', 'bug'];
        $cat2 = ['fighting', 'dragon', 'normal', 'fairy', 'flying', 'steel'];
        $cat3 = ['poison', 'psychic', 'electric', 'ghost', 'dark', 'fire'];

        if (count(array_intersect($types, $cat1))) {
            $array[] = $cat1;
        }

        if (count(array_intersect($types, $cat2))) {
            $array[] = $cat2;
        }

        if (count(array_intersect($types, $cat3))) {
            $array[] = $cat3;
        }

        return array_collapse($array);
    }

    /**
     * Retrieve move details
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->select(['id', 'level', 'name', 'type'])->findOrFail($id);
    }

    /**
     * Retrieve all moves by type
     *
     * @param $type
     * @return mixed
     */
    public function allByType($type)
    {
        return $this->where('type', $type)->where('is_utility', '!=', 2)->orderBy('level')->select(['id', 'name', 'level', 'type', 'primary_attr', 'description'])->simplePaginate(10);
    }

    /**
     * Pokemon learn the move
     *
     * @param $pokemon
     * @param $move
     * @return bool
     */
    public function learn($pokemon, $move)
    {
        $pokemon->moves()->attach($move->id);

        auth()->user()->gold -= $move->level * 695;

        if (auth()->user()->gold < 0) {
            auth()->user()->gold = 0;
        }

        auth()->user()->save();

        return true;
    }

    /**
     * Check if move type belongs to the category
     *
     * @param $type
     * @param $move
     * @return bool
     */
    public function typeCategory($type, $move)
    {
        //type of pokemon and learn move
        $types = [$type, $move];

        $cat1 = ['ice', 'water', 'ground', 'rock', 'grass', 'bug'];
        $cat2 = ['fighting', 'dragon', 'normal', 'fairy', 'flying', 'steel'];
        $cat3 = ['poison', 'psychic', 'electric', 'ghost', 'dark', 'fire'];

        if (count(array_diff($types, $cat1)) == 0) {
            return true;
        } elseif (count(array_diff($types, $cat2)) == 0) {
            return true;
        } elseif (count(array_diff($types, $cat3)) == 0) {
            return true;
        }

        return false;
    }
}