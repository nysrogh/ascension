<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/25/2017
 * Time: 8:16 AM
 */

namespace App\Traits;


trait PokemonStatsTrait
{
    /**
     * Calculate initial stats
     *
     * @param $pokemon
     * @param $level
     * @return object
     */
    public function initialStats($pokemon, $level)
    {
        $array = [];
        $ratings = 0;
        $perfect = 0;
        $stats = ['hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed'];

        foreach ($stats as $attr) {
            $value = random_int($pokemon->{$attr} * .80, $pokemon->{$attr});
            ($attr == 'hp') ? $array[$attr] = round($value * 1.35) : $array[$attr] = $value;
            $ratings += $value;
            $perfect += $pokemon->{$attr};
        }

        $array['pokemon_id'] = $pokemon->id;
        $array['gender'] = $this->gender($pokemon->gender_ratio);
        $array['ratings'] = round(($ratings / $perfect) * 100);
        $array['level'] = $level;

        return (object)$array;
    }

    /**
     * Increment stats based on level
     *
     * @param $pokemon
     * @return mixed
     */
    public function incrementStats($pokemon)
    {
        $increase = (3 + (($pokemon->ratings - 80) / 2)) / 100;
        $hp = 0;
        $attack = 0;
        $defense = 0;
        $special_attack = 0;
        $special_defense = 0;
        $speed = 0;

        for ($i = 1; $i < $pokemon->level; $i++) {
            $hp += $pokemon->hp * $increase;
            $attack += $pokemon->attack * $increase;
            $defense += $pokemon->defense * $increase;
            $special_attack += $pokemon->special_attack * $increase;
            $special_defense += $pokemon->special_defense * $increase;
            $speed += $pokemon->speed * $increase;
        }

        $pokemon->hp += round($hp);
        $pokemon->attack += round($attack);
        $pokemon->defense += round($defense);
        $pokemon->special_attack += round($special_attack);
        $pokemon->special_defense += round($special_defense);
        $pokemon->speed += round($speed);

        return $pokemon;
    }

    /**
     * Generate pokemon gender
     *
     * @param $ratio
     * @return int
     */
    public function gender($ratio)
    {
        if ($ratio != 999) {
            return (random_int(1, 100) <= $ratio) ? 1 : 2;
        }

        return 0;
    }

    /**
     * Evolution lists
     *
     * @param $id
     * @return mixed
     */
    public function evolution($id)
    {
        return $this->where('evolution_from', $id)->select(['id', 'evolution_type', 'name'])->get();
    }

    /**
     * Forms lists
     *
     * @param $id
     * @return mixed
     */
    public function forms($id)
    {
        return $this->where('alternate_form', $id)->select(['id', 'alternate_cost', 'name'])->get();
    }

    /**
     * Change form
     *
     * @param $pokemon
     * @param $form
     * @return bool
     */
    public function changeForm($pokemon, $form)
    {
        $data = (array)$this->incrementStats($this->initialStats($form, $pokemon->level));
        $data['gender'] = $pokemon->gender;
        $data['vitamins_used'] = 0;

        $pokemon->update($data);

        return true;
    }
}