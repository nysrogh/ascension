<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/25/2017
 * Time: 8:38 PM
 */

namespace App\Traits;

use Carbon\Carbon;

trait RewardTrait
{
    /**
     * Available rewards for user
     *
     * @return mixed
     */
    public function available()
    {
        return $this->with(['modifiers.item' => function ($q) {
            $q->select(['id']);
        }, 'user' => function ($q) {
            $q->where('id', auth()->user()->id)->select('id');
        }])->whereHas('user', function ($q) {
            $q->where('id', auth()->user()->id)->select('id');
        })->select(['id', 'title', 'description', 'pokedex', 'dungeon', 'mission', 'hours'])->get();
    }

    /**
     * Find specific rewards for user
     *
     * @param $id
     * @return mixed
     */
    public function findReward($id)
    {
        return $this->with(['modifiers.item' => function ($q) {
            $q->select(['id']);
        }, 'user' => function ($q) {
            $q->where('id', auth()->user()->id)->select('id');
        }])->whereHas('user', function ($q) {
            $q->where('id', auth()->user()->id)->select('id');
        })->select(['id', 'dungeon', 'pokedex', 'mission', 'next_reward', 'hours'])->findOrFail($id);
    }

    /**
     * Time left for a reward to be able to claim again
     *
     * @param $started
     * @return string
     */
    public function timeLeft($started)
    {
        $started = new Carbon($started);

        return Carbon::now() < $started ? $started->diffForHumans() : 'claim';
    }

}