<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/25/2017
 * Time: 9:20 PM
 */

namespace App\Traits;


trait UserItemTrait
{
    /**
     * Add or Update user item
     *
     * @param $item
     * @param $qty
     * @return bool
     */
    public function addOrUpdateItem($item, $qty)
    {
        $exists = $this->where('item_id', $item->id)->where('user_id', auth()->user()->id)->first(['item_id', 'quantity']);

        if (!$exists) {
            $exists = $this->create([
                'user_id' => auth()->user()->id,
                'item_id' => $item->id,
                'quantity' => $qty,
            ]);
        } else {
            $exists->increment('quantity', $qty);
        }

        return $exists->info->name;
    }

    /**
     * Find evolution stone by name
     *
     * @param $name
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function findStone($name)
    {
        return $this->where('user_id', auth()->user()->id)
            ->whereHas('info', function($query) use ($name) {
                $query->where('name', ucwords(preg_replace('/-/', ' ', $name)));
            })->first(['quantity']);
    }

    /**
     * User bag
     *
     * @param $category
     * @return mixed
     */
    public function bag($category)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'category', 'section', 'name']);
        }])
            ->where('user_id', auth()->user()->id)
            ->where('is_held', 0)
            ->whereHas('info', function($query) use ($category) {
                $query->where('category', $category);
            })->select(['id', 'item_id', 'quantity'])->latest()->simplePaginate(10);
    }

    /**
     * Find user item by id
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'category', 'section', 'name', 'description', 'primary_attr', 'primary_value', 'secondary_attr', 'secondary_value']);
        }])->select(['id', 'user_id', 'item_id', 'quantity', 'is_held'])->findOrFail($id);
    }

    /**
     * Update held item from pokemon
     *
     * @param $item
     * @param $pokemon
     * @return bool
     */
    public function hold($item, $pokemon)
    {
        //remove hold item from pokemon
        $this->where('is_held', $pokemon)->update(['is_held' => 0]);

        if ($item->quantity > 1) {
            $this->create([
                'user_id' => auth()->user()->id,
                'item_id' => $item->item_id,
                'quantity' => 1,
                'is_held' => $pokemon
            ]);

            $item->decrement('quantity');
        } else {
            $item->update(['is_held' => $pokemon]);
        }

        return true;
    }

    /**
     * Find by orig item
     *
     * @param $id
     * @return mixed
     */
    public function findByOrigItem($id)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name', 'description']);
        }])->where('user_id', auth()->user()->id)->where('item_id', $id)->select(['id', 'item_id', 'quantity', 'user_id'])->firstOrFail();
    }
}