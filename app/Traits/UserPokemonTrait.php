<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/25/2017
 * Time: 7:21 PM
 */

namespace App\Traits;


trait UserPokemonTrait
{
    /**
     * Generate next level exp
     *
     * @param $level
     * @return float
     */
    public function nextLevel($level)
    {
        return ceil(pow($level, 1.35) * 14.975);
    }

    /**
     * Retrieve starter pokemon
     *
     * @return mixed
     */
    public function starter()
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name']);
        }])->where('user_id', auth()->user()->id)->first(['pokemon_id']);
    }

    /**
     * Count/retrieve pokemon in trade
     *
     * @param bool $count
     * @return mixed
     */
    public function inTrade($count = false)
    {
        if ($count) {
            return $this->where('user_id', auth()->user()->id)->where('in_trade', 1)->count();
        }

        return $this->where('user_id', auth()->user()->id)->where('in_trade', 1)->latest('updated_at')->select(['id', 'pokemon_id'])->get();
    }

    /**
     * Retrieve user pokemon box
     *
     * @param null $user
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function box($user = null)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name', 'primary_type', 'secondary_type']);
        }, 'user' => function ($q) {
            $q->select(['id', 'username']);
        }])
            ->where('user_id', $user ?: auth()->user()->id)
            ->where('in_team', 0)
            ->where('in_shop', 0)
            ->where('in_trade', 0)
            ->orderBy('updated_at', 'desc')->select(['id', 'user_id', 'gender', 'pokemon_id', 'level', 'ratings'])->paginate(10);
    }

    /**
     * Search pokemon in user's box
     *
     * @param $user
     * @param $keyword
     * @return mixed
     */
    public function search($user, $keyword)
    {
        return $this->with(['info' => function ($q) {
                $q->select(['id', 'name', 'primary_type', 'secondary_type']);
            }, 'user' => function ($q) {
                $q->select(['id', 'username']);
            }])
            ->where('user_id', $user)
            ->where('in_team', 0)
            ->where('in_shop', 0)
            ->where('in_trade', 0)
            ->whereHas('info', function ($q) use ($keyword) {
                $q->where('name', 'like', "%{$keyword}%");
            })
            ->orderBy('updated_at', 'desc')->select(['id', 'user_id', 'gender', 'pokemon_id', 'level', 'ratings'])->paginate(10);
    }

    /**
     * View pokemon details
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name', 'rarity', 'primary_type', 'secondary_type', 'hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed']);
        }, 'moves' => function ($q) {
            $q->select(['id', 'name', 'description', 'type', 'primary_attr']);
        }, 'item' => function ($q) {
            $q->with(['info' => function ($q) {
                $q->select(['id', 'name', 'description']);
            }])->select(['id', 'item_id', 'is_held']);
        }])->findOrFail($id);
    }

    /**
     * Get default user team
     *
     * @param null $id
     * @return mixed
     */
    public function team($id = null)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name', 'primary_type', 'secondary_type']);
        }, 'item' => function ($q) {
            $q->select('id');
        }])
            ->where('user_id', $id ?? auth()->user()->id)
            ->where('in_team', '!=', 0)
            ->latest()
            ->select(['id', 'pokemon_id', 'level', 'gender', 'hp'])
            ->get();
    }

    /**
     * Level up pokemon
     *
     * @param $pokemon
     * @return bool
     */
    public function levelUp($pokemon)
    {
        //$pokemon = $this->show($id);

        if ((!session('use') && $pokemon->experience < $this->nextLevel($pokemon->level))) return false;

        $increase = (3 + (($pokemon->ratings - 80) / 2)) / 100;

        $stats = ['hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed'];

        foreach ($stats as $attr) {
            $pokemon->{$attr} += round($pokemon->info->{$attr} * $increase);
        }

        $pokemon->experience -= $this->nextLevel($pokemon->level);
        if ($pokemon->experience <= 0) $pokemon->experience = 0;

        $pokemon->level += 1;
        $pokemon->save();

        return true;
    }

    /**
     * Pokemon in trade list
     *
     * @return mixed
     */
    public function tradeOffers()
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name', 'primary_type', 'secondary_type']);
        }])->where('user_id', auth()->user()->id)->where('in_trade', 1)->latest('updated_at')
            ->select(['id', 'pokemon_id', 'gender', 'level'])->get();
    }

    /**
     * Sell pokemon in bazaar
     *
     * @param $id
     * @return mixed
     */
    public function sell($id)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name']);
        }])->select(['id', 'pokemon_id', 'level'])->findOrFail($id);
    }
}