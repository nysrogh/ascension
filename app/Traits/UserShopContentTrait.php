<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/29/2017
 * Time: 8:14 PM
 */

namespace App\Traits;


trait UserShopContentTrait
{
    /**
     * User shop content - items
     *
     * @param $id
     * @return mixed
     */
    public function itemList($id)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name', 'description']);
        }])->where('user_shop_id', $id)->where('item_id', '!=', 0)
            ->select(['id', 'item_id', 'price', 'available'])->orderBy('price', 'desc')->simplePaginate(5);
    }

    /**
     * User shop content - pokemon
     *
     * @param $id
     * @return mixed
     */
    public function pokemonList($id)
    {
        return $this->with(['pokemon' => function ($q) {
            $q->with(['info' => function ($q) {
                $q->select(['id', 'name', 'primary_type', 'secondary_type']);
            }])->select(['id', 'pokemon_id', 'level', 'ratings']);
        }, 'trades'])->where('user_shop_id', $id)->where('user_pokemon_id', '!=', 0)
            ->select(['id', 'user_pokemon_id', 'price', 'available'])->orderBy('price', 'desc')->simplePaginate(5);
    }

    /**
     * Search store
     *
     * @param $keyword
     * @return mixed
     */
    public function searchStore($keyword)
    {
        return $this->with(['info' => function ($q) {
            $q->select(['id', 'name']);
        }, 'pokemon' => function ($q) {
            $q->with(['info' => function ($q) {
                $q->select(['id', 'name', 'primary_type', 'secondary_type']);
            }])->select(['id', 'pokemon_id', 'level', 'ratings']);
        }, 'shop' => function ($q) {
            $q->select(['id', 'name']);
        }])->whereHas('info', function($q) use ($keyword) {
            $q->where('name', 'LIKE', "%{$keyword}%");
        })->orWhereHas('pokemon.info', function($q) use ($keyword) {
            $q->where('name', 'LIKE', "%{$keyword}%");
        })->limit(15)->select(['id', 'user_shop_id', 'user_pokemon_id', 'item_id', 'price'])->get();
    }

    /**
     * Find shop content
     *
     * @param $cat
     * @param $id
     * @return mixed
     */
    public function findContent($cat, $id)
    {
        if ($cat == 'item') {
            return $this->where('user_shop_id', auth()->user()->shop->id)->where('item_id', $id)->first();
        }

        return $this->where('user_shop_id', auth()->user()->shop->id)->where('user_pokemon_id', $id)->first();
    }
}