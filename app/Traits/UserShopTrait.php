<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 9/29/2017
 * Time: 7:38 PM
 */

namespace App\Traits;

use Intervention\Image\ImageManagerStatic as Image;

trait UserShopTrait
{
    /**
     * Retrieve random stores
     *
     * @return mixed
     */
    public function randomShop()
    {
        return $this->with(['contents' => function ($q) {
            $q->select(['available']);
        }, 'user' => function ($q) {
            $q->select(['id', 'avatar']);
        }])
            ->whereHas('contents', function ($q) {
                $q->where('available', '>=', 1);
            })
            ->where('user_id', '!=', auth()->user()->id)->select(['id', 'user_id', 'name', 'description'])
            ->inRandomOrder()->limit(10)->get();
    }

    /**
     * Show user shop
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->with(['user' => function ($q) {
            $q->select(['id', 'username', 'gold', 'avatar']);
        }])->select(['id', 'user_id', 'name', 'description'])->findOrFail($id);
    }

    /**
     * User shop background
     *
     * @param $owner
     * @param $buyer
     * @return string
     */
    public function background($owner, $buyer)
    {
        $background = Image::make(public_path("images/locations/shop.png"));

        //owner
        $background->insert(public_path("images/trainers/thumbnail/{$owner}.png"), '', 25, 33);
        //buyer
        $background->insert(public_path("images/trainers/thumbnail/{$buyer}.png"), '', 80, 60);

        return 'data:image/png;base64,' . base64_encode($background->encode('jpg', 70));
    }
}