<?php
/**
 * Created by PhpStorm.
 * User: Taguiwalo
 * Date: 9/24/2017
 * Time: 10:04 PM
 */

namespace App\Traits;

use Carbon\Carbon;

trait UserTrait
{
    /**
     * Get users within an hour
     *
     * @return mixed
     */
    public function currentUsers()
    {
        return $this->whereIn('status', [1,2])
            ->where('updated_at', '>=', Carbon::now()->subHours(6))
            ->select(['id', 'username'])->get();
    }

    /**
     * Update user Pokedex
     *
     * @param $id
     * @param null $user
     * @return bool
     */
    public function pokedex($id, $user = null)
    {
        $userDex = ($user) ? auth()->user()->select(['pokedex'])->findOrFail($user) : auth()->user();
        $pokedex = explode(',', $userDex->pokedex);

        if (!in_array($id, $pokedex)) {
            $newDex = implode(',',array_prepend($pokedex, $id));

            $userDex->pokedex = $newDex;
            $userDex->save();
        }

        return false;
    }

    /**
     * Profile by username
     *
     * @param null $username
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function profile($username = null)
    {
        return $this->with(['arena' => function ($q) {
            $q->select(['user_id', 'rank']);
        }, 'club' => function ($q) {
            $q->select(['id', 'badge', 'name']);
        }, 'shop' => function ($q) {
            $q->select(['id', 'user_id']);
        }])->where('username', $username ?? auth()->user()->username)->select(['id', 'status', 'username', 'avatar', 'gender', 'pokedex', 'dungeons_completed', 'missions_completed', 'battle_win', 'battle_lose', 'club_id', 'club_role', 'created_at'])->firstOrFail();
    }

    /**
     * Search user
     *
     * @param $keyword
     * @return mixed
     */
    public function search($keyword)
    {
        return $this->where('username', 'LIKE', "%{$keyword}%")->select(['username', 'status', 'updated_at'])->limit(15)->get();
    }

    /**
     * Retrieve club's leader(s)
     *
     * @param $club
     * @return mixed
     */
    public function clubLeader($club)
    {
        return $this->where('club_id', $club)->where('club_role', 3)->select(['username'])->get();
    }

    /**
     * Retrieve members
     *
     * @param $club
     * @return mixed
     */
    public function clubMembers($club)
    {
        return $this->where('club_id', $club)->orderBy('club_point', 'desc')->select(['id', 'username', 'club_role', 'club_point', 'avatar', 'gender'])->paginate(15);
    }

    /**
     * Check user club status
     *
     * @param $id
     * @return mixed
     */
    public function userClubStatus($id)
    {
        return $this->select(['id', 'club_id', 'club_role', 'username', 'club_point'])->findOrFail($id);
    }

    public function countRoles($role)
    {
        return $this->where('club_id', auth()->user()->club_id)->where('club_role', $role)->count();
    }
}