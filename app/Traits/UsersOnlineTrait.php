<?php
/**
 * Created by PhpStorm.
 * User: Dante
 * Date: 5/10/2017
 * Time: 11:21 AM
 */
namespace App\Traits;

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

trait UsersOnlineTrait
{
	public function currentUsers()
    {
        return $this->whereIn('status', [1,2])
            ->where('updated_at', '>=', Carbon::now()->subHours(6))
            ->select(['id', 'username', 'ip_address'])->get();
    }
	
    public function allOnline()
    {
		return Cache::remember('online-users', 15, function () {
            return $this->currentUsers()->filter->isOnline();
        });
    }

    public function isOnline()
    {
        return Cache::has($this->getCacheKey());
    }

    public function alreadyOnline($username, $byIp = false)
    {
        if ($byIp) {
            $count = 0;
            foreach ($this->allOnline() as $user) {
                if ($user->ip_address == $username) {
                    $count++;
                }
            }

            return $count;
        }

        foreach ($this->allOnline() as $user) {
            if ($user->username == $username) {
                return true;
            }
        }

        return false;
    }

    public function onlineClubMembers($club)
    {
        $count = 0;

        foreach ($this->allOnline()->where('club_id', $club) as $user) {
            $count++;
        }

        return $count;
    }

    public function getLocation()
    {
        if (empty($cache = Cache::get($this->getCacheKey()))) {
            return 0;
        }
        return $cache['location'];
    }

//    public function onlineByLocation($location)
//    {
//        return $this->allOnline()
//            ->where('location', $location)
//            ->count();
//    }

    public function getCachedAt()
    {
        if (empty($cache = Cache::get($this->getCacheKey()))) {
            return 0;
        }
        return $cache['cachedAt'];
    }

    public function setCache($minutes = 5)
    {
        return Cache::put($this->getCacheKey(), $this->getCacheContent(), $minutes);
    }

    public function getCacheContent()
    {
        if (!empty($cache = Cache::get($this->getCacheKey()))) {
            return $cache;
        }
        $cachedAt = Carbon::now();
        return [
            'cachedAt' => $cachedAt,
            'user' => $this
        ];
    }

    public function pullCache()
    {
        Cache::pull($this->getCacheKey());
    }

    public function getCacheKey()
    {
        return sprintf('%s-%s', "UserOnline", $this->id);
    }
}