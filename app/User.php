<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\UsersOnlineTrait;

class User extends Authenticatable
{
    use Notifiable;
    use UsersOnlineTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'gender', 'password', 'avatar', 'setting_bgcolor'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pokemon()
    {
        return $this->hasMany('App\UserPokemon');
    }

    public function rewards()
    {
        return $this->belongsToMany('App\Reward')->withPivot('claim');
    }

    public function arena()
    {
        return $this->hasOne('App\Arena');
    }

    public function shop()
    {
        return $this->hasOne('App\UserShop');
    }

    public function club()
    {
        return $this->belongsTo('App\Club');
    }

    public function mission()
    {
        return $this->belongsTo('App\Mission');
    }

    public function war()
    {
        return $this->belongsToMany('App\War')->withPivot(['win', 'lose', 'attack_left']);
    }

    public function trades()
    {
        return $this->hasMany('App\Trade');
    }
}
