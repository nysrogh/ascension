<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserItem extends Model
{
    protected $fillable = [
        'user_id', 'item_id', 'quantity', 'is_held', 'primary_value', 'upgrade'
    ];

    public function info()
    {
        return $this->hasOne('App\Item', 'id', 'item_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function addOrUpdateItem($item, $qty)
    {
        $exists = $this->where('item_id', $item->id)->where('user_id', auth()->user()->id)->first();

        if (!$exists) {
            $exists = $this->create([
                'user_id' => auth()->user()->id,
                'item_id' => $item->id,
                'quantity' => $qty,
            ]);
        } else {
            $exists->increment('quantity', $qty);
        }

        return $exists->info->name;
    }
}
