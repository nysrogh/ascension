<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPokemon extends Model
{
    protected $table = 'user_pokemon';

    protected $fillable = [
        'user_id', 'pokemon_id', 'gender', 'hp', 'attack', 'level',
        'defense', 'special_attack', 'special_defense', 'speed',
        'ratings', 'in_team', 'in_trade', 'in_shop', 'vitamins_used',
        'tmp_hp', 'tmp_attack', 'tmp_defense', 'tmp_special_attack', 'tmp_special_defense', 'tmp_speed'
    ];

    public function info()
    {
        return $this->hasOne('App\Pokemon', 'id', 'pokemon_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function item()
    {
        return $this->hasOne('App\UserItem', 'is_held', 'id');
    }

    public function moves()
    {
        return $this->belongsToMany('App\Move');
    }

    public function offer()
    {
        return $this->belongsToMany('App\Content');
    }
}
