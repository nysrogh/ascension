<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserShop extends Model
{
    protected $fillable = [
        'user_id', 'pokemon_id', 'gender', 'hp', 'attack', 'level',
        'defense', 'special_attack', 'special_defense', 'speed',
        'ratings', 'in_team'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function contents()
    {
        return $this->hasMany('App\Content');
    }
}
