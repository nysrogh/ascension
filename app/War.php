<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class War extends Model
{
    protected $fillable = [
        'club_id', 'enemy_club', 'status', 'members', 'score'
    ];
}
