<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->tinyInteger('status')->default(0);
            $table->boolean('gender')->default(1);
            $table->string('avatar')->nullable();
            $table->mediumInteger('club_id')->default(0);
            $table->tinyInteger('club_role')->default(1);
            $table->mediumInteger('club_point')->default(0);
            $table->tinyInteger('floor_id')->unsinged()->default(1);
            $table->integer('gold')->default(1000);
            $table->mediumInteger('crystal')->default(0);
            $table->mediumInteger('medal')->default(0);
            $table->string('arena_message')->default("Too bad! You need to train harder.");
            $table->string('setting_bgcolor')->default('#000000');
            $table->mediumText('pokedex')->nullable();
            $table->mediumInteger('dungeons_completed')->default(0);
            $table->mediumInteger('missions_completed')->default(0);
            $table->smallInteger('mission_id')->default(0);
            $table->mediumInteger('mission_progress')->default(0);
            $table->tinyInteger('mission_left')->default(10);
            $table->mediumInteger('battle_win')->default(0);
            $table->mediumInteger('battle_lose')->default(0);
            $table->tinyInteger('legend_limit')->default(0);
            $table->string('ip_address')->nullable();
            $table->mediumText('report')->nullable();
            $table->mediumText('like')->nullable();
            $table->mediumText('dislike')->nullable();
            $table->mediumText('introduction')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
