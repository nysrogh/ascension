<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePokemonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->tinyInteger('dungeon_id')->default(0);
            $table->string('primary_type')->nullable();
            $table->string('secondary_type')->nullable();
            $table->string('name')->nullable();
            $table->smallInteger('evolution_from')->default(0);
            $table->string('evolution_type')->nullable();
            $table->smallInteger('alternate_form')->default(0);
            $table->smallInteger('alternate_cost')->default(0);
            $table->tinyInteger('rarity')->default(1);
            $table->smallInteger('gender_ratio')->default(50);
            $table->smallInteger('catch_rate')->default(0);
            $table->smallInteger('hp')->unsigned()->default(0);
            $table->smallInteger('attack')->unsigned()->default(0);
            $table->smallInteger('defense')->unsigned()->default(0);
            $table->smallInteger('special_attack')->unsigned()->default(0);
            $table->smallInteger('special_defense')->unsigned()->default(0);
            $table->smallInteger('speed')->unsigned()->default(0);
            $table->mediumText('description')->nullable();
            $table->smallInteger('front_x')->default(132);
            $table->tinyInteger('front_y')->default(29);
            $table->tinyInteger('back_x')->default(23);
            $table->tinyInteger('back_y')->default(67);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemon');
    }
}
