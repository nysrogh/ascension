<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPokemonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_pokemon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->smallInteger('pokemon_id')->unsigned();
            $table->tinyInteger('level')->default(1);
            $table->tinyInteger('gender')->default(0);
            $table->integer('experience')->default(0);
            $table->smallInteger('hp')->unsigned()->default(0);
            $table->smallInteger('attack')->unsigned()->default(0);
            $table->smallInteger('defense')->unsigned()->default(0);
            $table->smallInteger('special_attack')->unsigned()->default(0);
            $table->smallInteger('special_defense')->unsigned()->default(0);
            $table->smallInteger('speed')->unsigned()->default(0);
            $table->smallInteger('tmp_hp')->default(0);
            $table->smallInteger('tmp_attack')->default(0);
            $table->smallInteger('tmp_defense')->default(0);
            $table->smallInteger('tmp_special_attack')->default(0);
            $table->smallInteger('tmp_special_defense')->default(0);
            $table->smallInteger('tmp_speed')->default(0);
            $table->tinyInteger('ratings')->default(0);
            $table->tinyInteger('in_shop')->default(0);
            $table->tinyInteger('in_team')->default(0);
            $table->tinyInteger('in_trade')->default(0);
            $table->tinyInteger('vitamins_used')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('pokemon_id')->references('id')->on('pokemon')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_pokemon');
    }
}
