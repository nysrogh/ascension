<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moves', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->tinyInteger('level')->default(1);
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->string('primary_attr')->nullable();
            $table->tinyInteger('primary_value')->unsigned()->default(0);
            $table->string('secondary_attr')->nullable();
            $table->tinyInteger('secondary_value')->unsigned()->default(0);
            $table->tinyInteger('is_utility')->default(0);
            $table->mediumText('description')->nullable();
            $table->timestamps();
        });

        Schema::create('move_user_pokemon', function (Blueprint $table) {
            $table->mediumInteger('move_id')->unsigned();
            $table->integer('user_pokemon_id')->unsigned();

            $table->foreign('move_id')->references('id')->on('moves')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_pokemon_id')->references('id')->on('user_pokemon')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['move_id', 'user_pokemon_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moves');
        Schema::dropIfExists('moves_user_pokemon');
    }
}
