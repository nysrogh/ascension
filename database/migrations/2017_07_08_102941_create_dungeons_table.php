<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDungeonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dungeons', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('floor_id')->unsigned();
            $table->tinyInteger('background')->default(1);
            $table->string('name')->nullable();
            $table->string('types')->nullable();
            $table->tinyInteger('level')->unsigned()->default(1);
            $table->tinyInteger('distance')->unsigned()->default(1);
            $table->mediumText('description')->nullable();
            $table->tinyInteger('is_unlocked')->default(0);
            $table->integer('current_mp')->default(0);
            $table->integer('max_mp')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dungeons');
    }
}
