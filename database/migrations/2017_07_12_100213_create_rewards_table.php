<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewards', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('title');
            $table->mediumText('description');
            $table->smallInteger('pokedex')->default(0);
            $table->smallInteger('dungeon')->default(0);
            $table->smallInteger('next_reward')->default(0);
            $table->tinyInteger('hours')->unsigned()->default(0);
            $table->timestamps();
        });

        Schema::create('reward_modifiers', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->smallInteger('reward_id')->unsigned();
            $table->string('attribute')->nullable();
            $table->smallInteger('item_id')->default(0);
            $table->tinyInteger('rarity')->default(0);
            $table->smallInteger('value')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('reward_id')->references('id')->on('rewards')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('reward_user', function (Blueprint $table) {
            $table->smallInteger('reward_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamp('claim')->nullable();

            $table->foreign('reward_id')->references('id')->on('rewards')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['reward_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rewards');
        Schema::dropIfExists('reward_user');
        Schema::dropIfExists('reward_modifiers');
    }
}
