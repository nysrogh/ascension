<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('category')->default(1); //0 - pokeball, 1 - recovery, 2 - held, 3 - misc
            $table->tinyInteger('section')->default(0); //0 - pokeball, 1 - recovery, 2 - held, 3 - misc
            $table->string('name')->nullable();
            $table->string('primary_attr')->nullable();
            $table->smallInteger('primary_value')->default(0);
            $table->string('secondary_attr')->nullable();
            $table->smallInteger('secondary_value')->nullable();
            $table->mediumInteger('price')->default(1);
            $table->mediumText('description')->nullable();
            $table->timestamps();
        });

        Schema::create('user_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->smallInteger('primary_value')->unsigned()->default(0);
            $table->tinyInteger('upgrade')->unsigned()->default(0);
            $table->smallInteger('quantity')->default(1);
            $table->integer('is_held')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
