<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_shops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('name')->nullable();
            $table->mediumText('description')->nullable();
            $table->tinyInteger('is_open')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_shop_id')->unsigned();
            $table->integer('user_pokemon_id')->default(0);
            $table->integer('item_id')->default(0);
            $table->integer('price')->default(1);
            $table->smallInteger('primary_value')->default(0);
            $table->tinyInteger('upgrade')->default(0);
            $table->integer('price')->default(1);
            $table->tinyInteger('available')->default(1);
            $table->tinyInteger('is_sold')->default(0);
            $table->timestamps();


            $table->foreign('user_shop_id')->references('id')->on('user_shops')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('content_user_pokemon', function (Blueprint $table) {
            $table->integer('content_id')->unsigned();
            $table->integer('user_pokemon_id')->unsigned();

            $table->foreign('user_pokemon_id')->references('id')->on('user_pokemon')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('content_id')->references('id')->on('contents')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_shops');
    }
}
