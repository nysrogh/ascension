<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('name')->nullable();
            $table->tinyInteger('level')->default(1);
            $table->integer('point')->default(0);
            $table->integer('coin')->default(0);
            $table->string('badge')->nullable();
            $table->string('max_member')->default(15);
            $table->string('max_privilege')->default(1);
            $table->string('description')->nullable();
            $table->string('notice')->nullable();
            $table->smallInteger('war_win')->default(0);
            $table->smallInteger('war_lose')->default(0);
            $table->timestamps();
        });

        Schema::create('club_applicants', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('club_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->timestamps();
        });

        //privileges
        Schema::create('privileges', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('title')->nullable();
            $table->tinyInteger('level')->default(1);
            $table->mediumInteger('coin_cost')->default(0);
            $table->string('modifier')->nullable();
            $table->smallInteger('value')->default(0);
            $table->tinyInteger('expiration')->default(0);
            $table->string('description')->nullable();
            $table->timestamps();
        });

        //club privileges
        Schema::create('club_privilege', function (Blueprint $table) {
            $table->mediumInteger('club_id')->unsigned();
            $table->tinyInteger('privilege_id')->unsigned();
            $table->timestamp('expired_on')->nullable();

            $table->foreign('club_id')->references('id')->on('clubs')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('privilege_id')->references('id')->on('privileges')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['club_id', 'privilege_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
