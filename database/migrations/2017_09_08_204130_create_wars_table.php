<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wars', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->mediumInteger('club_id');
            $table->mediumInteger('enemy_club');
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('members')->default(10);
            $table->tinyInteger('score')->default(0);
            $table->timestamps();
        });

        Schema::create('user_war', function (Blueprint $table) {
            $table->mediumInteger('war_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('win')->default(0);
            $table->tinyInteger('lose')->default(0);
            $table->tinyInteger('attack_left')->default(2);
            $table->timestamps();

            $table->foreign('war_id')->references('id')->on('wars')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['war_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wars');
    }
}
