<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->foreign('content_id')->references('id')->on('contents')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->increments('id');
            $table->integer('content_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('trade_offers', function (Blueprint $table) {
            $table->foreign('trade_id')->references('id')->on('trades')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->increments('id');
            $table->integer('trade_id')->unsigned();
            $table->integer('user_pokemon_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->integer('gold')->nullable();
            $table->integer('crystal')->nullable();
            $table->integer('primary_value')->default(0);
            $table->integer('upgrade')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
