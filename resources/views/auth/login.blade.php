@extends('layouts.app')

@section('title', "Not Logged In")

@section('content')
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="card card-outline-warning">
                    <div class="card-header p-1"><strong>Session Expired</strong></div>
                    <div class="card-block p-1 bg-faded">
                        <div class="mt-2">
                            Your session has expired! Please return to <a href="{{ url('/') }}">Homepage</a> to login.
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">Not Logged In</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection