@extends('layouts.app')

@section('title', 'Register')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Register</strong></div>
                    <div class="card-block">
                        <form action="{{ url('/register') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group @if ($errors->has('username')) has-danger @endif">
                                <label class="form-control-label mb-0" for="username">Username</label>
                                <input type="text" class="form-control form-control-danger" id="username" name="username" value="{{ old('username') }}" required>

                                @if ($errors->has('username'))
                                    <div class="form-control-feedback">{{ $errors->first('username') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('email')) has-danger @endif">
                                <label class="form-control-label mb-0" for="email">Email Address</label>
                                <input type="email" class="form-control form-control-danger" id="email" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                                @endif
                                <small class="form-text text-muted">Your email will only be used to reset your password.</small>
                            </div>

                            <div class="form-group @if ($errors->has('password')) has-danger @endif">
                                <label class="form-control-label mb-0" for="password">Password</label>
                                <input type="password" class="form-control form-control-danger" id="password" name="password" required>

                                @if ($errors->has('password'))
                                    <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('password_confirmation')) has-danger @endif">
                                <label class="form-control-label mb-0" for="password_confirmation">Repeat Password</label>
                                <input type="password" class="form-control form-control-danger" id="password_confirmation" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <div class="form-control-feedback">{{ $errors->first('password_confirmation') }}</div>
                                @endif
                            </div>

                            <hr>

                            <div class="form-group @if ($errors->has('gender')) has-danger @endif">
                                <label class="form-control-label mb-0" for="gender">Gender</label>
                                <select name="gender" id="gender" class="form-control form-control-danger" required>
                                    <option value="" selected disabled>[select]</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </select>

                                @if ($errors->has('gender'))
                                    <div class="form-control-feedback">{{ $errors->first('gender') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('type')) has-danger @endif">
                                <label class="form-control-label mb-0" for="type">Favorite Type</label>
                                <select name="type" id="type" class="form-control form-control-danger" required>
                                    <option value="" selected disabled>[select]</option>
                                    <option value="normal">Normal</option>
                                    <option value="fire">Fire</option>
                                    <option value="fighting">Fighting</option>
                                    <option value="water">Water</option>
                                    <option value="flying">Flying</option>
                                    <option value="grass">Grass</option>
                                    <option value="poison">Poison</option>
                                    <option value="electric">Electric</option>
                                    <option value="ground">Ground</option>
                                    <option value="psychic">Psychic</option>
                                    <option value="rock">Rock</option>
                                    <option value="ice">Ice</option>
                                    <option value="bug">Bug</option>
                                    <option value="ghost">Ghost</option>
                                    <option value="dark">Dark</option>
                                    <option value="steel">Steel</option>
                                    <option value="fairy">Fairy</option>
                                    <option value="dragon">Dragon</option>
                                </select>

                                @if ($errors->has('type'))
                                    <div class="form-control-feedback">{{ $errors->first('type') }}</div>
                                @endif
                            </div>

                            <hr>

                            <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                <label class="form-control-label mb-0" for="captcha">Enter CAPTCHA</label>
                                <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                <input type="text" class="form-control form-control-danger" id="captcha" name="captcha" autocomplete="off" required>

                                @if ($errors->has('captcha'))
                                    <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                @endif

                                <small class="form-text text-muted">Captcha code: <strong>{{ $captcha }}</strong></small>
                            </div>

                            <p class="text-justify text-warning">
                                Upon registration, you will be given a completely random Pokemon as your starter based on your favorite type.
                            </p>

                            <button type="submit" class="form-control btn btn-success my-1">Proceed</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">Register</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
