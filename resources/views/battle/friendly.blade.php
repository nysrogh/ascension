@extends('layouts.app')

@section('title', 'Friendly Battle!')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-danger bg-faded">
                    <div class="card-header p-1"><strong>Friendly Battle!</strong>@if (!session()->has('result')) <span class="float-right"><a href="{{ route('friendly.end') }}" class="btn btn-sm btn-outline-danger">Forfeit</a></span>@endif</div>
                    <div class="card-block p-1">
                        <div class="row mb-1">
                            <div class="col-4 text-right">
                                @if (is_array(session("friendly.enemy.tmp")))
                                    @foreach (session("friendly.enemy.tmp") as $key => $value)
                                        @if ($key == 'burn')
                                            <span class="badge badge-pill" style="background-color:red; line-height: 6px; border-radius: 5px"><small>BRN</small></span>
                                        @endif

                                        @if ($key == 'poison')
                                            <span class="badge badge-pill" style="background-color:purple; line-height: 6px; border-radius: 5px"><small>PSN</small></span>
                                        @endif

                                        @if ($key == 'confuse')
                                            <span class="badge badge-pill" style="background-color:darkorange; line-height: 6px; border-radius: 5px"><small>CNF</small></span>
                                        @endif

                                        @if ($key == 'freeze')
                                            <span class="badge badge-pill" style="background-color:#0275d8; line-height: 6px; border-radius: 5px"><small>FRZ</small></span>
                                        @endif

                                        @if ($key == 'paralyze')
                                            <span class="badge badge-pill" style="background-color:orange; line-height: 6px; border-radius: 5px"><small>PAR</small></span>
                                        @endif

                                        @if ($key == 'sleep')
                                            <span class="badge badge-pill" style="background-color:#110069; line-height: 6px; border-radius: 5px"><small>SLP</small></span>
                                        @endif

                                        @if ($key == 'infatuate')
                                            <span class="badge badge-pill" style="background-color:#ff00aa; line-height: 6px; border-radius: 5px"><small>INF</small></span>
                                        @endif

                                        @if ($key == 'flinch')
                                            <span class="badge badge-pill" style="background-color:#75453a; line-height: 6px; border-radius: 5px"><small>STN</small></span>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-8">
                                <div>
                                    {{ $enemy['stats']->info->name }} <img class="img-fluid mb-1" src="{{ asset("images/game/".$enemy['stats']->gender.".png") }}" alt="Gender" />
                                    <span class="float-right">
                                       Lv{{ $enemy['stats']->level }}
                                   </span>
                                </div>
                                <span class="float-left badge badge-pill text-warning" style="line-height: 6px; background-color: #444; border-radius: 3px 0 0 3px"><strong>HP</strong></span>
                                <div class="progress" style="border: 1px ridge #000; background-color: #999; border-radius: 0 3px 3px 0">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round($enemy["enemy_current_hp"]/$enemy['stats']->hp * 100) }}%; line-height: 9px; height: 9px; color: #000">

                                        <small>{{ $enemy["enemy_current_hp"] ."/". $enemy['stats']->hp }}</small>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-sm-12 text-center">
                                <img class="img-fluid" src="{{ session('background') }}" width="100%" style="max-width: 215px" alt="">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-8">
                                <div>
                                    {{ $pokemon['stats']->info->name }} <img class="img-fluid mb-1" src="{{ asset("images/game/".$pokemon['stats']->gender.".png") }}" alt="Gender" />
                                    <span class="float-right">
                                       Lv{{ $pokemon['stats']->level }}
                                   </span>
                                </div>
                                <span class="float-left badge badge-pill text-warning" style="line-height: 6px; background-color: #444; border-radius: 3px 0 0 3px"><strong>HP</strong></span>
                                <div class="progress" style="border: 1px ridge #000; background-color: #999; border-radius: 0 3px 3px 0">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round($pokemon['pokemon_current_hp']/$pokemon['stats']->hp * 100) }}%; line-height: 9px; height: 9px; color: #000">

                                        <small>{{ $pokemon["pokemon_current_hp"] ."/". $pokemon['stats']->hp }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                @if (is_array(session('friendly.pokemon.tmp')))
                                    @foreach (session('friendly.pokemon.tmp') as $key => $value)
                                        @if ($key == 'burn')
                                            <span class="badge badge-pill" style="background-color:red; line-height: 6px; border-radius: 5px"><small>BRN</small></span>
                                        @endif

                                        @if ($key == 'poison')
                                            <span class="badge badge-pill" style="background-color:purple; line-height: 6px; border-radius: 5px"><small>PSN</small></span>
                                        @endif

                                        @if ($key == 'confuse')
                                            <span class="badge badge-pill" style="background-color:darkorange; line-height: 6px; border-radius: 5px"><small>CNF</small></span>
                                        @endif

                                        @if ($key == 'freeze')
                                            <span class="badge badge-pill" style="background-color:#0275d8; line-height: 6px; border-radius: 5px"><small>FRZ</small></span>
                                        @endif

                                        @if ($key == 'paralyze')
                                            <span class="badge badge-pill" style="background-color:orange; line-height: 6px; border-radius: 5px"><small>PAR</small></span>
                                        @endif

                                        @if ($key == 'sleep')
                                            <span class="badge badge-pill" style="background-color:#110069; line-height: 6px; border-radius: 5px"><small>SLP</small></span>
                                        @endif

                                        @if ($key == 'infatuate')
                                            <span class="badge badge-pill" style="background-color:#ff00aa; line-height: 6px; border-radius: 5px"><small>INF</small></span>
                                        @endif

                                        @if ($key == 'flinch')
                                            <span class="badge badge-pill" style="background-color:#75453a; line-height: 6px; border-radius: 5px"><small>STN</small></span>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-12" style="border: 1px ridge #000; background: #ddd">
                                @if (!session('result'))
                                    <div class="row small">
                                        @foreach ($pokemon['stats']->moves as $move)
                                            <div class="col-6 my-1">
                                                <form action="{{ route('friendly.fight') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $move->id }}">
                                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                                    <input type="submit" class="form-control form-control-sm" value="{{ $move->name }}">
                                                </form>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                        <hr class="my-1">
                        <div class="col-sm-12 text-center">
                            <div class="row">
                                <div class="col-5">
                                    <img class="img-fluid float-left" width="26" src="{{ asset("images/trainers/thumbnail/".auth()->user()->avatar.".png") }}" alt="">
                                    <div class="text-left">
                                        {{ auth()->user()->username }}
                                        <br>
                                        @foreach (session("friendly.pokemon_team") as $poke)
                                            @if (session()->has("friendly.pokemon_dead") && in_array($poke->id, session("friendly.pokemon_dead")))
                                                <img class="img-fluid" style="opacity: 0.3" width="8" src="{{ asset('images/game/caught.png') }}" alt="">
                                            @else
                                                <img class="img-fluid" width="8" src="{{ asset('images/game/caught.png') }}" alt="">
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-2">
                                    @if (session()->has('result'))
                                        <a href="{{ route('friendly.end') }}" class="btn btn-sm btn-success">Continue</a>
                                    @endif
                                </div>
                                <div class="col-5">
                                    <img class="img-fluid float-right" width="26" src="{{ asset("images/trainers/thumbnail/".session('friendly.opponent')->avatar.".png") }}" alt="">
                                    <div class="text-right">
                                        {{ session('friendly.opponent')->username }}
                                        <br>
                                        @foreach (session("friendly.enemy_team") as $poke)
                                            @if (session()->has("friendly.enemy_dead") && in_array($poke->id, session("friendly.enemy_dead")))
                                                <img class="img-fluid" style="opacity: 0.3" width="8" src="{{ asset('images/game/caught.png') }}" alt="">
                                            @else
                                                <img class="img-fluid" width="8" src="{{ asset('images/game/caught.png') }}" alt="">
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-1">
                        <div class="row">
                            <div class="col-sm-12">
                                <strong>Battle Log (Turns: {{ session('friendly.turns') }})</strong><br>
                                @if (session('friendly_log'))
                                    @foreach (session('friendly_log') as $log)
                                        <div class="p-1">&rsaquo; {!! $log !!}</div>
                                    @endforeach
                                @else
                                    No battle logs yet.
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Arena Battle</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection