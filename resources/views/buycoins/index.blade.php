@extends('layouts.app')

@section('title', 'Buy Coins')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header p-1"><strong>Buy Coins</strong></div>
                    <div class="alert alert-success">
                        &rsaquo; Click each prices to see the <strong>freebies</strong> included with your purchase.
                    </div>
                    <div class="card-block text-justify">
                        <div class="col-sm-12 text-center">
                            Pokemon Ascension is simply made by a fan for fans of Pokemon.<br><br>
                            All Pokemon Names, Images, ect are Copyright By The Respective owners<br><br>
                            Pokemon Ascension is in no way in relation to Nintendo, Gamefreak, Pokemon Company, Creatures Inc, or any other company that holds the rights to Pokemon.
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>
                            <span class="breadcrumb-item active">Legal</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection