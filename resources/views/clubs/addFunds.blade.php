@extends('layouts.app')

@section('title', 'Add Funds')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Add Coins to Treasury</strong></div>
                    <div class="card-block">
                        <div class="alert alert-info p-1">
                            &rsaquo; You earn 1 cp for every <strong>7,000</strong> coins you add to club treasury.
                        </div>
                        <form action="{{ route('club.storeFunds') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            
                            <div class="mb-2">
                                Your coins: <img src="{{ asset('images/game/gold.png') }}" alt="Coins" width="12"> {{ number_format(auth()->user()->gold) }}
                            </div>

                            <div class="form-group @if ($errors->has('amount')) has-danger @endif">
                                <label class="form-control-label mb-0" for="amount">Amount</label>
                                <input type="number" class="form-control form-control-danger" id="amount" name="amount" min="1" max="{{ auth()->user()->gold }}" required>

                                @if ($errors->has('amount'))
                                    <div class="form-control-feedback">{{ $errors->first('amount') }}</div>
                                @endif

                                <small class="form-text text-muted">Specify an amount you want to be added to your club's treasury.</small>
                            </div>

                            <hr class="my-1">

                            <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                <label for="captcha">Enter captcha: <em>{{ $captcha }}</em></label>
                                <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                <input type="text" name="captcha" class="form-control" autocomplete="off" minlength="3" maxlength="3" placeholder="(enter the 3 digit code above)" />

                                @if ($errors->has('captcha'))
                                    <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Add</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('club.index') }}">Club</a>
                            <span class="breadcrumb-item active">Add Coins</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
