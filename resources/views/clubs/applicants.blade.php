@extends('layouts.app')

@section('title', 'Club Applicants')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Club Applicants</strong></div>
                    <div class="card-block p-1">
                        @forelse ($applicants as $applicant)
                            <div class="row">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px groove #fff">
                                    @if (in_array(auth()->user()->club_role, [2,3]))
                                        <div class="float-right">
                                            <form action="{{ route('club.applicantsAction') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $applicant->user->id }}">
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                <select name="appAction" style="background-color: #ddd" required>
                                                    <option value="" selected disabled>- action -</option>
                                                    <option value="1">Accept</option>
                                                    <option value="2">Reject</option>
                                                </select>

                                                <input type="submit" class="btn btn-sm btn-primary" value="Submit" style="line-height: 10px">
                                            </form>
                                        </div>
                                    @endif
                                    <a href="{{ route('trainer.show', $applicant->user->username) }}">
                                    <span class="mr-1 float-left mb-1">
                                        <img class="img-fluid item-list" src="{{ asset("images/trainers/thumbnail/".$applicant->user->avatar.".png") }}" alt="{{ $applicant->user->username }}">
                                    </span>
                                        {{ $applicant->user->username }}</a> <img class="img-fluid" src="{{ asset("images/game/". $applicant->user->gender .".png") }}" alt="Gender" />
                                </div>
                            </div>
                        @empty
                            <p>No applicant(s) as of the moment.</p>
                        @endforelse
                        {{ $applicants->links('vendor.pagination.simple-bootstrap-4') }}
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('club.index') }}">Club</a>
                            <a class="breadcrumb-item" href="{{ route('club.members') }}">Club Members</a>
                            <span class="breadcrumb-item active">Club Applicants</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection