@extends('layouts.app')

@section('title', 'Create Club')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Create Club</strong> <span class="float-right"><img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format(auth()->user()->gold) }}</small></span></div>
                    <div class="alert alert-info mb-1">
                        &rsaquo; Creating your own club costs 30,000 coins
                    </div>
                    <div class="card-block">
                        <form action="{{ route('club.store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <div class="form-group @if ($errors->has('username')) has-danger @endif">
                                <label class="form-control-label mb-0" for="name">Name</label>
                                <input type="text" class="form-control form-control-danger" id="name" name="name" value="{{ old('name') }}" maxlength="20" required>

                                @if ($errors->has('name'))
                                    <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('description')) has-danger @endif">
                                <label class="form-control-label mb-0" for="description">Description</label>
                                <textarea class="form-control form-control-danger" id="description" maxlength="80" name="description" required></textarea>

                                @if ($errors->has('description'))
                                    <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                @endif

                                <small class="form-text text-muted">General description of the club.</small>
                            </div>

                            <div class="form-group @if ($errors->has('badge')) has-danger @endif">
                                <label class="form-control-label mb-0" for="badge">Badge</label>
                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/1.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="1">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/2.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="2">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/3.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="3">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/4.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="4">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/45.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="45">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/46.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="46">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/47.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="47">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/48.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="48">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/9.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="9">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/10.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="10">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/11.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="11">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/12.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="12">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/33.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="33">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/34.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="34">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/35.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="35">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/36.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="36">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/16.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="16">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/15.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="15">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/14.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="14">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/13.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="13">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/17.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="17">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/18.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="18">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/19.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="19">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/20.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="20">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/37.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="37">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/38.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="38">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/39.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="39">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/40.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="40">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/21.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="21">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/22.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="22">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/23.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="23">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/24.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="24">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/28.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="28">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/27.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="27">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/26.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="26">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/25.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="25">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/29.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="29">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/30.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="30">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/31.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="31">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/32.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="32">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/5.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="5">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/8.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="8">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/7.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="7">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/6.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="6">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/41.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="41">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/42.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="42">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/43.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="43">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/44.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="44">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1 text-center">
                                    <div class="col-3"></div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/49.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="49">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset('images/badges/50.png') }}" width="24">
                                        </div>
                                        <div>
                                            <input type="radio" name="badge" value="50">
                                        </div>
                                    </div>
                                    <div class="col-3"></div>
                                </div>

                                @if ($errors->has('badge'))
                                    <div class="form-control-feedback">{{ $errors->first('badge') }}</div>
                                @endif
                            </div>

                            <hr class="my-1">

                            <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                <label for="captcha">Enter the code: <em>{{ $captcha }}</em></label>
                                <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                <input type="text" name="captcha" class="form-control" autocomplete="off" minlength="3" maxlength="3" placeholder="(enter the 3 digit code above)" />

                                @if ($errors->has('captcha'))
                                    <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Create</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.trainers-club') }}">Trainers Club</a>
                            <span class="breadcrumb-item active">Create a Club</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
