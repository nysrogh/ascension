@extends('layouts.app')

@section('title', 'Club Home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 mb-0">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded mb-0">
                    <div class="card-header p-1">
                        <strong>{{ $club->name }}</strong>
                        <span class="float-right small">
                            {{ $club->applicants()->count() }} applicant(s)
                        </span>
                    </div>
                    <div class="card-block p-1 mb-0">
                        <div class="row mb-2">
                            <div class="col-sm-12">
                                <div class=" text-center">
                                    &rsaquo; Club Notice &lsaquo;
                                </div>
                                <div class="mt-1 p-1 text-center" style="border: 1px solid #999; background: #ccc; border-radius: 2px">
                                    <p>{{ $club->notice }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <img class="img-fluid club-badge p-1" src="{{ asset("images/badges/{$club->badge}.png") }}" style="border: 1px ridge #ccc" alt="{{ $club->name }}" />
                                <br>
                                Lv. {{ $club->level }}<br>
                                <div class="progress" style="background: #ccc">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: {{ round(($club->point/$nextLevel)*100, 2) }}%; line-height: 8px; height: 8px; min-width: 2em; color: #000">

                                        <small>{{ round(($club->point/$nextLevel)*100, 2) }}%</small>

                                    </div>
                                </div>
                            </div>
                            <div class="col-9">
                                <table class="table table-sm table-bordered mb-1">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <strong>Treasury:</strong>
                                                <span class="float-right">{{ number_format($club->coin) }} <img src="{{ asset('images/game/gold.png') }}" alt="Coins" width="12" height="12"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Members:</strong> <span class="float-right">{{ $club->members()->count() }}/{{ $club->max_member }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Club Points:</strong> <span class="float-right">{{ number_format($club->point) }}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 mb-1">
                                <a href="{{ route('club.manage') }}" class="btn btn-sm btn-outline-primary form-control">Manage</a>
                            </div>
                            <div class="col-6 mb-1">
                                <a href="{{ route('club.members') }}" class="btn btn-sm btn-outline-info form-control">Members</a>
                            </div>
                            <div class="col-6">
                                <a href="{{ route('club.logs') }}" class="btn btn-sm btn-outline-danger form-control">Logs</a>
                            </div>
                            <div class="col-6">
                                <a href="{{ route('club.addFunds') }}" class="btn btn-sm btn-outline-success form-control">Add Funds</a>
                            </div>
                        </div>
                        <hr class="my-1">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <div class="row">
                                    <div class="col-3">
                                        <a href="{{ route('mission.index') }}">
                                            <div class="mb-1">
                                                <img src="{{ asset('/images/game/mission.png') }}" alt="Missions">
                                            </div>
                                            <div>
                                                Missions
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-3">
                                        <a href="{{ route('privileges.index') }}">
                                            <div class="mb-1">
                                                <img src="{{ asset('/images/game/privilege.png') }}" alt="Perks">
                                            </div>
                                            <div>
                                                Perks
                                            </div>
                                        </a>
                                    </div>
                                    {{--<div class="col-3">--}}
                                        {{--<a href="#">--}}
                                            {{--<div class="mb-1">--}}
                                                {{--<img src="{{ asset('/images/game/wars.png') }}" alt="Wars">--}}
                                            {{--</div>--}}
                                            {{--<div>--}}
                                                {{--War--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    <div class="col-3">
                                        <a href="{{ route('club.mart') }}">
                                            <div class="mb-1">
                                                <img src="{{ asset('/images/game/club-store.png') }}" alt="Store">
                                            </div>
                                            <div>
                                                Store
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-1">
                        <div class="mt-1">
                            <form action="{{ route('club.chat') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="prevent" value="{{ random_int(100, 999) }}">
                                <input type="hidden" name="club_id" value="{{ $club->id }}">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="message" autocomplete="off" maxlength="150" placeholder="(enter chat message here)" required>
                                    <div class="mt-1">
                                        <input type="submit" class="btn btn-sm btn-success" value="Send"> - <a href="" class="btn btn-sm btn-outline-primary">Refresh</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                        @forelse ($club->chats as $chat)
                            <span class="float-left mr-1">
                                <img class="img-fluid" src="{{ asset("/images/trainers/thumbnail/{$chat->users->avatar}.png") }}" alt="#" />
                            </span>
                            <div>
                                <a href="{{ route('trainer.show', $chat->users->username) }}">
                                    <strong>{{ $chat->users->username }}</strong>
                                </a>
                                <span class="float-right"><small>{{ $chat->created_at->diffForHumans() }}</small></span>
                            </div>
                            <div class="card card-outline-secondary mb-1" style="background-color: #fff;">
                                <div class="card-block p-1">
                                    {{ $chat->message }}
                                </div>
                            </div>
                        @empty
                            <p>No chat message(s) found.</p>
                        @endforelse
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ url('/trainers-club') }}">Trainers Club</a>
                            <span class="breadcrumb-item active">{{ $club->name }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection