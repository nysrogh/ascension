@extends('layouts.app')

@section('title', 'Club Logs')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Logs</strong></div>
                    <div class="card-block p-1">
                        @forelse ($logs as $log)
                            <div class="row mt-1">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    <span class="float-right small">{{ $log->created_at->diffForHumans() }}</span>
                                    <span class="mr-1 mb-1">
                                        <i class="fa fa-edit"></i> &rsaquo; {{ $log->message }}
                                    </span>
                                </div>
                            </div>
                        @empty
                            <span>You club logs yet.</span>
                        @endforelse

                        <div class="row">
                            <div class="col-sm-12">
                                {{ $logs->links('vendor.pagination.simple-bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('club.index') }}">Club</a>
                            <span class="breadcrumb-item active">Club Logs</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection