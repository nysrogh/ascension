@extends('layouts.app')

@section('title', 'Manage Club')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Manage Club</strong></div>
                    <div class="card-block">
                        <form action="{{ route('club.edit') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                            <div class="form-group @if ($errors->has('description')) has-danger @endif">
                                <label class="form-control-label mb-0" for="description">Description</label>
                                <textarea class="form-control form-control-danger" id="description" maxlength="150" name="description" required>{{ old('description') ?? $club->description }}</textarea>

                                @if ($errors->has('description'))
                                    <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                @endif
                                <small class="form-text text-muted">General description of the club.</small>
                            </div>

                            <hr class="my-1">

                            <div class="form-group @if ($errors->has('description')) has-danger @endif">
                                <label class="form-control-label mb-0" for="notice">Notice</label>
                                <textarea class="form-control form-control-danger" id="notice" maxlength="150" name="notice" required>{{ old('notice') ?? $club->notice }}</textarea>

                                @if ($errors->has('notice'))
                                    <div class="form-control-feedback">{{ $errors->first('notice') }}</div>
                                @endif
                                <small class="form-text text-muted">Notice for club members.</small>
                            </div>

                            <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                <label for="captcha">Enter the code: <em>{{ $captcha }}</em></label>
                                <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                <input type="text" name="captcha" class="form-control" autocomplete="off" minlength="3" maxlength="3" placeholder="(enter the 3 digit code above)" />

                                @if ($errors->has('captcha'))
                                    <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Update</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('club.index') }}">Club</a>
                            <span class="breadcrumb-item active">Manage Club</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
