@extends('layouts.app')

@section('title', 'Club Members')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Club Members ({{ $members->total() }}/{{ $club->max_member }})</strong>
                        &rsaquo; <a href="{{ route('club.leave') }}" class="btn btn-sm btn-outline-danger">Leave Club</a>
                    </div>
                    <div class="card-block p-1">
                        <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                            <div class="float-right">
                                <form action="{{ route('club.upgradeMembers') }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="prevent" value="{{ random_int(100, 999) }}">

                                    <img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($club->max_member * 20000) }}</small> &rsaquo;

                                    <input type="submit" class="btn btn-sm btn-success" value="Upgrade">
                                </form>
                                {{--<a href="{{ route('location.poke-mart.buy', $item->id) }}" class="btn btn-sm btn-success">Buy</a>--}}
                            </div>
                            <div class="mb-1">
                                <strong>&rsaquo; Max Members</strong>
                                <br>
                                <small style="color: #777">Increase max members by 5.</small>
                            </div>
                        </div>
                        <a href="{{ route('club.applicants') }}" class="btn btn-sm btn-outline-info">Applicants</a>
                        <hr class="my-1">
                        @forelse ($members as $member)
                            <div class="row">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px groove #fff">
                                    @if (in_array(auth()->user()->club_role, [2,3]))
                                        <div class="float-right">
                                            <form action="{{ route('club.changeRole') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $member->id }}">
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                <select name="role" style="background-color: #ddd" required>
                                                    <option value="" selected disabled>- action -</option>
                                                    @if (auth()->user()->club_role == 3)
                                                        <option value="3">Leader</option>
                                                        <option value="2">Executive</option>
                                                    @endif
                                                    <option value="1">Member</option>
                                                    <option value="kick">[Kick]</option>
                                                </select>

                                                <input type="submit" class="btn btn-sm btn-primary" value="Submit" style="line-height: 10px">
                                            </form>
                                        </div>
                                    @endif
                                    <a href="{{ route('trainer.show', $member->username) }}">
                                    <span class="mr-1 float-left mb-1">
                                        <img class="img-fluid item-list" src="{{ asset("images/trainers/thumbnail/".$member->avatar.".png") }}" alt="{{ $member->username }}">
                                    </span>
                                        {{ $member->username }}</a> <img class="img-fluid" src="{{ asset("images/game/". $member->gender .".png") }}" alt="Gender" />
                                    <br>
                                    <div class="mb-1 small">
                                        <strong>
                                        @if ($member->club_role == 3)
                                            <span class="text-danger">(Leader)</span>
                                        @elseif ($member->club_role == 2)
                                            <span class="text-success">(Executive)</span>
                                        @else
                                            (Member)
                                        @endif
                                        </strong>
                                    </div>
                                    <div class="small">
                                        CP: {{ number_format($member->club_point) }}
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>No member(s) found. Please report to admin immediately.</p>
                        @endforelse

                        <div class="row">
                            <div class="col-sm-12">
                                {{ $members->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('club.index') }}">Club</a>
                            <span class="breadcrumb-item active">Club Members</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection