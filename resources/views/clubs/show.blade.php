@extends('layouts.app')

@section('title', 'Team')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Trainer Club #{{ $club->id }}</strong></div>
                    <div class="card-block p-1">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img class="club-badge img-fluid" src="{{ asset("images/badges/{$club->badge}.png") }}" alt="">
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-sm table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <strong>Name</strong><span class="float-right">{{ $club->name }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Leader(s)</strong><span class="float-right">
                                                    @foreach ($leader as $lead)
                                                    <a href="{{ route('trainer.show', $lead->username) }}">{{ $lead->username }}</a>
                                                    @endforeach
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Members</strong><span class="float-right">{{ $club->members()->count() }}/{{ $club->max_member }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Level</strong><span class="float-right">{{ $club->level }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Club Points</strong><span class="float-right">{{ $club->point }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Description</strong><span class="float-right">{{ $club->description }}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            @if (!auth()->user()->club_id)
                            <div class="col-sm-12 text-center">
                                <form action="{{ route('club.apply') }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $club->id }}">
                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                    <input type="submit" class="btn btn-success" value="Apply">
                                </form>
                            </div>
                            @endif
                            <div class="col-sm-12">
                                <hr class="mt-1 mb-2">
                                <div class="mb-2">
                                    <strong>Active Perks</strong>
                                </div>
                                <div class="row text-center">
                                    @forelse ($club->privileges as $privilege)
                                        <div class="col-3">
                                            <div class="mb-1">
                                                <img src="{{ asset("images/game/perks-{$privilege->modifier}.png") }}" alt="">
                                            </div>
                                            {{ $privilege->title }}
                                        </div>
                                    @empty
                                        no active perks.
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.trainers-club') }}">Trainers Club</a>
                            <span class="breadcrumb-item active">{{ $club->name }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection