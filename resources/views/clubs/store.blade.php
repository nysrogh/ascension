@extends('layouts.app')

@section('title', 'Club Store')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Club Store</strong> <span class="float-right">CP: {{ number_format(auth()->user()->club_point) }}</span></div>
                    <div class="card-block p-1">
                        @forelse ($items as $item)
                            <div class="row mt-1">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    <div class="float-right">
                                        <form action="{{ route('club.martBuy') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                            <input type="hidden" name="id" value="{{ $item->id }}">

                                            <small class="text-warning">{{ number_format($item->price) }} CP</small> &rsaquo;

                                            <input type="submit" class="btn btn-sm btn-success" value="Buy">
                                        </form>
                                        {{--<a href="{{ route('arena.shop.buy', $item->id) }}" class="btn btn-sm btn-success">Buy</a>--}}
                                    </div>
                                    <div class="mb-1">
                                        <img class="img-fluid item-list float-left mr-1" src="{{ asset("images/items/".str_slug($item->name).".png") }}" alt="{{ $item->name }}">
                                        <div>
                                            <strong>{{ $item->name }}</strong>
                                            <br>
                                            <small style="color: #777">{!! strip_tags(vsprintf($item->description, $item->primary_value), '<br><span>') !!}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>No items found in mart.</p>
                        @endforelse
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ url('/club') }}">Club</a>
                            <span class="breadcrumb-item active">Club Store</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection