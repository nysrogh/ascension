<div class="alert alert-success mb-1">Congratulations!</div>

<div class="mb-2 p-1">
    You have completed the dungeon. Please select from 3 treasure chest below to claim your reward.
</div>

<div class="mb-2 text-center">
    <div class="row">
        <div class="col-4">
            <div class="mb-1">
                <img class="img-fluid item-list p-1" src="{{ asset('images/game/chest.png') }}" alt="Treasure Chest">
            </div>
            @if (session('dungeon.progress') == 'boss')
                <form action="{{ route('dungeon.chest') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                    <input type="hidden" name="chest" value="1">
                    <input type="submit" class="btn btn-sm btn-primary" value="select">
                </form>
                {{--<a href="{{ route('dungeon.chest', 1) }}" class="btn btn-sm btn-primary">select</a>--}}
            @else
                <span class="@if (session('dungeon.selected') == 1)text-success @else text-muted @endif">{{ session('dungeon.chest_1') }}</span>
            @endif
        </div>
        <div class="col-4">
            <div class="mb-1">
                <img class="img-fluid item-list p-1" src="{{ asset('images/game/chest.png') }}" alt="Treasure Chest">
            </div>
            @if (session('dungeon.progress') == 'boss')
                <form action="{{ route('dungeon.chest') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                    <input type="hidden" name="chest" value="2">
                    <input type="submit" class="btn btn-sm btn-primary" value="select">
                </form>
                {{--<a href="{{ route('dungeon.chest', 2) }}" class="btn btn-sm btn-primary">select</a>--}}
            @else
                <span class="@if (session('dungeon.selected') == 2)text-success @else text-muted @endif">{{ session('dungeon.chest_2') }}</span>
            @endif
        </div>
        <div class="col-4">
            <div class="mb-1">
                <img class="img-fluid item-list p-1" src="{{ asset('images/game/chest.png') }}" alt="Treasure Chest">
            </div>
            @if (session('dungeon.progress') == 'boss')
                <form action="{{ route('dungeon.chest') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                    <input type="hidden" name="chest" value="3">
                    <input type="submit" class="btn btn-sm btn-primary" value="select">
                </form>
                {{--<a href="{{ route('dungeon.chest', 3) }}" class="btn btn-sm btn-primary">select</a>--}}
            @else
                <span class="@if (session('dungeon.selected') == 3)text-success @else text-muted @endif">{{ session('dungeon.chest_3') }}</span>
            @endif
        </div>
    </div>
</div>