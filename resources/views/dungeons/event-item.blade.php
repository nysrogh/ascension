<img class="img-fluid mt-1" src="{{ asset("images/locations/dungeons/pokeball.png") }}" alt="" width="40" height="40">

<div class="mb-2 p-1">
    You've found a pokeball on the ground. What will you do?
</div>

<div class="mb-2">
    <a href="#" class="btn btn-primary btn-sm">Get it!</a>
</div>