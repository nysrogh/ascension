<img class="img-fluid mt-1" src="{{ asset("images/npc/".str_slug(session('dungeon.event')->npc).".png") }}" alt="{{ session('dungeon.event')->npc }}">

<div>
    <strong>{{ session('dungeon.event')->npc }}</strong>
</div>

<div class="mb-2 p-1">
    {{ session('dungeon.event')->message }}
</div>

@if (session('dungeon.event')->npc == 'Nurse Joy')
    <div class="mb-2">
        <span class="text-success">Your team's HP has been restored!</span>
    </div>
@endif

@if (session('dungeon.event')->npc == 'Ruin Maniac Fred')
    <div class="mb-2">
        <form action="{{ route('dungeon.buy') }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
            <input type="submit" class="btn btn-sm btn-primary" value="Buy">
        </form>
        {{--<a href="{{ route('dungeon.buy') }}" class="btn btn-primary">Buy</a>--}}
    </div>

    <div class="mb-1">
        <img class="img-fluid item-list" src="{{ asset("images/items/".str_slug(session('dungeon.event')->item->name).".png") }}" alt="session('dungeon.event')->item->item->name"> {{ session('dungeon.event')->item->name }} for <img src="{{ asset('images/game/gold.png') }}" alt="" width="12" height="12"> {{ number_format(session('dungeon.event')->price) }}
    </div>

    <div class="mb-1 small">
        (Your coins <img src="{{ asset('images/game/gold.png') }}" alt="" width="12" height="12"> {{ number_format(auth()->user()->gold) }})
    </div>
@endif
