<img class="img-fluid mt-1" src="{{ asset("images/pokemon/".str_slug(session('wild.info')->name).".png") }}" alt="{{ session('wild.info')->name }}">
<br>
@if(session('wild.info')->rarity == 2)
★★
@elseif(session('wild.info')->rarity == 3)
★★★
@elseif(session('wild.info')->rarity == 4)
★★★★
@elseif(session('wild.info')->rarity == 5)
★★★★★
@else
★
@endif

@if (!is_numeric(session('dungeon.progress')))
    <div class="alert alert-danger mb-1">WARNING!</div>
    <div class="mb-1">The dungeon boss <strong>{{ session('wild.info')->name }} <small>(Lv {{ session('wild.level') }})</small></strong> appeared!</div>
    <div class="my-1">
        <a href="{{ route('pokemon.pokedex-show', session('wild.info')->id) }}" class="btn btn-sm btn-outline-info">pokedex</a>
    </div>
@else
    <div>A wild <strong>{{ session('wild.info')->name }} <small>(Lv {{ session('wild.level') }})</small></strong> appeared!</div>
    <div class="my-1">
        <a href="{{ route('pokemon.pokedex-show', session('wild.info')->id) }}" class="btn btn-sm btn-outline-info">pokedex</a>
    </div>
@endif

<div class="my-2">
    <a href="{{ route('dungeon.battle') }}" class="btn btn-primary">Battle</a>
</div>