@extends('layouts.app')

@section('title', "Exploring - ".session('dungeon.info')->name."")

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.flash-messages')
            <div class="col-sm-12">
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Exploring &rsaquo; <a href="{{ route('dungeon.show', session('dungeon.info')->id) }}">{{ session('dungeon.info')->name }}</a></strong>
                        <span class="float-right">
                            <a href="{{ route('dungeon.leave') }}" class="btn btn-sm btn-danger">Leave</a>
                        </span>
                    </div>
                    <div class="card-block p-0">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="progress pb-0">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: @if (is_numeric(session('dungeon.progress'))) {{ round((session('dungeon.progress')/session('dungeon.info')->distance)*100) }}% @else 100% @endif; line-height: 8px; height: 8px; min-width: 2em; color: #000">

                                        <small>@if (is_numeric(session('dungeon.progress'))) {{ round((session('dungeon.progress')/session('dungeon.info')->distance)*100) }}% @else 100% @endif</small>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 my-2 text-center">
                <div class="row">
                    <div class="col-3">
                        <a href="{{ route('team') }}">
                            <div class="mb-1">
                                <img src="{{ asset('/images/game/team.png') }}" alt="Team">
                            </div>
                            <div>
                                Team
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="{{ route('bag') }}">
                            <div class="mb-1">
                                <img src="{{ asset('/images/game/bag.png') }}" alt="Bag">
                            </div>
                            <div>
                                Bag
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="{{ route('pokemon.pokedex') }}">
                            <div class="mb-1">
                                <img src="{{ asset("/images/game/pokedex.png") }}" alt="Pokedex">
                            </div>
                            <div>
                                Pokedex
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="{{ route('pokemon.index') }}">
                            <div class="mb-1">
                                <img src="{{ asset("/images/game/pokemon.png") }}" alt="Box">
                            </div>
                            <div>
                                Box
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card card-outline-info bg-faded">
                    <div class="card-block p-0">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="mt-2">
                                    <img class="img-fluid mr-1 mb-1" src="{{ asset("images/locations/dungeons/".str_slug(session('dungeon.info')->name).".png") }}" alt="{{ session('dungeon.info')->name }}" style="border: 1px groove #ccc">
                                    <div class="mb-1">
                                        @if (session('dungeon.difficulty') == 55)
                                            <small class="text-warning"><strong>(Hard Difficulty)</strong> Pokemon level: 38 - 55</small>
                                        @elseif (session('dungeon.difficulty') == 85)
                                            <small class="text-danger"><strong>(Master Difficulty)</strong> Pokemon level: 68 - 85</small>
                                        @else
                                            <small><strong>(Normal Difficulty)</strong> Pokemon level: 3 - 20</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="my-1" style="border: 1px ridge #ccc">
                                    @include("dungeons.event-". session('dungeon.event')->name)
                                </div>
                                <div class="mt-0 mr-1 ml-1 mb-1">
                                    @if (!is_numeric(session('dungeon.progress')))
                                        <a href="#" class="btn btn-success p-1 disabled">Explore</a>
                                    @else
                                        <form action="{{ route('dungeon.explore') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                            <input type="submit" class="btn btn-success p-1" value="Explore">
                                        </form>
                            {{--<a href="{{ route('dungeon.explore') }}" class="btn btn-success p-1">Explore</a>--}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('dungeon.maps') }}">Map</a>
                            <span class="breadcrumb-item active">{{ session('dungeon.info')->name }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection