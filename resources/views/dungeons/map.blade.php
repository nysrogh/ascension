@extends('layouts.app')

@section('title', 'Map')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Dungeons</strong></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            The dungeon is where you catch and train your Pokemon. Complete and defeat the boss at the end of each dungeons to receive rewards.
                        </div>
                        <hr class="my-1">
                        <div class="row">
                            @foreach ($maps as $map)
                                <div class="col-md-12 mb-1 pb-1" style="border-bottom: 1px groove #ccc">
                                    <div class="float-right">
                                        <form action="{{ route('dungeon.store') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="dungeon_id" value="{{ $map->id }}">
                                            <select name="difficulty" style="background-color: #ddd">
                                                <option value="20" selected>Normal</option>
                                                <option value="55" class="text-warning">Hard</option>
                                                <option value="85" class="text-danger">Master</option>
                                            </select>
                                            <input type="submit" value="Enter" class="btn btn-sm btn-success" style="line-height: 11px">
                                        </form>
                                    </div>
                                    <img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/dungeons/".str_slug($map->name).".png") }}" alt="{{ $map->name }}" width="20%" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong><a href="{{ route('dungeon.show', $map->id) }}">{{ $map->name }}</a></strong>
                                    </div>
                                    <div class="mb-2">
                                        @foreach (explode(',', $map->types) as $type)
                                            <img class="img-fluid" src="{{ asset("images/game/".$type.".png") }}" alt="{{ $type }}">
                                        @endforeach
                                    </div>
                                    <div class="small">
                                        {{ $map->description }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Map</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection