@extends('layouts.app')

@section('title', "Map - $dungeon->name")

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>{{ $dungeon->name }}</strong></div>
                    <div class="card-block p-1">
                        <div class="row">
                            <div class="col-md-12 mb-1 pb-1">
                                <img class="img-fluid mr-1 mb-1" src="{{ asset("images/locations/dungeons/".str_slug($dungeon->name).".png") }}" alt="{{ $dungeon->name }}" style="border: 1px groove #ccc">
                                <div class="mb-1">
                                    @foreach (explode(',', $dungeon->types) as $type)
                                        <img class="img-fluid" src="{{ asset("images/game/".$type.".png") }}" alt="{{ $type }}">
                                    @endforeach
                                </div>
                                <div class="mb-2">
                                    {{ $dungeon->description }}
                                </div>
                                <hr>
                                <div class="row">
                                    @foreach ($pokemon as $poke)
                                        <div class="col-3 mb-1">
                                            <a href="{{ route('pokemon.pokedex-show', $poke->id) }}">
                                                <img class="img-fluid item-list float-left mr-1" src="{{ asset("images/pokemon/thumbnail/".str_slug($poke->name).".png") }}" alt="{{ $poke->name }}">
                                                {{ $poke->name }}
                                            </a>
                                            <div>
                                                @if($poke->rarity == 2)
                                                    ★★
                                                @elseif($poke->rarity == 3)
                                                    ★★★
                                                @elseif($poke->rarity == 4)
                                                    ★★★★
                                                @elseif($poke->rarity == 5)
                                                    ★★★★★
                                                @else
                                                    ★
                                                @endif
                                            </div>
                                            @if (in_array($poke->id, $pokedex))
                                            <div>
                                                <img class="img-fluid" src="{{ asset("images/game/caught.png") }}" width="12" height="12">
                                            </div>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('dungeon.maps') }}">Map</a>
                            <span class="breadcrumb-item active">{{ $dungeon->name }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection