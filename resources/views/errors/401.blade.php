@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="card card-outline-warning">
                    <div class="card-header p-1"><strong>Multiple Login Error</strong></div>
                    <div class="card-block bg-faded">
                        Sorry! Multiple account(s) login is not allowed.<br><br>
                        <em>(If you logged in on a different browser/device, please wait for at least <strong>5 minutes</strong> before you can login again.)</em>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">401 Error</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection