@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="card card-outline-warning">
                    <div class="card-header p-1">Authorization Failed</div>
                    <div class="card-block bg-faded">
                        Stop! You are not allowed to continue this action.
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">403 Error</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection