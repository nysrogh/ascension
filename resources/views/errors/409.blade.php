@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="card card-outline-warning">
                    <div class="card-header p-1">Server Full</div>
                    <div class="card-block bg-faded">
                        Sorry! The game is currently full. Please try again later.<br><br>
                        <span class="small">(Max online limit of <strong>50</strong> users has been imposed for smoother gameplay.)</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <nav class="breadcrumb text-center mb-0 mt-2 py-1 bg-faded">
                    <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                    <span class="breadcrumb-item active">405 Error</span>
                </nav>
            </div>
        </div>
    </div>
@endsection