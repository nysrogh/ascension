@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="card card-outline-warning">
                    <div class="card-header p-1">Double Process Prevention</div>
                    <div class="card-block bg-faded">
                        Oops! It seems you have clicked/pressed too fast.
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">500 Error</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection