@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-sm-12">
                <div class="card card-outline-warning">
                    <div class="card-header p-1">Update Maintenance</div>
                    <div class="card-block bg-faded">
                        {{--<div class="mb-2">--}}
                            {{--Thanks for playing! See you soon...--}}
                        {{--</div>--}}
                        {{--<div class="mb-2">--}}
                            {{--Preparing updates... baby shark doo doo doo--}}
                        {{--</div>--}}
                        <div class="my-1">
                            <strong>3 hours</strong>
                        </div>
                        <div class="my-1">
                            <img class="img-fluid" src="{{ asset('images/game/update-maintenance.gif') }}" alt="">
                        </div>
                        {{--<div>--}}
                            {{--<em>Server maintenance is automated daily (00:00 server time GMT+8) and lasts for 15-minutes.</em>--}}
                        {{--</div>--}}
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">503 Error</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection