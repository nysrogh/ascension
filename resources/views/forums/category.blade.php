@extends('layouts.app')

@section('title', 'Forums')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.flash-messages')
            <div class="col-sm-12">
                <div class="card card-outline-primary bg-faded">
                    <div class="card-header p-1">
                        <strong>
                            @if ($cat == 1)
                                General Discussions
                            @elseif ($cat == 2)
                                Bugs and Glitches
                            @elseif ($cat == 3)
                                Ideas and Suggestions
                            @else
                                Announcements
                            @endif
                        </strong> @if (auth()->check())<span class="float-right"><a href="{{ route('forum.create') }}">Create Topic</a></span>@endif</div>
                    <div class="card-block p-1">
                        @forelse ($forums as $topic)
                            <div class="col-sm-12 mb-1" style="border-bottom: 1px ridge #ccc">
                                <span class="float-right small"><em>{{ $topic->updated_at->diffForHumans() }}</em></span>
                                <img src="{{ asset("images/game/forum-{$topic->category}.png") }}" class="float-left mr-1" alt=""> &rsaquo;
                                <strong><a href="{{ route('forum.show', $topic->slug) }}">{{ str_limit($topic->title, 35) }} ({{ $topic->comments->count() }})</a></strong>
                                <div class="small my-1">
                                    {{ str_limit($topic->body, 70) }}<br>
                                    created by: <a href="{{ route('trainer.show', $topic->user->username) }}"><span class="{{ $topic->user->setting_bgcolor }}">{{ $topic->user->username }}</span></a>
                                </div>
                            </div>
                        @empty
                            No forum topic has been posted in this category yet.
                        @endforelse
                        <div class="col-md-12">
                            {{ $forums->links('vendor.pagination.simple-bootstrap-4') }}
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('forum') }}">Forums</a>
                            <span class="breadcrumb-item active">Category</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection