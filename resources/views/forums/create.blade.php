@extends('layouts.app')

@section('title', 'Create New Forum Topic')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Create New Forum Topic</strong></div>
                    <div class="card-block">
                        <form action="{{ route('forum.store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <div class="form-group @if ($errors->has('title')) has-danger @endif">
                                <label class="form-control-label mb-0" for="title">Title</label>
                                <input type="text" class="form-control form-control-danger" id="title" name="title" value="{{ old('title') }}" maxlength="50" required autocomplete="off">

                                @if ($errors->has('title'))
                                    <div class="form-control-feedback">{{ $errors->first('title') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('category')) has-danger @endif">
                                <label class="form-control-label mb-0" for="category">Category</label>
                                <select name="category" id="category" class="form-control form-control-danger" required>
                                    <option value="" selected disabled>[select]</option>
                                    @if (auth()->user()->status == 2)
                                        <option value="4">Announcements</option>
                                    @endif
                                    <option value="1">General Discussions</option>
                                    <option value="2">Bugs and Glitches</option>
                                    <option value="3">Ideas and Suggestions</option>
                                </select>

                                @if ($errors->has('category'))
                                    <div class="form-control-feedback">{{ $errors->first('category') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('body')) has-danger @endif">
                                <label class="form-control-label mb-0" for="body">Body</label>
                                <textarea class="form-control form-control-danger" id="body" maxlength="1000" rows="7" name="body" required>{{ old('body') }}</textarea>

                                @if ($errors->has('body'))
                                    <div class="form-control-feedback">{{ $errors->first('body') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                <label class="form-control-label mb-0" for="captcha">Enter CAPTCHA</label>
                                <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                <input type="text" class="form-control form-control-danger" id="captcha" name="captcha" autocomplete="off" required>

                                @if ($errors->has('captcha'))
                                    <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                @endif

                                <small class="form-text text-muted">Captcha code: <strong>{{ $captcha }}</strong></small>
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Create</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('forum') }}">Forums</a>
                            <span class="breadcrumb-item active">Create Topic</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
