@extends('layouts.app')

@section('title', 'Forums')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.flash-messages')
            <div class="col-sm-12">
                <div class="card card-outline-primary bg-faded">
                    <div class="card-header p-1"><strong>Latest Discussions</strong> @if (auth()->check())<span class="float-right"><a href="{{ route('forum.create') }}">Create Topic</a></span>@endif</div>
                    <div class="card-block p-1">
                        @forelse ($forums as $topic)
                            <div class="col-sm-12 mb-1" style="border-bottom: 1px ridge #ccc">
                                <span class="float-right small"><em>{{ $topic->updated_at->diffForHumans() }}</em></span>
                                <img src="{{ asset("images/game/forum-{$topic->category}.png") }}" class="float-left mr-1" alt=""> &rsaquo;
                                <strong><a href="{{ route('forum.show', $topic->slug) }}">{{ str_limit($topic->title, 35) }} ({{ $topic->comments->count() }})</a></strong>
                                <div class="small my-1">
                                    {{ str_limit($topic->body, 70) }}<br>
                                    created by: <a href="{{ route('trainer.show', $topic->user->username) }}"><span class="{{ $topic->user->setting_bgcolor }}">{{ $topic->user->username }}</span></a>
                                </div>
                            </div>
                        @empty
                            No forum topic has been posted yet.
                        @endforelse
                        <div class="my-3" style="border-bottom: 1px dotted #aaa"></div>
                        <div class="mb-2">
                            <strong>Forum Categories</strong>
                        </div>
                        <div class="col-sm-12 mb-1" style="border-bottom: 1px ridge #ccc">
                            <img src="{{ asset('images/game/forum-4.png') }}" class="float-left mr-1" alt="">
                            <strong><a href="{{ route('forum.category', 4) }}">Announcements</a></strong>
                            <div class="small my-1">
                                Posts regarding the latest news and updates for the game.<br>
                            </div>
                        </div>
                        <div class="col-sm-12 mb-1" style="border-bottom: 1px ridge #ccc">
                            <img src="{{ asset('images/game/forum-1.png') }}" class="float-left mr-1" alt="">
                            <strong><a href="{{ route('forum.category', 1) }}">General Discussions</a></strong>
                            <div class="small my-1">
                                Posts regarding the overall experience of the game.<br>
                            </div>
                        </div>
                        <div class="col-sm-12 mb-1" style="border-bottom: 1px ridge #ccc">
                            <img src="{{ asset('images/game/forum-2.png') }}" class="float-left mr-1" alt="">
                            <strong><a href="{{ route('forum.category', 2) }}">Bugs and Glitches</a></strong>
                            <div class="small my-1">
                                Posts regarding bugs, glitches, and balancing issues encountered.<br>
                            </div>
                        </div>
                        <div class="col-sm-12" style="border-bottom: 1px ridge #ccc">
                            <img src="{{ asset('images/game/forum-3.png') }}" class="float-left mr-1" alt="">
                            <strong><a href="{{ route('forum.category', 3) }}">Ideas and Suggestions</a></strong>
                            <div class="small my-1">
                                Posts regarding ideas and features players want to implement in game.<br>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Forums</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection