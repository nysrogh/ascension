@extends('layouts.app')

@section('title', 'Pokemon Tech')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>{{ $topic->title }}</strong></div>
                    <div class="card-block p-1">
                        <span class="float-left mr-1">
                            <img class="img-fluid" src="{{ asset("/images/trainers/thumbnail/{$topic->user->avatar}.png") }}" alt="#" />
                        </span>
                        <div>
                            <a href="{{ route('trainer.show', $topic->user->username) }}">
                                <span class="{{ $topic->user->setting_bgcolor }}">{{ $topic->user->username }}</span>
                            </a>
                            @if (auth()->check() && auth()->user()->status == 2)
                            &rsaquo; <a href="{{ route('forum.delete', $topic->id) }}" class="btn btn-sm btn-outline-danger">delete</a>
                            @endif
                            <span class="float-right"><small>{{ $topic->created_at->diffForHumans() }}</small></span>
                        </div>
                        <div class="card card-outline-success mb-1" style="background-color: #fff;">
                            <div class="card-block p-1">
                                {!! strip_tags(nl2br($topic->body), '<br><em><strong>') !!}
                            </div>
                        </div>

                        <div class="my-1" style="border-bottom: 1px dotted #aaa"></div>

                        <div class="col-md-12 my-1 text-center">
                            <a href="{{ route('forum.show', [$topic->slug, 'desc']) }}" class="btn btn-sm btn-outline-info">Latest</a>
                        </div>

                        @forelse ($comments as $comment)
                            <div>
                                <a href="{{ route('trainer.show', $comment->user->username) }}">
                                    <span class="float-left mr-1">
                                        <img class="img-fluid" src="{{ asset("/images/trainers/thumbnail/{$comment->user->avatar}.png") }}" alt="#" />
                                    </span>
                                    <span class="{{ $comment->user->setting_bgcolor }}">{{ $comment->user->username }}</span>
                                </a>
                                @if (auth()->check() && auth()->user()->status == 2)
                                &rsaquo; <a href="{{ route('forum.delete-comment', $comment->id) }}" class="btn btn-sm btn-outline-danger">delete</a>
                                @endif
                                <span class="float-right">
                                    <small>{{ $comment->created_at->diffForHumans() }}</small>
                                </span>
                            </div>
                            <div class="card card-outline-secondary mb-1" style="background-color: #fff;">
                                <div class="card-block p-1">
                                    {{ $comment->body }}
                                </div>
                            </div>
                        @empty
                            <p>No comment(s) yet.</p>
                        @endforelse

                        <div class="row">
                            <div class="col-sm-12">
                                {{ $comments->links('vendor.pagination.simple-bootstrap-4') }}
                            </div>
                        </div>

                        <div class="my-1" style="border-bottom: 1px dotted #aaa"></div>

                        @if (auth()->check())
                        <div class="col-sm-12">
                            <form action="{{ route('forum.comment') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="forum_id" value="{{ $topic->id }}">
                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                <div class="form-group">
                                    <label class="form-control-label mb-0" for="captcha">Comment</label>
                                    <textarea type="text" class="form-control" name="body" rows="5" maxlength="300" required></textarea>
                                </div>
                                <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                    <label class="form-control-label mb-0" for="captcha">Enter CAPTCHA</label>
                                    <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                    <input type="text" class="form-control form-control-danger" id="captcha" name="captcha" autocomplete="off" required>

                                    @if ($errors->has('captcha'))
                                        <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                    @endif

                                    <small class="form-text text-muted">Captcha code: <strong>{{ $captcha }}</strong></small>
                                </div>
                                <div>
                                    <input type="submit" class="btn btn-sm btn-success" value="Submit">
                                </div>
                            </form>
                        </div>
                        @endif
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('forum') }}">Forums</a>
                            <span class="breadcrumb-item active">{{ $topic->title }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection