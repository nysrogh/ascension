@extends('layouts.app')

@section('title', 'Home')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('trainers.menu')
                <div class="card card-outline-success bg-faded">
                    <div class="card-header card-success p-1">
                        <strong>Ascension City</strong>
                    </div>
                    <div class="card-block p-1">
                        <div class="alert alert-success text-center">
                            <strong>WARNING</strong><br><br>
                            <a href="http://ascensionrpg.mobi">ascensionrpg.mobi</a> is the <strong>ONLY</strong> domain of this game. Other sites are fake/phishing sites so don't ever login your accounts in them. Thanks!
                            {{--<div class="my-2">--}}
                                {{--In celebration of our 2nd monthsary, we will be having a <strong>2x exp, coins, rate</strong> event for 1 day! enjoy!--}}
                            {{--</div>--}}
                        </div>
                        {{--<div class="mb-1 text-center">--}}
                            {{--<img class="img-fluid item-list mr-1" src="{{ asset('/images/locations/pewter-city.png') }}" alt="Palace">--}}
                        {{--</div>--}}
                        <div class="row">
                            <div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">
                                <a href="{{ route('location.poke-center') }}" class="parpg-link-hover">
                                    <img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/pokemon-center.png") }}" alt="Pokemon Center" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong>Pokemon Center</strong>
                                    </div>
                                </a>
                                <div class="small mt-1 text-muted">
                                    &rsaquo; Chat and make friends with other trainers.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">
                                <a href="{{ route('location.poke-tech') }}" class="parpg-link-hover">
                                    <img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/pokemon-tech.png") }}" alt="Pokemon Tech" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong>Pokemon Tech</strong>
                                    </div>
                                </a>
                                <div class="small mt-1 text-muted">
                                    &rsaquo; Teach your Pokemon some advanced moves.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">
                                <a href="{{ route('location.poke-mart') }}" class="parpg-link-hover">
                                    <img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/pokemon-mart.png") }}" alt="Pokemon Mart" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong>Poke Mart</strong>
                                    </div>
                                </a>
                                <div class="small mt-1 text-muted">
                                    &rsaquo; Gather your day-to-day adventure items here.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">
                                <a href="{{ route('location.grand-bazaar') }}" class="parpg-link-hover">
                                    <img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/pokemon-bazaar.png") }}" alt="Grand Bazaar" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong>Grand Bazaar</strong>
                                    </div>
                                </a>
                                <div class="small mt-1 text-muted">
                                    &rsaquo; Buy or trade items and Pokemon with other trainers.
                                </div>
                            </div>
                        </div>
                        {{--<div class="row">--}}
                            {{--<div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">--}}
                                {{--<a href="#" class="parpg-link-hover">--}}
                                    {{--<img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/game-corner.png") }}" alt="Game Corner" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">--}}
                                    {{--<div>--}}
                                        {{--<strong>Game Corner</strong>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                                {{--<div class="small mt-1 text-muted">--}}
                                    {{--&rsaquo; <em>Under Construction</em>.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="row">
                            <div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">
                                <a href="{{ route('location.trainers-club') }}" class="parpg-link-hover">
                                    <img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/trainers-club.png") }}" alt="Trainers Club" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong>Trainers Club</strong>
                                    </div>
                                </a>
                                <div class="small mt-1 text-muted">
                                    &rsaquo; Join trainers club to earn coins and exclusive rewards.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">
                                <a href="{{ route('arena') }}" class="parpg-link-hover">
                                    <img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/stadium.png") }}" alt="Battle Stadium" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong>Battle Stadium</strong>
                                    </div>
                                </a>
                                <div class="small mt-1 text-muted">
                                    &rsaquo; Challenge trainers here to become the champion.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">
                                <a href="{{ route('dungeon.maps') }}" class="parpg-link-hover">
                                    <img class="img-fluid mr-1 mb-1 float-left" src="{{ asset("images/locations/map.png") }}" alt="Dungeons" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong>Map</strong>
                                    </div>
                                </a>
                                <div class="small mt-1 text-muted">
                                    &rsaquo; Explore different dungeons as you train and catch Pokemon.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 py-1" style="border-top: 1px ridge #aaa">
                                <a href="{{ route('paragon.entrance') }}" class="parpg-link-hover">
                                    <img class="mr-1 mb-1 float-left" height="30" width="60" src="{{ asset("images/locations/paragon-tower.png") }}" alt="Paragon Tower" style="border: 1px groove #ccc; border-radius: 0 0 5px 0">
                                    <div>
                                        <strong>Paragon Tower</strong>
                                    </div>
                                </a>
                                <div class="small mt-1 text-muted">
                                    &rsaquo; An unknown tower said to be connected with a group of extra-terrestrial Pokemon.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection