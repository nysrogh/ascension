@extends('layouts.app')

@section('title', 'A New and Unique Pokemon Text-Based RPG for Mobile')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card" style="border-top: 2px solid red; border-bottom: 2px solid darkgray">
                    <div class="card-block">
                        <div class="text-center my-1">
                            <h1><strong>Pokemon Ascension</strong></h1>
                        </div>

                        <div class="text-justify text-center my-2">
                            Play the classic Pokemon game in this new and unique text-based online RPG for mobile.
                        </div>

                        <div class="text-justify text-center my-1">
                             Train, catch, and explore dungeons as you make your way to become a Pokemon master.
                        </div>

                        <div class="text-center mt-2">
                            <a href="{{ url('/register') }}" class="btn btn-success">Join now!</a>
                            <br><br>
                            <span class="small">
                                Open-Beta &rsaquo; v1.5
                            </span>
                        </div>
                        {{--<div class="col-sm-12 mt-2 text-center">--}}
                            {{--<a href="https://0.freebasics.com/?ref=badges">--}}
                                {{--<img class="img-fluid" src="{{ asset('images/game/freebasics.png') }}" alt="" style="max-height: 60px">--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mt-2">
                <div class="card">
                    <div class="card-header p-1"><strong>Updates</strong></div>
                    <div class="card-block p-1">
                        @forelse ($updates as $topic)
                            <div class="col-sm-12 mb-1 pb-1" style="border-bottom: 1px ridge #ccc">
                                <span class="float-right small"><em>{{ $topic->created_at->diffForHumans() }}</em></span>
                                <img src="{{ asset("images/game/forum-{$topic->category}.png") }}" alt=""> &rsaquo;
                                <strong>
                                    <a href="{{ route('forum.show', $topic->slug) }}">{{ str_limit($topic->title, 35) }}</a>
                                </strong>
                            </div>
                        @empty
                            No updates yet.
                        @endforelse
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <form class="form-inline justify-content-center" action="{{ url('login') }}" method="POST">

                    {{ csrf_field() }}

                    <div class="form-group @if ($errors->all()) has-danger @endif">
                        <label class="sr-only" for="username">Username</label>
                        <input type="text" class="form-control form-control-danger my-2 mr-2" id="username" name="username" placeholder="Username">
                        @if ($errors->all())
                            <div class="form-control-feedback">Invalid Username or Password!</div>
                        @endif

                        <label class="sr-only" for="password">Password</label>
                        <input type="password" class="form-control form-control-danger my-2 mr-2" id="password" name="password" placeholder="Password">

                        <button type="submit" class="form-control btn btn-primary my-2">Login</button>
                    </div>

                </form>
            </div>

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header p-1"><strong>Game Features</strong></div>
                    <div class="card-block text-justify">
                        <div class="mb-2">
                            <img class="float-left m-2" src="{{ asset('images/game/team.png') }}" alt="Gotta Catch `em All">
                            <span class="text-danger">Gotta Catch `em All</span>
                            <div class="small my-1">
                                Featuring all generations of Pokemon (802 in total) for you to catch and train. Following the exact catch rate and capture algorithm of the actual game, you will be engaged in strategic situation on how to capture the pokemon.
                            </div>
                        </div>
                        <div>
                            <img class="float-left m-2" src="{{ asset('images/game/mission.png') }}" alt="Dungeon Exploration">
                            <span class="text-danger">Dungeon Exploration</span>
                            <div class="small my-1">
                                This game offers a unique dungeon-based exploration where you can catch and train your Pokemon. These dungeons respawns type-specific Pokemon and their levels vary with the difficulty you choose (Normal, Hard, Master). And lastly, beware of the boss at the end of the dungeon.
                            </div>
                        </div>
                        <div>
                            <img class="float-left m-2" src="{{ asset('images/game/mission-arena.png') }}" alt="Strategic Battles">
                            <span class="text-danger">Strategic Battles</span>
                            <div class="small my-1">
                                This game has a similar battle system as the actual game including over 300+ damage-dealing, status-inducing and stats-modifying moves. In wild encounters, you will be able to switch between your available Pokemon and use or hold items and berries.
                            </div>
                        </div>
                        <div>
                            <img class="float-left m-2" src="{{ asset('images/game/pokemon.png') }}" alt="Ladder-based Tournament">
                            <span class="text-danger">Ladder-based Tournament</span>
                            <div class="small my-1">
                                One of the most exciting feature in-game is the ladder tournament. Your goal is to crawl all the way up to the top challenging trainers. You will be given 3 trainers ahead of your current rank and be able to choose who to fight, then swap rank if your win. Unlike in wild battles, you won't be able to switch Pokemon here and the order of Pokemon will be randomized. Also, no items allowed.
                            </div>
                        </div>
                        <div>
                            <img class="float-left m-2" src="{{ asset('images/game/store.png') }}" alt="Unique Merchant System">
                            <span class="text-danger">Unique Merchant System</span>
                            <div class="small my-1">
                                Create and manage your own store in Grand Bazaar where you can sell items and/or Pokemon. Give your store a unique name and description while other trainers can also made trade offers on your Pokemon in sale.
                            </div>
                        </div>
                        <div>
                            <img class="float-left m-2" src="{{ asset('images/game/club.png') }}" alt="Trainers Club">
                            <span class="text-danger">Trainers Club</span>
                            <div class="small my-1">
                                Establish and manage your own club or join other trainers' club. Do daily missions to upgrade your club and enjoy the perks given exclusively to club members. Participate in a club war to earn more coins and points for your club.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection