@extends('layouts.app')

@section('title', "Item - {$item->info->name}")

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.flash-messages')
            <div class="col-sm-12">
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>{{ $item->info->name }} {{ $item->upgrade > 0 ? '+'. $item->upgrade : '' }}</strong></div>
                    <ul class="nav nav-tabs small">
                        @if (!session('battle.enemy') && ($item->info->category != 1 && !$item->info->section) || ($item->info->category == 4 && $item->info->section == 1))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('bag.useItem', $item->id) }}">
                                <i class="fa fa-plus"></i> Use/Hold
                            </a>
                        </li>
                        @endif
                        @if (auth()->user()->shop)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('shop.sellItem', $item->id) }}">
                                <i class="fa fa-shopping-cart"></i> Add to Store
                            </a>
                        </li>
                        @endif
                    </ul>
                    <div class="card-block p-1">
                        <div class="row">
                            <div class="col-md-12 mb-1">
                                <img class="img-fluid item-list" src="{{ asset("images/items/".str_slug($item->info->name).".png") }}" alt="{{ $item->info->name }}">
                                <div class="mt-1">
                                    {!! nl2br(vsprintf($item->info->description, $item->primary_value != 0 ? $item->primary_value : $item->info->primary_value)) !!}
                                </div>

                                @if ($item->user->id == auth()->user()->id && $item->info->category == 4 && $item->info->section == 1)
                                <hr class="my-1">
                                <h5><strong>Enhancement</strong></h5>

                                <div class="alert alert-info">
                                    &rsaquo; Enhancing increases the item's effect. Max 10.
                                </div>
                                <div class="pl-1">
                                    &rsaquo; Success rate: <strong>{{ $ups['chance'] / 10 }}%</strong>
                                    <br>
                                    @if ($item->upgrade < 7)
                                    &rsaquo; Cost: <strong>{{ number_format($ups['cost']) }} coins</strong> <span class="small">(Your coins: {{ number_format(auth()->user()->gold) }})</span>
                                    @else
                                    &rsaquo; Cost: <strong>{{ number_format($ups['cost']) }} crystal</strong> <span class="small">(Your crystal: {{ number_format(auth()->user()->crystal) }})</span>
                                    @endif
                                    <br><br>
                                    <form action="{{ route('bag.enhance') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <input type="submit" class="btn btn-sm btn-primary" value="Enhance">
                                    </form>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('bag') }}">Bag</a>
                            <span class="breadcrumb-item active">{{ $item->info->name }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection