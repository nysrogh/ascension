<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="propeller" content="9d944b1435853c5ace5538567253a866" />
    <title>Pokemon Ascension - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app-prod.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}" />
    {{--<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" />

    {{--<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
    {{--<script>--}}
        {{--(adsbygoogle = window.adsbygoogle || []).push({--}}
            {{--google_ad_client: "ca-pub-8746764478079576",--}}
            {{--enable_page_level_ads: true--}}
        {{--});--}}
    {{--</script>--}}
</head>
<body style="max-width: 480px; margin: 0 auto" class="bg-inverse">
{{--<div class="col-sm-12 text-center">--}}
    {{--<img class="img-fluid" src="{{ asset('images/namebg/halloween.gif') }}" alt="">--}}
{{--</div>--}}

{{--<script type="text/javascript" src="//go.onclasrv.com/apu.php?zoneid=1400356"></script>--}}
@yield('head')
@yield('menu')
@yield('content')
</body>

<footer class="mt-2 mb-2 text-center text-muted small">
    <a href="{{ route('forum') }}">Forum</a> &middot; <a href="{{ route('legal') }}">Legal</a> &middot;
    <a href="{{ route('online') }}">Online ({{ $user->allOnline()->count() }})</a> @if (auth()->user()) &middot; <a href="{{ route('trainer.settings') }}">Settings</a> &middot; <a href="{{ url('logout') }}">Logout</a> @endif<br>
    <div class="mt-1 mb-1">
        Pokemon Ascension &copy; {{ date('Y') }}
    </div>

    {{--<a title="Web Analytics" href="http://clicky.com/101072375"><img alt="Web Analytics" src="//static.getclicky.com/media/links/badge.gif" border="0" width="1" height="1" /></a>--}}
    {{--<img alt="Clicky" width="1" height="1" src="//in.getclicky.com/101072375ns.gif" />--}}
</footer>
</html>