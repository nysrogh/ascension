<div class="col-sm-12">
    @if (session('error'))
        <div class="alert alert-danger text-center">
            <span>{!! session('error') !!}</span>
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success text-center">
            <span>{!! session('success') !!}</span>
        </div>
    @endif
    @if (session('info'))
        <div class="alert alert-info text-center">
            <span>{!! session('info') !!}</span>
        </div>
    @endif
    @if ($errors->all())
        <div class="alert alert-danger text-center">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
    @endif
    @if (session('rewards'))
            <div class="alert alert-success text-center">
                @foreach (session('rewards') as $reward)
                <span>{!! $reward !!}</span>
                @endforeach
            </div>
    @endif
</div>
