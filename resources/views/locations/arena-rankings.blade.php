@extends('layouts.app')

@section('title', 'Arena - Rankings')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Arena Rankings (Top 100)</strong></div>
                    <div class="card-block p-1">
                        @forelse ($rankings as $trainer)
                            <div class="row">
                                <div class="col-md-12 pb-1" style="border-bottom: 1px ridge #ddd">
                                    @if (in_array($trainer->rank, [1,2,3]))
                                        <a href="{{ route('trainer.show', $trainer->user->username) }}">
                                            <strong>{{ $trainer->rank }} &rsaquo; </strong>
                                            <img class="item-list img-fluid float-left mr-2" src="{{ asset("images/trainers/{$trainer->user->avatar}.png") }}" alt="{{ $trainer->user->username }}" width="50">
                                            {{ $trainer->user->username }}
                                        </a>
                                        <div class="mt-1">{{ $trainer->win }}W / {{ $trainer->lose }}L <small>({{ ($trainer->win) ? round(100 * $trainer->win / ($trainer->win + $trainer->lose)) : 0 }}% Win rate)</small></div>
                                    @else
                                        <a href="{{ route('trainer.show', $trainer->user->username) }}">
                                            <img class="img-fluid float-left mr-2" src="{{ asset("images/trainers/thumbnail/{$trainer->user->avatar}.png") }}" alt="" width="24">
                                            {{ $trainer->user->username }}
                                        </a>
                                        <div class="mt-1">{{ $trainer->win }}W / {{ $trainer->lose }}L <small>({{ ($trainer->win) ? round(100 * $trainer->win / ($trainer->win + $trainer->lose)) : 0 }}% Win rate)</small></div>
                                    @endif
                                </div>
                            </div>
                        @empty
                            <span>No rankings yet.</span>
                        @endforelse

                        {{ $rankings->links('vendor.pagination.simple-bootstrap-4') }}
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('arena') }}">Arena</a>
                            <span class="breadcrumb-item active">Rankings</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection