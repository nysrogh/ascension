@extends('layouts.app')

@section('title', 'Pokemon Tech')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Battle Stadium</strong> <span class="float-right">Battles left: {{ $rank ? $rank->battles_left : 0 }}/20</span></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ asset('/images/locations/battle-stadium-interior.png') }}" alt="Battle Stadium">
                        </div>

                        <hr class="my-1">

                        <div class="col-sm-12 text-center mb-2">
                            Welcome to the Battle Arena. Here you will test your Pokemon team skills as you challenge other trainers and aim to become the champion.
                            <br><br>
                            <strong>@if ($tournament) Tournament will end {{ $tournament }} @else Tournament has ended. @endif</strong>
                            <br><br>
                            <span class="text-info">Your Rank:<br><strong>{{ $rank ? number_format($rank->rank) : 'n/a' }}</strong></span>
                        </div>

                        <div class="col-sm-12 text-center">
                            <a href="{{ route('arena.rankings') }}" class="btn btn-sm btn-warning">Rankings</a> &middot; @if (!$rank) <a href="{{ route('arena.join') }}" class="btn btn-success">Join</a> &middot; @endif <a href="{{ route('arena.shop') }}" class="btn btn-sm btn-info">Medal Shop</a>
                        </div>

                        <hr class="my-1">

                        <div class="row text-center">
                            @forelse ($trainers as $trainer)
                                <div class="col-4">
                                    <img class="img-fluid center-block" src="{{ asset("images/trainers/{$trainer->user->avatar}.png") }}" alt="">
                                    <div class="card card-outline-danger p-1 mt-1">
                                        <strong style="font-size: 120%">#{{ number_format($trainer->rank) }}</strong>
                                        <div class="mt-1">
                                            <a href="{{ route('trainer.show', $trainer->user->username) }}">{{ $trainer->user->username }}</a>
                                        </div>
                                        @if ($rank && $trainer->user_id != auth()->user()->id)
                                        <div class="mt-2">
                                            <a href="{{ route('arena.challenge', $trainer->user_id) }}" class="btn btn-sm btn-outline-danger">Battle</a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            @empty
                                <p>No participants yet.</p>
                            @endforelse
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Battle Stadium</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection