@extends('layouts.app')

@section('title', 'Rewards')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Rewards</strong></div>
                    <div class="card-block p-1">
                        <div class="row">
                            @foreach ($rewards as $reward)
                                <div class="col-md-12 mb-1 pb-1" style="border-bottom: 1px groove #ccc">
                                    @if ($reward->hours != 0)
                                        @if ($claimed[$reward->id] != 'claim')
                                            <span class="badge badge-info float-right">{{ $claimed[$reward->id] }}</span>
                                        @else
                                            <div class="float-right">
                                                <form action="{{ route('reward.claim') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                    <input type="hidden" name="id" value="{{ $reward->id }}">

                                                    <input type="submit" class="btn btn-sm btn-success" value="Claim">
                                                </form>
                                            </div>
                                        @endif
                                    @else
                                        <span class="float-right">
                                            <form action="{{ route('reward.claim') }}" method="POST">
                                                    {{ csrf_field() }}
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                <input type="hidden" name="id" value="{{ $reward->id }}">

                                                <input type="submit" class="btn btn-sm btn-success" value="Claim">
                                            </form>
                                            {{--<a href="{{ route('reward.claim', $reward->id) }}" class="btn btn-sm btn-success">claim</a>--}}
                                        </span>
                                    @endif
                                    <img class="img-fluid mr-1 mb-1" src="{{ asset("images/game/rewards-".str_slug($reward->title).".png") }}" alt="{{ $reward->title }}"> <strong>{{ $reward->title }}</strong>
                                    <div class="small">
                                        @if ($reward->pokedex)
                                            <div class="mb-1">
                                                <strong>Pokedex Required:</strong> <span class="@if (count(explode(',', auth()->user()->pokedex)) >= $reward->pokedex) text-success @else text-danger @endif">{{ count(explode(',', auth()->user()->pokedex)) }}/{{ $reward->pokedex }}</span>
                                            </div>
                                        @elseif ($reward->dungeon)
                                            <div class="mb-1">
                                                <strong>Dungeon Completed:</strong> <strong class="@if (auth()->user()->dungeons_completed >= $reward->dungeon) text-success @else text-danger @endif">{{ auth()->user()->dungeons_completed }}/{{ $reward->dungeon }}</strong>
                                            </div>
                                        @elseif ($reward->mission)
                                            <div class="mb-1">
                                                <strong>Mission Completed:</strong> <strong class="@if (auth()->user()->missions_completed >= $reward->mission) text-success @else text-danger @endif">{{ auth()->user()->missions_completed }}/{{ $reward->mission }}</strong>
                                            </div>
                                        @endif
                                        {{ $reward->description }}
                                        <hr class="my-1">
                                        <em>
                                            @foreach ($reward->modifiers as $modifier)
                                                +
                                                @if ($modifier->item_id)
                                                    <img class="img-fluid" src="{{ asset("images/game/ri_{$modifier->attribute}_2.png") }}" alt="{{ $modifier->attribute }}"> x{{ $modifier->value }}
                                                @else
                                                    <img class="img-fluid" src="{{ asset("images/game/ri_{$modifier->attribute}.png") }}" alt="{{ $modifier->attribute }}"> x{{ $modifier->value }}
                                                    @endif
                                                    &middot;
                                                    @endforeach
                                        </em>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Game Corner</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection