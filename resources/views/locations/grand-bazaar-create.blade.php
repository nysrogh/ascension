@extends('layouts.app')

@section('title', 'Setup Store')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Setup Store</strong></div>
                    <div class="alert alert-info mb-1">
                        &rsaquo; You need to have atleast 15 Pokedex records to setup a store
                    </div>
                    <div class="card-block">
                        <form action="{{ route('location.grand-bazaar') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <div class="form-group @if ($errors->has('username')) has-danger @endif">
                                <label class="form-control-label mb-0" for="name">Name</label>
                                <input type="text" class="form-control form-control-danger" id="name" name="name" value="{{ old('name') }}" maxlength="25" required>

                                @if ($errors->has('name'))
                                    <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('description')) has-danger @endif">
                                <label class="form-control-label mb-0" for="description">Description</label>
                                <textarea class="form-control form-control-danger" id="description" maxlength="80" name="description" required></textarea>

                                @if ($errors->has('description'))
                                    <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Done</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.grand-bazaar') }}">Grand Bazaar</a>
                            <span class="breadcrumb-item active">Setup Store</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
