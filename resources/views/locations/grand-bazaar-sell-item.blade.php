@extends('layouts.app')

@section('title', 'Sell Item')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Sell Item</strong></div>
                    <div class="card-block">
                        <div class="mb-1">
                            <img class="img-fluid item-list" src="{{ asset("images/items/".str_slug($item->info->name).".png") }}" alt="{{ $item->info->name }}"> <strong>{{ $item->info->name }} {{ $item->upgrade > 0 ? '+'. $item->upgrade : '' }}</strong>
                            <div class="mt-1">
                                {!! nl2br(vsprintf($item->info->description, $item->primary_value != 0 ? $item->primary_value : $item->info->primary_value)) !!}
                            </div>
                        </div>

                        <hr class="my-1">

                        <form action="{{ route('shop.sellStore') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <input type="hidden" name="id" value="{{ $item->id }}">
                            <input type="hidden" name="item_id" value="{{ $item->item_id }}">
                            <input type="hidden" name="category" value="item">
                            <div class="form-group @if ($errors->has('price')) has-danger @endif">
                                <label class="form-control-label mb-0" for="price">Price</label>
                                <input type="number" class="form-control form-control-danger" id="price" name="price" min="1" max="99999999" required autocomplete="off">

                                @if ($errors->has('price'))
                                    <div class="form-control-feedback">{{ $errors->first('price') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('quantity')) has-danger @endif">
                                <label class="form-control-label mb-0" for="quantity">Quantity (Max: {{ $item->quantity }})</label>
                                <input type="number" class="form-control form-control-danger" id="quantity" name="quantity" min="1" max="{{ ($item) ? $item->quantity : 99 }}" required autocomplete="off">

                                @if ($errors->has('quantity'))
                                    <div class="form-control-feedback">{{ $errors->first('quantity') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Sell</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.grand-bazaar') }}">Grand Bazaar</a>
                            <span class="breadcrumb-item active">Sell Item</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
