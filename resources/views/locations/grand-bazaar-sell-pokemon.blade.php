@extends('layouts.app')

@section('title', 'Sell Pokemon')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Sell Pokemon</strong></div>
                    <div class="card-block">
                        <div class="mb-1">
                            <img class="img-fluid item-list" src="{{ asset("images/pokemon/thumbnail/".str_slug($pokemon->info->name).".png") }}" alt="{{ $pokemon->info->name }}"> <strong>{{ $pokemon->info->name }}</strong> (Lv {{ $pokemon->level }})
                        </div>
                        <form action="{{ route('shop.sellStore') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <input type="hidden" name="id" value="{{ $pokemon->id }}">
                            <input type="hidden" name="category" value="pokemon">
                            <div class="form-group @if ($errors->has('price')) has-danger @endif">
                                <label class="form-control-label mb-0" for="price">Price</label>
                                <input type="number" class="form-control form-control-danger" id="price" name="price" value="{{ old('price') }}" min="1" max="99999999" required autocomplete="off">

                                @if ($errors->has('price'))
                                    <div class="form-control-feedback">{{ $errors->first('price') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Sell</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.grand-bazaar') }}">Grand Bazaar</a>
                            <span class="breadcrumb-item active">Sell Pokemon</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
