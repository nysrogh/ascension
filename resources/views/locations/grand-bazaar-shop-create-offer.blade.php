@extends('layouts.app')

@section('title', 'Grand Bazaar - Create Offer')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Create Offer ({{ count(session('offers')) ?? 0 }}/10)</strong></div>
                    <ul class="nav nav-tabs small">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('pokemon.index') }}">
                                Pokemon
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('bag', 1) }}">
                                Item
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('shop.trade.coins-crystal') }}">
                                Coins/Crystal
                            </a>
                        </li>
                    </ul>
                    <div class="card-block p-1">
                        @if (session()->has('offers'))
                            @foreach (session('offers') as $key => $offer)
                                <div class="row">
                                    <div class="col-sm-12 mb-1 pb-1" style="border-bottom: 1px groove #ccc">
                                        <a class="btn btn-sm btn-outline-danger float-right" href="{{ route('shop.trade.remove', $key) }}">Remove</a>
                                        @if ($offer['img'] == 'pokemon/thumbnail')
                                            <a href="{{ route('pokemon.show', $offer['details']->id) }}">
                                                <span class="mr-1 float-left mb-1">
                                                    <img class="img-fluid item-list" src="{{ asset('images/'. $offer['img'] .'/'. str_slug($offer['details']->info->name) .'.png') }}" alt="{{ $offer['details']->info->name }}">
                                                </span>
                                                {{ $offer['details']->info->name }} (Lv {{ $offer['details']->level }})</a> <img class="img-fluid" src="{{ asset("images/game/". $offer['details']->gender .".png") }}" alt="Gender" />
                                            <br>
                                            <div class="mb-1">
                                                <img class="img-fluid" src="{{ asset("images/game/".$offer['details']->info->primary_type.".png") }}" alt="{{ $offer['details']->info->primary_type }}">
                                                @if (isset($offer['details']->info->secondary_type))
                                                    <img class="img-fluid" src="{{ asset("images/game/".$offer['details']->info->secondary_type.".png") }}" alt="{{ $offer['details']->info->secondary_type }}">
                                                @endif
                                            </div>
                                            <div class="small">
                                                Ratings: <strong>{{ $offer['details']->ratings }}%</strong>
                                            </div>
                                        @elseif ($offer['img'] == 'items')
                                            <a href="{{ route('bag.show', $offer['details']->id) }}">
                                                <span class="mr-1 float-left mb-1">
                                                    <img class="img-fluid item-list" src="{{ asset("images/items/".str_slug($offer['details']->info->name).".png") }}" alt="{{ $offer['details']->info->name }}"> {{ $offer['details']->info->name }}  {{ $offer['details']->upgrade > 0 ? '+'. $offer['details']->upgrade : '' }}
                                                </span>
                                            </a>
                                        @else
                                            <span class="mr-1 float-left mb-1">
                                                <img class="img-fluid item-list" src="{{ asset('images/'. $offer['img'] .'.png') }}" alt=""> &rsaquo; {{ number_format($offer['details']) }}
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p>No offer(s) yet.</p>
                        @endif
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="my-1">
                                    <small style="color: #636363">Tax fee = <strong>offers x 1000</strong></small>
                                </div>

                                <form action="{{ route('shop.trade.store') }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                    <button type="submit" class="btn p-1 btn-success">Submit</button>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Create Offer</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection