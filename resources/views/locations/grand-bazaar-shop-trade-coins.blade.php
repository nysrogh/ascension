@extends('layouts.app')

@section('title', 'Grand Bazaar - Create Offer')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Offer Coins or Crystal</strong></div>
                    <div class="card-block">
                        <form action="{{ route('shop.trade.coins-store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                            <div class="mb-2">
                                Your coins: <img src="{{ asset('images/game/perks-coins.png') }}" alt="Coins" width="12"> {{ number_format(auth()->user()->gold) }}
                                <br>
                                Your coins: <img src="{{ asset('images/game/perks-crystal.png') }}" alt="Crystal" width="12"> {{ number_format(auth()->user()->crystal) }}
                            </div>

                            <div class="form-group @if ($errors->has('amount')) has-danger @endif">
                                <label class="form-control-label mb-0" for="amount">Amount</label>
                                <input type="number" class="form-control form-control-danger" id="amount" name="amount" min="1" required>

                                @if ($errors->has('amount'))
                                    <div class="form-control-feedback">{{ $errors->first('amount') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('type')) has-danger @endif">
                                <label class="form-control-label mb-0" for="type">Currency Type</label>

                                <select class="form-control" name="type" id="type">
                                    <option value="coins">Coins</option>
                                    <option value="cyrstal">Crystal</option>
                                </select>
                                
                                @if ($errors->has('type'))
                                    <div class="form-control-feedback">{{ $errors->first('type') }}</div>
                                @endif
                            </div>

                            <hr class="my-1">

                            <button type="submit" class="form-control btn btn-success my-1">Add</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('club.index') }}">Club</a>
                            <span class="breadcrumb-item active">Add Coins</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
