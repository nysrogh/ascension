@extends('layouts.app')

@section('title', 'Grand Bazaar - View Offer')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>View Offers</strong></div>
                    <div class="card-block p-1">
                        @if ($offers[0]->trade->content->shop->user_id == auth()->user()->id)
                            <div class="row mb-1" style="border-bottom: 1px ridge #777;">
                                <div class="col-6 mx-auto mb-2 text-center">
                                    <form action="{{ route('shop.trade.accept') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                        <input type="hidden" name="id" value="{{ $offers[0]->trade->id }}">
                                        <input type="hidden" name="shop" value="{{ $offers[0]->trade->content->shop->id }}">

                                        <button type="submit" class="btn  py-1 px-4 btn-success">Accept</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                        @foreach ($offers as $offer)
                            <div class="row">
                                <div class="col-sm-12 mb-1 pb-1" style="border-bottom: 1px dotted #777;">
                                    @if ($offer->pokemon)
                                        <a href="{{ route('pokemon.show', $offer->pokemon->id) }}">
                                            <span class="mr-1 float-left mb-1">
                                                <img class="img-fluid item-list" src="{{ asset('images/pokemon/thumbnail/'. str_slug($offer->pokemon->info->name) .'.png') }}" alt="{{ $offer->pokemon->info->name }}">
                                            </span>
                                            {{ $offer->pokemon->info->name }} (Lv {{ $offer->pokemon->level }})</a> <img class="img-fluid" src="{{ asset("images/game/". $offer->pokemon->gender .".png") }}" alt="Gender" />
                                        <br>
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset("images/game/".$offer->pokemon->info->primary_type.".png") }}" alt="{{ $offer->pokemon->info->primary_type }}">
                                            @if (isset($offer->pokemon->info->secondary_type))
                                                <img class="img-fluid" src="{{ asset("images/game/".$offer->pokemon->info->secondary_type.".png") }}" alt="{{ $offer->pokemon->info->secondary_type }}">
                                            @endif
                                        </div>
                                        <div class="small">
                                            Ratings: <strong>{{ $offer->pokemon->ratings }}%</strong>
                                        </div>
                                    @elseif ($offer->item)
                                        <a href="">
                                            <span class="mr-1 float-left mb-1">
                                                <img class="img-fluid item-list" src="{{ asset("images/items/".str_slug($offer->item->name).".png") }}" alt="{{ $offer->item->name }}"> {{ $offer->item->name }}  {{ $offer->upgrade > 0 ? '+'. $offer->upgrade : '' }}
                                            </span>
                                        </a>
                                    @elseif ($offer->gold)
                                        <span class="mr-1 float-left mb-1">
                                            <img class="img-fluid item-list" src="{{ asset('images/game/perks-coins.png') }}" alt=""> &rsaquo; {{ number_format($offer->gold) }}
                                        </span>
                                    @elseif ($offer->crystal)
                                        <span class="mr-1 float-left mb-1">
                                            <img class="img-fluid item-list" src="{{ asset('images/game/perks-crystal.png') }}" alt=""> &rsaquo; {{ number_format($offer->crystal) }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">View Offer</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection