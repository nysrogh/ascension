@extends('layouts.app')

@section('title', 'Grand Bazaar Trade')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Trade Offers ({{ $trade->trades->count() }})</strong></div>
                    <ul class="nav nav-tabs small">
                        @if ($trade->shop->user_id != auth()->user()->id)
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('shop.trade.create') }}">
                                <i class="fa fa-plus"></i> Create Offer
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('shop.trade.cancel', $trade) }}">
                                <i class="fa fa-trash"></i> Cancel Offer
                            </a>
                        </li>
                        @endif
                    </ul>
                    <div class="card-block p-1">
                        @forelse ($trade->trades as $offer)
                            <div class="row">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px groove #fff">
                                    <img class="img-fluid" src="{{ asset('images/trainers/thumbnail/'. $offer->user->avatar .'.png') }}" alt=""> <a href="{{ route('trainer.show', $offer->user->username) }}"> {{ $offer->user->username }}</a>
                                    <a class="btn btn-sm btn-info float-right" href="{{ route('shop.trade.offer-view', $offer->id) }}">View Offer</a>
                                </div>
                            </div>
                        @empty
                            <p>No offer(s) has been made for this Item/Pokemon yet.</p>
                        @endforelse
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.grand-bazaar.shop', $trade->shop->id) }}">{{ $trade->shop->name }}</a>
                            <span class="breadcrumb-item active">Trade Offers</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection