@extends('layouts.app')

@section('title', 'Pokemon Tech')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>{{ $shop->name }}</strong> <span class="float-right"><img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format(auth()->user()->gold) }}</small></span></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ $shopBg }}" alt="Owner">
                        </div>

                        @if (auth()->user()->id == $shop->user_id)
                            <div class="my-1 text-center">
                                <a href="{{ route('bag') }}" class="btn btn-sm btn-outline-warning">Add Item</a> - <a href="{{ route('pokemon.index') }}" class="btn btn-sm btn-outline-warning">Add Pokemon</a> - <a href="{{ route('location.grand-bazaar.edit') }}" class="btn btn-sm btn-outline-info">Edit Store</a>
                            </div>
                        @endif

                        <div class="my-1 text-center">
                            {{ $shop->description }}
                        </div>
                        
                        <div class="my-1">
                            Owner: <a href="{{ route('trainer.show', $shop->user->username) }}">{{ $shop->user->username }}</a>
                        </div>

                        <hr class="my-1">

                        <div class="mb-2">
                            <strong>Items For Sale</strong>
                            <div class="row mt-1">
                                @forelse($items as $item)
                                <div class="col-md-12 mb-1 pb-1" style="border-bottom: 1px ridge #ddd">
                                    <div class="float-right">
                                        @if (auth()->user()->id == $shop->user_id)
                                            <form action="{{ route('shop.remove') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="item" value="{{ $item->id }}">
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                                <img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($item->price) }}</small> &rsaquo;

                                                <input type="submit" class="btn btn-sm btn-outline-danger" value="Remove"> - <a href="{{ route('shop.trade', $item->id) }}" class="btn btn-sm btn-primary">Trade ({{ $item->trades->count() }})</a>
                                            </form>
                                            {{--<a href="{{ route('shop.remove', $item->id) }}" class="btn btn-sm btn-outline-danger">Remove</a>--}}
                                        @else
                                            <form action="{{ route('shop.buy') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="item" value="{{ $item->id }}">
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                                <img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($item->price) }}</small> &rsaquo;

                                                <input type="submit" class="btn btn-sm btn-success" value="Buy"> - <a href="{{ route('shop.trade', $item->id) }}" class="btn btn-sm btn-primary">Trade ({{ $item->trades->count() }})</a>
                                            </form>
                                            {{--<a href="{{ route('shop.buy', $item->id) }}" class="btn btn-sm btn-success">Buy</a>--}}
                                        @endif
                                    </div>
                                    <div class="mb-1">
                                        <img class="img-fluid item-list float-left mr-1" src="{{ asset("images/items/".str_slug($item->info->name).".png") }}" alt="Item">
                                        <div>
                                            <a href="#">{{ $item->info->name }} {{ $item->upgrade > 0 ? '+'. $item->upgrade : '' }}</a>
                                            <div class="small">
                                                &rsaquo; Available: <strong>{{ $item->available }}</strong>
                                                <br>
                                                <em>{!! strip_tags(vsprintf($item->info->description, $item->primary_value != 0 ? $item->primary_value : $item->info->primary_value)) !!}</em>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                    <div class="col-md-12 mb-1 pb-1" style="border-bottom: 1px ridge #ddd">
                                        No item(s) for sale.
                                    </div>
                                @endforelse
                            </div>
                            {{ $items->links('vendor.pagination.simple-bootstrap-4') }}
                        </div>

                        <div class="mb-2">
                            <strong>Pokemon For Sale</strong>
                            <div class="row mt-1">
                                @forelse ($pokemon as $poke)
                                    <div class="col-md-12 mb-1 pb-1" style="border-bottom: 1px ridge #ddd">
                                        <div class="float-right">
                                            @if (auth()->user()->id == $shop->user_id)
                                                <form action="{{ route('shop.remove') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="item" value="{{ $poke->id }}">
                                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                                    <img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($poke->price) }}</small> &rsaquo;

                                                    <input type="submit" class="btn btn-sm btn-outline-danger" value="Remove"> &middot; <a href="{{ route('shop.trade', $poke->id) }}" class="btn btn-sm btn-outline-primary">Offers ({{ $poke->trades->count() }})</a>
                                                </form>
                                            @else
                                                <form action="{{ route('shop.buy') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="item" value="{{ $poke->id }}">
                                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                                    <img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($poke->price) }}</small> &rsaquo;

                                                    <input type="submit" class="btn btn-sm btn-success" value="Buy"> &middot; <a href="{{ route('shop.trade', $poke->id) }}" class="btn btn-sm btn-primary">Trade ({{ $poke->trades->count() }})</a>
                                                </form>
                                            @endif
                                        </div>
                                        <div class="mb-1">
                                            <img class="img-fluid item-list float-left mr-1" src="{{ asset("images/pokemon/thumbnail/".str_slug($poke->pokemon->info->name).".png") }}" alt="Pokemon">
                                            <div>
                                                <a href="{{ route('pokemon.show', $poke->pokemon->id) }}">{{ $poke->pokemon->info->name }} (Lv {{ $poke->pokemon->level }})</a>
                                                @if (in_array($poke->pokemon->info->id, $pokedex))
                                                    <img class="img-fluid" src="{{ asset("images/game/caught.png") }}" width="12" height="12">
                                                @endif
                                                <div class="small">
                                                    &rsaquo; Ratings: <strong>{{ $poke->pokemon->ratings }}%</strong>
                                                    <div class="mb-1">
                                                        <img class="img-fluid" src="{{ asset("images/game/".$poke->pokemon->info->primary_type.".png") }}" alt="{{ $poke->pokemon->info->primary_type }}">
                                                        @if (isset($poke->pokemon->info->secondary_type))
                                                            <img class="img-fluid" src="{{ asset("images/game/".$poke->pokemon->info->secondary_type.".png") }}" alt="{{ $poke->pokemon->info->secondary_type }}">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <div class="col-md-12 mb-1 pb-1" style="border-bottom: 1px ridge #ddd">
                                        No Pokemon for sale.
                                    </div>
                                @endforelse
                            </div>

                            {{ $pokemon->links('vendor.pagination.simple-bootstrap-4') }}
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.grand-bazaar') }}">Grand Bazaar</a>
                            <span class="breadcrumb-item active">{{ $shop->name }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection