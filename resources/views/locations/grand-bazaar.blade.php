@extends('layouts.app')

@section('title', 'Grand Bazaar')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Grand Bazaar</strong></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ asset('/images/locations/grand-bazaar.png') }}" alt="Grand Bazaar">
                        </div>

                        <hr class="my-1">
                        <div class="text-center">
                            The bazaar is where you can find stores setup by other trainers. You can buy items and Pokemons here as well as offer trades.
                        </div>
                        <hr class="my-1">
                        <form action="{{ route('location.grand-bazaar.search') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <input type="text" class="form-control form-control-sm" name="keyword" maxlength="30" placeholder="Search Item or Pokemon" required>
                            <input type="submit" class="btn btn-sm btn-success mt-1" value="Search"> -
                            <a href="{{ route('location.grand-bazaar') }}" class="btn btn-sm btn-secondary mt-1">Refresh</a>
                        </form>
                        <hr class="my-1">

                        <div class="row mt-1">
                            @if ($results)
                                @forelse ($results as $result)
                                    <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                        <div class="mb-1">
                                            @if ($result->item_id)
                                                <span class="float-right"><img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($result->price) }}</small></span>
                                                <div>
                                                    <img class="img-fluid item-list float-left mr-1" src="{{ asset("images/items/".str_slug($result->info->name).".png") }}" alt="Item">
                                                    <strong>{{ $result->info->name }}</strong>
                                                    <div class="small mt-1">
                                                        Buy at &rsaquo; <a href="{{ route('location.grand-bazaar.shop', $result->shop->id) }}">{{ $result->shop->name }}</a>
                                                    </div>
                                                </div>
                                            @else
                                                <span class="float-right"><img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($result->price) }}</small></span>
                                                <div>
                                                    <img class="img-fluid item-list float-left mr-1" src="{{ asset("images/pokemon/thumbnail/".str_slug($result->pokemon->info->name).".png") }}" alt="Pokemon">
                                                    <a href="{{ route('pokemon.show', $result->pokemon->id) }}">{{ $result->pokemon->info->name }} (Lv {{ $result->pokemon->level }})</a>
                                                    <div class="small">
                                                        &rsaquo; Ratings: <strong>{{ $result->pokemon->ratings }}%</strong>
                                                        <div class="mb-1">
                                                            <img class="img-fluid" src="{{ asset("images/game/".$result->pokemon->info->primary_type.".png") }}" alt="{{ $result->pokemon->info->primary_type }}">
                                                            @if (isset($result->pokemon->info->secondary_type))
                                                                <img class="img-fluid" src="{{ asset("images/game/".$result->pokemon->info->secondary_type.".png") }}" alt="{{ $result->pokemon->info->secondary_type }}">
                                                            @endif
                                                        </div>
                                                            Buy at &rsaquo; <a href="{{ route('location.grand-bazaar.shop', $result->shop->id) }}">{{ $result->shop->name }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @empty
                                    No result(s) found.
                                @endforelse
                            @else
                                @forelse ($shops as $shop)
                                    <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                        <div class="mb-1">
                                            <a href="{{ route('location.grand-bazaar.shop', $shop->id) }}">
                                                <img class="img-fluid float-left mr-1" src="{{ asset("images/trainers/thumbnail/{$shop->user->avatar}.png") }}" alt="Shop">
                                                <strong>{{ $shop->name }}</strong>
                                            </a>
                                            <div>
                                                <small style="color: #777">{{ str_limit($shop->description, 50) }}</small>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    No shop has been setup yet.
                                @endforelse
                            @endif
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Grand Bazaar</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection