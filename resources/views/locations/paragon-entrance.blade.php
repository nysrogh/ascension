@extends('layouts.app')

@section('title', 'Paragon Tower')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Paragon Tower</strong></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ asset('/images/locations/paragon-tower.png') }}" width="100" alt="Paragon Tower">
                        </div>

                        <hr class="my-1">
                        <div class="text-center">
                            This marvelous tower is said appear out of nowhere but there are rumors and possible encounters of a group of extra-terrestrial Pokemon called <strong>Ultra Beasts</strong>. As to their exact location, still a mystery.
                        </div>
                        <hr class="my-1">
                            <div class="text-center">
                                @if (auth()->user()->paragon_limit)
                                    <form action="{{ route('paragon.enter') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                        <button type="submit" class="btn btn-success p-1 mb-2">Enter</button>
                                    </form>
                                @endif
                                <a href="{{ route('paragon.rankings') }}" class="btn btn-sm btn-outline-info">Rankings</a>
                            </div>
                        <hr class="my-1">
                        <div>
                            <strong>Paragon Challenge Rules</strong>
                            <ul>
                                <li>Available only once per day.</li>
                                <li>The goal is to defeat as many random Pokemon you encounter as you can.</li>
                                <li>Difficulty is scaled and based on you team's highest level Pokemon.</li>
                                <li>If all of your Pokemon are below or equal Lv50, you'll fight Lv50 Pokemon.</li>
                                <li>Otherwise, you'll fight Lv100 Pokemon.</li>
                                <li>Every 50 tower levels, you'll encounter a random Ultra Beast.</li>
                                <li>The only Pokemon you are able to catch are Ultra Beasts.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Paragon Tower - Entrance</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection