@extends('layouts.app')

@section('title', 'Pokemon Tech')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Pokemon Center</strong></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ asset('/images/locations/pokemon-center-interior.png') }}" alt="Professor Oak" width="150px">
                        </div>

                        <hr class="my-1">

                        <div>
                            <form action="{{ route('location.poke-center.chat') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="prevent" value="{{ random_int(100, 999) }}">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="message" autocomplete="off" maxlength="150" placeholder="(enter chat message here)" required>
                                    <div>
                                        <label class="form-control-label mb-0" for="captcha">Enter CAPTCHA</label>
                                        <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                        <input type="text" id="captcha" name="captcha" autocomplete="off" required>

                                        @if ($errors->has('captcha'))
                                            <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                        @endif

                                        <small class="form-text text-muted">Captcha code: <strong>{{ $captcha }}</strong></small>
                                    </div>
                                    <div class="mt-1">
                                        <input type="submit" class="btn btn-sm btn-success" value="Send"> - <a href="{{ route('location.poke-center') }}" class="btn btn-sm btn-outline-primary">Refresh</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                        @forelse ($chats as $chat)
                            <div>
                                <a href="{{ route('trainer.show', $chat->users->username) }}">
                                    <span class="float-left mr-1">
                                        <img class="img-fluid" src="{{ asset("/images/trainers/thumbnail/{$chat->users->avatar}.png") }}" alt="#" />
                                    </span>
                                    <span class="{{ $chat->users->setting_bgcolor }}">{{ $chat->users->username }}</span>
                                </a>
                                @if (auth()->check() && auth()->user()->status == 2)
                                &rsaquo; <a href="{{ route('chat.delete', $chat->id) }}" class="text-danger">[delete]</a>
                                @endif
                                <span class="float-right"><small>{{ $chat->created_at->diffForHumans() }}</small></span>
                            </div>
                            <div class="card card-outline-secondary mb-1" style="background-color: #fff;">
                                <div class="card-block p-1">
                                    {{ $chat->message }}
                                </div>
                            </div>
                        @empty
                            <p>No chat message(s) found.</p>
                        @endforelse

                        <div class="row">
                            <div class="col-sm-12">
                                {{--{{ $chats->links('vendor.pagination.bootstrap-4') }}--}}
                                {{ $chats->links('vendor.pagination.simple-bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Pokemon Center</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection