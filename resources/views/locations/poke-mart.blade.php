@extends('layouts.app')

@section('title', 'Poke Mart')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Poke Mart</strong> <span class="float-right"><img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> {{ number_format(auth()->user()->gold) }}</span></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ asset('/images/locations/pokemon-center-interior.png') }}" alt="Professor Oak" width="150px">
                        </div>

                        <hr class="my-1">

                        @forelse ($items as $item)
                            <div class="row mt-1">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    <div class="float-right">
                                        <form action="{{ route('location.poke-mart.buy') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="prevent" value="{{ random_int(100, 999) }}">
                                            <input type="hidden" name="id" value="{{ $item->id }}">

                                            <img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($item->price) }}</small> &rsaquo;

                                            <select name="qty" id="qty">
                                                <option value="1">1</option>
                                                <option value="5">5</option>
                                                <option value="10">10</option>
                                                <option value="30">30</option>
                                                <option value="50">50</option>
                                            </select>
                                            <input type="submit" class="btn btn-sm btn-success" value="Buy">
                                        </form>
                                        {{--<a href="{{ route('location.poke-mart.buy', $item->id) }}" class="btn btn-sm btn-success">Buy</a>--}}
                                    </div>
                                    <div class="mb-1">
                                        <img class="img-fluid item-list float-left mr-1" src="{{ asset("images/items/".str_slug($item->name).".png") }}" alt="{{ $item->name }}">
                                        <div>
                                            <strong>{{ $item->name }}</strong>
                                            <br>
                                            <small style="color: #777">{{ $item->description }}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>No items found in mart.</p>
                        @endforelse
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Poke Mart</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection