@extends('layouts.app')

@section('title', 'Pokemon Tech - Moves List')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Available Moves</strong>
                    <span class="float-right">{{ number_format(auth()->user()->gold) }} <img class="img-fluid" src="{{ asset('images/game/gold.png') }}" alt="Coins" width="12"></span>
                    </div>
                    <div class="card-block p-1">
                        <div class="small">
                            <img class="img-fluid" src="{{ asset("images/game/attack_i.png") }}" alt="Attack" width="24px"> &rsaquo; Atk &middot; <img class="img-fluid" src="{{ asset("images/game/special_attack_i.png") }}" alt="Attack" width="24px"> &rsaquo; Sp. Atk &middot; <img class="img-fluid" src="{{ asset("images/game/status_i.png") }}" alt="Attack" width="24px"> &rsaquo; Status
                        </div>

                        <hr class="my-1">
                        @forelse ($moves as $move)
                            <div class="col-md-12 mb-1 pb-1" style="border-bottom: 1px groove #ccc">
                                <span class="float-right">
                                    <a href="{{ route('location.poke-tech.learn', $move->id) }}" class="btn btn-sm btn-success">Learn</a>
                                </span>
                                @if ($move->primary_attr == 'attack')
                                    <img class="img-fluid mb-1" src="{{ asset("images/game/attack_i.png") }}" alt="Attack">
                                @elseif ($move->primary_attr == 'special_attack')
                                    <img class="img-fluid mb-1" src="{{ asset("images/game/special_attack_i.png") }}" alt="Special Attack">
                                @else
                                    <img class="img-fluid mb-1" src="{{ asset("images/game/status_i.png") }}" alt="Status">
                                @endif
                                <strong>{{ $move->name }}</strong>
                                <div class="small">
                                    <div class="mb-1">
                                        <strong>Required Level: {{ $move->level }}</strong>
                                    </div>
                                    <div class="mb-1">
                                        <strong>Cost: {{ number_format($move->level * 695) }} <img class="img-fluid" src="{{ asset('images/game/gold.png') }}" alt="Coins" width="12"></strong>
                                    </div>
                                    {{ $move->description }}
                                </div>
                            </div>
                        @empty
                            <span>This section is empty.</span>
                        @endforelse

                        {{ $moves->links('vendor.pagination.simple-bootstrap-4') }}
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ url('/pokemon-tech') }}">Pokemon Tech</a>
                            <span class="breadcrumb-item active">Learn Moves</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection