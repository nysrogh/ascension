@extends('layouts.app')

@section('title', 'Pokemon Tech')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Pokemon Tech</strong></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ asset('images/npc/prof-juniper.png') }}" alt="Professor Oak">
                        </div>

                        <div class="mb-2">
                            Hi, welcome to Pokemon Tech. Your Pokemon can <strong>learn</strong> new moves here. Your Pokemon can now learn moves from any type belonging to the same category as their primary or secondary types.
                        </div>

                        <hr class="my-1" style="background: #333">

                        <div class="col-sm-12 text-center">
                            <div class="row">
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'ice') }}">
                                        <img src="{{ asset('images/game/ice.png') }}" alt="Fire">
                                        <div>
                                            Ice
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'water') }}">
                                        <img src="{{ asset('images/game/water.png') }}" alt="Fire">
                                        <div>
                                            Water
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'ground') }}">
                                        <img src="{{ asset('images/game/ground.png') }}" alt="Fire">
                                        <div>
                                            Ground
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'rock') }}">
                                        <img src="{{ asset('images/game/rock.png') }}" alt="Fire">
                                        <div>
                                            Rock
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3"></div>
                                <div class="col-3 mb-0">
                                    <a href="{{ route('location.poke-tech.moves', 'grass') }}">
                                        <img src="{{ asset('images/game/grass.png') }}" alt="Fire">
                                        <div>
                                            Grass
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-0">
                                    <a href="{{ route('location.poke-tech.moves', 'bug') }}">
                                        <img src="{{ asset('images/game/bug.png') }}" alt="Fire">
                                        <div>
                                            Bug
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <hr class="my-1" style="background: #333">

                        <div class="col-sm-12 mt-1 text-center">
                            <div class="row">
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'fighting') }}">
                                        <img src="{{ asset('images/game/fighting.png') }}" alt="Fire">
                                        <div>
                                            Fighting
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'dragon') }}">
                                        <img src="{{ asset('images/game/dragon.png') }}" alt="Fire">
                                        <div>
                                            Dragon
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'fairy') }}">
                                        <img src="{{ asset('images/game/fairy.png') }}" alt="Fire">
                                        <div>
                                            Fairy
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'flying') }}">
                                        <img src="{{ asset('images/game/flying.png') }}" alt="Fire">
                                        <div>
                                            Flying
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3"></div>
                                <div class="col-3 mb-0">
                                    <a href="{{ route('location.poke-tech.moves', 'normal') }}">
                                        <img src="{{ asset('images/game/normal.png') }}" alt="Fire">
                                        <div>
                                            Normal
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-0">
                                    <a href="{{ route('location.poke-tech.moves', 'steel') }}">
                                        <img src="{{ asset('images/game/steel.png') }}" alt="Fire">
                                        <div>
                                            Steel
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <hr class="my-1" style="background: #333">

                        <div class="col-sm-12 mt-1 text-center">
                            <div class="row">
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'poison') }}">
                                        <img src="{{ asset('images/game/poison.png') }}" alt="Fire">
                                        <div>
                                            Poison
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'psychic') }}">
                                        <img src="{{ asset('images/game/psychic.png') }}" alt="Fire">
                                        <div>
                                            Psychic
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'ghost') }}">
                                        <img src="{{ asset('images/game/ghost.png') }}" alt="Fire">
                                        <div>
                                            Ghost
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-2">
                                    <a href="{{ route('location.poke-tech.moves', 'fire') }}">
                                        <img src="{{ asset('images/game/fire.png') }}" alt="Fire">
                                        <div>
                                            Fire
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3"></div>
                                <div class="col-3 mb-0">
                                    <a href="{{ route('location.poke-tech.moves', 'electric') }}">
                                        <img src="{{ asset('images/game/electric.png') }}" alt="Fire">
                                        <div>
                                            Electric
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 mb-0">
                                    <a href="{{ route('location.poke-tech.moves', 'dark') }}">
                                        <img src="{{ asset('images/game/dark.png') }}" alt="Fire">
                                        <div>
                                            Dark
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Pokemon Tech</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection