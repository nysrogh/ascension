@extends('layouts.app')

@section('title', 'Club Rankings')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Top Clubs</strong></div>
                    <div class="card-block p-1">
                        <div class="row mt-1">
                            @forelse ($rankings as $club)
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    <div class="mb-2">
                                        <span class="float-right">
                                            {{ number_format($club->point) }} Points
                                        </span>
                                        <a href="{{ route('club.info', $club->id) }}">
                                            <img class="img-fluid club-badge-sm float-left mr-1" src="{{ asset("images/badges/{$club->badge}.png") }}" width="24" height="24">
                                            <strong>{{ $club->name }}</strong>
                                        </a>
                                        <div class="small" style="color: #777">
                                            <span>{{ str_limit($club->description, 50) }}</span>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                No club has been established yet.
                            @endforelse
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                {{ $rankings->links('vendor.pagination.simple-bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.trainers-club') }}">Trainers Club</a>
                            <span class="breadcrumb-item active">Rankings</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection