@extends('layouts.app')

@section('title', 'Trainers Club')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Trainers Club</strong></div>
                    <div class="card-block p-1">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ asset('/images/locations/trainers-club.png') }}" alt="Trainers Club">
                        </div>

                        <hr class="my-1">
                        <div class="text-center">
                            The Trainers Club is where you create or join a club. Below is the list of available clubs to join to.
                        </div>
                        <hr class="my-1">
                        @if (!auth()->user()->club_id)
                            <div class="text-center">
                                <a href="{{ route('club.create') }}" class="btn btn-sm btn-outline-success">Create Own Club</a>
                            </div>
                        @else
                            <div class="text-center">
                                <a href="{{ route('club.rankings') }}" class="btn btn-sm btn-outline-info">Club Rankings</a>
                            </div>
                        @endif
                        <hr class="my-1">

                        <div class="row mt-1">
                            @forelse ($clubs as $club)
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    <div class="mb-2">
                                        <a href="{{ route('club.info', $club->id) }}">
                                            <img class="img-fluid club-badge-sm float-left mr-1" src="{{ asset("images/badges/{$club->badge}.png") }}" width="24" height="24">
                                            <strong>{{ $club->name }}</strong>
                                        </a>
                                        <div class="small" style="color: #777">
                                            <span>{{ str_limit($club->description, 50) }}</span>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                No club has been established yet.
                            @endforelse
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Trainers Club</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection