@extends('layouts.app')

@section('title', 'Trainer Mail - Create')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Send Mail</strong></div>
                    <div class="card-block">
                        <form action="{{ route('mail.store') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group @if ($errors->has('message')) has-danger @endif">
                                <label class="form-control-label mb-0" for="message">Message</label>
                                <textarea name="message" class="form-control form-control-danger" rows="5" placeholder="(content of your message here)" maxlength="500" required>{{ old('message') }}</textarea>

                                @if ($errors->has('message'))
                                    <div class="form-control-feedback">{{ $errors->first('message') }}</div>
                                @endif
                            </div>

                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                            <div class="form-group @if ($errors->has('user_id')) has-danger @endif">
                                <label class="form-control-label mb-0" for="user_id">Recipient</label>
                                <input type="text" name="user_id" value="{{ $username ?? '' }}" class="form-control form-control-danger" placeholder="(enter trainer ID or Username here)" required/>

                                @if ($errors->has('user_id'))
                                    <div class="form-control-feedback">{{ $errors->first('user_id') }}</div>
                                @endif
                            </div>

                            <hr class="my-1">

                            <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                <label for="captcha">Enter the code: <em>{{ $captcha }}</em></label>
                                <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                <input type="text" name="captcha" class="form-control" autocomplete="off" minlength="3" maxlength="3" placeholder="(enter the 3 digit code above)" />

                                @if ($errors->has('captcha'))
                                    <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Send</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">Send Mail</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
