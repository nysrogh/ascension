@extends('layouts.app')

@section('title', 'Trainer Mail')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Trainer Mail</strong>
                        <span class="float-right"><a href="{{ route('mail.create') }}">Compose</a></span>
                    </div>
                    <div class="card-block p-1">
                        @forelse ($mails as $mail)
                            <div class="row mt-1">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    @if ($mail->is_read)
                                        <span class="mr-1 float-left mb-1">
                                            <i class="fa fa-envelope-open-o"></i> &rsaquo; <a href="{{ route('mail.show', $mail->id) }}">{{ $mail->sender->username }}: {{ str_limit($mail->message, 30) }}</a>
                                        </span>
                                    @else
                                        <span class="mr-1 float-left mb-1">
                                            <i class="fa fa-envelope-o"></i> &rsaquo; <a href="{{ route('mail.show', $mail->id) }}"><strong>{{ $mail->sender->username }}: {{ str_limit($mail->message, 30) }}</strong></a>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @empty
                            <span>You have no mail at the moment.</span>
                        @endforelse

                        <div class="row">
                            <div class="col-sm-12">
                                {{ $mails->links('vendor.pagination.simple-bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Mail</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection