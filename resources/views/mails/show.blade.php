@extends('layouts.app')

@section('title', 'Trainer Mail - Read')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Mail from {{ $mail->sender->username }}</strong></div>
                    <div class="card-block p-1">
                        @if ($reply)
                            <span class="float-left mr-1">
                                <img class="img-fluid" src="{{ asset("/images/trainers/thumbnail/{$reply->sender->avatar}.png") }}" alt="#" />
                            </span>
                            <div>
                                <a href="{{ route('trainer.show', $reply->sender->username) }}">
                                    <strong>{{ $reply->sender->username }}</strong>
                                </a>
                                <span class="float-right"><small>{{ $reply->created_at->diffForHumans() }}</small></span>
                            </div>
                            <div class="card card-outline-default mb-1" style="background-color: #fff;">
                                <div class="card-block p-1">
                                    {{ $reply->message }}
                                </div>
                            </div>
                            <hr class="my-1">
                        @endif
                        <span class="float-left mr-1">
                            <img class="img-fluid" src="{{ asset("/images/trainers/thumbnail/{$mail->sender->avatar}.png") }}" alt="#" />
                        </span>
                        <div>
                            <a href="{{ route('trainer.show', $mail->sender->username) }}">
                                <strong>{{ $mail->sender->username }}</strong>
                            </a>
                            <span class="float-right"><small>{{ $mail->created_at->diffForHumans() }}</small></span>
                        </div>
                        <div class="card card-outline-default mb-1" style="background-color: #fff;">
                            <div class="card-block p-1">
                                {{ $mail->message }}
                            </div>
                        </div>

                        <div class="mt-2 mb-1" style="border-bottom: 1px dotted #aaa"></div>

                        @if (auth()->check())
                            <div class="col-sm-12">
                                <form action="{{ route('mail.store') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group @if ($errors->has('message')) has-danger @endif">
                                        <label class="form-control-label mb-0" for="message">Reply</label>
                                        <textarea name="message" class="form-control form-control-danger" rows="5" placeholder="(content of your message here)" maxlength="500" required>{{ old('message') }}</textarea>

                                        @if ($errors->has('message'))
                                            <div class="form-control-feedback">{{ $errors->first('message') }}</div>
                                        @endif
                                    </div>

                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                    <input type="hidden" name="user_id" value="{{ $mail->sender->id }}"/>

                                    <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                        <label for="captcha">Enter the code: <em>{{ $captcha }}</em></label>
                                        <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                        <input type="text" name="captcha" class="form-control" autocomplete="off" minlength="3" maxlength="3" placeholder="(enter the 3 digit code above)" />

                                        @if ($errors->has('captcha'))
                                            <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                        @endif
                                    </div>

                                    <button type="submit" class="form-control btn btn-success my-1">Send</button>
                                </form>
                            </div>
                        @endif
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('mail.index') }}">Mail</a>
                            <span class="breadcrumb-item active">Read</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection