@extends('layouts.app')

@section('title', 'Club Missions')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Club Missions </strong> <span class="float-right">Mission: {{ auth()->user()->mission_left }}/10</span>
                    </div>
                    <div class="card-block p-1">
                        @if (auth()->user()->mission)
                        <div class="alert alert-info">
                            &rsaquo; 30% of coin rewards will also go to club's treasury.
                        </div>
                        <div class="col-md-12 mb-1">
                            <div class="mb-1">
                                <span class="mr-1 float-left mb-1">
                                    <img class="img-fluid" src="{{ asset("images/game/mission-". auth()->user()->mission->modifier.".png") }}" alt="Mission">
                                </span>
                                <strong>&rsaquo; {{ auth()->user()->mission->title }}</strong>

                                <div class="small">
                                    <span style="color: #777">{{ auth()->user()->mission->description }}</span>
                                </div>

                                <div class="mt-2 small">
                                    <strong>Rewards:</strong>
                                    <br>
                                    &rsaquo; {{ number_format(auth()->user()->mission->coin) }} coins<br>
                                    &rsaquo; {{ number_format(auth()->user()->mission->point) }} cp
                                </div>

                                <div class="my-2">
                                    <span class="text-info">Progress: {{ auth()->user()->mission_progress }} / {{ (auth()->user()->mission->modifier == 'catch') ? 1 : auth()->user()->mission->value }}</span>
                                </div>

                            </div>
                            <div class="mt-1">
                                @if (auth()->user()->mission->modifier != 'catch' && auth()->user()->mission_progress >= auth()->user()->mission->value)
                                    <form action="{{ route('mission.report') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                        <input type="submit" class="btn btn-sm btn-success" value="Report">
                                    </form>
                                @elseif (auth()->user()->mission->modifier == 'catch' && auth()->user()->mission_progress != 0)
                                    <form action="{{ route('mission.report') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                        <input type="submit" class="btn btn-sm btn-success" value="Report">
                                    </form>
                                @else
                                    <a href="{{ route('mission.abort') }}" class="btn btn-sm btn-danger">Abort</a>
                                @endif
                            </div>
                        </div>
                        @else
                            <div class="alert alert-info p-1">
                                &rsaquo; Earn club points and coins by doing daily missions
                            </div>
                            @forelse ($missions as $mission)
                                <div class="row">
                                    <div class="col-md-12 mb-1" style="border-bottom: 1px groove #fff">
                                        <div class="float-right">
                                            <form action="{{ route('mission.store') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $mission->id }}">
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                                <input type="submit" class="btn btn-sm btn-success" value="Accept">
                                            </form>
                                        </div>
                                        <span class="mr-1 float-left mb-1">
                                            <img class="img-fluid" src="{{ asset("images/game/mission-".$mission->modifier.".png") }}" alt="Mission">
                                        </span>
                                            <strong>{{ $mission->title }}</strong>
                                        <br>
                                        <div class="mb-2 small">
                                            <em>{{ $mission->description }}</em>
                                        </div>
                                        <div class="mb-1 small">
                                            <strong>Rewards:</strong>
                                            <br>
                                            &rsaquo; {{ number_format($mission->coin) }} coins<br>
                                            &rsaquo; {{ number_format($mission->point) }} cp
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p>No mission(s) found. Please report to admin immediately.</p>
                            @endforelse
                        @endif
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('club.index') }}">Club</a>
                            <span class="breadcrumb-item active">Club Missions</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection