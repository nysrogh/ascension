@extends('layouts.app')

@section('title', 'Paragon Battle!')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-danger bg-faded">
                    <div class="card-header p-1"><strong>Paragon Challenge!</strong>
                        <a href="{{ route('paragon.leave') }}" class="btn btn-sm btn-outline-danger float-right">Escape</a>
                    </div>
                    <div class="card-block p-1">
                        <div class="row mb-1">
                            <div class="col-4 text-right">
                                @if (session("tmp.enemy_{$enemy['stats']->id}"))
                                    @foreach (session("tmp.enemy_{$enemy['stats']->id}") as $key => $value)
                                        @if ($key == 'burn')
                                            <span class="badge badge-pill small" style="background-color:red; line-height: 6px; border-radius: 5px">BRN</span>
                                        @endif

                                        @if ($key == 'poison')
                                            <span class="badge badge-pill small" style="background-color:purple; line-height: 6px; border-radius: 5px">PSN</span>
                                        @endif

                                        @if ($key == 'confuse')
                                            <span class="badge badge-pill small" style="background-color:darkorange; line-height: 6px; border-radius: 5px">CNF</span>
                                        @endif

                                        @if ($key == 'freeze')
                                            <span class="badge badge-pill small" style="background-color:#0275d8; line-height: 6px; border-radius: 5px">FRZ</span>
                                        @endif

                                        @if ($key == 'paralyze')
                                            <span class="badge badge-pill small" style="background-color:orange; line-height: 6px; border-radius: 5px">PAR</span>
                                        @endif

                                        @if ($key == 'sleep')
                                            <span class="badge badge-pill small" style="background-color:#110069; line-height: 6px; border-radius: 5px">SLP</span>
                                        @endif

                                        @if ($key == 'infatuate')
                                            <span class="badge badge-pill small" style="background-color:#ff00aa; line-height: 6px; border-radius: 5px">INF</span>
                                        @endif

                                        @if ($key == 'flinch')
                                            <span class="badge badge-pill small" style="background-color:#75453a; line-height: 6px; border-radius: 5px">STN</span>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-8">
                                <div>
                                    {{ $enemy['stats']->info->name }} <img class="img-fluid mb-1" src="{{ asset("images/game/".$enemy['stats']->gender.".png") }}" alt="Gender" />
                                   <span class="float-right">
                                       Lv{{ $enemy['stats']->level }}
                                   </span>
                                </div>
                                <span class="float-left badge badge-pill text-warning" style="line-height: 6px; background-color: #444; border-radius: 3px 0 0 3px"><strong>HP</strong></span>
                                <div class="progress" style="border: 1px ridge #000; background-color: #999; border-radius: 0 3px 3px 0">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round($enemy["current_hp_{$enemy['stats']->id}"]/$enemy['stats']->hp * 100) }}%; line-height: 9px; height: 9px; color: #000">
                                        <small>{{ $enemy["current_hp_{$enemy['stats']->id}"] ."/". $enemy['stats']->hp }}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-sm-12 text-center">
                                <img class="img-fluid" src="{{ session('paragon.background') }}" width="100%" style="max-width: 215px" alt="">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-8">
                                <div>
                                    {{ $pokemon['stats']->info->name }} <img class="img-fluid mb-1" src="{{ asset("images/game/".$pokemon['stats']->gender.".png") }}" alt="Gender" />
                                    <span class="float-right">
                                       Lv{{ $pokemon['stats']->level }}
                                   </span>
                                </div>
                                <span class="float-left badge badge-pill text-warning" style="line-height: 6px; background-color: #444; border-radius: 3px 0 0 3px"><strong>HP</strong></span>
                                <div class="progress" style="border: 1px ridge #000; background-color: #999; border-radius: 0 3px 3px 0">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round($pokemon["current_hp_{$pokemon['stats']->id}"]/$pokemon['stats']->hp * 100) }}%; line-height: 9px; height: 9px; color: #000">

                                        <small>{{ $pokemon["current_hp_{$pokemon['stats']->id}"] ."/". $pokemon['stats']->hp }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                @if (session("tmp.pokemon_{$pokemon['stats']->id}"))
                                    @foreach (session("tmp.pokemon_{$pokemon['stats']->id}") as $key => $value)
                                        @if ($key == 'burn')
                                            <span class="badge badge-pill small" style="background-color:red; line-height: 6px; border-radius: 5px">BRN</span>
                                        @endif

                                        @if ($key == 'poison')
                                            <span class="badge badge-pill small" style="background-color:purple; line-height: 6px; border-radius: 5px">PSN</span>
                                        @endif

                                        @if ($key == 'confuse')
                                            <span class="badge badge-pill small" style="background-color:darkorange; line-height: 6px; border-radius: 5px">CNF</span>
                                        @endif

                                        @if ($key == 'freeze')
                                            <span class="badge badge-pill small" style="background-color:#0275d8; line-height: 6px; border-radius: 5px">FRZ</span>
                                        @endif

                                        @if ($key == 'paralyze')
                                            <span class="badge badge-pill small" style="background-color:orange; line-height: 6px; border-radius: 5px">PAR</span>
                                        @endif

                                        @if ($key == 'sleep')
                                            <span class="badge badge-pill small" style="background-color:#110069; line-height: 6px; border-radius: 5px">SLP</span>
                                        @endif

                                        @if ($key == 'infatuate')
                                            <span class="badge badge-pill small" style="background-color:#ff00aa; line-height: 6px; border-radius: 5px">INF</span>
                                        @endif

                                        @if ($key == 'flinch')
                                            <span class="badge badge-pill small" style="background-color:#75453a; line-height: 6px; border-radius: 5px">STN</span>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row mb-1">
                            @if (!session()->has('paragon.result') && $pokemon["current_hp_{$pokemon['stats']->id}"] > 0)
                                <div class="col-12" style="border: 1px ridge #000; background: #ddd">
                                    <div class="row small">
                                        @foreach ($pokemon['stats']->moves as $move)
                                            <div class="col-6 my-1">
                                                <form action="{{ route('paragon.fight') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $move->id }}">
                                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                                    <input type="submit" class="form-control form-control-sm" value="{{ $move->name }}">
                                                </form>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                        <hr class="my-1">
                        @if (!session()->has('paragon.result'))
                            <div class="col-sm-12">
                                <a href="{{ route('team') }}" class="btn btn-sm btn-outline-success mr-1">
                                    <img src="{{ asset('images/game/team.png') }}" alt="Team">
                                    <div class="mt-1">Team</div>
                                </a>
                                @if ($pokemon["current_hp_{$pokemon['stats']->id}"] >= 1)
                                    <a href="{{ route('bag') }}" class="btn btn-sm btn-outline-info">
                                        <img src="{{ asset('images/game/bag.png') }}" alt="Bag">
                                        <div class="mt-1">Bag</div>
                                    </a>
                                @endif
                            </div>
                        @else
                            <div class="col-sm-12 text-center">
                                <a href="{{ route('paragon.end') }}" class="btn btn-success p-1">Continue</a>
                            </div>
                        @endif
                        <hr class="my-1">
                        <div class="row">
                            <div class="col-sm-12">
                                <strong>Battle Log (Turns: {{ session('paragon.turns') }})</strong><br>
                                @if (session('paragon.battle_log'))
                                    @foreach (session('paragon.battle_log') as $log)
                                        <div class="p-1">&rsaquo; {!! $log !!}</div>
                                    @endforeach
                                @else
                                    No battle logs yet.
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">Paragon Battle</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection