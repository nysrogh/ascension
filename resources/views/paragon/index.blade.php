@extends('layouts.app')

@section('title', "Exploring - Paragon Tower")

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.flash-messages')
            <div class="col-sm-12">
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Exploring Paragon Tower</strong>
                        <span class="float-right">
                            <a href="{{ route('paragon.leave') }}" class="btn btn-sm btn-danger">Leave</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-12 my-2 text-center">
                <div class="row">
                    <div class="col-3">
                        <a href="{{ route('team') }}">
                            <div class="mb-1">
                                <img src="{{ asset('/images/game/team.png') }}" alt="Team">
                            </div>
                            <div>
                                Team
                            </div>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="{{ route('bag') }}">
                            <div class="mb-1">
                                <img src="{{ asset('/images/game/bag.png') }}" alt="Bag">
                            </div>
                            <div>
                                Bag
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card card-outline-info bg-faded">
                    <div class="card-block p-0">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="mt-2">
                                    <img class="img-fluid mr-1 mb-1" src="{{ asset("images/locations/paragon-tower.png") }}" width="100" alt="Paragon Tower" style="border: 1px groove #ccc">
                                    <div class="mb-1">
                                        Current Level
                                        <h2 class="my-3">{{ session('paragon.progress') }}</h2>
                                    </div>
                                </div>
                                <div class="mt-0 mr-1 ml-1 mb-1">
                                    <form action="{{ route('paragon.challenge') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                        <input type="submit" class="btn btn-success p-1" value="Challenge">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Paragon Tower</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection