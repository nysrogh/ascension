@extends('layouts.app')

@section('title', 'Paragon - Rankings')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Paragon Rankings (Top 30)</strong></div>
                    <div class="card-block p-1">
                        @forelse ($rankings as $trainer)
                            <div class="row">
                                <div class="col-md-12 pb-1" style="border-bottom: 1px ridge #ddd">
                                    <a href="{{ route('trainer.show', $trainer->username) }}">
                                        <img class="img-fluid float-left mr-2" src="{{ asset("images/trainers/thumbnail/{$trainer->avatar}.png") }}" alt="" width="24">
                                        {{ $trainer->username }}
                                    </a>
                                    <div class="mt-1 float-right">Level {{ $trainer->paragon_level }}</div>
                                </div>
                            </div>
                        @empty
                            <span>No rankings yet.</span>
                        @endforelse
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('paragon.entrance') }}">Paragon Tower</a>
                            <span class="breadcrumb-item active">Rankings</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection