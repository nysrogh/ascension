@extends('layouts.app')

@section('title', 'Pokemon Box')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>{{ ($box->count()) ? $box[0]->user->username ."'s" : '' }} Pokemon Box ({{ $box->total() }})  <span class="float-right">Crystal: {{ number_format(auth()->user()->crystal) }}</span></strong></div>
                    <div class="card-block p-1">
                        <form action="{{ route('pokemon.search') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <input type="hidden" name="user" value="{{ ($box->count()) ? $box[0]->user->id : auth()->user()->id }}">
                            <input type="text" class="form-control form-control-sm" name="keyword" maxlength="30" placeholder="Search Pokemon" required>
                            <input type="submit"     class="btn btn-sm btn-success mt-1" value="Search"> &middot;
                            <a href="{{ route('pokemon.index', ($box->count()) ? $box[0]->user->id : ($userBox) ?? auth()->user()->id) }}" class="btn btn-sm btn-secondary mt-1">Reset</a>
                        </form>
                        <hr class="my-1">
                        @forelse ($box as $pokemon)
                            <div class="row">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px groove #ccc">
                                    @if (session()->has('trade'))
                                        <div class="float-right">
                                            <form action="{{ route('shop.trade.offer') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                <input type="hidden" name="offer" value="{{ $pokemon->id }}">
                                                <input type="hidden" name="type" value="pokemon">

                                                <input type="submit" class="btn btn-sm btn-success" value="Offer">
                                            </form>
                                        </div>
                                    @endif
                                    <a href="{{ route('pokemon.show', $pokemon->id) }}">
                                    <span class="mr-1 float-left mb-1">
                                        <img class="img-fluid item-list" src="{{ asset("images/pokemon/thumbnail/".str_slug($pokemon->info->name).".png") }}" alt="{{ $pokemon->info->name }}">
                                    </span>
                                    {{ $pokemon->info->name }} (Lv {{ $pokemon->level }})</a> <img class="img-fluid" src="{{ asset("images/game/". $pokemon->gender .".png") }}" alt="Gender" />
                                    <br>
                                    <div class="mb-1">
                                        <img class="img-fluid" src="{{ asset("images/game/".$pokemon->info->primary_type.".png") }}" alt="{{ $pokemon->info->primary_type }}">
                                        @if (isset($pokemon->info->secondary_type))
                                            <img class="img-fluid" src="{{ asset("images/game/".$pokemon->info->secondary_type.".png") }}" alt="{{ $pokemon->info->secondary_type }}">
                                        @endif
                                    </div>
                                    <div class="small">
                                        Ratings: <strong>{{ $pokemon->ratings }}%</strong>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>Pokemon box is empty.</p>
                        @endforelse

                        <div class="row">
                            <div class="col-sm-12">
                                {{ $box->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('team') }}">Team</a>
                            <span class="breadcrumb-item active">Pokemon Box</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection