@extends('layouts.app')

@section('title', 'Pokedex')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card card-outline-danger bg-faded">
                    <div class="card-header p-1"><strong>#{{ $pokemon->id }} &rsaquo;  {{ $pokemon->name }}</strong></div>
                    <div class="card-block p-1">
                        <div class="row">
                            <div class="col-sm-12">
                                @if($pokemon->rarity == 2)
                                    ★★
                                @elseif($pokemon->rarity == 3)
                                    ★★★
                                @elseif($pokemon->rarity == 4)
                                    ★★★★
                                @elseif($pokemon->rarity == 5)
                                    ★★★★★
                                @else
                                    ★
                                @endif
                                <span class="ml-1">
                                <img class="img-fluid" src="{{ asset("images/game/".$pokemon->primary_type.".png") }}" alt="{{ $pokemon->primary_type }}">
                                    @if (isset($pokemon->secondary_type))
                                        <img class="img-fluid" src="{{ asset("images/game/".$pokemon->secondary_type.".png") }}" alt="{{ $pokemon->primary_type }}">
                                    @endif
                            </span>
                                <br>
                                <img class="float-left" src="{{ asset("images/pokemon/thumbnail/".str_slug($pokemon->name).".png") }}" alt="{{ $pokemon->name }}">
                                <div>
                                    {{ $pokemon->description }}
                                </div>
                            </div>
                            <div class="col-sm-12 mb-2">
                                <strong>Gender ratio</strong>
                                @if ($pokemon->gender_ratio != 999)
                                    <div class="progress w-50" style="background: #ff609a">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: {{ $pokemon->gender_ratio }}%; height: 8px">
                                        </div>
                                    </div>
                                    <span class="small">{{ $pokemon->gender_ratio }}% male, {{ 100 - $pokemon->gender_ratio }}% female</span>
                                @else
                                    <div><em>genderless</em></div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <strong>Base Stats</strong>
                                <span class="small float-right"><strong>Lv100 (100% ratings)</strong></span>
                                <table class="table table-sm table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span class="small"><strong>HP:</strong> {{ $pokemon->hp }}</span>
                                                <span class="small float-right">{{ $pokemon->hp * ((3 + ((100 - 80) / 2)) / 100 ) * 100 }}</span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round(($pokemon->hp/255) * 100) }}%;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="small"><strong>ATK:</strong> {{ $pokemon->attack }}</span>
                                                <span class="small float-right">{{ $pokemon->attack * ((3 + ((100 - 80) / 2)) / 100 ) * 100 }}</span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round(($pokemon->attack/255) * 100) }}%;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="small"><strong>DEF:</strong> {{ $pokemon->defense }}</span>
                                                <span class="small float-right">{{ $pokemon->defense * ((3 + ((100 - 80) / 2)) / 100 ) * 100 }}</span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round(($pokemon->defense/255) * 100) }}%;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="small"><strong>SATK:</strong> {{ $pokemon->special_attack }}</span>
                                                <span class="small float-right">{{ $pokemon->special_attack * ((3 + ((100 - 80) / 2)) / 100 ) * 100 }}</span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round(($pokemon->special_attack/255) * 100) }}%;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="small"><strong>SDEF:</strong> {{ $pokemon->special_defense }}</span>
                                                <span class="small float-right">{{ $pokemon->special_defense * ((3 + ((100 - 80) / 2)) / 100 ) * 100 }}</span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round(($pokemon->special_defense/255) * 100) }}%;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="small"><strong>SPD:</strong> {{ $pokemon->speed }}</span>
                                                <span class="small float-right">{{ $pokemon->speed * ((3 + ((100 - 80) / 2)) / 100 ) * 100 }}</span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar" style="width: {{ round(($pokemon->speed/255) * 100) }}%;">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-4 text-center">
                                <img class="img-fluid" src="{{ asset("images/pokemon/".str_slug($pokemon->name).".png") }}" alt="{{ $pokemon->name }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <hr class="my-1">
                                <strong>Damaged normally by:</strong>
                                <div class="mb-1">
                                    @foreach ($typeEffects as $type => $value)
                                        @if ($value == 1)
                                            <img class="img-fluid mr-1" src="{{ asset("images/game/{$type}.png") }}" alt="{{ $type }}">
                                        @endif
                                    @endforeach
                                </div>
                                <strong>Weak to:</strong>
                                <div class="mb-1">
                                    @foreach ($typeEffects as $type => $value)
                                        @if ($value > 1)
                                            <img class="img-fluid mr-1" src="{{ asset("images/game/{$type}.png") }}" alt="{{ $type }}">
                                        @endif
                                    @endforeach
                                </div>
                                <strong>Immune to:</strong>
                                <div class="mb-1">
                                    @foreach ($typeEffects as $type => $value)
                                        @if ($value == 0)
                                            <img class="img-fluid mr-1" src="{{ asset("images/game/{$type}.png") }}" alt="{{ $type }}">
                                        @endif
                                    @endforeach
                                </div>
                                <strong>Resistant to:</strong>
                                <div class="mb-1">
                                    @foreach ($typeEffects as $type => $value)
                                        @if ($value < 1 && $value != 0)
                                            <img class="img-fluid mr-1" src="{{ asset("images/game/{$type}.png") }}" alt="{{ $type }}">
                                        @endif
                                    @endforeach
                                </div>
                                <hr class="my-1">
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-12">
                                <strong>Evolution</strong>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="text-center">
                                            <img src="{{ asset("images/pokemon/thumbnail/".str_slug($pokemon->name).".png") }}" alt="{{ $pokemon->name }}">
                                            <div>
                                                {{ $pokemon->name }}
                                            </div>
                                        </div>
                                    </div>
                                    @forelse($evolution as $evo)
                                        <div class="col-3">
                                            <div class="text-center">
                                                <a href="{{ route('pokemon.pokedex-show', $evo->id) }}">
                                                    <img src="{{ asset("images/pokemon/thumbnail/".str_slug($evo->name).".png") }}" alt="{{ $evo->name }}">
                                                    <div>
                                                        {{ $evo->name }}
                                                    </div>
                                                </a>
                                                <div class="small">
                                                    @if (is_numeric($evo->evolution_type))
                                                        <em>(Lv {{ $evo->evolution_type }})</em>
                                                    @else
                                                        <em>({{ ucwords(preg_replace('/-/', ' ', $evo->evolution_type)) }})</em>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                    @endforelse

                                    @if ($evolution2)
                                        @forelse($evolution2 as $evo)
                                            <div class="col-3">
                                                <div class="text-center">
                                                    <a href="{{ route('pokemon.pokedex-show', $evo->id) }}">
                                                        <img src="{{ asset("images/pokemon/thumbnail/".str_slug($evo->name).".png") }}" alt="{{ $evo->name }}">
                                                        <div>
                                                            {{ $evo->name }}
                                                        </div>
                                                    </a>
                                                    <div class="small">
                                                        @if (is_numeric($evo->evolution_type))
                                                            <em>(Lv {{ $evo->evolution_type }})</em>
                                                        @else
                                                            <em>({{ ucwords(preg_replace('/-/', ' ', $evo->evolution_type)) }})</em>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                        @endforelse
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-12">
                                <strong>Forms</strong>
                                <div class="row">
                                    @forelse($forms as $form)
                                        <div class="col-3">
                                            <div class="text-center">
                                                <a href="{{ route('pokemon.pokedex-show', $form->id) }}">
                                                    <img src="{{ asset("images/pokemon/thumbnail/".str_slug($form->name).".png") }}" alt="{{ $form->name }}">
                                                    <div>
                                                        {{ $form->name }}
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('pokemon.pokedex') }}">Pokedex</a>
                            <span class="breadcrumb-item active">{{ $pokemon->name }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection