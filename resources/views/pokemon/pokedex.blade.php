@extends('layouts.app')

@section('title', 'Pokedex')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Pokedex</strong> <span class="float-right small">@if (!$result) {{ round(count(explode(',', auth()->user()->pokedex))/802 * 100) }}% Complete @endif</span></div>
                    <div class="card-block p-1">
                        <form action="{{ route('pokemon.pokedex-search') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <input type="text" class="form-control form-control-sm" name="keyword" maxlength="30" placeholder="Search Pokemon" required>
                            <input type="submit" class="btn btn-sm btn-success mt-1" value="Search"> &middot;
                            <a href="{{ route('pokemon.pokedex') }}" class="btn btn-sm btn-outline-warning mt-1">Reset</a>
                        </form>
                        <hr class="my-1">
                        @if ($result)
                            @forelse ($result as $pokemon)
                                <div class="row">
                                    <div class="col-md-12 mb-1" style="border-bottom: 1px groove #ccc">
                                        <a href="{{ route('pokemon.pokedex-show', $pokemon->id) }}">
                                            <span class="mr-1 float-left mb-1">
                                                <img class="img-fluid item-list" src="{{ asset("images/pokemon/thumbnail/".str_slug($pokemon->name).".png") }}" alt="{{ $pokemon->name }}">
                                            </span>
                                            {{ $pokemon->name }}
                                        </a>
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset("images/game/".$pokemon->primary_type.".png") }}" alt="{{ $pokemon->primary_type }}">
                                            @if (isset($pokemon->secondary_type))
                                                <img class="img-fluid" src="{{ asset("images/game/".$pokemon->secondary_type.".png") }}" alt="{{ $pokemon->secondary_type }}">
                                            @endif
                                        </div>
                                        @if (in_array($pokemon->id, $dex))
                                            <div>
                                                <img class="img-fluid" src="{{ asset("images/game/caught.png") }}" width="12" height="12">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @empty
                                <p>No result(s) found.</p>
                            @endforelse
                        @else
                            @forelse ($pokedex as $pokemon)
                                <div class="row">
                                    <div class="col-md-12 mb-1" style="border-bottom: 1px groove #ccc">
                                        <a href="{{ route('pokemon.pokedex-show', $pokemon->id) }}">
                                            <span class="mr-1 float-left mb-1">
                                                <img class="img-fluid item-list" src="{{ asset("images/pokemon/thumbnail/".str_slug($pokemon->name).".png") }}" alt="{{ $pokemon->name }}">
                                            </span>
                                            {{ $pokemon->name }}
                                        </a>
                                        <div class="mb-1">
                                            <img class="img-fluid" src="{{ asset("images/game/".$pokemon->primary_type.".png") }}" alt="{{ $pokemon->primary_type }}">
                                            @if (isset($pokemon->secondary_type))
                                                <img class="img-fluid" src="{{ asset("images/game/".$pokemon->secondary_type.".png") }}" alt="{{ $pokemon->secondary_type }}">
                                            @endif
                                        </div>
                                        @if (in_array($pokemon->id, $dex))
                                            <div>
                                                <img class="img-fluid" src="{{ asset("images/game/caught.png") }}" width="12" height="12">
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @empty
                                <p>Pokedex is empty. Please report to admin.</p>
                            @endforelse

                            <div class="row">
                                <div class="col-sm-12">
                                    {{ $pokedex->links('vendor.pagination.simple-bootstrap-4') }}
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Pokedex</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection