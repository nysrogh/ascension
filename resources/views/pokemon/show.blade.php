@extends('layouts.app')

@section('title', 'Pokemon')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.flash-messages')
            <div class="col-sm-12">
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>{{ $pokemon->info->name }}</strong> <small>(Lv {{ $pokemon->level }})</small>
                        <img class="img-fluid" src="{{ asset("images/game/". $pokemon->gender .".png") }}" alt="Gender" />
                        <a class="float-right btn btn-sm btn-outline-danger" href="{{ route('pokemon.release', $pokemon->id) }}">
                            Release
                        </a>
                    </div>
                    @if ($pokemon->user_id == auth()->user()->id)
                    <ul class="nav nav-tabs small">
                        @if (!$pokemon->in_team)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('pokemon.add', $pokemon->id) }}">
                                    <i class="fa fa-plus"></i> Add to Team
                                </a>
                            </li>
                        @endif
                        @if (auth()->user()->shop)
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('shop.sellPokemon', $pokemon->id) }}">
                                    <i class="fa fa-shopping-cart"></i> Add to Store
                                </a>
                            </li>
                        @endif

                    </ul>
                    @endif
                    <div class="progress">
                        @if ($pokemon->level < 100)
                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{ round(($pokemon->experience/$nextLevel)*100, 2) }}%; line-height: 8px; height: 8px; min-width: 2em; color: #000">

                            <small>{{ round(($pokemon->experience/$nextLevel)*100, 2) }}%</small>

                        </div>
                        @else
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 100%; line-height: 8px; height: 8px; min-width: 2em; color: #000">

                                <small>max</small>

                            </div>
                        @endif
                    </div>
                    <div class="card-block p-1">
                        owner: <a href="{{ route('trainer.show', $pokemon->user->username) }}">{{ $pokemon->user->username }}</a>
                        <span class="float-right">ratings: <strong>{{ $pokemon->ratings }}%</strong></span>
                        <div class="text-center mb-2">
                            <img class="img-fluid" src="{{ asset("images/pokemon/".str_slug($pokemon->info->name).".png") }}" alt="{{ $pokemon->info->name }}">
                            <br>
                            @if($pokemon->info->rarity == 2)
                                ★★
                            @elseif($pokemon->info->rarity == 3)
                                ★★★
                            @elseif($pokemon->info->rarity == 4)
                                ★★★★
                            @elseif($pokemon->info->rarity == 5)
                                ★★★★★
                            @else
                                ★
                            @endif
                            <br>
                            <img class="img-fluid" src="{{ asset("images/game/".$pokemon->info->primary_type.".png") }}" alt="{{ $pokemon->primary_type }}">
                            @if (isset($pokemon->info->secondary_type))
                                <img class="img-fluid" src="{{ asset("images/game/".$pokemon->info->secondary_type.".png") }}" alt="{{ $pokemon->primary_type }}">
                            @endif
                            <div class="mt-1">
                                <a href="{{ route('pokemon.pokedex-show', $pokemon->info->id) }}" class="btn btn-sm btn-outline-info">pokedex</a>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <div class="row">
                                <div class="col-4 mb-1">
                                    <h1 class="mb-0">
                                        @if ($pokemon->tmp_hp > 0)
                                            <span class="text-success">{{ number_format($pokemon->hp) }}</span>
                                        @elseif ($pokemon->tmp_hp < 0)
                                            <span class="text-danger">{{ number_format($pokemon->hp) }}</span>
                                        @else
                                            {{ number_format($pokemon->hp) }}
                                        @endif
                                    </h1>
                                    <small>HP</small>
                                </div>
                                <div class="col-4 mb-1">
                                    <h1 class="mb-0">
                                        @if ($pokemon->tmp_attack > 0)
                                            <span class="text-success">{{ number_format($pokemon->attack) }}</span>
                                        @elseif ($pokemon->tmp_attack < 0)
                                            <span class="text-danger">{{ number_format($pokemon->attack) }}</span>
                                        @else
                                            {{ number_format($pokemon->attack) }}
                                        @endif
                                    </h1>
                                    <small>Attack</small>
                                </div>
                                <div class="col-4 mb-1">
                                    <h1 class="mb-0">
                                        @if ($pokemon->tmp_special_attack > 0)
                                            <span class="text-success">{{ number_format($pokemon->special_attack) }}</span>
                                        @elseif ($pokemon->tmp_special_attack < 0)
                                            <span class="text-danger">{{ number_format($pokemon->special_attack) }}</span>
                                        @else
                                            {{ number_format($pokemon->special_attack) }}
                                        @endif
                                    </h1>
                                    <small>Special Attack</small>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <h1 class="mb-0">
                                        @if ($pokemon->tmp_speed > 0)
                                            <span class="text-success">{{ number_format($pokemon->speed) }}</span>
                                        @elseif ($pokemon->tmp_speed < 0)
                                            <span class="text-danger">{{ number_format($pokemon->speed) }}</span>
                                        @else
                                            {{ number_format($pokemon->speed) }}
                                        @endif
                                    </h1>
                                    <small>Speed</small>
                                </div>
                                <div class="col-4">
                                    <h1 class="mb-0">
                                        @if ($pokemon->tmp_defense > 0)
                                            <span class="text-success">{{ number_format($pokemon->defense) }}</span>
                                        @elseif ($pokemon->tmp_defense < 0)
                                            <span class="text-danger">{{ number_format($pokemon->defense) }}</span>
                                        @else
                                            {{ number_format($pokemon->defense) }}
                                        @endif
                                    </h1>
                                    <small>Defense</small>
                                </div>
                                <div class="col-4">
                                    <h1 class="mb-0">
                                        @if ($pokemon->tmp_special_defense > 0)
                                            <span class="text-success">{{ number_format($pokemon->special_defense) }}</span>
                                        @elseif ($pokemon->tmp_special_defense < 0)
                                            <span class="text-danger">{{ number_format($pokemon->special_defense) }}</span>
                                        @else
                                            {{ number_format($pokemon->special_defense) }}
                                        @endif
                                    </h1>
                                    <small>Special Defense</small>
                                </div>
                            </div>
                        </div>
                        <hr class="my-1">
                        <div class="mt-2">
                            <strong>Next Evolution</strong>
                            @forelse($evolution as $evo)
                                <div class="mt-1">
                                    <span><a href="{{ route('pokemon.evolve', [$pokemon->id, $evo->evolution_type]) }}" class="btn btn-sm btn-success">evolve</a></span>
                                    &rsaquo; <a href="{{ route('pokemon.pokedex-show', $evo->id) }}">{{ $evo->name }}</a>
                                    @if (is_numeric($evo->evolution_type))
                                        at Lv {{ $evo->evolution_type }}
                                    @else
                                        using {{ ucwords(preg_replace('/-/', ' ', $evo->evolution_type)) }}
                                    @endif
                                </div>
                            @empty
                                <div class="mt-1">No possible evolution for this Pokemon.</div>
                            @endforelse
                        </div>
                        <div class="mt-2">
                            <strong>Alternate Forms</strong>
                            @forelse($forms as $form)
                                <div class="mt-1">
                                    <span><a href="{{ route('pokemon.form', [$pokemon->id, $form->id]) }}" class="btn btn-sm btn-info">change</a></span>
                                    &rsaquo; <a href="{{ route('pokemon.pokedex-show', $form->id) }}">{{ $form->name }}</a>
                                    costs <span class="text-success">{{ number_format($form->alternate_cost) }}</span> crystals
                                </div>
                            @empty
                                <div class="mt-1">No possible alternate form for this Pokemon.</div>
                            @endforelse
                        </div>
                        <hr class="my-1">
                        <div class="mt-2">
                            <strong>Held Item</strong>
                            @if ($pokemon->item)
                            <div class="mt-1">
                                <span class="float-right">
                                    <form action="{{ route('bag.unequip') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                        <input type="hidden" name="poke" value="{{ $pokemon->id }}">
                                        <input type="hidden" name="id" value="{{ $pokemon->item->id }}">
                                        <input type="submit" class="btn btn-sm btn-danger" value="remove">
                                    </form>
                                </span>
                                <img src="{{ asset("images/items/".str_slug($pokemon->item->info->name).".png") }}" class="float-left mr-1" alt="{{ $pokemon->item->info->name }}">
                                <a href="{{ route('bag.show', $pokemon->item->id) }}">{{ $pokemon->item->info->name }} {{ $pokemon->item->upgrade > 0 ? '+'. $pokemon->item->upgrade : '' }}</a>
                                <div class="small my-1">
                                    {!! nl2br(vsprintf($pokemon->item->info->description, $pokemon->item->primary_value != 0 ? $pokemon->item->primary_value : $pokemon->item->info->primary_value)) !!}
                                </div>
                            </div>
                            @else
                                <div class="mt-1">Pokemon has no held item.</div>
                            @endif
                        </div>
                        <hr class="my-1">
                        <div class="mt-2">
                            <strong>Moves ({{ $pokemon->moves->count() }}/4)</strong>
                            @foreach($pokemon->moves as $move)
                                <div class="mt-1">
                                    <div class="pb-1" style="border-bottom: 1px groove #ccc">
                                        <strong>&rsaquo; @if ($move->primary_attr == 'attack')
                                                <img class="img-fluid mb-1" src="{{ asset("images/game/attack_i.png") }}" alt="Attack">
                                            @elseif ($move->primary_attr == 'special_attack')
                                                <img class="img-fluid mb-1" src="{{ asset("images/game/special_attack_i.png") }}" alt="Special Attack">
                                            @else
                                                <img class="img-fluid mb-1" src="{{ asset("images/game/status_i.png") }}" alt="Status">
                                            @endif {{ $move->name }}</strong> <img class="ml-1" src="{{ asset("images/game/".$move->type.".png") }}" alt="{{ $move->type }}">
                                        <span class="float-right">
                                            <a href="{{ route('pokemon.forget', [$pokemon->id, $move->id]) }}" class="btn btn-sm btn-danger">forget</a>
                                        </span>
                                        <div class="small my-1">
                                            {!! nl2br(vsprintf($move->description, [$move->primary_value, $move->secondary_value])) !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('pokemon.index') }}">Pokemon</a>
                            <a class="breadcrumb-item" href="{{ route('team') }}">Team</a>
                            <span class="breadcrumb-item active">{{ $pokemon->info->name }}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection