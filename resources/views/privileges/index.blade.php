@extends('layouts.app')

@section('title', 'Club Missions')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Club Perks </strong> <span class="float-right small text-primary">Active: {{ auth()->user()->club->privileges->count() }}/{{ auth()->user()->club->max_privilege }} (max upgrade: {{ auth()->user()->club->level }})</span>
                    </div>
                    <div class="card-block p-1">
                        <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #000">
                            <div class="float-right">
                                <form action="{{ route('club.upgradePrivilege') }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="prevent" value="{{ random_int(100, 999) }}">

                                    <img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format(auth()->user()->club->max_privilege * 130000) }}</small> &rsaquo;

                                    <input type="submit" class="btn btn-sm btn-success" value="Upgrade">
                                </form>
                                {{--<a href="{{ route('location.poke-mart.buy', $item->id) }}" class="btn btn-sm btn-success">Buy</a>--}}
                            </div>
                            <div class="mb-1">
                                <strong>&rsaquo; Max Perks</strong>
                                <br>
                                <small style="color: #777">Increase max perks by 1.</small>
                            </div>
                        </div>
                        <div class="alert alert-info py-1 mb-1">
                            &rsaquo; Only the Leader or Executive can purchase a perk.<br>
                            &rsaquo; More perks will be unlocked as your club gain more levels.<br>
                            &rsaquo; Perks effects can be stacked with each other.
                        </div>
                        @forelse ($perks as $perk)
                            <div class="row">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px groove #fff">
                                    <div class="float-right">
                                        @if (!empty($perk->club[0]))
                                            <span class="badge badge-info">{{ Carbon\Carbon::parse($perk->club[0]->pivot->expired_on)->diffForHumans() }}</span>
                                        @else
                                            <form action="{{ route('privileges.store') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $perk->id }}">
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                                                <img class="img-fluid" width="12" src="{{ asset('images/game/gold.png') }}" alt=""> <small>{{ number_format($perk->coin_cost) }}</small> &rsaquo;

                                                <input type="submit" class="btn btn-sm btn-success" value="Buy">
                                            </form>
                                        @endif
                                    </div>
                                    <span class="mr-1 float-left mb-1">
                                        <img class="img-fluid" src="{{ asset("images/game/perks-".$perk->modifier.".png") }}" alt="Perks">
                                    </span>
                                    <strong class="@if (!empty($perk->club[0])) text-success @endif">{{ $perk->title }}</strong>
                                    <br>
                                    <div class="mb-2 small">
                                        <em>{{ $perk->description }}</em>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p>No perks(s) found. Please report to admin immediately.</p>
                        @endforelse
                        <div class="row">
                            <div class="col-sm-12">
                                {{ $perks->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('club.index') }}">Club</a>
                            <span class="breadcrumb-item active">Club Perks</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection