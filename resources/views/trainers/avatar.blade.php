@extends('layouts.app')

@section('title', 'Change Avatar')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Change Avatar</strong></div>
                    <div class="card-block p-1">
                        @foreach ($avatars as $avatar)
                            <div class="row mt-1">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    <div class="float-right">
                                        <form action="{{ route('trainer.avatarChange') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                            <input type="hidden" name="id" value="{{ $avatar[0] }}">

                                            <img src="{{ asset('images/game/gold.png') }}" alt=""> {{ number_format($avatar[1]) }} &rsaquo;
                                            <input type="submit" class="btn btn-success p-1" value="Change">
                                        </form>
                                    </div>
                                    <span class="mr-1 float-left mb-1">
                                        <img class="img-fluid" src="{{ asset("images/trainers/". auth()->user()->gender ."-{$avatar[0]}.png") }}" alt="Avatar"> <img class="img-fluid" src="{{ asset("images/trainers/thumbnail/". auth()->user()->gender ."-{$avatar[0]}.png") }}" alt="Avatar Thumbnail" width="32">
                                    </span>
                                </div>
                            </div>
                        @endforeach
                        {{ $avatars->links('vendor.pagination.bootstrap-4') }}
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('trainer.settings') }}">Settings</a>
                            <span class="breadcrumb-item active">Change Avatar</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection