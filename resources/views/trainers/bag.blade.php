@extends('layouts.app')

@section('title', 'Trainer Bag')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Trainer Bag</strong></div>
                    <div class="card-block p-1">
                        <div class="row text-center pb-1" style="border-bottom: 1px groove #ccc;">
                            <div class="col-3">
                                <a href="{{ route('bag', 1) }}">
                                    <div class="mb-1">
                                        <img class="img-fluid" src="{{ asset('/images/game/bag-0.png') }}" alt="Items">
                                    </div>
                                    <div>
                                        Items
                                    </div>
                                </a>
                            </div>
                            <div class="col-3">
                                <a href="{{ route('bag', 2) }}">
                                    <div class="mb-1">
                                        <img class="img-fluid" src="{{ asset('/images/game/bag-1.png') }}" alt="Medicines">
                                    </div>
                                    <div>
                                        Meds
                                    </div>
                                </a>
                            </div>
                            <div class="col-3">
                                <a href="{{ route('bag', 3) }}">
                                    <div class="mb-1">
                                        <img class="img-fluid" src="{{ asset('/images/game/bag-2.png') }}" alt="Berries">
                                    </div>
                                    <div>
                                        Berries
                                    </div>
                                </a>
                            </div>
                            <div class="col-3">
                                <a href="{{ route('bag', 4) }}">
                                    <div class="mb-1">
                                        <img class="img-fluid" src="{{ asset('/images/game/bag-3.png') }}" alt="Misc">
                                    </div>
                                    <div>
                                        Misc
                                    </div>
                                </a>
                            </div>
                        </div>
                        @forelse ($bag as $item)
                            <div class="row mt-1">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    @if (session()->has('battle.enemy') || session()->has('paragon') && $item->info->category != 4)
                                        @if (($item->info->category == 1 && !$item->info->section && session()->has('battle.enemy')) || ($item->info->category == 1 && !$item->info->section && session()->has('paragon.enemy') && in_array(session("paragon.enemy.stats")->info->id, [793,794,795,796,797,798,799])))
                                            <div class="float-right">
                                                @if (session()->has('paragon'))
                                                <form action="{{ route('paragon.catch') }}" method="POST">
                                                @else
                                                <form action="{{ route('battle.catch') }}" method="POST">
                                                @endif
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                    <input type="hidden" name="ball" value="{{ $item->id }}">

                                                    <input type="submit" class="btn btn-sm btn-success" value="Use">
                                                </form>
                                            </div>

                                            {{--<span class="float-right"><a href="{{ route('battle.catch', $item->id) }}" class="btn btn-sm btn-success">Use</a></span>--}}
                                        @else
                                            <span class="float-right"><a href="{{ route('bag.useItem', $item->id) }}" class="btn btn-sm btn-success">Use</a></span>
                                        @endif
                                    @elseif (session()->has('trade'))
                                        <div class="float-right">
                                            <form action="{{ route('shop.trade.offer') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                <input type="hidden" name="offer" value="{{ $item->id }}">
                                                <input type="hidden" name="type" value="item">

                                                <input type="submit" class="btn btn-sm btn-success" value="Offer">
                                            </form>
                                        </div>
                                    @endif
                                    <a href="{{ route('bag.show', $item->id) }}">
                                        <span class="mr-1 float-left mb-1">
                                            <img class="img-fluid item-list" src="{{ asset("images/items/".str_slug($item->info->name).".png") }}" alt="{{ $item->info->name }}"> {{ $item->info->name }}  {{ $item->upgrade > 0 ? '+'. $item->upgrade : '' }} ({{ $item->quantity }})
                                        </span>
                                    </a>
                                </div>
                            </div>
                        @empty
                            <span>This section is empty.</span>
                        @endforelse

                        <div class="row">
                            <div class="col-sm-12">
                                {{ $bag->links('vendor.pagination.simple-bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Trainer Bag</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection