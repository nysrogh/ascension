<div class="card @if (auth()->user()->gender == 1) card-outline-danger @else card-outline-info @endif bg-faded">
    <div class="card-header @if (auth()->user()->gender == 1) card-danger @else card-info @endif p-1" style="color: #fff"><strong>{{ auth()->user()->username }}</strong> <img src="{{ asset("images/game/".auth()->user()->gender.".png") }}" alt="{{ (auth()->user()->gender == 1) ? 'Male' : 'Female' }}" class="pb-1"></div>
    <div class="card-block p-1">
        <div class="row" style="border-bottom: 1px solid #ccc">
            <div class="col-sm-12 mb-1">
                <span class="float-left mr-1">
                    <img class="img-fluid item-list" src="{{ asset("images/trainers/thumbnail/".auth()->user()->avatar.".png") }}" style="border: 1px ridge #ccc" alt="{{ auth()->user()->username }}" />
                </span>

                <div>

                    <span class="pull-right">
                        <img src="{{ asset('images/game/gold.png') }}" alt="Coins" width="12" height="12"> {{ number_format(auth()->user()->gold) }}
                    </span>
                </div>
                <div class="mt-1">
                    <i class="fa fa-envelope ml-1" aria-hidden="true"></i>
                    <a href="{{ route('mail.index') }}">
                        <small>Mail ({{ $mail }})</small>
                    </a>

                    <i class="fa fa-bell ml-1" aria-hidden="true"></i>
                    <a href="{{ route('notifs') }}">
                        <small>Notifs ({{ $notifs }})</small>
                    </a>

                    <i class="fa fa-retweet ml-1" aria-hidden="true"></i>
                    <a href="{{ route('trade-offers') }}">
                        <small>Trades ({{ $trades }})</small>
                    </a>
                </div>
            </div>
        </div>
        <div class="row text-center mt-1">
            <div class="col-3">
                <a href="{{ route('team') }}">
                    <div class="mb-1">
                        <img src="{{ asset('/images/game/team.png') }}" alt="Team">
                    </div>
                    <div>
                        Team
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="{{ route('trainer.show') }}">
                    <div class="mb-1">
                        <img src="{{ asset("/images/game/profile_".auth()->user()->gender.".png") }}" alt="Profile">
                    </div>
                    <div>
                        Profile
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="{{ route('bag') }}">
                    <div class="mb-1">
                        <img src="{{ asset('/images/game/bag.png') }}" alt="Bag">
                    </div>
                    <div>
                        Bag
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="{{ route('pokemon.index') }}">
                    <div class="mb-1">
                        <img src="{{ asset('/images/game/pokemon.png') }}" alt="Box">
                    </div>
                    <div>
                        Box
                    </div>
                </a>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-3 mt-1">
                @if (auth()->user()->club_id)
                    <a href="{{ route('club.index') }}">
                        <div class="mb-1">
                            <img src="{{ asset('/images/game/club.png') }}" alt="Box">
                        </div>
                        <div>
                            Club
                        </div>
                    </a>
                @else
                    <a href="{{ route('location.trainers-club') }}">
                        <div class="mb-1">
                            <img src="{{ asset('/images/game/club.png') }}" alt="Box">
                        </div>
                        <div>
                            Club
                        </div>
                    </a>
                @endif

            </div>
            <div class="col-3 mt-1">
                <a href="{{ route('rewards') }}">
                    <div class="mb-1">
                        <img src="{{ asset('/images/game/rewards.png') }}" alt="Rewards">
                    </div>
                    <div>
                        Rewards
                    </div>
                </a>
            </div>
            <div class="col-3 mt-1">
                <a @if (auth()->user()->shop) href="{{ route('location.grand-bazaar.shop', auth()->user()->shop->id) }}" @else href="{{ route('location.grand-bazaar.create') }}" @endif>
                    <div class="mb-1">
                        <img src="{{ asset('/images/game/store.png') }}" alt="Store">
                    </div>
                    <div>
                        Store
                    </div>
                </a>
            </div>
            <div class="col-3 mt-1">
                <a href="{{ route('pokemon.pokedex') }}">
                    <div class="mb-1">
                        <img src="{{ asset('/images/game/pokedex.png') }}" alt="Pokedex">
                    </div>
                    <div>
                        Pokedex
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
