@extends('layouts.app')

@section('title', 'Trainer Notifications')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Notifications ({{ $notifs->total() }})</strong></div>
                    <div class="card-block p-1">
                        @forelse ($notifs as $notif)
                            <div class="row mt-1">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px ridge #ddd">
                                    <span class="float-right small">{{ $notif->created_at->diffForHumans() }}</span>
                                    <span class="mr-1 mb-1">
                                        <i class="fa fa-bell-o"></i> &rsaquo; {{ $notif->message }}
                                    </span>
                                </div>
                            </div>
                        @empty
                            <span>You have no notification(s) at the moment.</span>
                        @endforelse

                        <div class="row">
                            <div class="col-sm-12">
                                {{ $notifs->links('vendor.pagination.bootstrap-4') }}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Notifications</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection