@extends('layouts.app')

@section('title', 'Online Users')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Online Users ({{ $user->allOnline()->count() }})</strong></div>
                    <div class="card-block p-1">
                        <form action="{{ route('trainer.search') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <input type="text" class="form-control form-control-sm" name="keyword" maxlength="30" placeholder="Search User" required>
                            <input type="submit" class="btn btn-sm btn-success mt-1" value="Search"> &middot;
                            <a href="{{ route('online') }}" class="btn btn-sm btn-secondary mt-1">Reset</a>
                        </form>
                        <hr class="my-1">
                        @if ($results)
                            @forelse ($results as $trainer)
                                <div class="row">
                                    <div class="col-md-12 pb-1" style="border-bottom: 1px ridge #ddd">
                                        <span class="float-right small">{{ ($trainer->getCachedAt()) ? 'online' : $trainer->updated_at->diffForHumans() }}</span>
                                        &rsaquo; <a href="{{ route('trainer.show', $trainer->username) }}" @if ($trainer->status == 2) style="color: gold; text-shadow: 0 0 4px #fffb00" @endif>
                                            {{ $trainer->username }}
                                        </a>
                                    </div>
                                </div>
                            @empty
                                <span>No result(s) found.</span>
                            @endforelse
                        @else
                            @forelse ($user->allOnline() as $trainer)
                                <div class="row">
                                    <div class="col-md-12 pb-1" style="border-bottom: 1px ridge #ddd">
                                        &rsaquo; <a href="{{ route('trainer.show', $trainer->username) }}" @if ($trainer->status == 2) style="color: gold; text-shadow: 0 0 4px #fffb00" @endif>
                                            {{ $trainer->username }}
                                        </a>
                                    </div>
                                </div>
                            @empty
                                <span>No online user(s).</span>
                            @endforelse
                        @endif
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">Online Users</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection