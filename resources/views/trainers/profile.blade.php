@extends('layouts.app')

@section('title', 'Trainer Profile')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Trainer Card #{{ $trainer->id }}</strong>
                        @if (auth()->user()->id == 1)
                            @if ($trainer->status == 4)
                                <a href="{{ route('trainer.unban', $trainer->id) }}" class="btn btn-sm mr-1 btn-warning">Unban</a>
                            @else
                                <a href="{{ route('trainer.ban', $trainer->id) }}" class="btn btn-sm mr-1 btn-danger">Ban User</a>
                            @endif
                                <a href="{{ route('activate-user', $trainer->username) }}" class="btn btn-sm mr-1 btn-info">Activate</a>
                            <span class="float-right small">{{ $trainer->email }}</span>
                        @endif
                    </div>
                    <div class="card-block p-1">
                        <div class="row">
                            <div class="col-sm-12 text-center pb-1" style="border-bottom: 1px ridge #ddd">
                                <a href="{{ route('mail.create', $trainer->username) }}" class="btn btn-sm btn-success">Send Mail</a> @if ($trainer->shop) - <a href="{{ route('location.grand-bazaar.shop', $trainer->shop->id) }}" class="btn btn-sm btn-info">View Store</a> @endif - <a href="{{ route('pokemon.index', $trainer->id) }}" class="btn btn-sm btn-primary">View Box</a> - <a href="{{ route('friendly.challenge', $trainer->id) }}" class="btn btn-sm btn-danger">Battle</a>
                            </div>
                            <div class="col-sm-12 text-center">
                                <div class="my-1">
                                     <span style="font-size: 150%; font-weight: 500" class="text-info">
                                         <img src="{{ asset('images/game/like.png') }}" width="16" alt=""> {{ number_format($likes) }}
                                     </span>
                                </div>
                                <img class="img-fluid mb-2" src="{{ asset("images/trainers/". $trainer->avatar .".png") }}" alt="{{ $trainer->username }}">
                            </div>
                            <div class="col-sm-12 text-center">
                                <p>
                                    {{ $trainer->introduction }}
                                </p>
                                @if ($trainer->id == auth()->user()->id)
                                    <form action="{{ route('trainer.intro') }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                        <div class="form-group mb-1">
                                            <textarea name="message" class="form-control" maxlength="150" cols="30" rows="5" placeholder="Short description/introduction"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn btn-info mb-1">Save</button>
                                    </form>
                                @else
                                    <div class="float-left mb-1">
                                        <a href="{{ route('trainer.like', $trainer->id) }}"><img class="img-thumbnail" src="{{ asset('images/game/like.png') }}" alt="Like"></a>
                                    </div>

                                    <div class="float-right mb-1">
                                        <form action="{{ route('trainer.report') }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                            <input type="hidden" name="id" value="{{ $trainer->id }}">

                                            <input type="submit" class="btn btn-sm btn-outline-danger mb-1" value="Report Abuse">

                                        </form>
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-sm table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <strong>Username</strong><span class="float-right {{ $trainer->setting_bgcolor }}">{{ $trainer->username }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Gender</strong><span class="float-right">{{ $trainer->gender == 1 ? 'Male' : 'Female' }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Status</strong>
                                                <span class="float-right">
                                                    @if ($trainer->status == 2)
                                                        <span style="color: gold; text-shadow: 0 0 4px #fffb00">Administrator</span>
                                                    @elseif ($trainer->status == 3)
                                                        <span style="color: red; text-shadow: 0 0 4px #ff7815">Champion</span>
                                                    @elseif ($trainer->status == 4)
                                                        <span class="text-danger">Banned</span>
                                                    @else
                                                        Member
                                                    @endif
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Pokedex</strong><span class="float-right">{{ count(explode(',', $trainer->pokedex)) }}/971</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Dungeons Completed</strong><span class="float-right">{{ number_format($trainer->dungeons_completed) }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Missions Completed</strong><span class="float-right">{{ number_format($trainer->missions_completed) }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Battle Stats</strong><span class="float-right">
                                                    {{ $trainer->battle_win }}W/{{ $trainer->battle_lose }}L <small>({{ ($trainer->battle_win) ? round(100 * $trainer->battle_win / ($trainer->battle_win + $trainer->battle_lose)) : 0 }}% Win rate)</small>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Arena Rank</strong><span class="float-right">
                                                    @if ($trainer->arena)
                                                    {{ $trainer->arena->rank }}
                                                    @else
                                                    n/a
                                                    @endif
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Joined</strong><span class="float-right">{{ $trainer->created_at }}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Club</strong>
                                                <span class="float-right">
                                                    @if ($trainer->club_id)
                                                        <img class="img-fluid mr-1" src="{{ asset("images/badges/{$trainer->club->badge}.png") }}" alt="Badge" width="16">
                                                        <a href="{{ route('club.info', $trainer->club_id) }}">{{ $trainer->club->name }}</a>
                                                        @if ($trainer->club_role == 3)
                                                            <span class="text-danger">(Leader)</span>
                                                        @elseif ($trainer->club_role == 2)
                                                            <span class="text-success">(Executive)</span>
                                                        @else
                                                            <span>(member)</span>
                                                        @endif
                                                    @else
                                                        <small><em>(none)</em></small>
                                                    @endif
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-12 text-center">
                                <hr class="mt-1 mb-2">
                                <div class="mb-1">
                                    <strong>Pokemon Team</strong>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        @foreach ($team as $pokemon)
                                            <a href="{{ route('pokemon.show', $pokemon->id) }}" class="col-2 img-fluid" style="border: 2px ridge #bbb; background: url('/images/pokemon/{{ str_slug($pokemon->info->name) }}.png') no-repeat center/cover #d0d8d3; height: 70px; max-width: 70px"></a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('pokemon.index') }}">Pokemon</a>
                            <a class="breadcrumb-item" href="{{ route('team') }}">Team</a>
                            <span class="breadcrumb-item active">Profile</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection