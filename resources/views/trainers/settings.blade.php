@extends('layouts.app')

@section('title', 'Settings')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Settings</strong> <span class="float-right"><a href="{{ route('trainer.avatars') }}" class="btn btn-sm btn-outline-primary">Change Avatar</a></span></div>
                    <div class="card-block">
                        <div class="mb-2">
                            <strong>Change Password</strong>
                        </div>
                        <form action="{{ route('trainer.changePassword') }}" method="POST">
                            {{ csrf_field() }}

                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">

                            <div class="form-group @if ($errors->has('password')) has-danger @endif">
                                <label class="form-control-label mb-0" for="password">New Password</label>
                                <input type="password" class="form-control form-control-danger" id="password" name="password" required>

                                @if ($errors->has('password'))
                                    <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('password_confirmation')) has-danger @endif">
                                <label class="form-control-label mb-0" for="password_confirmation">Repeat New Password</label>
                                <input type="password" class="form-control form-control-danger" id="password_confirmation" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <div class="form-control-feedback">{{ $errors->first('password_confirmation') }}</div>
                                @endif
                            </div>

                            <hr>

                            <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                <label class="form-control-label mb-0" for="captcha">Enter CAPTCHA</label>
                                <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                <input type="text" class="form-control form-control-danger" id="captcha" name="captcha" autocomplete="off" required>

                                @if ($errors->has('captcha'))
                                    <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                @endif

                                <small class="form-text text-muted">Captcha code: <strong>{{ $captcha }}</strong></small>
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Update</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <span class="breadcrumb-item active">Settings</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
