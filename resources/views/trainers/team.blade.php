@extends('layouts.app')

@section('title', 'Team')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Team ({{ $team->count() }}/6)</strong></div>
                    <div class="card-block p-1">
                        @forelse ($team as $pokemon)
                            <div class="row">
                                <div class="col-md-12 mb-1" style="border-bottom: 1px groove #ccc">
                                    @if (session()->has('battle.enemy') || session()->has('paragon.enemy'))
                                        @if (session()->has('paragon') && session("paragon.pokemon.stats")->id == $pokemon->id)
                                            <span class="float-right">
                                                <a href="#" class="btn btn-sm btn-primary disabled">Switch</a>
                                            </span>
                                        @else
                                            <span class="float-right">
                                                @if (session()->has('paragon'))
                                                <a href="{{ route('paragon.recall', $pokemon->id) }}" class="btn btn-sm btn-primary">Switch</a>
                                                @else
                                                    <a href="{{ route('battle.recall', $pokemon->id) }}" class="btn btn-sm btn-primary">Switch</a>
                                                @endif
                                            </span>
                                        @endif
                                    @elseif (session()->has('dungeon') || session()->has('paragon'))
                                        @if ((is_numeric(session("battle.pokemon.current_hp_{$pokemon->id}")) && session("battle.pokemon.current_hp_{$pokemon->id}") <= 0) || (session()->has("paragon.pokemon.current_hp_{$pokemon->id}") && session("paragon.pokemon.current_hp_{$pokemon->id}") <= 0))
                                            <span class="float-right">
                                                <a href="#" class="btn btn-sm btn-primary disabled">Top</a>
                                            </span>
                                        @else
                                            <span class="float-right ml-1">
                                                <a href="{{ route('pokemon.move', $pokemon->id) }}" class="btn btn-sm btn-primary">Top</a>
                                            </span>
                                            @if (session('use'))
                                            <span class="float-right ml-1">
                                                <form action="{{ route('bag.useOn') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                    <input type="hidden" name="pokemon" value="{{ $pokemon->id }}">

                                                    <input type="submit" class="btn btn-sm btn-success" value="Use">
                                                </form>
                                                {{--<a href="{{ route('bag.useOn', $pokemon->id) }}" class="btn btn-sm btn-success">Use</a>--}}
                                            </span>
                                            <span class="float-right">
                                                <form action="{{ route('bag.equipOn') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                    <input type="hidden" name="pokemon" value="{{ $pokemon->id }}">

                                                    <input type="submit" class="btn btn-sm btn-info" value="Hold">
                                                </form>
                                                {{--<a href="{{ route('bag.equipOn', $pokemon->id) }}" class="btn btn-sm btn-info">Hold</a>--}}
                                            </span>
                                            @endif
                                        @endif
                                    @elseif (session('use'))
                                        <span class="float-right ml-1">
                                            <form action="{{ route('bag.useOn') }}" method="POST">
                                                    {{ csrf_field() }}
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                    <input type="hidden" name="pokemon" value="{{ $pokemon->id }}">

                                                    <input type="submit" class="btn btn-sm btn-success" value="Use">
                                            </form>
                                            {{--<a href="{{ route('bag.useOn', $pokemon->id) }}" class="btn btn-sm btn-success">Use</a>--}}
                                        </span>
                                        <span class="float-right">
                                            <form action="{{ route('bag.equipOn') }}" method="POST">
                                                    {{ csrf_field() }}
                                                <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                                                    <input type="hidden" name="pokemon" value="{{ $pokemon->id }}">

                                                    <input type="submit" class="btn btn-sm btn-info" value="Hold">
                                            </form>
                                            {{--<a href="{{ route('bag.equipOn', $pokemon->id) }}" class="btn btn-sm btn-info">Hold</a>--}}
                                        </span>
                                    @elseif (session('learn'))
                                        <span class="float-right ml-1">
                                            <a href="{{ route('location.poke-tech.learn-by', $pokemon->id) }}" class="btn btn-sm btn-success">Learn</a>
                                        </span>
                                    @else
                                        <span class="float-right ml-1">
                                            <a href="{{ route('pokemon.remove', $pokemon->id) }}" class="btn btn-sm btn-danger">Remove</a>
                                        </span>
                                        <span class="float-right">
                                            <a href="{{ route('pokemon.move', $pokemon->id) }}" class="btn btn-sm btn-primary">Top</a>
                                        </span>
                                    @endif
                                    <a href="{{ route('pokemon.show', $pokemon->id) }}">
                                    <span class="mr-1 float-left mb-1">
                                        <img class="img-fluid item-list" src="{{ asset("images/pokemon/thumbnail/".str_slug($pokemon->info->name).".png") }}" alt="{{ $pokemon->info->name }}">
                                    </span>
                                    {{ $pokemon->info->name }} (Lv {{ $pokemon->level }})</a> <img class="img-fluid" src="{{ asset("images/game/". $pokemon->gender .".png") }}" alt="Gender" />
                                    @if ($pokemon->item)
                                        <img class="img-fluid" src="{{ asset("images/game/held.png") }}" alt="Held Item" />
                                    @endif
                                    <br>
                                    <div class="mb-1">
                                        <img class="img-fluid" src="{{ asset("images/game/".$pokemon->info->primary_type.".png") }}" alt="{{ $pokemon->info->primary_type }}">
                                        @if (isset($pokemon->info->secondary_type))
                                            <img class="img-fluid" src="{{ asset("images/game/".$pokemon->info->secondary_type.".png") }}" alt="{{ $pokemon->info->secondary_type }}">
                                        @endif
                                    </div>
                                        <span class="float-left badge badge-pill text-warning" style="line-height: 6px; background-color: #444; border-radius: 3px 0 0 3px"><strong>HP</strong></span>
                                        <div class="progress w-50" style="border: 1px ridge #000; background-color: #999; border-radius: 0 3px 3px 0">
                                            @if (session()->has('paragon'))
                                            <div class="progress-bar bg-success" role="progressbar" style="width: @if (is_numeric(session("paragon.pokemon.current_hp_{$pokemon->id}"))) {{ round(session("paragon.pokemon.current_hp_{$pokemon->id}")/$pokemon->hp * 100) }}% @else 100% @endif;line-height: 9px; height: 9px; color: #000">

                                                <small>@if (is_numeric(session("paragon.pokemon.current_hp_{$pokemon->id}"))) {{ session("paragon.pokemon.current_hp_{$pokemon->id}") }} @else {{ $pokemon->hp }} @endif / {{ $pokemon->hp }}</small>
                                            </div>
                                            @else
                                                <div class="progress-bar bg-success" role="progressbar" style="width: @if (is_numeric(session("battle.pokemon.current_hp_{$pokemon->id}"))) {{ round(session("battle.pokemon.current_hp_{$pokemon->id}")/$pokemon->hp * 100) }}% @else 100% @endif;line-height: 9px; height: 9px; color: #000">

                                                    <small>@if (is_numeric(session("battle.pokemon.current_hp_{$pokemon->id}"))) {{ session("battle.pokemon.current_hp_{$pokemon->id}") }} @else {{ $pokemon->hp }} @endif / {{ $pokemon->hp }}</small>
                                                </div>
                                            @endif
                                        </div>
                                </div>
                            </div>
                        @empty
                            <p>You have no pokemon in team.</p>
                        @endforelse
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('pokemon.index') }}">Pokemon</a>
                            <span class="breadcrumb-item active">Team</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection