@extends('layouts.app')

@section('title', 'Trainer Trade Offers')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-info bg-faded">
                    <div class="card-header p-1"><strong>Trade Offers</strong></div>
                    <div class="card-block p-1">
                        @forelse (auth()->user()->trades as $trade)
                            <div class="row">
                                <div class="col-md-12 py-1 mb-1" style="border-bottom: 1px dotted #a9a9a9">
                                    &rsaquo; <a href="{{ route('shop.trade', $trade->content_id) }}">{{ $trade->content->shop->name }}</a>
                                </div>
                            </div>
                        @empty
                            <span>You have no trade offers at the moment.</span>
                        @endforelse
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/home') }}">Home</a>
                            <span class="breadcrumb-item active">Trade Offers</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection