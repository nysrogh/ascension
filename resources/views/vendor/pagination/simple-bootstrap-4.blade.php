@if ($paginator->hasPages())
    <ul class="pagination small">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled"><span class="page-link p-1">@lang('pagination.previous')</span></li>
        @else
            <li class="page-item"><a class="page-link p-1" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item"><a class="page-link p-1" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a></li>
        @else
            <li class="page-item disabled"><span class="page-link p-1">@lang('pagination.next')</span></li>
        @endif
    </ul>
@endif
