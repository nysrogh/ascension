@extends('layouts.app')

@section('title', 'Search War')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card card-outline-warning bg-faded">
                    <div class="card-header p-1"><strong>Search Club War</strong></div>
                    <div class="alert alert-info mb-1">
                        &rsaquo; Winning in war earns your club 100 points, 100,000 treasury coins and 50,000 for each members participated (25% earned by losing club)<br>
                    </div>
                    <div class="card-block">
                        <form action="{{ route('club.store') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="prevent" value="{{ random_int(100,999) }}">
                            <div class="form-group @if ($errors->has('username')) has-danger @endif">
                                <label class="form-control-label mb-0" for="name">Name</label>
                                <input type="text" class="form-control form-control-danger" id="name" name="name" value="{{ old('name') }}" maxlength="20" required>

                                @if ($errors->has('name'))
                                    <div class="form-control-feedback">{{ $errors->first('name') }}</div>
                                @endif
                            </div>

                            <div class="form-group @if ($errors->has('description')) has-danger @endif">
                                <label class="form-control-label mb-0" for="description">Description</label>
                                <textarea class="form-control form-control-danger" id="description" maxlength="80" name="description" required></textarea>

                                @if ($errors->has('description'))
                                    <div class="form-control-feedback">{{ $errors->first('description') }}</div>
                                @endif

                                <small class="form-text text-muted">General description of the club.</small>
                            </div>

                            <hr class="my-1">

                            <div class="form-group @if ($errors->has('captcha')) has-danger @endif">
                                <label for="captcha">Enter the code: <em>{{ $captcha }}</em></label>
                                <input type="hidden" name="captcha_confirmation" value="{{ $captcha }}"/>
                                <input type="text" name="captcha" class="form-control" autocomplete="off" minlength="3" maxlength="3" placeholder="(enter the 3 digit code above)" />

                                @if ($errors->has('captcha'))
                                    <div class="form-control-feedback">{{ $errors->first('captcha') }}</div>
                                @endif
                            </div>

                            <button type="submit" class="form-control btn btn-success my-1">Create</button>
                        </form>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ url('/') }}">Home</a>
                            <a class="breadcrumb-item" href="{{ route('location.trainers-club') }}">Trainers Club</a>
                            <span class="breadcrumb-item active">Create a Club</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
