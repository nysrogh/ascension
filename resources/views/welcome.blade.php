@extends('layouts.app')

@section('title', 'A New and Unique Pokemon Text-Based RPG for Mobile')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.flash-messages')
                <div class="card">
                    <div class="card-header p-1"><strong>Introduction</strong></div>
                    <div class="card-block text-justify">
                        <div class="text-center">
                            <img class="img-fluid" src="{{ asset('images/npc/prof-oak.png') }}" alt="Professor Oak">
                        </div>

                        <div class="mb-2">
                            Hello, <strong>{{ auth()->user()->username }}</strong>! Glad to meet you! Welcome to the world of Pokémon! My name is Oak.
                        </div>

                        <div class="mb-1">
                            This world is inhabited far and wide by creatures called Pokémon! For some people, Pokémon are pets. Other use them for battling. Right beside you is your partner, <strong>{{ $starter->info->name }}</strong>.
                        </div>

                        <div class="text-center my-2">
                            <div class="row">
                                <div class="col-md-6 col-6 m-0">
                                    <img class="img-fluid" src="{{ asset("images/trainers/". auth()->user()->avatar .".png") }}" alt="{{ auth()->user()->username }}">
                                </div>
                                <div class="col-md-6 col-6 mt-3">
                                    <img class="img-fluid" src="{{ asset("images/pokemon/front/".str_slug($starter->info->name).".png") }}" alt="{{ $starter->info->name }}">
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            Go and end Team Rocket's evil plan as your own very Pokémon legend is about to unfold! A world of dreams and adventures with Pokémon awaits!
                        </div>

                        <div class="text-center">
                            <form action="{{ route('home.store') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="prevent" value="{{ random_int(100, 999) }}">

                                <div class="form-group @if ($errors->all()) has-danger @endif">
                                    <input type="text" class="form-control" name="code" autocomplete="off" required>
                                    <small class="form-text text-muted">Please message our official facebook page together with your <strong>username and email</strong> to get your activation code.</small>
                                    <small class="form-text text-muted mt-1"><a href="https://www.facebook.com/PokemonAscensionRPG/">Pokémon Ascension RPG</a></small>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="form-control btn btn-success">Start Adventure</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-footer p-0">
                        <nav class="breadcrumb text-center py-1 my-0 px-1">
                            <a class="breadcrumb-item" href="{{ route('home') }}">Home</a>
                            <span class="breadcrumb-item active">Welcome</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection