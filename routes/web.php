<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GameController@index')->name('index');
Route::get('/online', 'GameController@online')->name('online');

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('buy-coins', 'HomeController@buyCoins')->name('buy');

/**
 * Home
 */
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/welcome', 'HomeController@welcome')->name('home.welcome');
Route::post('/home', 'HomeController@store')->name('home.store');
Route::get('/legal', 'GameController@legal')->name('legal');
Route::get('/notifs', 'NotifController@index')->name('notifs');
Route::get('/clear', 'UserController@clear')->name('clear');
Route::get('/trade-offers', 'HomeController@tradeOffers')->name('trade-offers');
Route::get('/arena-reset', 'UserController@arenaReset')->name('arena-reset');
Route::get('/activate/{username}', 'UserController@getActivation')->name('activate-user');

/**
 * Forums
 */
Route::get('/forums', 'ForumController@index')->name('forum');
Route::get('/forum/create', 'ForumController@create')->name('forum.create');
Route::post('/forums', 'ForumController@store')->name('forum.store');
Route::get('/forum/{slug}/sort/{order?}', 'ForumController@show')->name('forum.show');
Route::post('/forum-comment', 'ForumController@comment')->name('forum.comment');
Route::get('/forums/category/{cat}', 'ForumController@category')->name('forum.category')->where(['cat' => '[0-9]+']);
Route::get('/forum/delete/{id}', 'ForumController@delete')->name('forum.delete')->where(['id' => '[0-9]+']);
Route::get('/forum/delete-comment/{id}', 'ForumController@deleteComment')->name('forum.delete-comment')->where(['id' => '[0-9]+']);

/**
 * Pokemon
 */
Route::get('/pokemon/{id}', 'PokemonController@show')->name('pokemon.show')->where(['id' => '[0-9]+']);
Route::get('/pokemon/remove/{id}', 'PokemonController@remove')->name('pokemon.remove')->where(['id' => '[0-9]+']);
Route::get('/pokemon/add/{id}', 'PokemonController@add')->name('pokemon.add')->where(['id' => '[0-9]+']);
Route::get('/pokemon/release/{id}', 'PokemonController@release')->name('pokemon.release')->where(['id' => '[0-9]+']);
Route::get('/pokemon/evolve/{id}/{method}', 'PokemonController@evolve')->name('pokemon.evolve')->where(['id' => '[0-9]+']);
Route::get('/pokemon/forget/{id}/{move}', 'PokemonController@forget')->name('pokemon.forget')
    ->where(['id' => '[0-9]+', 'move' => '[0-9]+']);
Route::get('/update', 'PokemonController@update')->name('pokemon.update');
Route::get('/pokemon/move/{id}', 'PokemonController@move')->name('pokemon.move')->where(['id' => '[0-9]+']);
Route::post('/pokemon/search', 'PokemonController@search')->name('pokemon.search');
Route::get('/pokedex', 'PokemonController@pokedex')->name('pokemon.pokedex');
Route::get('/pokedex/{id}', 'PokemonController@pokedexShow')->name('pokemon.pokedex-show');
Route::post('/pokedex/search', 'PokemonController@pokedexSearch')->name('pokemon.pokedex-search');
Route::get('/pokemon/form/{id}/{form}', 'PokemonController@form')->name('pokemon.form')->where(['id' => '[0-9]+']);

/**
 * Trainer
 */
Route::get('/trainer/team', 'PokemonController@team')->name('team');
Route::get('/trainer/box/{username?}', 'PokemonController@index')->name('pokemon.index');
Route::get('/trainer/profile/{username?}', 'UserController@show')->name('trainer.show');
Route::get('/trainer/settings', 'UserController@settings')->name('trainer.settings');
Route::get('/trainer/avatars', 'UserController@avatars')->name('trainer.avatars');
Route::post('/trainer/avatars', 'UserController@avatarChange')->name('trainer.avatarChange');
Route::post('/trainer/password', 'UserController@changePassword')->name('trainer.changePassword');
Route::post('/trainer/search', 'UserController@search')->name('trainer.search');
Route::get('/trainer/ban/{id}', 'UserController@banUser')->name('trainer.ban')->where(['id' => '[0-9]+']);
Route::get('/trainer/unban/{id}', 'UserController@unbanUser')->name('trainer.unban')->where(['id' => '[0-9]+']);
Route::get('/trainer/like/{id}', 'UserController@like')->name('trainer.like')->where(['id' => '[0-9]+']);
//Route::get('/trainer/dislike/{id}', 'UserController@dislike')->name('trainer.dislike')->where(['id' => '[0-9]+']);
Route::post('/trainer/report', 'UserController@report')->name('trainer.report');
Route::post('/trainer/update-intro', 'UserController@updateIntro')->name('trainer.intro');

/**
 * Mail
 */
Route::resource('mail', 'MailController');
Route::get('/mail/create/{username?}', 'MailController@create')->name('mail.create');

/**
 * Battle
 */
Route::get('/battle', 'BattleController@index')->name('battle');
Route::post('/battle/fight', 'BattleController@fight')->name('battle.fight');
Route::get('/battle/end', 'BattleController@end')->name('battle.end');
Route::post('/battle/catch', 'BattleController@capture')->name('battle.catch');
Route::get('/battle/recall/{id}', 'BattleController@recall')->name('battle.recall')->where(['id' => '[0-9]+']);

/**
 * Rewards
 */
Route::get('/rewards', 'RewardController@index')->name('rewards');
Route::post('/rewards', 'RewardController@claim')->name('reward.claim');

/**
 * Dungeon
 */
Route::get('/map', 'DungeonController@map')->name('dungeon.maps');
Route::get('/dungeon', 'DungeonController@index')->name('dungeon');
Route::get('/dungeon/{id}', 'DungeonController@show')->name('dungeon.show')->where(['id' => '[0-9]+']);
Route::post('/dungeon', 'DungeonController@store')->name('dungeon.store');
Route::post('/dungeon/explore', 'DungeonController@explore')->name('dungeon.explore');
Route::get('/dungeon/leave', 'DungeonController@leave')->name('dungeon.leave');
Route::get('/dungeon/battle', 'DungeonController@battle')->name('dungeon.battle');
Route::post('/dungeon/chest', 'DungeonController@chest')->name('dungeon.chest');
Route::get('/dungeon/mirage/{id}', 'DungeonController@mirage')->name('dungeon.mirage')->where(['id' => '[0-9]+']);
Route::post('/dungeon/buy', 'DungeonController@buy')->name('dungeon.buy');

/**
 * Bag
 */
Route::get('/bag/{category?}', 'ItemController@index')->name('bag')->where(['category' => '[0-9]+']);
Route::get('/bag/view/{id}', 'ItemController@show')->name('bag.show')->where(['id' => '[0-9]+']);
Route::get('/bag/use/{id}', 'ItemController@useItem')->name('bag.useItem')->where(['id' => '[0-9]+']);
Route::post('/bag/use-on', 'ItemController@useOn')->name('bag.useOn');
Route::post('/bag/equip-on', 'ItemController@equipOn')->name('bag.equipOn');
Route::post('/bag/unequip', 'ItemController@unequip')->name('bag.unequip');
Route::post('/bag/enhance', 'ItemController@enhance')->name('bag.enhance');

/**
 * Location - Pokemon Tech
 */
Route::get('/pokemon-tech', 'HomeController@pokeTech')->name('location.poke-tech');
Route::get('/pokemon-tech/{type}', 'HomeController@pokeTechMoves')->name('location.poke-tech.moves')->where(['type' => '[a-z]+']);
Route::get('/pokemon-tech/learn/{id}', 'PokemonController@learn')->name('location.poke-tech.learn')->where(['id' => '[0-9]+']);
Route::get('/pokemon-tech/learn-by/{id}', 'PokemonController@learnBy')->name('location.poke-tech.learn-by')->where(['id' => '[0-9]+']);

/**
 * Location - Pokemon Center
 */
Route::get('/pokemon-center', 'HomeController@pokeCenter')->name('location.poke-center');
Route::post('/pokemon-center/chat', 'HomeController@pokeCenterChat')->name('location.poke-center.chat');
Route::get('/delete/chat/{id}', 'HomeController@delete')->name('chat.delete')->where(['id' => '[0-9]+']);

/**
 * Location - Arena
 */
Route::get('/arena', 'ArenaController@index')->name('arena');
Route::get('/arena/battle', 'ArenaController@battle')->name('arena.battle');
Route::get('/arena/join', 'ArenaController@join')->name('arena.join');
Route::get('/arena/challenge/{id}', 'ArenaController@challenge')->name('arena.challenge')->where(['id' => '[0-9]+']);
Route::post('/arena/fight', 'ArenaController@fight')->name('arena.fight');
Route::get('/arena/battle/end', 'ArenaController@end')->name('arena.end');
Route::get('/arena/rankings', 'ArenaController@rankings')->name('arena.rankings');
Route::get('/arena/shop', 'ArenaController@shop')->name('arena.shop');
Route::post('/arena/shop/buy', 'ArenaController@shopBuy')->name('arena.shop.buy');

/**
 * Location - Poke Mart
 */
Route::get('/poke-mart', 'HomeController@pokeMart')->name('location.poke-mart');
Route::post('/poke-mart/buy', 'HomeController@pokeMartBuy')->name('location.poke-mart.buy');

/**
 * Location - Grand Bazaar
 */
Route::get('/grand-bazaar', 'ShopController@index')->name('location.grand-bazaar');
Route::get('/grand-bazaar/{id}', 'ShopController@show')->name('location.grand-bazaar.shop')->where(['id' => '[0-9]+']);
Route::get('/grand-bazaar/store/setup', 'ShopController@create')->name('location.grand-bazaar.create');
Route::get('/grand-bazaar/store/edit', 'ShopController@edit')->name('location.grand-bazaar.edit');
Route::post('/grand-bazaar/store/save', 'ShopController@save')->name('location.grand-bazaar.save');
Route::post('/grand-bazaar', 'ShopController@store')->name('location.grand-bazaar');
Route::post('/grand-bazaar/search', 'ShopController@search')->name('location.grand-bazaar.search');
Route::get('/shop/sell-item/{id}', 'ShopController@sellItem')->name('shop.sellItem')->where(['id' => '[0-9]+']);
Route::get('/shop/sell-pokemon/{id}', 'ShopController@sellPokemon')->name('shop.sellPokemon')->where(['id' => '[0-9]+']);
Route::post('/shop/sell/store', 'ShopController@sellStore')->name('shop.sellStore');
Route::post('/shop/remove', 'ShopController@remove')->name('shop.remove');
Route::post('/shop/buy', 'ShopController@buy')->name('shop.buy');
Route::get('/shop/trade/create', 'ShopController@tradeCreate')->name('shop.trade.create');
Route::post('/shop/trade/create-store', 'ShopController@tradeStore')->name('shop.trade.store');
Route::get('/shop/trade/offer/{id}', 'ShopController@tradeOfferView')->name('shop.trade.offer-view')->where(['id' => '[0-9]+']);
Route::get('/shop/trade/remove/{key}', 'ShopController@tradeRemove')->name('shop.trade.remove')->where(['key' => '[0-9]+']);
Route::get('/shop/trade/{id}', 'ShopController@tradeView')->name('shop.trade')->where(['id' => '[0-9]+']);
Route::post('/shop/trade/offer', 'ShopController@tradeOffer')->name('shop.trade.offer');
Route::get('/shop/trade/cancel/{id}', 'ShopController@tradeCancel')->name('shop.trade.cancel')->where(['id' => '[0-9]+']);
Route::post('/shop/trade/accept', 'ShopController@tradeAccept')->name('shop.trade.accept');
Route::get('/shop/trade/coins-crystal', 'ShopController@tradeCoinsCrystal')->name('shop.trade.coins-crystal');
Route::post('/shop/trade/coins-crystal', 'ShopController@tradeCoinsCrystalStore')->name('shop.trade.coins-store');

/**
 * Location - Trainers Club
 */
Route::get('/trainers-club', 'ClubController@clubs')->name('location.trainers-club');
Route::get('/club', 'ClubController@index')->name('club.index');
Route::get('/club/info/{id}', 'ClubController@show')->name('club.info')->where(['id' => '[0-9]+']);
Route::get('/club/create', 'ClubController@create')->name('club.create');
Route::post('/club', 'ClubController@store')->name('club.store');
Route::post('/club/chat', 'ClubController@chat')->name('club.chat');
Route::get('/club/leave', 'ClubController@leave')->name('club.leave');
Route::get('/club/manage', 'ClubController@manage')->name('club.manage');
Route::post('/club/edit', 'ClubController@edit')->name('club.edit');
Route::get('/club/members', 'ClubController@members')->name('club.members');
Route::post('/club/changeRole', 'ClubController@changeRole')->name('club.changeRole');
Route::get('/club/applicants', 'ClubController@applicants')->name('club.applicants');
Route::post('/club/applicants', 'ClubController@applicantsAction')->name('club.applicantsAction');
Route::post('/club/apply', 'ClubController@apply')->name('club.apply');
Route::post('/club/upgrade/members', 'ClubController@upgradeMembers')->name('club.upgradeMembers');
Route::post('/club/upgrade/perks', 'ClubController@upgradePrivilege')->name('club.upgradePrivilege');
Route::get('/club/store', 'ClubController@mart')->name('club.mart');
Route::post('/club/store', 'ClubController@martBuy')->name('club.martBuy');
Route::get('/club/addCoins', 'ClubController@addFunds')->name('club.addFunds');
Route::post('/club/storeFund', 'ClubController@storeFunds')->name('club.storeFunds');
Route::get('/club/rankings', 'ClubController@rankings')->name('club.rankings');
Route::get('/club/logs', 'ClubController@logs')->name('club.logs');
Route::post('/club/chat', 'ClubController@clubChat')->name('club.chat');

/**
 * Trainers Club - Missions
 */
Route::get('/club/missions', 'MissionController@index')->name('mission.index');
Route::post('/club/mission', 'MissionController@store')->name('mission.store');
Route::get('/club/mission/abort', 'MissionController@abort')->name('mission.abort');
Route::post('/club/mission/report', 'MissionController@report')->name('mission.report');

/**
 * Trainers Club - Privileges
 */
Route::get('/club/perks', 'PrivilegeController@index')->name('privileges.index');
Route::post('/club/perks', 'PrivilegeController@store')->name('privileges.store');

/**
 * Location - Paragon
 */
Route::get('/paragon-tower', 'ParagonController@entrance')->name('paragon.entrance');
Route::post('/paragon-tower', 'ParagonController@enter')->name('paragon.enter');
Route::get('/paragon', 'ParagonController@index')->name('paragon.index');
Route::post('/paragon/challenge', 'ParagonController@challenge')->name('paragon.challenge');
Route::get('/paragon/battle', 'ParagonController@battle')->name('paragon.battle');
Route::post('/paragon/fight', 'ParagonController@fight')->name('paragon.fight');
Route::get('/paragon/recall/{id}', 'ParagonController@recall')->name('paragon.recall')->where(['id' => '[0-9]+']);
Route::get('/paragon/end', 'ParagonController@end')->name('paragon.end');
Route::get('/paragon/leave', 'ParagonController@leave')->name('paragon.leave');
Route::post('/paragon/catch', 'ParagonController@capture')->name('paragon.catch');
Route::get('/paragon/rankings', 'ParagonController@rankings')->name('paragon.rankings');

/**
 * Trainers Club - Wars
*/
Route::get('/club/war', 'WarController@index')->name('war.index');
Route::get('/club/war/start', 'WarController@start')->name('war.start');
Route::post('/club/war', 'WarController@store')->name('war.store');

/**
 * Friendly Battle
 */
Route::get('/trainer/challenge/{id}', 'FriendlyController@challenge')->name('friendly.challenge')->where(['id' => '[0-9]+']);
Route::get('/friendly-battle', 'FriendlyController@index')->name('friendly.index');
Route::post('/friendly-battle/fight', 'FriendlyController@fight')->name('friendly.fight');
Route::get('/friendly-battle/end', 'FriendlyController@end')->name('friendly.end');

///**
// * Location Game Corner
// */
//Route::get('/game-corner', 'CasinoController@index')->name('casino.index');
//Route::get('/game-corner/slot-machine', 'CasinoController@slotMachine')->name('casino.slot-machine');

//
//Route::get('/moves/{id}', 'PokemonController@listMoves')->name('moves');
//Route::post('/move-update', 'PokemonController@updateMoves')->name('moves.update');
//Route::post('/move/search', 'PokemonController@searchMove')->name('moves.search');