INSERT INTO `move_pokemon` (`move_id`, `pokemon_id`, `level`) VALUES
(1,	41,	1),
(1,	42,	1),
(1,	43,	1),
(1,	44,	1),
(1,	46,	11),
(1,	47,	11),
(3,	23,	20),
(3,	24,	20),
(3,	43,	9),
(3,	44,	9),
(5,	23,	28),
(5,	24,	32),
(8,	21,	15),
(8,	22,	15),
(10,	35,	58),
(11,	15,	38),
(11,	16,	29),
(11,	17,	32),
(11,	18,	32),
(11,	21,	25),
(11,	22,	27),
(11,	25,	45),
(12,	41,	19),
(12,	42,	19),
(13,	6,	45),
(13,	12,	43),
(13,	16,	49),
(13,	17,	57),
(13,	18,	62),
(13,	41,	41),
(13,	42,	48),
(20,	7,	28),
(20,	8,	33),
(20,	9,	33),
(22,	45,	1),
(22,	46,	43),
(22,	47,	51),
(25,	15,	26),
(25,	19,	19),
(25,	20,	19),
(25,	21,	22),
(25,	22,	23),
(25,	52,	41),
(26,	41,	7),
(26,	42,	7),
(26,	50,	8),
(26,	51,	7),
(34,	37,	9),
(41,	23,	38),
(41,	24,	50),
(45,	7,	16),
(45,	8,	17),
(45,	9,	17),
(45,	19,	10),
(45,	20,	10),
(45,	23,	9),
(45,	24,	10),
(45,	29,	21),
(45,	30,	23),
(45,	41,	11),
(45,	42,	11),
(45,	52,	6),
(51,	31,	35),
(51,	35,	40),
(51,	39,	32),
(62,	7,	13),
(62,	8,	13),
(62,	9,	13),
(64,	10,	9),
(64,	13,	9),
(65,	12,	31),
(65,	49,	63),
(67,	50,	18),
(67,	51,	22),
(73,	12,	37),
(73,	29,	43),
(73,	30,	50),
(73,	32,	43),
(73,	33,	50),
(73,	37,	47),
(73,	52,	46),
(78,	31,	23),
(78,	34,	23),
(84,	23,	44),
(84,	24,	60),
(87,	37,	12),
(87,	38,	1),
(87,	41,	17),
(87,	42,	17),
(88,	12,	11),
(88,	48,	11),
(88,	49,	11),
(93,	35,	34),
(100,	47,	59),
(101,	19,	22),
(101,	20,	24),
(101,	24,	53),
(101,	29,	37),
(101,	30,	43),
(102,	28,	33),
(111,	27,	1),
(111,	28,	1),
(111,	35,	13),
(111,	39,	3),
(111,	40,	1),
(116,	27,	30),
(116,	28,	38),
(116,	50,	31),
(116,	51,	48),
(117,	39,	14),
(117,	40,	1),
(117,	48,	1),
(117,	49,	1),
(118,	35,	1),
(118,	36,	1),
(118,	39,	11),
(119,	25,	34),
(124,	29,	9),
(124,	30,	9),
(124,	31,	1),
(124,	32,	9),
(124,	33,	9),
(124,	34,	1),
(125,	35,	10),
(125,	36,	1),
(125,	39,	17),
(125,	40,	1),
(126,	25,	23),
(127,	1,	27),
(127,	2,	31),
(127,	3,	31),
(127,	19,	31),
(127,	20,	39),
(127,	39,	45),
(127,	40,	1),
(131,	6,	66),
(135,	4,	16),
(135,	5,	17),
(135,	6,	17),
(141,	21,	36),
(141,	22,	41),
(142,	22,	45),
(145,	31,	43),
(145,	34,	43),
(145,	51,	44),
(146,	27,	46),
(146,	28,	60),
(146,	50,	39),
(146,	51,	54),
(152,	25,	13),
(155,	4,	7),
(155,	5,	7),
(155,	6,	7),
(155,	37,	1),
(156,	35,	1),
(157,	15,	41),
(157,	19,	34),
(157,	20,	48),
(162,	37,	31),
(167,	52,	9),
(170,	16,	25),
(170,	17,	27),
(170,	18,	27),
(171,	25,	21),
(171,	52,	50),
(172,	37,	23),
(172,	52,	22),
(173,	15,	44),
(176,	37,	42),
(177,	4,	25),
(177,	5,	28),
(177,	6,	28),
(177,	24,	46),
(181,	4,	43),
(181,	5,	50),
(181,	6,	56),
(181,	37,	15),
(183,	50,	43),
(183,	51,	60),
(185,	4,	28),
(185,	5,	32),
(185,	6,	32),
(185,	37,	28),
(188,	4,	37),
(188,	5,	43),
(188,	6,	47),
(188,	37,	36),
(188,	38,	1),
(189,	6,	78),
(191,	9,	45),
(192,	29,	33),
(192,	30,	38),
(192,	32,	33),
(192,	33,	38),
(199,	15,	20),
(199,	19,	7),
(199,	20,	7),
(199,	21,	29),
(199,	22,	32),
(199,	32,	7),
(199,	33,	7),
(199,	34,	1),
(202,	48,	1),
(202,	49,	1),
(209,	15,	11),
(209,	21,	11),
(209,	22,	11),
(209,	32,	19),
(209,	33,	20),
(210,	27,	11),
(210,	28,	11),
(210,	46,	17),
(210,	47,	17),
(211,	27,	20),
(211,	28,	20),
(211,	29,	19),
(211,	30,	20),
(211,	52,	14),
(218,	43,	31),
(218,	44,	34),
(218,	46,	38),
(218,	47,	44),
(221,	23,	12),
(221,	24,	12),
(225,	43,	47),
(225,	44,	54),
(226,	35,	49),
(227,	1,	3),
(227,	2,	3),
(227,	3,	3),
(227,	4,	1),
(227,	5,	1),
(227,	6,	1),
(227,	21,	1),
(227,	22,	1),
(227,	25,	5),
(227,	29,	1),
(227,	30,	1),
(227,	35,	1),
(227,	50,	3),
(227,	51,	4),
(227,	52,	1),
(228,	1,	25),
(228,	2,	28),
(228,	3,	28),
(228,	43,	1),
(228,	44,	1),
(228,	46,	33),
(228,	47,	37),
(232,	23,	49),
(232,	24,	63),
(233,	12,	1),
(233,	16,	9),
(233,	17,	9),
(233,	18,	9),
(233,	49,	39),
(234,	27,	34),
(234,	28,	43),
(234,	39,	35),
(238,	11,	1),
(238,	14,	1),
(239,	23,	41),
(239,	24,	57),
(239,	41,	35),
(239,	42,	40),
(247,	35,	55),
(251,	6,	70),
(253,	37,	26),
(258,	32,	21),
(258,	33,	23),
(259,	32,	45),
(259,	33,	58),
(262,	16,	53),
(262,	17,	62),
(262,	18,	68),
(264,	7,	40),
(264,	8,	49),
(264,	9,	60),
(266,	19,	16),
(266,	20,	16),
(267,	39,	41),
(274,	24,	44),
(281,	37,	39),
(281,	38,	1),
(283,	4,	46),
(283,	5,	54),
(283,	6,	62),
(283,	37,	50),
(287,	7,	34),
(287,	8,	41),
(287,	9,	47),
(303,	41,	31),
(303,	42,	35),
(303,	48,	35),
(303,	49,	37),
(304,	2,	7),
(304,	3,	7),
(305,	21,	4),
(305,	22,	4),
(305,	23,	1),
(305,	24,	1),
(305,	32,	1),
(305,	33,	1),
(307,	25,	53),
(313,	35,	37),
(313,	43,	23),
(313,	44,	24),
(325,	27,	14),
(325,	28,	14),
(325,	50,	14),
(325,	51,	14),
(328,	41,	29),
(328,	42,	32),
(330,	43,	19),
(330,	44,	19),
(330,	45,	1),
(333,	34,	58),
(338,	35,	50),
(339,	35,	31),
(339,	36,	1),
(341,	39,	38),
(343,	35,	25),
(343,	36,	1),
(346,	16,	45),
(346,	17,	52),
(346,	18,	56),
(346,	21,	18),
(346,	22,	18),
(351,	35,	46),
(351,	43,	43),
(353,	35,	43),
(353,	43,	27),
(353,	44,	29),
(355,	23,	33),
(355,	24,	39),
(355,	50,	25),
(355,	51,	38),
(358,	50,	10),
(358,	51,	10),
(362,	38,	1),
(362,	52,	38),
(363,	43,	39),
(363,	44,	44),
(369,	51,	25),
(369,	52,	49),
(372,	25,	29),
(383,	52,	30),
(384,	37,	18),
(385,	21,	1),
(385,	22,	1),
(385,	32,	1),
(385,	33,	1),
(385,	34,	1),
(387,	3,	50),
(387,	44,	49),
(387,	45,	49),
(388,	3,	63),
(388,	43,	51),
(388,	44,	59),
(388,	45,	59),
(390,	15,	32),
(391,	25,	7),
(391,	39,	9),
(392,	40,	1),
(393,	22,	21),
(394,	29,	45),
(394,	30,	58),
(394,	41,	25),
(394,	42,	27),
(394,	48,	41),
(394,	49,	55),
(396,	15,	35),
(396,	32,	37),
(396,	33,	43),
(397,	1,	13),
(397,	2,	13),
(397,	3,	13),
(397,	12,	13),
(397,	43,	13),
(397,	44,	13),
(397,	45,	1),
(397,	46,	6),
(397,	47,	6),
(397,	48,	13),
(397,	49,	13),
(398,	13,	1),
(398,	23,	4),
(398,	24,	3),
(398,	27,	5),
(398,	28,	5),
(398,	29,	13),
(398,	30,	13),
(398,	31,	1),
(398,	32,	13),
(398,	33,	13),
(398,	34,	1),
(401,	35,	1),
(401,	39,	5),
(414,	7,	22),
(414,	8,	25),
(414,	9,	25),
(415,	12,	17),
(415,	48,	17),
(415,	49,	17),
(417,	48,	47),
(417,	49,	59),
(428,	15,	17),
(428,	19,	13),
(428,	20,	13),
(428,	21,	8),
(428,	22,	8),
(430,	16,	13),
(430,	17,	13),
(430,	18,	13),
(430,	19,	4),
(430,	20,	4),
(430,	25,	10),
(430,	26,	1),
(430,	37,	10),
(430,	38,	1),
(431,	41,	43),
(431,	42,	51),
(432,	12,	47),
(432,	49,	67),
(433,	15,	14),
(434,	7,	37),
(434,	8,	45),
(434,	9,	54),
(435,	7,	19),
(435,	8,	21),
(435,	9,	21),
(435,	27,	9),
(435,	28,	9),
(436,	1,	19),
(436,	2,	20),
(436,	3,	20),
(443,	39,	30),
(448,	37,	7),
(459,	27,	7),
(459,	28,	7),
(459,	39,	20),
(460,	16,	37),
(460,	17,	42),
(460,	18,	44),
(460,	21,	32),
(460,	22,	36),
(461,	51,	35),
(462,	39,	22),
(465,	12,	25),
(465,	37,	34),
(465,	38,	1),
(466,	16,	5),
(466,	17,	5),
(466,	18,	5),
(466,	27,	3),
(466,	28,	3),
(466,	50,	1),
(466,	51,	1),
(467,	27,	23),
(467,	28,	24),
(467,	51,	18),
(468,	27,	42),
(468,	28,	53),
(470,	4,	19),
(470,	5,	21),
(470,	6,	21),
(470,	20,	21),
(471,	4,	1),
(471,	5,	1),
(471,	6,	1),
(471,	27,	1),
(471,	28,	1),
(471,	29,	1),
(471,	30,	1),
(471,	31,	1),
(471,	46,	1),
(471,	47,	1),
(471,	50,	1),
(471,	51,	1),
(471,	52,	1),
(472,	23,	17),
(472,	24,	17),
(472,	42,	1),
(472,	52,	17),
(475,	1,	37),
(481,	6,	30),
(492,	48,	25),
(492,	49,	25),
(493,	12,	19),
(493,	49,	21),
(494,	35,	7),
(494,	36,	1),
(494,	39,	1),
(494,	40,	1),
(496,	7,	31),
(496,	8,	37),
(496,	9,	40),
(501,	25,	37),
(502,	4,	34),
(502,	5,	39),
(502,	6,	41),
(502,	27,	26),
(502,	28,	28),
(502,	46,	27),
(502,	47,	29),
(502,	50,	35),
(502,	51,	47),
(502,	52,	33),
(503,	1,	13),
(503,	2,	13),
(503,	3,	13),
(503,	12,	13),
(503,	43,	15),
(503,	44,	15),
(503,	48,	29),
(503,	49,	29),
(512,	4,	10),
(512,	5,	10),
(512,	6,	10),
(518,	2,	44),
(518,	3,	53),
(518,	45,	69),
(522,	25,	26),
(531,	23,	25),
(531,	24,	27),
(531,	39,	25),
(533,	46,	22),
(533,	47,	22),
(539,	23,	25),
(539,	24,	27),
(539,	39,	25),
(543,	35,	28),
(547,	10,	1),
(547,	13,	1),
(549,	12,	13),
(549,	43,	14),
(549,	44,	14),
(549,	45,	1),
(549,	46,	6),
(549,	47,	6),
(549,	48,	23),
(549,	49,	23),
(551,	19,	25),
(551,	20,	29),
(551,	50,	22),
(551,	51,	30),
(554,	19,	28),
(554,	20,	34),
(555,	31,	58),
(556,	12,	23),
(556,	41,	5),
(556,	42,	5),
(556,	48,	5),
(556,	49,	5),
(559,	23,	25),
(559,	24,	27),
(559,	39,	25),
(561,	1,	21),
(561,	2,	23),
(561,	3,	23),
(561,	43,	5),
(561,	44,	5),
(562,	27,	17),
(562,	28,	17),
(562,	41,	23),
(562,	42,	24),
(563,	20,	44),
(563,	27,	38),
(563,	28,	48),
(565,	1,	33),
(565,	2,	39),
(565,	3,	45),
(566,	1,	1),
(566,	2,	1),
(566,	3,	1),
(566,	7,	1),
(566,	8,	1),
(566,	9,	1),
(566,	10,	1),
(566,	16,	1),
(566,	17,	1),
(566,	18,	1),
(566,	19,	1),
(566,	20,	1),
(566,	48,	1),
(566,	49,	1),
(569,	7,	4),
(569,	8,	4),
(569,	9,	4),
(569,	19,	1),
(569,	20,	1),
(569,	25,	1),
(569,	26,	1),
(569,	29,	7),
(569,	30,	7),
(569,	31,	1),
(569,	37,	4),
(570,	12,	41),
(570,	16,	41),
(570,	17,	47),
(570,	18,	50),
(571,	1,	15),
(571,	2,	15),
(571,	3,	15),
(572,	52,	25),
(578,	34,	35),
(580,	25,	58),
(581,	24,	42),
(583,	25,	1),
(583,	26,	1),
(584,	25,	18),
(585,	25,	42),
(585,	26,	1),
(589,	43,	35),
(589,	44,	39),
(590,	15,	29),
(590,	29,	31),
(590,	30,	35),
(590,	32,	31),
(593,	51,	42),
(598,	15,	1),
(599,	16,	21),
(599,	17,	22),
(599,	18,	22),
(603,	15,	23),
(603,	41,	37),
(603,	42,	43),
(605,	1,	7),
(605,	2,	9),
(605,	3,	9),
(608,	35,	22),
(608,	39,	27),
(609,	7,	7),
(609,	8,	7),
(609,	9,	7),
(611,	7,	25),
(611,	8,	29),
(611,	9,	29),
(618,	12,	29),
(618,	16,	17),
(618,	17,	17),
(618,	18,	17),
(619,	25,	50),
(620,	37,	20),
(621,	6,	15),
(621,	16,	33),
(621,	17,	37),
(621,	18,	38),
(621,	41,	13),
(621,	42,	13),
(623,	7,	10),
(623,	8,	10),
(623,	9,	10),
(627,	1,	31),
(627,	2,	36),
(627,	3,	39),
(628,	23,	1),
(628,	24,	1),
(630,	46,	54),
(630,	47,	66),
(633,	48,	37),
(633,	49,	47)
ON DUPLICATE KEY UPDATE `move_id` = VALUES(`move_id`), `pokemon_id` = VALUES(`pokemon_id`), `level` = VALUES(`level`);
